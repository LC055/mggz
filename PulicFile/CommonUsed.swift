//
//  CommonUsed.swift
//  mggz
//
//  Created by QinWei on 2018/4/26.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class CommonUsed: NSObject {
    
}
extension CommonUsed{
    
    //上传轨迹的方法
   class func uploadTrajectoryData(_ demandBaseMigrantWorkerID: String, isException: Bool, longitude: String, latitude: String,userAdress:String,completion:@escaping (Bool) -> Void){
    var guid: String?
    let dispatchGroup = DispatchGroup()
    dispatchGroup.enter()
    UserModel.getGuid { (response) in
        if response.success {
            guid = response.value
        }
        else {
            WWError(response.message)
        }
        dispatchGroup.leave()
    }
    
    dispatchGroup.notify(queue: DispatchQueue.main) {
        guard guid != nil else {
            return
        }
        RecordTrack.uploadRecordTrack(guid!, demandBaseMigrantWorkerID: demandBaseMigrantWorkerID, isException: isException, longitude: longitude, latitude: latitude, userAdress:userAdress){(response) in
            if response.success {
               
                completion(true)
            }
            else {
                completion(false)
            }
        }
    }
 }
    
    
    
    
    
    class func commonAlertStytle(_ title:String,rightTitle:String,leftTitle:String,controller:UIViewController,completion:@escaping () -> Void){
        
        let alert  = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        
        let cancel = UIAlertAction(title: leftTitle, style: .cancel) { (action) in
            
        }
        cancel .setValue(UIColor.black, forKey: "titleTextColor")
        let choose = UIAlertAction(title: rightTitle, style: .default) { (action) in
             completion()
        }
        alert.addAction(cancel)
        alert.addAction(choose)
        controller.present(alert, animated: true) {
            
        }
    }
    //切换项目签入提醒接口
    class func checkOtherProjectSignedIn(demandBaseMigrantWorkerID:String,completion:@escaping (Bool?) -> Void){
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
    LCAPiSubManager.request(.CheckOtherProjectSignedIn(demandBaseMigrantWorkerID: demandBaseMigrantWorkerID, AccountId: accountId)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
               
                guard (jsonString["State"]) as! String == "OK" else{
                    completion(false)
                    return
                }
                completion(true)
            }
        }
    }
    //切换项目签出提醒接口
    class func checkOtherProjectSignedOut(demandBaseMigrantWorkerID:String,completion:@escaping (Bool?) -> Void){
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
    LCAPiSubManager.request(.CheckOtherProjectSignedOut(demandBaseMigrantWorkerID: demandBaseMigrantWorkerID, AccountId: accountId)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
                
                guard (jsonString["State"]) as! String == "OK" else{
                    completion(false)
                    return
                }
                completion(true)
            }
        }
    }
    class func setMigrantWorkerSignedAlert(demandBaseMigrantWorkerID:String,alertSort:String,completionHandler: @escaping(Bool) -> Void){
        var currentDate:String?
        currentDate = dateToString(date: Date())
        if (currentDate != nil){
          currentDate = dateToString(date: Date())
        }
        
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
    LCAPiSubManager.request(.SetMigrantWorkerSignedAlert(demandBaseMigrantWorkerID: demandBaseMigrantWorkerID, alertType: alertSort, signedDate: currentDate!, AccountId: accountId)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
               
                guard (jsonString["State"]) as! String == "OK" else{
                    completionHandler(false)
                    return
                }
                completionHandler(true)
            }
        }
     }
    //获取系统时间
    class func getSystemTime(_ completion:@escaping (_ currentDate:String) -> Void){
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
       
        LCAPiSubManager.request(.GetSystemTime(AccountId: accountId)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
                
                guard (jsonString["State"]) as! String == "OK" else{
                    return
                }
                guard let resultDic = jsonString["Result"] as? String else{
                    return
                }
                let index1 = resultDic.index(of: "T")
               let currentDate  = String(resultDic.prefix(upTo: index1!))
                completion(currentDate)
            }
        }
    }
    
}
