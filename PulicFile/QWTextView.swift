//
//  QWTextView.swift
//  mggz
//
//  Created by QinWei on 2018/4/26.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class QWTextView: UITextView {
    //存储属性，存放placeHolder内容
    var placeHolder: String? = "" {
        //属性观察者
        didSet {
            if self.text == "" {
                self.text = placeHolder
                self.textColor = .lightGray
            }
        }
    }
//监听事件，根据文本框内容改变文字及颜色
override func becomeFirstResponder() -> Bool {
    if self.text == placeHolder||self.text == "" {
        self.text = ""
        self.textColor = .black
    }
    return super.becomeFirstResponder()
}

override func resignFirstResponder() -> Bool {
    text = self.text.replacingOccurrences(of: " ", with: "")
    if text == "" {
        self.text = placeHolder
        self.textColor = .lightGray
    }
    return super.resignFirstResponder()
}
}
