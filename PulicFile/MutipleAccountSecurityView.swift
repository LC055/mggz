//
//  MutipleAccountSecurityView.swift
//  mggz
//
//  Created by QinWei on 2018/3/16.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class MutipleAccountSecurityView: UIView {
    
    @IBOutlet weak var firstView: UIView!
    
    @IBOutlet weak var secondView: UIView!
    
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var securityTextField: UITextField!
    
    @IBOutlet weak var securityButton: UIButton!
    
    @IBOutlet weak var makeSureButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "MutipleAccountSecurityView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        view.backgroundColor = UIColor.white
        self.firstView.layer.cornerRadius = 5
        self.firstView.layer.masksToBounds = true
        self.firstView.layer.borderWidth = 1
        self.firstView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.secondView.layer.cornerRadius = 5
        self.secondView.layer.masksToBounds = true
        self.secondView.layer.borderWidth = 1
        self.secondView.layer.borderColor = UIColor.lightGray.cgColor
        self.makeSureButton.isEnabled = false
        return view
    }
    
}
