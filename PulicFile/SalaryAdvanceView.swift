//
//  SalaryAdvanceView.swift
//  mggz
//
//  Created by QinWei on 2018/3/26.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class SalaryAdvanceView: UIView {

    @IBOutlet weak var salaryNumTextField: UITextField!
    
    @IBOutlet weak var mostMoney: UILabel!
    
    @IBOutlet weak var mostDays: UILabel!
    
    @IBOutlet weak var makeSureButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var prepaidLabel: UILabel!
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "SalaryAdvanceView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        return view
    }
}
