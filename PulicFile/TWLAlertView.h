//
//  TWLAlertView.h
//  DefinedSelf
//
//  Created by 涂婉丽 on 15/12/15.
//  Copyright © 2015年 涂婉丽. All rights reserved.
//xchjds

#import <UIKit/UIKit.h>
@interface TWLAlertView : UIView
@property (nonatomic,strong)UIView *blackView;
@property (strong,nonatomic)UIView * alertview;
-(void)initWithCustomView:(UIView*)custom Frame:(CGRect)frame;
- (void)cancleView;
@end
