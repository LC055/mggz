//
//  PayResetPopViw.swift
//  mggz
//
//  Created by QinWei on 2018/4/2.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class PayResetPopViw: UIView {

    @IBOutlet weak var phoneNum: UILabel!
    
    @IBOutlet weak var securityTextField: UITextField!
    
    @IBOutlet weak var securityButton: UIButton!
    
    @IBOutlet weak var sureButton: UIButton!
    
    @IBOutlet weak var comImage: UIImageView!
    
    @IBOutlet weak var comLabel: UILabel!
    
    @IBOutlet weak var cancel: UIButton!
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "PayResetPopViw", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        self.securityTextField.layer.cornerRadius = 5
        self.securityTextField.layer.masksToBounds = true
        self.securityButton.layer.cornerRadius = 5
        self.securityButton.layer.masksToBounds = true
        
        return view
    }
}
