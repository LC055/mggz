//
//  HomeRecordAlertView.swift
//  mggz
//
//  Created by QinWei on 2018/3/15.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class HomeRecordAlertView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "HomeRecordAlertView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        view.backgroundColor = UIColor(red: 1, green: 0.96, blue: 0.89, alpha: 1)
        self.titleLabel.textColor = UIColor(red: 0.96, green: 0.65, blue: 0.14, alpha: 1)
        return view
    }


}
