//
//  WorkerSalaryFPreView.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/16.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class WorkerSalaryFPreView: UIView {

    
    
    @IBOutlet weak var companyLabel: UILabel!
    
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "WorkerSalaryFPreView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        return view
    }
    

}
