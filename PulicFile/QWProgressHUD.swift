//
//  QWProgressHUD.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/2/9.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import MBProgressHUD
open class QWProgressHUD: NSObject {
    open class func show() {
        
    }

//    open class func inform(_ status:String!) {
//        QWProgressHUD.showInfo(withStatus: status)
//    }
}


public func QWTextonlyHud(_ title:String!,target:UIView,delay:TimeInterval? = 2) {
    let hud = MBProgressHUD.showAdded(to: target, animated: true)
    hud.mode = MBProgressHUDMode.text
    hud.label.text = NSLocalizedString(title, comment: "HUD message title")
    hud.label.numberOfLines = 0
   // hud.offset = target.center
    hud.hide(animated: true, afterDelay: delay!)
}
public func QWShowOverWindow(_ target:UIView){
    let  hud = MBProgressHUD.showAdded(to: target, animated: true)
    DispatchQueue.global().async {
        sleep(3)
        DispatchQueue.main.async {
            hud.hide(animated: true)
        }
    }
}
public func QWTextWithBlackStatusHud(_ title:String!,target:UIView) {
    let hud = MBProgressHUD.showAdded(to: target, animated: true)
    hud.bezelView.backgroundColor = UIColor.black
   // hud.removeFromSuperViewOnHide = true
    //[UIActivityIndicatorView appearanceWhenContainedIn:[MBProgressHUD class], nil].color = [UIColor whiteColor];
    hud.label.text = NSLocalizedString(title, comment: "HUD message title")
    DispatchQueue.global().async {
        sleep(1000)
        DispatchQueue.main.async {
            hud.hide(animated: true)
        }
    }
}
public func QWTextWithStatusHud(_ title:String!,target:UIView) {
    let hud = MBProgressHUD.showAdded(to: target, animated: true)
    hud.label.text = NSLocalizedString(title, comment: "HUD message title")
    DispatchQueue.global().async {
        sleep(1000)
        DispatchQueue.main.async {
            hud.hide(animated: true)
        }
    }
}

public func QWCustomViewHudWith(_ imageName:String,title:String,target:UIView){
    let hud = MBProgressHUD.showAdded(to: target, animated: true)
    hud.mode = MBProgressHUDMode.customView
    let image = UIImage(named: imageName)
    hud.customView = UIImageView.init(image: image)
    hud.isSquare = true
    hud.label.text = NSLocalizedString(title, comment: "Hud done title")
    hud.hide(animated: true, afterDelay: 3.0)
}
public func QWHudDiss(_ target:UIView) {
    MBProgressHUD.hide(for: target, animated: true)
}









