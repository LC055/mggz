//
//  MyTrackModel.swift
//  mggz
//
//  Created by QinWei on 2018/2/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class MyTrackModel: Mappable {
    var Address : String?
    var ID : String?
    var IsException : Int?
    var Latitude : String?
    var LocateTime : String?
    var Longitude : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Address       <- map["Address"]
        ID       <- map["ID"]
        IsException       <- map["IsException"]
        Latitude       <- map["Latitude"]
        LocateTime       <- map["LocateTime"]
        Longitude       <- map["Longitude"]
    }
    
}
