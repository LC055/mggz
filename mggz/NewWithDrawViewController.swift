//
//  NewWithDrawViewController.swift
//  mggz
//
//  Created by QinWei on 2018/3/15.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import MJRefresh
class NewWithDrawViewController: UIViewController {

    fileprivate var _tableView: UITableView!
    fileprivate var tableView: UITableView {
        get {
            if _tableView != nil {
                return _tableView
            }
            _tableView = UITableView()
            _tableView.delegate = self
            _tableView.dataSource = self
            _tableView.tableFooterView = UIView()
            _tableView.showsVerticalScrollIndicator = false
            _tableView.backgroundColor = UIColor.mg_backgroundGray
            
            _tableView.register(UITableViewCell.self, forCellReuseIdentifier: "\(UITableViewCell.self)")
            _tableView.register(WithDrawDetailCell.self, forCellReuseIdentifier: "\(WithDrawDetailCell.self)")
            //_tableView.register(UINib.init(nibName: "WithDrawDetailCell", bundle: nil), forCellReuseIdentifier: "\(SalaryFlowDetailCell.self)")
            return _tableView
        }
    }
    fileprivate lazy var noDataImage:UIImageView = {
        let noDataImage = UIImageView.init()
        noDataImage.image = UIImage(named: "Icon_NoData")
        return noDataImage
    }()
    var salaryFlowModel = [SalaryFlowModel]()
    
    fileprivate var currentPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "明细"
        self.view.addSubview(self.noDataImage)
        self.noDataImage.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.size.equalTo(CGSize(width: 90, height: 90))
        }
        self.view.backgroundColor = UIColor.mg_backgroundGray
        
        //        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "点击", style: .done, target: self, action: #selector(doRightAction))
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        let tableFooter = MJRefreshAutoNormalFooter { [unowned self] in
            self.getNextPage()
        }
        self.tableView.mj_footer = tableFooter
        
        self.getData()
    }
    
    func doRightAction() {
        
    }
    
    func getData() {
        self.currentPage = 1
        SalaryFlowModel.getSalaryFlowDetail { (response) in
            if response.success {
                self.salaryFlowModel = response.value!
                self.tableView.reloadData()
            }
            else {
                WWError(response.message)
            }
        }
    }
    
    func getNextPage() {
        
        if self.salaryFlowModel.count <= 0 {
            self.tableView.mj_footer.endRefreshingWithNoMoreData()
        }
        self.currentPage += 1
        
        SalaryFlowModel.getSalaryFlowDetail(pageIndex: self.currentPage) { (response) in
            if response.success {
                if response.value!.count <= 0 {
                    self.tableView.mj_footer.endRefreshingWithNoMoreData()
                    return
                }
                self.salaryFlowModel += response.value!
                
                self.tableView.reloadData()
                self.tableView.mj_footer.endRefreshing()
            }
            else {
                WWError(response.message)
            }
        }
    }

}
extension NewWithDrawViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.salaryFlowModel.count > 0{
            self.tableView.isHidden = false
        }else{
           self.tableView.isHidden = true
        }
        return self.salaryFlowModel.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(WithDrawDetailCell.self)", for: indexPath) as! WithDrawDetailCell
        cell.bind(self.salaryFlowModel[indexPath.row])
        return cell
    }
}

