//
//  AddressModel.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/5.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Alamofire

class AddressModel: NSObject {

}

class ProviceModel {
    var name: String?
    var aId: String? //省对应的id
    
    required init(rootDict: [String: Any]) {
        if let name = rootDict["Name"]  {
            self.name = name as? String
        }
        self.aId = rootDict["ID"] as? String
    }
    
    class func getProvinceList(_ completionHandler: @escaping (WWValueResponse<[ProviceModel]>) -> Void) {
        let url = JsonAPIURL + "CallMethod"
        guard let httpHeader = mobile_cilent_headers() else {
            return
        }
        guard let model = WWUser.sharedInstance.userModel, let accountId = model.accountId else {
            return
        }
        let method = "MarTian.WebSitePortal.Handler.AjaxCommon.GetProvinceList"
        
        let params = ["Method": method, "AccountId": accountId]
        
        Alamofire.request(url, method: .post, parameters: params, headers: httpHeader).responseJSON { (response) in
            if response.result.isSuccess {
                if let arr = response.result.value as? [[String: Any]] {
                    var callbackArr = [ProviceModel]()
                    for dict in arr {
                        let model = ProviceModel(rootDict: dict)
                        callbackArr.append(model)
                    }
                    completionHandler(WWValueResponse.init(value: callbackArr, success: true))
                    return
                }
                completionHandler(WWValueResponse.init(success: false, message: "数据解析出错"))
            }
            else {
                
                completionHandler(WWValueResponse.init(success: false, message: "网络访问错误"))
            }
        }
    }
}

class CityModel {
    var name: String?
    var aId: String? //省对应的id
    
    required init(rootDict: [String: Any]) {
        if let name = rootDict["Name"]  {
            self.name = name as? String
        }
        self.aId = rootDict["ID"] as? String
    }
    
    class func getCityList(_ province: String ,completionHandler: @escaping (WWValueResponse<[CityModel]>) -> Void) {
        let url = JsonAPIURL + "CallMethod"
        guard let httpHeader = mobile_cilent_headers() else {
            return
        }
        guard let model = WWUser.sharedInstance.userModel, let accountId = model.accountId else {
            return
        }
        let method = "MarTian.WebSitePortal.Handler.AjaxCommon.GetCityListByProvinceId"
        
        let params = ["Method": method, "provinceId":province, "AccountId": accountId]
        
        Alamofire.request(url, method: .post, parameters: params, headers: httpHeader).responseJSON { (response) in
            if response.result.isSuccess {
                if let arr = response.result.value as? [[String: Any]] {
                    var callbackArr = [CityModel]()
                    for dict in arr {
                        let model = CityModel(rootDict: dict)
                        callbackArr.append(model)
                    }
                    completionHandler(WWValueResponse.init(value: callbackArr, success: true))
                    return
                }
                completionHandler(WWValueResponse.init(success: false, message: "数据解析出错"))
            }
            else {
                
                completionHandler(WWValueResponse.init(success: false, message: "网络访问错误"))
            }
        }
    }
}
