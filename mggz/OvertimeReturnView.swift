//
//  OvertimeReturnView.swift
//  mggz
//
//  Created by QinWei on 2018/4/20.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class OvertimeReturnView: UIView {

    @IBOutlet weak var returnReason: UILabel!
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "OvertimeReturnView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        self.backgroundColor = UIColor.mg_backgroundGray
        return view
    }

}
