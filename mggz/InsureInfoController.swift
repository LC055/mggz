//
//  InsureInfoController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/30.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class InsureInfoController: UIViewController {
    
    fileprivate var _tableView: UITableView!
    fileprivate var tableView: UITableView {
        get {
            if _tableView != nil {
                return _tableView
            }
            _tableView = UITableView()
            _tableView.delegate = self
            _tableView.dataSource = self
            _tableView.backgroundColor = UIColor.clear
            _tableView.separatorStyle = .none
            _tableView.tableFooterView = UIView()
            _tableView.showsVerticalScrollIndicator = false
            
            regClass(_tableView, cell: UITableViewCell.self)
            regClass(_tableView, cell: InsureInfoCell.self)
            
            return _tableView
        }
    }
    
    var insureInfoModels: [InsureModel]?
    fileprivate lazy var noDataImage:UIImageView = {
        let noDataImage = UIImageView.init()
        noDataImage.image = UIImage(named: "Icon_NoData")
        return noDataImage
    }()
    fileprivate lazy var noDataLabel:UILabel = {
        let label = UILabel.init()
        label.text = "您还未办理任何保险!"
        label.font = wwFont_Regular(15)
        label.textAlignment = .center
        label.textColor = UIColor(red: 0.80, green: 0.80, blue: 0.80, alpha: 1)
        return label
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "保险情况"
        self.view.backgroundColor = UIColor.mg_backgroundGray

        self.view.addSubview(self.noDataImage)
        self.noDataImage.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.size.equalTo(CGSize(width: 90, height: 90))
        }
        self.view.addSubview(self.noDataLabel)
        self.noDataLabel.snp.makeConstraints { (make) in
        make.top.equalTo(self.noDataImage.snp.bottom).offset(5)
            make.size.equalTo(CGSize(width: 150, height: 21))
            make.centerX.equalTo(self.noDataImage)
        }
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        self.getData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        WWProgressHUD.dismiss()
    }
    func getData() {
        
        WWBeginLoadingWithStatus("获取数据中..")
        InsureModel.getInsure { (response) in
            if response.success {
                self.insureInfoModels = response.value
                
                
                self.tableView.reloadData()
                WWEndLoading()
            }
            else {
                WWError(response.message)
            }
        }
    }
}

extension InsureInfoController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.insureInfoModels?.count == 0{
            self.tableView.isHidden = true
        }else{
            self.tableView.isHidden = false
        }
        return self.insureInfoModels?.count ?? 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = getCell(tableView, cell: InsureInfoCell.self, indexPath: indexPath)
        cell.bind(self.insureInfoModels![indexPath.row])
        return cell
    }
}
