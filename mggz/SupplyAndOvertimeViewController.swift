//
//  SupplyAndOvertimeViewController.swift
//  mggz
//
//  Created by QinWei on 2018/4/17.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class SupplyAndOvertimeViewController: UIViewController {
    var projectModel: ProjectModel?//当前项目
    @IBOutlet weak var supplyButton: UIButton!
    
    @IBOutlet weak var overtimeButton: UIButton!
    
    fileprivate lazy var showView : UIView = {
        let showView = UIView(frame: CGRect(x: SCREEN_WIDTH/8, y: 104, width: SCREEN_WIDTH/4, height: 2))
        showView.backgroundColor = UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1)
        return showView
    }()
    fileprivate lazy var scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = UIColor.white
        scrollView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.scrollsToTop = false
        scrollView.bounces = false
        scrollView.isScrollEnabled = false
        scrollView.contentSize = CGSize(width: 2*SCREEN_WIDTH, height: 0)
        return scrollView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.supplyButton.setTitleColor(UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1), for: .normal)
        self.navigationItem.title = "记录"
       self.overtimeButton.setTitleColor(UIColor.lightGray, for: .normal)
        self.view.addSubview(scrollView)
         self.setupChildController()
        self.addChildVcIntoScrollView()
        self.view.bringSubview(toFront: self.supplyButton)
        self.view.bringSubview(toFront: self.overtimeButton)
        self.view.addSubview(showView)
    }
    private func setupChildController(){
        
        let vc = RecordSupplyListController()
        vc.currentProject = self.projectModel
        self.addChildViewController(vc)
        
        
        let memorand = OvertimeRLViewController(nibName: "OvertimeRLViewController", bundle: nil)
        memorand.projectModel = self.projectModel
        self.addChildViewController(memorand)
    }
    private func addChildVcIntoScrollView(){
        for i in 0..<2 {
            let childVc = self.childViewControllers[i]
            if childVc.isViewLoaded {
                return
            }
            let childVcView = childVc.view
            let scrollViewW = CGFloat(i) * SCREEN_WIDTH
            childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
            scrollView.addSubview(childVcView!)
        }
    }
    @IBAction func supplyButtonClick(_ sender: Any) {
        UIView.animate(withDuration: 0.2) {
            self.scrollView.contentOffset = CGPoint(x: 0, y: self.scrollView.contentOffset.y)
            self.supplyButton.setTitleColor(UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1), for: .normal)
          self.overtimeButton.setTitleColor(UIColor.lightGray, for: .normal)
            
            self.showView.frame = CGRect(x: SCREEN_WIDTH/8, y: self.showView.frame.origin.y, width: self.showView.frame.size.width, height: self.showView.frame.size.height)
        }
    }
    
    @IBAction func overtimeButtonClick(_ sender: Any) {
        UIView.animate(withDuration: 0.2) {
            self.scrollView.contentOffset = CGPoint(x: SCREEN_WIDTH, y: self.scrollView.contentOffset.y)
            self.supplyButton.setTitleColor(UIColor.lightGray, for: .normal)
        self.overtimeButton.setTitleColor(UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1), for: .normal)
            
            self.showView.frame = CGRect(x: 5*SCREEN_WIDTH/8, y: self.showView.frame.origin.y, width: self.showView.frame.size.width, height: self.showView.frame.size.height)
        }
    }
    
}
extension SupplyAndOvertimeViewController : UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
}
