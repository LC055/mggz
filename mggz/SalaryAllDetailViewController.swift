//
//  SalaryAllDetailViewController.swift
//  mggz
//
//  Created by QinWei on 2018/3/14.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Moya
import ObjectMapper
import MJRefresh
class SalaryAllDetailViewController: BaseViewController {
    fileprivate var currentPage = 1
    fileprivate var totalPage:Int?
    @IBOutlet weak var tableView: UITableView!
    lazy var itemArray : [SalaryAllDetailModel] = [SalaryAllDetailModel]()
    var isChangeSelection = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "工资明细"
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 10)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 128
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        view.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.showsHorizontalScrollIndicator = false
        
        self.tableView.register(UINib.init(nibName: "AccountDetailedCell", bundle: nil), forCellReuseIdentifier: "AccountDetailed")
        
        let tableHeader = MJRefreshNormalHeader { [unowned self] in
            self.setupSalaryListData()
        }
        tableHeader?.stateLabel.isHidden = true
        self.tableView.mj_header = tableHeader
        
        
        
        let tableFooter = MJRefreshAutoNormalFooter { [unowned self] in
            self.setupNextPageListData()
        }
        self.tableView.mj_footer = tableFooter
        
        self.tableView.mj_header.beginRefreshing()
        
        
    }
    
    fileprivate func setupNextPageListData(){
        guard (self.totalPage != nil) else {
            return
        }
        if self.currentPage >= self.totalPage!{
      self.tableView.mj_footer.endRefreshingWithNoMoreData()
            return
        }
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
        self.currentPage += 1
        
    LCAPiSubManager.request(.GetProjectSalaryPayList(AccountId: accountId, pageIndex: self.currentPage, pageSize: 25)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
               
                
                
                guard (jsonString["State"]) as! String == "OK" else{
                    return
                }
                guard  let resultDic : NSDictionary = jsonString["Result"] as? NSDictionary else{
                    return
                }
                guard  let resultArray : NSArray = resultDic["ProjectList"] as? NSArray else{
                    return
                }
                var reponseArray = [SalaryAllDetailModel].init()
                reponseArray = Mapper<SalaryAllDetailModel>().mapArray(JSONObject: resultArray)!
                if reponseArray.count <= 0 {
            self.tableView.mj_footer.endRefreshingWithNoMoreData()
                    return
                }
                self.itemArray += reponseArray
                self.tableView.reloadData()
                self.tableView.mj_footer.endRefreshing()
            }
        }
    }
    fileprivate func setupSalaryListData(){
        
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
        
        self.currentPage = 1
    LCAPiSubManager.request(.GetProjectSalaryPayList(AccountId: accountId, pageIndex: self.currentPage, pageSize: 25)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
                guard (jsonString["State"]) as! String == "OK" else{
                    return
                }
                guard  let resultDic : NSDictionary = jsonString["Result"] as? NSDictionary else{
                    return
                }
                guard  let resultArray : NSArray = resultDic["ProjectList"] as? NSArray else{
                    return
                }
                if let totalList = resultDic["Total"] as? Int {
                    self.totalPage = (totalList/25)+1
                }
                self.itemArray = Mapper<SalaryAllDetailModel>().mapArray(JSONObject: resultArray)!
                
                self.tableView.reloadData()
            }
        if self.tableView.mj_header.isRefreshing {
            self.tableView.mj_header.endRefreshing()
        }
        self.tableView.mj_footer.resetNoMoreData()
        self.hideLoadingView()
        }
    }

}
extension SalaryAllDetailViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 12
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 12))
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.itemArray.count == 0{
            self.tableView.isHidden = true
        }else{
            self.tableView.isHidden = false
        }
        if self.itemArray.count < 25 {
            self.tableView.mj_footer.isHidden = true
        }else{
            self.tableView.mj_footer.isHidden = false
        }
        
        
        return self.itemArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : AccountDetailedCell = tableView.dequeueReusableCell(withIdentifier: "AccountDetailed") as! AccountDetailedCell
        cell.setupCellData(self.itemArray[indexPath.section])
        cell.detailedButton.addTarget(self, action: #selector(detailedButtonClick(_:)), for: .touchUpInside)
        cell.detailedButton.tag = indexPath.section
        return cell
    }
    @objc fileprivate func detailedButtonClick(_ button:UIButton){
        
        let model = self.itemArray[button.tag]
        let salaryFlow = RecordSupplyListViewController(nibName: "RecordSupplyListViewController", bundle: nil)
        salaryFlow.companyID = model.CompanyID
        salaryFlow.companyName = model.CompanyName
  self.navigationController?.pushViewController(salaryFlow, animated: true)
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 128
    }
}















