//
//  PrepaidDetailModel.swift
//  mggz
//
//  Created by QinWei on 2018/3/27.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class PrepaidDetailModel: Mappable {
    var PrepaidAmount : Double?
    var PrepaidTime : String?
    var PrepaidType:String?
    
    required init?(map: Map) {
        
        
        
    }
    func mapping(map: Map) {
        
        PrepaidAmount       <- map["PrepaidAmount"]
        PrepaidTime       <- map["PrepaidTime"]
        PrepaidType       <- map["PrepaidType"]
        
    }
}
