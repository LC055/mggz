//
//  CertificationReviewController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/7.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class CertificationReviewController: UIViewController {
    
    fileprivate var _tableView: UITableView!
    fileprivate var tableView: UITableView {
        get {
            if _tableView != nil {
                return _tableView
            }
            _tableView = UITableView()
            _tableView.delegate = self
            _tableView.dataSource = self
            _tableView.backgroundColor = UIColor.clear
            _tableView.separatorStyle = .none
            _tableView.tableFooterView = UIView()
            _tableView.showsVerticalScrollIndicator = false
            
            regClass(_tableView, cell: UITableViewCell.self)
            regClass(_tableView, cell: CertificationReviewAvatarCell.self)
            regClass(_tableView, cell: CertificationReviewTextFieldCell.self)
            regClass(_tableView, cell: CertificationReviewIdPhotoCell.self)
            
            return _tableView
        }
    }
    
    fileprivate var userInfoModel: UserInfoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.mg_backgroundGray
        self.title = "认证资料"
        
        
        
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(self.view)
            make.bottom.equalTo(self.view).offset(-49)
        }
        
        self.getData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        WWProgressHUD.dismiss()
    }
    func getData() {
        UserInfoModel.getMigrantWorkInfo { (response) in
            if response.success {
                self.userInfoModel = response.value
                self.tableView.reloadData()
            }
            else {
                WWError(response.message)
            }
        }
    }
}

extension CertificationReviewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return [1,4,1][section]
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 140
        }
        if indexPath.section == 2 {
            return 20 + 80 + 10 + 40
        }
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = getCell(tableView, cell: CertificationReviewAvatarCell.self, indexPath: indexPath)
            if self.userInfoModel != nil {
                cell.bind(self.userInfoModel!)
            }
            return cell
        }
        if indexPath.section == 1 {
            let cell = getCell(tableView, cell: CertificationReviewTextFieldCell.self, indexPath: indexPath)
            
            let images = ["Icon_用户名", "Icon_性别","Icon_身份证号码", "Icon_籍贯"]
            cell.cellImageView.image = UIImage(named: images[indexPath.row])!.withRenderingMode(.alwaysTemplate)
            
            guard let model = self.userInfoModel  else{
                return cell
            }
            if indexPath.row == 0 {
                cell.cellTextField.text = model.username
            }
            if indexPath.row == 1 {
                if model.sex == .boy {
                    cell.cellTextField.text = "男"
                }
                else {
                    cell.cellTextField.text = "女"
                }
            }
            
            if indexPath.row == 2 {
                cell.cellTextField.text = model.idCard
            }
            if indexPath.row == 3 {
                let province = model.province ?? ""
                let city = model.city ?? ""
                cell.cellTextField.text = province + city
            }
            
            return cell
            
        }
        if indexPath.section == 2 {
            let cell = getCell(tableView, cell: CertificationReviewIdPhotoCell.self, indexPath: indexPath)
            if self.userInfoModel != nil {
                cell.bind(self.userInfoModel!)
            }
            
            return cell
        }
        let cell = getCell(tableView, cell: UITableViewCell.self, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 || section == 1 {
            return 10
        }
        return 0.1
    }
}

