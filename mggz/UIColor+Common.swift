//
//  UIColor+Common.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/9.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

func colorWith255RGB(_ r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1)
}
func colorWith255RGBA(_ r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) -> UIColor {
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
}
//常用颜色
extension UIColor {
    //蓝色
    class var mg_blue:UIColor {
        get {
            return colorWith255RGB(16, g: 142, b: 233)
        }
    }
    
    //粉色
    class var mg_pink: UIColor {
        get  {
            return colorWith255RGB(247, g: 153, b: 146)
        }
    }
    
    // 背景灰
    class var mg_backgroundGray: UIColor {
        get {
            return colorWith255RGB(242, g: 243, b: 245)
        }
    }
    
    //  无数据灰
    class var mg_noDataGray: UIColor {
        get {
            return colorWith255RGB(221, g: 221, b: 221)
        }
    }
    
    class var mg_lightGray: UIColor {
        get {
            return colorWith255RGB(102, g: 102, b: 102)
        }
    }
}
