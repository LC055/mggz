//
//  RecordWorkpointsView4.swift
//  mggz
//
//  Created by QinWei on 2018/3/19.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class RecordWorkpointsView4: UIView {
    
   fileprivate var myFrame: CGRect?
    var selectedPoints:((String) -> Void)?
    var systemPoints:String?
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var recordPoints: UIButton!
    @IBOutlet weak var pickView: UIPickerView!
    fileprivate var contentView:UIView?
    fileprivate let workNumArray = ["0.5","0.75","1.0","1.5","2.0","3.0"]
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        myFrame = frame
        self.setupSubviews()
        self.pickView.delegate = self
        self.pickView.dataSource = self
        self.pickView.reloadAllComponents()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setupSubviews()
    }
    func setupSubviews() {
        contentView = loadViewFromNib()
        addSubview(contentView!)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "RecordWorkpointsView4", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        return view
    }

}
extension RecordWorkpointsView4:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return 6
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        
        
        return 30
        
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        
        return 240
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return ["0.5","0.75","1.0","1.5","2.0","3.0"][row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let selectPointStr = ["0.5","0.75","1.0","1.5","2.0","3.0"][row]
        self.selectedPoints?(selectPointStr)
        
    }
    
}























