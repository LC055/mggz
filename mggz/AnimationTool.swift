//
//  AnimationTool.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/8.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class AnimationTool: NSObject {

    //涟漪动画
    class func createRippleAnimation(scale: CGFloat, duration: CGFloat, delegate:CAAnimationDelegate?) -> CAAnimationGroup {
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
        scaleAnimation.toValue = NSValue(caTransform3D: CATransform3DMakeScale(scale, scale, 1))
        
        let alphaAnimation = CABasicAnimation(keyPath: "opacity")
        alphaAnimation.fromValue = 1
        alphaAnimation.toValue = 0.3
        
        let animationGroup = CAAnimationGroup()
        animationGroup.delegate = delegate
        animationGroup.isRemovedOnCompletion = false
        animationGroup.fillMode = kCAFillModeForwards
        animationGroup.animations = [scaleAnimation, alphaAnimation]
        animationGroup.duration = CFTimeInterval(duration)
        animationGroup.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        return animationGroup
    }
}
