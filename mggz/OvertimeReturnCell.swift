//
//  OvertimeReturnCell.swift
//  mggz
//
//  Created by QinWei on 2018/4/18.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class OvertimeReturnCell: UITableViewCell {
   var textViewDidChange: ((String) -> Void)?
    @IBOutlet weak var otDetailTextView: UITextView!
    
    @IBOutlet weak var returnTextView: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func layoutSubviews() {
        self.otDetailTextView.delegate = self
        self.otDetailTextView.tag = 100
        self.otDetailTextView.layer.cornerRadius = 5
        self.otDetailTextView.layer.masksToBounds = true
        self.otDetailTextView.layer.borderWidth = 1
        self.otDetailTextView.layer.borderColor = UIColor.mg_backgroundGray.cgColor
        
        self.returnTextView.delegate = self
        self.returnTextView.isEditable = false
        self.returnTextView.layer.cornerRadius = 5
        self.returnTextView.layer.masksToBounds = true
        self.returnTextView.layer.borderWidth = 1
        self.returnTextView.layer.borderColor = UIColor.mg_backgroundGray.cgColor
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
extension OvertimeReturnCell:UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        if textView.tag == 100{
        if let changeHandler = self.textViewDidChange {
            changeHandler(textView.text)
        }
     }
    }
}
