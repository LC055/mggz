//
//  WithdrawSuccessController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/29.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class withdrawSuccessNav: UINavigationController {
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    var bankCardModel: BankCardModel?
    var amount: Float?
    var withdrawNavHandler:(() -> Void)?
    init(_ model: BankCardModel, amount: Float) {
        let vc = WithdrawSuccessController()
        vc.bankCardModel = model
        vc.amount = amount
        super.init(rootViewController: vc)
        weak var weakSelf = self
        vc.withdrawHandler = {
            weakSelf?.withdrawNavHandler!()
        }
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class WithdrawSuccessController: UIViewController {
    
    private var headView = UIView()
    var withdrawHandler:(() -> Void)?
    private var finishButton: UIButton = {
        let button = UIButton()
        button.setTitle("完成", for: .normal)
        button.layer.cornerRadius = 6
//        button.layer.shadowColor = UIColor.black.cgColor
//        button.layer.shadowOpacity = 0.3
//        button.layer.shadowOffset = CGSize(width: 1, height: 1)
        button.titleLabel?.font = wwFont_Medium(17)
        button.backgroundColor = colorWith255RGB(62, g: 207, b: 110)
        return button
    }()
    
    var bankCardDetailLabel: UILabel = {
        let bankCardDetailLabel = UILabel()
        bankCardDetailLabel.font = wwFont_Regular(15)
        bankCardDetailLabel.textAlignment = .right
        return bankCardDetailLabel
    }()
    
    var withdrawDetailLabel:UILabel = {
        let label = UILabel()
        label.font = wwFont_Regular(15)
        label.textAlignment = .right
        return label
    }()
    
    var bankCardModel: BankCardModel?
    var amount: Float?
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        WWSuccess("工资将会在24小时内转入您提现的银行卡")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.title = "详情"
        
        self.setupViews()
        self.updateView()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        WWProgressHUD.dismiss()
    }
    func setupViews() {
        self.view.addSubview(headView)
        headView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view).offset(64)
            make.left.right.equalTo(self.view)
            make.height.equalTo(SCREEN_HEIGHT * 0.35)
        }
        
        let successImageView = UIImageView()
        successImageView.image = UIImage(named: "打钩_黑色")!.withRenderingMode(.alwaysTemplate)
        successImageView.tintColor = colorWith255RGB(62, g: 207, b: 110)
        headView.addSubview(successImageView)
        successImageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(headView)
            make.centerY.equalTo(headView).offset(-15)
            make.width.height.equalTo(SCREEN_WIDTH * 0.3)
        }
        
        let successLabel = UILabel()
        successLabel.text = "提现申请已提交"
        successLabel.font = wwFont_Medium(22)
        headView.addSubview(successLabel)
        successLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(headView)
        make.top.equalTo(successImageView.snp.bottom).offset(20)
        }
        
        let grayLine = UIView()
        grayLine.backgroundColor = colorWith255RGB(233, g: 233, b: 233)
        headView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(headView)
            make.height.equalTo(1)
        }
        
        let middleView = UIView()
        self.view.addSubview(middleView)
        middleView.snp.makeConstraints { (make) in
            make.left.right.equalTo(headView)
            make.top.equalTo(headView.snp.bottom)
            make.height.equalTo(100)
        }
        
        let bankCardLabel = UILabel()
        bankCardLabel.textColor = UIColor.mg_lightGray
        bankCardLabel.font = wwFont_Regular(15)
        bankCardLabel.textAlignment = .center
        bankCardLabel.text = "银行卡号"
        middleView.addSubview(bankCardLabel)
        bankCardLabel.snp.makeConstraints { (make) in
            make.left.top.equalTo(middleView)
            make.width.equalTo(100)
        }
        
        middleView.addSubview(self.bankCardDetailLabel)
        bankCardDetailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(bankCardLabel.snp.right)
            make.right.equalTo(middleView).offset(-15)
            make.top.bottom.equalTo(bankCardLabel)
        }
   
        let withdrawLabel = UILabel()
        withdrawLabel.textColor = UIColor.mg_lightGray
        withdrawLabel.font = wwFont_Regular(15)
        withdrawLabel.textAlignment = .center
        withdrawLabel.text = "提现金额"
        middleView.addSubview(withdrawLabel)
        withdrawLabel.snp.makeConstraints { (make) in
            make.left.width.equalTo(bankCardLabel)
            make.top.equalTo(bankCardLabel.snp.bottom)
            make.bottom.equalTo(middleView)
            make.height.equalTo(bankCardLabel)
        }
        
        middleView.addSubview(self.withdrawDetailLabel)
        self.withdrawDetailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(withdrawLabel.snp.right)
            make.right.equalTo(middleView).offset(-15)
            make.bottom.top.equalTo(withdrawLabel)
        }

        let grayLine2 = UIView()
        grayLine2.backgroundColor = colorWith255RGB(233, g: 233, b: 233)
        self.view.addSubview(grayLine2)
        grayLine2.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.top.equalTo(middleView.snp.bottom)
            make.height.equalTo(1)
        }
        
        self.finishButton.addTarget(self, action: #selector(doFinishAction), for: .touchUpInside)
        self.view.addSubview(self.finishButton)
        self.finishButton.snp.makeConstraints { (make) in
            make.left.equalTo(self.view).offset(30)
            make.right.equalTo(self.view).offset(-30)
            make.height.equalTo(45)
           make.top.equalTo(grayLine2.snp.bottom).offset(40)
        }
    }
    
    func updateView() {
        if self.bankCardModel != nil {
            let lastFourNumber = lastFourNumbers(self.bankCardModel!.salaryCardBankAccount!)
            self.bankCardDetailLabel.text = self.bankCardModel!.bankName! + "-" + "尾号" + lastFourNumber
        }
        if self.amount != nil {
            withdrawDetailLabel.text = "￥" + "\(self.amount ?? 0)"
        }
    }
    
    func doFinishAction() {
        self.dismiss(animated: true, completion: nil)
        withdrawHandler!()
    }
}
