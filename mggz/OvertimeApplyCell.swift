//
//  OvertimeApplyCell.swift
//  mggz
//
//  Created by QinWei on 2018/4/17.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class OvertimeApplyCell: UITableViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    
    
    
    @IBOutlet weak var workLabel: UILabel!
    
    @IBOutlet weak var timeSelectButton: UIButton!
    
    @IBOutlet weak var workSelectButton: UIButton!
    
    @IBOutlet weak var rangeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
