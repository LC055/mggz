//
//  V2LoadingView.swift
//  V2EXLearining
//
//  Created by ShareAnimation on 17/4/10.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

let noticeString = [
    "正在拼命加载",
    "前方发现楼主",
    "年轻人,不要着急",
    "让我飞一会儿",
    "大爷,您又来了?",
    "楼主正在抓皮卡丘，等他一会儿吧",
    "爱我，就等我一万年",
    "未满18禁止入内",
    "正在前往 花村",
    "正在前往 阿努比斯神殿",
    "正在前往 沃斯卡娅工业区",
    "正在前往 观测站：直布罗陀",
    "正在前往 好莱坞",
    "正在前往 66号公路",
    "正在前往 国王大道",
    "正在前往 伊利奥斯",
    "正在前往 漓江塔",
    "正在前往 尼泊尔"
]

class V2LoadingView: UIView {

    var activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    init() {
        super.init(frame: CGRect.zero)
        self.addSubview(self.activityIndicatorView)
        self.activityIndicatorView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.centerY.equalTo(self).offset(-32)
        }
        
        let noticeLabel = UILabel()
        //修复BUG。做个小笔记给阅读代码的兄弟们提个醒
        //(Int)(arc4random())
        //上面这种写法有问题，arc4random()会返回 一个Uint32的随机数值。
        //在32位机器上,如果随机的数大于Int.max ,转换就会crash。
//        noticeLabel.text = noticeString[Int(arc4random() % UInt32(noticeString.count))]
        noticeLabel.text = "加载中..."
        noticeLabel.font = UIFont.systemFont(ofSize: 16)
        noticeLabel.textColor = colorWith255RGB(173, g: 173, b: 173)
        self.addSubview(noticeLabel)
        noticeLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.activityIndicatorView.snp.bottom).offset(10)
            make.centerX.equalTo(self.activityIndicatorView)
        }
        self.activityIndicatorView.activityIndicatorViewStyle = .gray
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        self.activityIndicatorView.startAnimating()
    }
    
    func hide() {
        UIView.animate(withDuration: 0.5, animations: { 
            self.alpha = 0
            }) { (finished) in
                if finished {
                    self.removeFromSuperview()
                }
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }

}
