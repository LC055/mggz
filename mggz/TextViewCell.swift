//
//  TextViewCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/22.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import YYText

class TextViewCell: UITableViewCell,UITextViewDelegate {
    
    var detailTextView: QWTextView = {
        let textView = QWTextView()
        textView.placeHolder = "请输入缺工原因..."
        textView.layer.borderColor = UIColor.mg_backgroundGray.cgColor
        textView.layer.borderWidth = 1
        textView.layer.cornerRadius = 8
        
        return textView
    }()

    var textViewDidChange: ((String) -> Void)?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        
    }
    
    func setupViews() {
        self.detailTextView.delegate = self
        self.contentView.addSubview(detailTextView)
        self.detailTextView.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(10)
            make.right.equalTo(self.contentView).offset(-10)
            make.top.equalTo(self.contentView).offset(5)
            make.bottom.equalTo(self.contentView).offset(-5)
            
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if let changeHandler = self.textViewDidChange {
            changeHandler(textView.text)
        }
    }
}
