//
//  CertificationModel.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/6.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Alamofire

class CertificationModel: NSObject {

    //提交认证资料
    class func submitCertificationInfo(_ guid: String, orderNo: String,name: String, sex:Int, iDCard: String, userAdress:String,startTime:String,endTime:String,avatar: ImageUploadModel, idPhotos: [ImageUploadModel],operate:String,completionHandler:@escaping (WWResponse) -> Void) {
        let url = API_CALL_METHOD
        guard let httpHeader = mobile_cilent_headers() else {
            return
        }
        guard let model = WWUser.sharedInstance.userModel, let accountId = model.accountId else {
            return
        }
        
        let pageName = "MarTian.Edit.JianZhuGongRenZheng"
        let orderConfigId = "57ed0987-1c79-4c67-b6f8-23ec6304ba38"
        let method = "KouFine.Handler.Core.AjaxOperate.SubmitProcess"
        let approvalNote = "提交审批"
        let carbonList = ""
        
        /***头像数据****/
        let avatarPhoto = ["Name":avatar.name ?? "", "Path": avatar.url ?? "", "ID":avatar.aId ?? ""]
        
        /*****照片数据****/
        var idCardImageLists = [[String: String]]()
        for item in idPhotos {
            let dict = ["Name": item.name ?? "", "Path": item.url ?? "", "ID":item.aId ?? ""]
            idCardImageLists.append(dict)
        }
        
//        /***省市数据***/
//        let province = provinceAndCity.0
//        let provinceLevel1 = ["ID":province.aId]
//
//        let cityLevel1:[String: String]!
//        if let city = provinceAndCity.1 {
//            cityLevel1 = ["ID": city.aId ?? ""]
//        }
//        else {
//            cityLevel1 = ["ID":""]
//
//        }
//        let origo = ["Province": provinceLevel1, "City": cityLevel1]

        let form = ["Name": name,"Photo":avatarPhoto, "Sex": sex, "IDCard": iDCard, "IDCardSmartImageList":idCardImageLists, "Origo": [String : AnyObject](), "OrderNo": orderNo, "IsAgainCredit":false,"Address":userAdress,"IDCardValidEndDate":endTime,"IDCardValidBeginDate":startTime] as [String : Any]
        print(form)
        let jsonData = ["ID": guid, "Form": form] as [String : Any]
        let jsonDataString = getJSONStringFromDictionary(dictionary: jsonData as NSDictionary)
        
        let params = ["pageName": pageName, "orderConfigId":orderConfigId, "orderId":guid, "jsonData": jsonDataString, "Method": method, "AccountId": accountId, "approvalNote":approvalNote, "carbonList":carbonList,"operate":operate]
        print(params)
        Alamofire.request(url, method:.post, parameters: params, headers: httpHeader).responseString { (response) in
            if response.result.isSuccess {
                if let valueString = response.result.value {
                    if valueString == "OK" {
                        completionHandler(WWResponse.init(success: true))
                    }
                    else {
                        completionHandler(WWResponse.init(success: false, message: valueString))
                    }
                    return
                }
                completionHandler(WWResponse.init(success: false, message: "数据解析出错"))
            }
            else {
                
                completionHandler(WWResponse.init(success: false, message: "网络访问错误！"))
            }
        }
    }
    
    //查询认证状态
    class func checkCertificationStatus(_ completionHandler: @escaping (WWResponse) -> Void) {
        let url = API_CALL_METHOD
        guard let httpHeader = mobile_cilent_headers() else {
            return
        }
        guard let model = WWUser.sharedInstance.userModel, let accountId = model.accountId, let companyId = model.companyId else {
            return
        }
        
        let method = "MarTian.MigrantWorkerManage.Handler.AjaxConstructionTeamDetail.GetMGAuthenticationState"
        
        let params = ["Method":method, "CompanyID":companyId, "AccountId": accountId]
        
        Alamofire.request(url, method: .post, parameters: params, headers: httpHeader).responseJSON { (response) in
            if response.result.isSuccess {
                if let dict = response.result.value as? [String: Any] {
                    print(dict)
                    if let statusString = dict["Status"] as? String, statusString == "OK" {
                        if let msgStr = dict["Msg"] as? NSString{
                         WWUser.sharedInstance.stateReason = msgStr as String
                        }
                        if let resultInt = dict["Result"] as? Int {
                            let certificationStatus = CertificationStatus(rawValue: resultInt)
                            WWUser.sharedInstance.certificationStatus = certificationStatus
                            completionHandler(WWResponse.init(success: true))
                            return
                        }
                        
                    }
                }
                completionHandler(WWResponse.init(success: false, message: "数据解析错误"))
            }
            else {
                
                completionHandler(WWResponse.init(success: false, message: "网络访问错误"))
            }
        }
    }
    
    //提交申诉资料
    class func submitAppeals(_ guid: String, form: [String: Any], completionHandler:@escaping (WWResponse) -> Void) {
        guard let httpHeader = WWUser.sharedInstance.mobile_cilent_headers else {
            return
        }
        guard let accountId = WWUser.sharedInstance.accountId else {
            return
        }
        
        let method = "KouFine.Handler.Core.AjaxOperate.SubmitProcess"
        let pageName = "KouFine.Container.Form"
        let approvalNote = "提交审批"
        let orderConfigId = "28CA1B8E-9720-425E-935D-D097A2B298F4"
        let carbonList = ""
        
        let jsonData = ["ID": guid, "Form": form] as [String : Any]
        let jsonDataString = getJSONStringFromDictionary(dictionary: jsonData as NSDictionary)
        
        let params = ["Method":method, "pageName":pageName, "approvalNote":approvalNote, "orderConfigId":orderConfigId, "orderId":guid, "jsonData":jsonDataString, "carbonList":carbonList, "AccountId":accountId]
        
        Alamofire.request(API_CALL_METHOD, method: .post, parameters: params, headers: httpHeader).responseString { (response) in
            if response.result.isSuccess {
                if let valueString = response.result.value {
                    if valueString == "OK" {
                        completionHandler(WWResponse.init(success: true))
                    }
                    else {
                        completionHandler(WWResponse.init(success: false, message: valueString))
                    }
                    return
                }
                completionHandler(WWResponse.init(success: false, message: "数据异常"))
            }
            else {
                completionHandler(WWResponse.init(success: false, message: "网络访问错误"))
            }
        }
    }
}
