//
//  MGWarningView.swift
//  mggzDemo
//
//  Created by ShareAnimation on 2017/9/4.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import YYText
import SnapKit

class MGWarningView: UIView {
    private var warningLabel: YYLabel  = {
        let label = YYLabel()
        label.numberOfLines = 0
        
        return label
    }()
    private var warningRightArrowImageView: UIImageView = {
        let imageview = UIImageView()
        imageview.image = UIImage(named: "Icon_右箭头")!.withRenderingMode(.alwaysTemplate)
        imageview.tintColor = UIColor(red: 245/255, green: 106/255, blue: 0/255, alpha: 1)
        imageview.contentMode = .scaleAspectFit
        
        return imageview
    }()
    
//    var warningText: String? {
//        willSet {
//            if newValue != nil {
//                DispatchQueue.main.async {
//                    let labelHeight = self.setupYYLabelContent(newValue!)
//                    self.contentHeight = labelHeight
//                }
//            }
//        }
//    }
    
//    func setWarningText(_ text: String , completion:@escaping () -> Void) {
//        //设置文字内容后，计算出高度的过程要在主线程完成。
//        DispatchQueue.main.async {
//            let labelHeight = self.setupYYLabelContent(text,textColor: )
//            self.contentHeight = labelHeight
//            completion()
//        }
//    }
    
    func setWarningText(_ text: String, textColor: UIColor, completion:@escaping () -> Void) {
        self.warningRightArrowImageView.tintColor = textColor
        //设置文字内容后，计算出高度的过程要在主线程完成。
        DispatchQueue.main.async {
            let labelHeight = self.setupYYLabelContent(text,textColor: textColor)
            self.contentHeight = labelHeight
            completion()
        }
    }
    
    //view的高度,在warningText赋值后会返回label的高度
    var contentHeight: CGFloat?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.addSubview(warningLabel)
        self.warningLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(self)
            make.left.equalTo(self).offset(20)
        }
        
        self.addSubview(self.warningRightArrowImageView)
        self.warningRightArrowImageView.snp.makeConstraints { (make) in
            make.top.bottom.right.equalTo(self)
            make.left.equalTo(self.warningLabel.snp.right)
            make.width.equalTo(30)
        }
    }
    
    //组合YYLabel的内容，并返回内容高度
    func setupYYLabelContent(_ text: String, textColor: UIColor) -> CGFloat {
        let attributedString: NSMutableAttributedString = NSMutableAttributedString.init(string: text)
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.image = UIImage(named: "Icon_广播")!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = textColor
        let attachAttributed = NSAttributedString.yy_attachmentString(withContent: imageView, contentMode: .scaleAspectFit, attachmentSize: imageView.bounds.size, alignTo: UIFont(name: "PingFangSC-Regular", size: 15)!, alignment: .center)
        
        
        attributedString.insert(NSAttributedString.init(string: "  "), at: 0)//增加空隙
        attributedString.insert(attachAttributed, at: 0)
        
        /*****内容设置*****/
        attributedString.yy_color = textColor//设置内容颜色，不包括图片
        attributedString.yy_font = UIFont(name: "PingFangSC-Regular", size: 15)//字体大小，不包括图片
        
        let textContainer = YYTextContainer(size: CGSize(width: self.frame.width-50, height: 1000))
        let textLayout = YYTextLayout(container: textContainer, text: attributedString)
        let introHeight = textLayout?.textBoundingSize.height //计算内容高度
        
        self.warningLabel.textLayout = textLayout
        
        return introHeight ?? 0
    }
}
