//
//  MMCalendar.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/23.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import FSCalendar

class MMCalendar: FSCalendar,FSCalendarDelegateAppearance,FSCalendarDelegate,FSCalendarDataSource {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.locale = Locale(identifier: "zh-CN")
        self.appearance.todayColor = UIColor.clear//去掉今日色
        self.appearance.titleTodayColor = UIColor.black//去掉今日色导致标题色消失，因此需要再次加上
        self.appearance.headerTitleColor = colorWith255RGB(74, g: 144, b: 226)//月份标题颜色
        self.appearance.weekdayTextColor = colorWith255RGB(66, g: 66, b: 66) //星期标题颜色
        self.placeholderType = .none    //占位用来显示上月尾部和下月头部的信息
        self.backgroundColor = UIColor.white
        
        self.delegate = self
        self.dataSource = self
  
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    //去掉选中色，需要titleSelectionColorFor，fillSelectionColorFor配合
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {
        return UIColor.black
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        return UIColor.clear
    }
}
