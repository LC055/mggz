//
//  MyTrackCell.swift
//  mggz
//
//  Created by QinWei on 2018/2/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class MyTrackCell: UITableViewCell {

    
    @IBOutlet weak var trackTimeLabel: UILabel!
    @IBOutlet weak var trackAdressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    public func setupTrackModelDataWith(_ model:MyTrackModel){
        if (model.LocateTime != nil) {
        let startStr = model.LocateTime
            let index1 = startStr?.index((startStr?.startIndex)!, offsetBy: 16)
            let result1 = startStr?.prefix(upTo: index1!)
            let index2 = result1?.index((result1?.startIndex)!, offsetBy: 5)
            let result2 = result1?.suffix(from: index2!)
            let makeSureTime = result2?.replacingOccurrences(of: "T", with: " ")
            self.trackTimeLabel.text = makeSureTime
        }
        if (model.Address != nil){
            
        self.trackAdressLabel.text = model.Address
            
        }else{
            self.trackAdressLabel.text = "未获取"
        }
    }
}
