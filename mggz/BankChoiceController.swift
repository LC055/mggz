//
//  BankChoiceController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/28.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import MJRefresh

class BankChoiceController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    fileprivate var _tableView: UITableView!
    fileprivate var tableView: UITableView {
        get {
            if _tableView != nil {
                return _tableView
            }
            _tableView = UITableView()
            _tableView.delegate = self
            _tableView.dataSource = self
            _tableView.showsVerticalScrollIndicator = false
            _tableView.tableFooterView = UIView()
            _tableView.backgroundColor = UIColor.mg_backgroundGray
            
            regClass(_tableView, cell: UITableViewCell.self)
            
            return _tableView
        }
    }
    
    private var bankModels:[BankModel]?
    var selectHandler: ((BankModel) -> Void)?
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        WWProgressHUD.dismiss()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "点击", style: .done, target: self, action: #selector(doRightAction))
        
        self.setupViews()
        
        let header = MJRefreshNormalHeader {
            self.getData()
        }
        header?.stateLabel.isHidden = true
        self.tableView.mj_header = header
        
        self.tableView.mj_header.beginRefreshing()
    }
    
    func setupViews() {
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
    
    func getData() {
        BankModel.getBankNames { (response) in
            if response.success {
                self.bankModels = response.value
                
                self.tableView.reloadData()
            }
            else {
                WWError(response.message)
            }
            self.tableView.mj_header.endRefreshing()
        }
    }
    
    func doRightAction() {
        self.getData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bankModels?.count ?? 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = getCell(tableView, cell: UITableViewCell.self, indexPath: indexPath)
        cell.textLabel?.text = self.bankModels![indexPath.row].bankName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.selectHandler != nil {
            self.selectHandler!(self.bankModels![indexPath.row])
            self.navigationController?.popViewController(animated: true)
        }
    }
}
