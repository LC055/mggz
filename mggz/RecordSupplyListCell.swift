//
//  RecordSupplyListCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/22.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class RecordSupplyListCell: UITableViewCell {
    
    //申请时间标题
    fileprivate var applyTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "申请时间:"
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(15)
        
        return label
    }()
    
    //申请时间细则
   fileprivate  var applyTimeDetailLabel: UILabel = {
        let label = UILabel()
        label.text = "2017-05-26 21:30:00"
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(15)
        label.textAlignment = .left
        return label
    }()
    
    //缺签时间
    fileprivate var unSignedLabel: UILabel = {
        let label = UILabel()
        label.text = "缺签时间:"
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(15)
        
        return label
    }()
    
    //缺签时间细则
    fileprivate var unSignedDetailLabel: UILabel = {
        let label = UILabel()
        label.text = "2017-05-24"
        label.textColor = UIColor.mg_lightGray
        label.textAlignment = .left
        label.font = wwFont_Regular(15)
        
        return label
    }()
    
    var workFlowLabel: UILabel = {
        let label = UILabel()
        label.text = "申请中"
        label.textColor = UIColor.mg_blue
        label.font = wwFont_Regular(15)
        
        return label
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func setupViews() {
        self.contentView.addSubview(self.workFlowLabel)
        self.workFlowLabel.snp.makeConstraints { (make) in
            make.top.bottom.right.equalTo(self.contentView)
            make.width.equalTo(50)
        }
        
        self.contentView.addSubview(applyTimeLabel)
        self.applyTimeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(15)
            make.top.equalTo(self.contentView).offset(8)
            make.width.equalTo(65)
        }
        
        self.contentView.addSubview(applyTimeDetailLabel)
        self.applyTimeDetailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.applyTimeLabel.snp.right).offset(10)
            make.centerY.equalTo(self.applyTimeLabel)
            make.right.equalTo(self.workFlowLabel.snp.left)
        }
        
        self.contentView.addSubview(self.unSignedLabel)
        self.unSignedLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.applyTimeLabel)
            make.top.equalTo(self.applyTimeLabel.snp.bottom).offset(10)
            make.width.equalTo(self.applyTimeLabel)
        }
        
        self.contentView.addSubview(self.unSignedDetailLabel)
        self.unSignedDetailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.applyTimeDetailLabel)
            make.top.equalTo(self.unSignedLabel)
            make.right.equalTo(self.applyTimeDetailLabel)
        }
        

    }
    
    func bind(_ model: RecordSupplyApply) {
        if let signedDate = model.signedDate {
            let signedDateString = dateToString(date: signedDate)
            self.unSignedDetailLabel.text = signedDateString
        }
        
        if let unsignedDate = model.createDate {
            let unsignedDateString = dateTimeToString(date: unsignedDate)
            self.applyTimeDetailLabel.text = unsignedDateString
        }
        
        if let flow = model.workFlowState {
            var flowText = ""
            switch flow {
            case .unCommited:
                flowText = "未提交"
            case .approvalPending:
                flowText = "待审批"
            case .inTheFlow:
                flowText = "流转中"
            case .finished:
                flowText = "已完结"
            case .stop:
                flowText = "被中止"
            case .returned:
                flowText = "被退回"
            case .exception:
                flowText = "异常"
            }
            
            self.workFlowLabel.text = flowText
        }
    }
}
