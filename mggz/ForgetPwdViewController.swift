//
//  ForgetPwdViewController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/7.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class ForgetPwdNavigation: UINavigationController {
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    init() {
        let vc = ForgetPwdViewController()
        super.init(rootViewController: vc)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


class ForgetPwdViewController:  UIViewController{
    var phoneNumberTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "请输入手机号码"
        textField.keyboardType = .phonePad
        textField.text = ""
        let imageView = UIImageView(image: UIImage.init(named: "Icon_手机")!.withRenderingMode(.alwaysTemplate))
        imageView.tintColor = UIColor.gray
        imageView.frame = CGRect(x: 0, y: 0, width: 28, height: 22)
        imageView.contentMode = .scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = .always
        
        return textField
    }()
    
    var securityTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "请输入短信验证码"
        textField.keyboardType = .phonePad
        
        let imageView = UIImageView(image: UIImage.init(named: "reply_n")!.withRenderingMode(.alwaysTemplate))
        imageView.tintColor = UIColor.gray
        imageView.frame = CGRect(x: 0, y: 0, width: 28, height: 22)
        imageView.contentMode = .scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = .always
        
        return textField
    }()
    
    var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.isSecureTextEntry = true
        textField.placeholder = "请设置新密码"
        
        let imageView = UIImageView(image: UIImage.init(named: "ic_lock")!.withRenderingMode(.alwaysTemplate))
        imageView.tintColor = UIColor.gray
        imageView.frame = CGRect(x: 0, y: 0, width: 28, height: 22)
        imageView.contentMode = .scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = .always
        
        return textField
    }()
    
    var confirmPasswordTextField: UITextField = {
        let textField = UITextField()
        textField.isSecureTextEntry = true
        textField.placeholder = "请确认新密码"
        
        let imageView = UIImageView(image: UIImage.init(named: "ic_lock")!.withRenderingMode(.alwaysTemplate))
        imageView.tintColor = UIColor.gray
        imageView.frame = CGRect(x: 0, y: 0, width: 28, height: 22)
        imageView.contentMode = .scaleAspectFit
        textField.leftView = imageView
        textField.leftViewMode = .always
        
        return textField
    }()
    
    var resetButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = colorWith255RGB(16, g: 142, b: 233)
        button.setTitle("提交", for: .normal)
        button.layer.cornerRadius = 6
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOpacity = 0.2
        button.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        return button
    }()
    
    var unavailableButton: UIButton = {
        let button = UIButton()
        button.setTitle("手机不可用？", for: .normal)
        button.setTitleColor(colorWith255RGB(16, g: 142, b: 233), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "忘记密码"
        self.view.backgroundColor = UIColor.white
        
        let firstButton = UIBarButtonItem(image: UIImage.init(named: "ic_keyboard_arrow_left_36pt"), style: .done, target: self, action: #selector(doLeftAction))
        firstButton.tintColor = UIColor.black
        let secondButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        secondButton.width = -15
        self.navigationItem.leftBarButtonItems = [secondButton,firstButton]
        
        self.unavailableButton.addTarget(self, action: #selector(doUnavailableButtonAction), for: .touchUpInside)
        
        self.setupViews()
    }
    
    func doResetAction() {
        guard let phoneNumber = self.phoneNumberTextField.text,phoneNumber.characters.count > 0 else{
            WWError("手机号码为空")
            return
        }
        guard Tool.isValidatePhoneNum(phoneNumber) else {
            WWError("不是正确的手机号")
            return
        }
        guard let varificationCode = self.securityTextField.text,varificationCode.characters.count > 0 else {
            WWError("验证码为空")
            return
        }
        guard let password = self.passwordTextField.text, password.characters.count > 0 else {
            WWError("密码为空")
            return
        }
        guard let confirmPassword = self.confirmPasswordTextField.text ,password == confirmPassword else {
            WWError("两次密码不同")
            return
        }
        guard password.range(of: "&") == nil,confirmPassword.range(of: "&") == nil else {
            WWError("密码必须为6-16位字母/数字组合！")
            return
        }
        
        WWBeginLoadingWithStatus("正在操作")
        UserModel.findPassword(phoneNumber: phoneNumber, verificationCode: varificationCode, password:password ) { (response) in
            if response.success {
                WWSuccess("设置成功")
                self.dismiss(animated: true, completion: nil)
            }
            else {
                WWError(response.message)
            }
        }
    }
    
    func doUnavailableButtonAction() {
        self.navigationController?.pushViewController(AppealPhoneNoController(), animated: true)
    }
    
    func doLeftAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func doCountDownAction(_ button: UIButton) {
        guard let phoneNumber = self.phoneNumberTextField.text, phoneNumber.characters.count > 0 else {
            WWError("请输入手机号码后再获取")
            return
        }
        guard Tool.isValidatePhoneNum(phoneNumber) else {
            WWError("不是正确的手机号")
            return
        }
        
        button.startWithTime(60, title: "获取验证码", countDownTitle: "重新发送", mainColor: button.backgroundColor!, countColor: UIColor.gray)
        
        UserModel.requestSecurityCode(phoneNumber: phoneNumber, flag: FlagType.findbackpwd) { (response) in
            if response.success {
                #if DEBUG
                    WWInform(response.value ?? "好像是空的")
                #else
                #endif
            }
            else {
               // WWInform(response.message)
            }
        }
    }
    
    func setupViews() {
        self.view.addSubview(self.phoneNumberTextField)
        self.phoneNumberTextField.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view).offset(15)
            make.top.equalTo(self.view).offset(64)
            make.height.equalTo(50)
        }
        
        let phoneNumberLine = UIView()
        phoneNumberLine.backgroundColor = UIColor.mg_backgroundGray
        self.view.addSubview(phoneNumberLine)
        phoneNumberLine.snp.makeConstraints { (make) in
            make.top.equalTo(self.phoneNumberTextField.snp.bottom)
            make.right.left.equalTo(self.view)
            make.height.equalTo(0.3)
        }
        
        self.view.addSubview(securityTextField)
        self.securityTextField.snp.makeConstraints { (make) in
            make.top.equalTo(phoneNumberLine.snp.bottom)
            make.left.height.equalTo(self.phoneNumberTextField)
        }
        
        let button = UIButton()
        button.addTarget(self, action: #selector(doCountDownAction(_:)), for: .touchUpInside)
        button.layer.cornerRadius = 10
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.setTitle("获取验证码", for: .normal)
        button.backgroundColor = colorWith255RGB(16, g: 142, b: 233)
        self.view.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.left.equalTo(self.securityTextField.snp.right)
            make.centerY.equalTo(self.securityTextField)
            make.right.equalTo(self.view).offset(-15)
            make.size.equalTo(CGSize.init(width: 80, height: 30))
        }
        
        let securityFieldLine = UIView()
        securityFieldLine.backgroundColor = UIColor.mg_backgroundGray
        self.view.addSubview(securityFieldLine)
        securityFieldLine.snp.makeConstraints { (make) in
            make.top.equalTo(self.securityTextField.snp.bottom)
            make.right.left.equalTo(self.view)
            make.height.equalTo(0.7)
        }
        
        self.view.addSubview(self.passwordTextField)
        self.passwordTextField.snp.makeConstraints { (make) in
            make.right.height.left.equalTo(self.phoneNumberTextField)
            make.top.equalTo(securityFieldLine.snp.bottom)
        }
        
        let grayLine3 = UIView()
        grayLine3.backgroundColor = UIColor.mg_backgroundGray
        self.view.addSubview(grayLine3)
        grayLine3.snp.makeConstraints { (make) in
            make.top.equalTo(self.passwordTextField.snp.bottom)
            make.right.left.equalTo(self.view)
            make.height.equalTo(0.7)
        }
        
        self.view.addSubview(self.confirmPasswordTextField)
        self.confirmPasswordTextField.snp.makeConstraints { (make) in
            make.right.height.left.equalTo(self.phoneNumberTextField)
            make.top.equalTo(grayLine3.snp.bottom)
        }
        
        let bottomView = UIView()
        bottomView.backgroundColor = colorWith255RGB(249, g: 249, b: 249)
        self.view.addSubview(bottomView)
        bottomView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(self.view)
            make.top.equalTo(self.confirmPasswordTextField.snp.bottom)
        }
        
        
        bottomView.addSubview(self.resetButton)
        self.resetButton.addTarget(self, action: #selector(doResetAction), for: .touchUpInside)
        self.resetButton.snp.makeConstraints { (make) in
            make.top.equalTo(bottomView).offset(60)
            make.right.equalTo(bottomView).offset(-40)
            make.left.equalTo(bottomView).offset(40)
            make.height.equalTo(40)
        }
        
        bottomView.addSubview(unavailableButton)
        self.unavailableButton.snp.makeConstraints { (make) in
            make.right.equalTo(bottomView).offset(-10)
            make.top.equalTo(bottomView).offset(10)
            make.width.equalTo(100)
        }
    }
}

