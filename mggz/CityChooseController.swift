//
//  CityChooseController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/5.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class CityChooseController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    fileprivate var _tableView: UITableView!
    fileprivate var tableView: UITableView {
        get {
            if _tableView != nil {
                return _tableView
            }
            _tableView = UITableView()
            _tableView.delegate = self
            _tableView.dataSource = self
            _tableView.tableFooterView = UIView()
            _tableView.separatorStyle = .none
            _tableView.showsVerticalScrollIndicator = false
            _tableView.backgroundColor = UIColor.mg_backgroundGray
            
            regClass(_tableView, cell: UITableViewCell.self)
            
            return _tableView
        }
    }
    var cityLists:[CityModel]?
    
    
    var currrentProvince: ProviceModel?
    var selectedHandler:(((ProviceModel,CityModel?)) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.currrentProvince?.name
        self.view.backgroundColor = UIColor.mg_backgroundGray
        
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "点击", style: .done, target: self, action: #selector(doRightAction))
        
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        self.getData()
    }
    
    
    func getData() {
        
        WWBeginLoadingWithStatus("正在获取数据")
        CityModel.getCityList(self.currrentProvince?.aId ?? "") { (response) in
            if response.success {
                if response.value?.count == 0 { //可能是澳门这种地方，没有市
                    if let handler = self.selectedHandler {
                        handler((self.currrentProvince!,nil))
                        
                        for vc in self.navigationController!.viewControllers {
                            if vc is CertificationSubmitController {
                                self.navigationController?.popToViewController(vc, animated: true)
                            }
                        }
                    }
                }
                self.cityLists = response.value
                WWEndLoading()
                self.tableView.reloadData()
            }
            else {
                WWError(response.message)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cityLists?.count ?? 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = getCell(tableView, cell: UITableViewCell.self, indexPath: indexPath)
        let model = self.cityLists![indexPath.row]
        cell.textLabel?.text = model.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.cityLists![indexPath.row]
        if let handler = selectedHandler {
            handler((self.currrentProvince!,model))
            
            for vc in self.navigationController!.viewControllers {
                if vc is CertificationSubmitController {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }
        
    }
}
