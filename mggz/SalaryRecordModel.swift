//
//  SalaryRecordModel.swift
//  mggz
//
//  Created by QinWei on 2018/3/16.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class SalaryRecordModel: Mappable {
    var ApplyAmount : Double?
    var ChangeType : String?
    var CreateTime : String?
    var ID : String?
    var MarketSupplyBaseID : String?
    var ProjectName : String?
    var SalaryMonths : Float?

    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        ApplyAmount       <- map["ApplyAmount"]
        ChangeType       <- map["ChangeType"]
        CreateTime       <- map["CreateTime"]
        ID       <- map["ID"]
        MarketSupplyBaseID       <- map["MarketSupplyBaseID"]
        ProjectName       <- map["ProjectName"]
        SalaryMonths       <- map["SalaryMonths"]
    }
}
