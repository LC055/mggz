//
//  TimePickerController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/21.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class TimePickerController: UIViewController {
    
    var picker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        picker.locale = Locale(identifier: "zh_CN")
        picker.backgroundColor = UIColor.white
        return picker
    }()
    
    var callBack:((String) -> Void)?
    
    var selectedDate: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "确认", style: .done, target: self, action: #selector(doRightAction))
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font:UIFont.systemFont(ofSize: 15)], for: .normal)
        self.setupViews()
    }
    
    func doRightAction() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let dateString = dateFormatter.string(from: self.picker.date)
        
        callBack!(dateString)
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupViews() {
        picker.frame = CGRect(x: 0, y: 200, width: SCREEN_WIDTH, height: 400)
        self.view.addSubview(picker)
        picker.center = self.view.center
    }
}
