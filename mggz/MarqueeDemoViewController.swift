//
//  MarqueeDemoViewController.swift
//  mggz
//
//  Created by QinWei on 2018/5/7.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class MarqueeDemoViewController: UIViewController {
    fileprivate var timer:Timer?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
     self.tableView.register(UINib.init(nibName: "ClockStateCell", bundle: nil), forCellReuseIdentifier: "clock")
    }
    
}
extension MarqueeDemoViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ClockStateCell = tableView.dequeueReusableCell(withIdentifier: "clock") as! ClockStateCell
        cell.mwWorkState = "1"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
}









