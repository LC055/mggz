//
//  MyWorkTrackViewController.swift
//  mggz
//
//  Created by QinWei on 2018/2/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Moya
import ObjectMapper
class MyWorkTrackViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    lazy var itemArray : [MyTrackModel] = [MyTrackModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "我的工作轨迹"
        self.setupTrackData()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 10)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        view.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        self.tableView.register(UINib.init(nibName: "MyTrackCell", bundle: nil), forCellReuseIdentifier: "track")
    }
    fileprivate func setupTrackData(){
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
    LCAPiSubManager.request(.GetMyWorkTracksData(AccountId: accountId)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
                
                guard (jsonString["State"]) as! String == "OK" else{
                    return
                }
                let resultArray : NSArray = jsonString["Result"] as! NSArray
                
                    
                self.itemArray = Mapper<MyTrackModel>().mapArray(JSONObject: resultArray)!
                self.tableView.reloadData()
                
            }
        }
    }
    
}
extension MyWorkTrackViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MyTrackCell = tableView.dequeueReusableCell(withIdentifier: "track") as! MyTrackCell
    cell.setupTrackModelDataWith(self.itemArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58
    }
}
