//
//  FangxinhuaView.swift
//  mggz
//
//  Created by QinWei on 2018/2/5.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class FangxinhuaView: UIView {
    
    @IBOutlet weak var maxAmountLabel: UILabel!
    @IBOutlet weak var applicationButton: UIButton!
    
    @IBOutlet weak var advanceDetail: UIButton!
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "FangxinhuaView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        view.backgroundColor = UIColor.white
        self.applicationButton.isEnabled = false
        self.applicationButton.backgroundColor = UIColor.lightGray
        return view
    }
}
