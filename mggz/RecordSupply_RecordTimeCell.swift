//
//  RecordSupply_RecordTimeCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/21.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class RecordSupply_RecordTimeCell: UITableViewCell {
    
    //标题
   fileprivate  var recordTimeLabel: UILabel = {
    let label = UILabel()
    label.text = "签到记录时间"
    label.font = wwFont_Medium(14)
    label.textColor = UIColor.mg_lightGray
    return label
    
    }()
    
    //入场的小圆标记
    fileprivate var entryMarkerLabel: UILabel = {
        let label = UILabel()
        label.text = "入"
        label.textColor = UIColor.mg_blue
        label.textAlignment = .center
        label.layer.cornerRadius = 8
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.mg_blue.cgColor
        label.font = wwFont_Medium(12)
        return label
    }()
    
    //出场的小圆标记
    fileprivate var exitMarkerLabel: UILabel = {
        let label = UILabel()
        label.text = "出"
        label.textColor = UIColor.mg_pink
        label.textAlignment = .center
        label.layer.cornerRadius = 8
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.mg_pink.cgColor
        label.font = wwFont_Medium(12)
        return label
    }()
    
    
    var entryDetailLabel: UILabel = {
        let label = UILabel()
        label.text = "入场签到时间  08:30"
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(14)
        return label
    }()
    
    var exitDetailLabel: UILabel = {
        let label = UILabel()
        label.text = "出场签到时间  09:30"
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(14)
        return label
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.contentView.addSubview(recordTimeLabel)
        self.recordTimeLabel.snp.makeConstraints { (make) in
            make.left.top.equalTo(self.contentView).offset(10)
        }
        
        self.contentView.addSubview(entryMarkerLabel)
        self.entryMarkerLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(15)
            make.top.equalTo(self.recordTimeLabel.snp.bottom).offset(8)
            make.height.width.equalTo(16)
        }
        
        self.contentView.addSubview(self.entryDetailLabel)
        self.entryDetailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.entryMarkerLabel.snp.right).offset(10)
            make.centerY.equalTo(self.entryMarkerLabel)
        }
        
        let verticalLine = UIView()
        verticalLine.backgroundColor = colorWith255RGB(207, g: 237, b: 240)
        self.contentView.addSubview(verticalLine)
        verticalLine.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.entryMarkerLabel)
            make.top.equalTo(self.entryMarkerLabel.snp.bottom)
            make.width.equalTo(1)
            make.height.equalTo(20)
        }
        
        self.contentView.addSubview(self.exitMarkerLabel)
        self.exitMarkerLabel.snp.makeConstraints { (make) in
            make.size.equalTo(self.entryMarkerLabel)
            make.centerX.equalTo(verticalLine)
            make.top.equalTo(verticalLine.snp.bottom)
            make.bottom.equalTo(self.contentView.snp.bottom).offset(-15)
        }
        
        self.contentView.addSubview(self.exitDetailLabel)
        self.exitDetailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.exitMarkerLabel.snp.right).offset(10)
            make.centerY.equalTo(self.exitMarkerLabel)
        }
    }

}
