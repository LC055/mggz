//
//  PhotoPickViewCell.swift
//  SwiftDemo2
//
//  Created by ShareAnimation on 2017/9/12.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class PhotoPickViewCell: UICollectionViewCell {
    
    var profilePhotoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    var closeButton: UIButton = {
        let button = UIButton()
        button.contentMode = .scaleAspectFit
        button.setImage(UIImage.init(named: "close"), for: .normal)
        return button
    }()
    
    var deleteImageActionHandler:(() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isUserInteractionEnabled = true
        
        self.setupViews()
        self.closeButton.addTarget(self, action: #selector(doCloseAction), for: .touchUpInside)
        self.profilePhotoImageView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(doProfileImageTapAction)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.contentView.addSubview(self.profilePhotoImageView)
        self.profilePhotoImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView).offset(10)
            make.right.equalTo(self.contentView).offset(-10)
            make.left.bottom.equalTo(self.contentView)
        }
        
        self.contentView.addSubview(self.closeButton)
        self.closeButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.profilePhotoImageView.snp.right)
            make.centerY.equalTo(self.profilePhotoImageView.snp.top)
            make.width.height.equalTo(20)
        }
    }
    
    func doCloseAction() {
        if let handler = self.deleteImageActionHandler {
            handler()
        }
    }
    
    //点击图片
    func doProfileImageTapAction() {
        var responder = self.next
        while !(responder is UIViewController) {
            responder = responder?.next
        }
        if responder is UIViewController {
            let vc = responder as! UIViewController
            
            let toVc = ShowBigImageController(self.profilePhotoImageView)
            vc.present(toVc, animated: true, completion: nil)
        }
    }
}
