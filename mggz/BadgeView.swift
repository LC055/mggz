//
//  BadgeView.swift
//  SwiftDemo2
//
//  Created by ShareAnimation on 2017/10/13.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class BadgeView: UIView {
    
    private var panelView = UIView()
    
    private var contentImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private var badgeDot: UIView = {
        let view = UIView()
        view.isHidden = true
        view.backgroundColor = UIColor.red
        return view
    }()
    
    var image: UIImage? {
        willSet {
            self.contentImageView.image = newValue
        }
    }
    
    var showBadge: Bool = false {
        willSet {
            self.badgeDot.isHidden = !newValue
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupViews() {
        
        self.addSubview(self.panelView)
        self.panelView.frame = self.bounds
        
        let width = self.frame.width
        let height = self.frame.height
        
        self.panelView.addSubview(self.contentImageView)
        self.contentImageView.frame = CGRect(x: 3, y: 3, width: width-4, height: height-4)
        self.panelView.addSubview(self.badgeDot)
        self.badgeDot.frame = CGRect(x: width-8, y: 0, width: 8, height: 8)
        self.badgeDot.layer.cornerRadius = 4
    }
}
