//
//  RecordTimelineCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/8.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class RecordTimelineCell: UITableViewCell {
    
    var markLabel: UILabel = {
       let label = UILabel()
        label.font = wwFont_Medium(12)
        label.layer.cornerRadius = 8
        label.layer.masksToBounds = true
        label.layer.borderWidth = 0.5
        label.layer.borderColor = colorWith255RGB(16, g: 142, b: 233).cgColor
        label.textColor = colorWith255RGB(16, g: 142, b: 233)
        label.textAlignment = .center
        
        return label
    }()
    
    var verticalLineTop: UIView = {
        let view = UIView()
        view.backgroundColor = colorWith255RGB(207, g: 237, b: 240)
        return view
    }()
    
    var verticalLineBottom: UIView = {
       let view = UIView()
        view.backgroundColor = colorWith255RGB(207, g: 237, b: 240)
        return view
    }()
    
    var cellNameLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Regular(14)
        label.textColor = colorWith255RGB(51, g: 51, b: 51)
        return label
    }()
    
    var cellTimeLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Regular(14)
        label.textColor = colorWith255RGB(51, g: 51, b: 51)
        return label
    }()
    
    var hideTopLine = false {
        willSet {
            if newValue == true {
                self.verticalLineTop.isHidden = true
            }
        }
    }
    
    var hideBottomLine = false {
        willSet {
            if newValue == true {
                self.verticalLineBottom.isHidden = true
            }
        }
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = UIColor.clear
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.contentView.addSubview(markLabel)
        self.markLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(20)
            make.centerY.equalTo(self.contentView)
            make.width.height.equalTo(16)
        }
        
        self.contentView.addSubview(verticalLineTop)
        self.verticalLineTop.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView)
            make.bottom.equalTo(self.markLabel.snp.top)
            make.width.equalTo(1)
            make.centerX.equalTo(self.markLabel)
        }
        
        self.contentView.addSubview(verticalLineBottom)
        self.verticalLineBottom.snp.makeConstraints { (make) in
            make.top.equalTo(self.markLabel.snp.bottom)
            make.bottom.equalTo(self.contentView)
            make.width.equalTo(1)
            make.centerX.equalTo(self.markLabel)
        }
        
        self.contentView.addSubview(cellNameLabel)
        self.cellNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.markLabel.snp.right).offset(15)
            make.centerY.equalTo(self.markLabel)
        }
        
        self.contentView.addSubview(cellTimeLabel)
        self.cellTimeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.cellNameLabel.snp.right).offset(15)
            make.centerY.equalTo(self.markLabel)
        }
    }
    
    
    func bind(_ itemData: RecordModel) {
        if let signedType = itemData.signedType {
            if signedType == .signedIn {
                self.markLabel.text = "入"
                self.cellNameLabel.text = "入场签到时间"
                self.markLabel.textColor = UIColor.mg_blue
                self.markLabel.layer.borderColor = UIColor.mg_blue.cgColor
            }
            else {
                self.markLabel.text = "出"
                self.cellNameLabel.text = "出场签到时间"
                self.markLabel.textColor = colorWith255RGB(247, g: 153, b: 146)
                self.markLabel.layer.borderColor = colorWith255RGB(247, g: 153, b: 146).cgColor
            }
            
        }
        
        if let signedTime = itemData.signedTime {
            let dateStirng = Tool.dateToStringWithHHMMFormat(date: signedTime)
            self.cellTimeLabel.text = dateStirng
            
        }
    }
}
