//
//  MyPrepaidViewController.swift
//  mggz
//
//  Created by QinWei on 2018/3/27.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
import Moya
class MyPrepaidViewController: UIViewController {
 
    @IBOutlet weak var tableView: UITableView!
    lazy var prepaidArray : [MyPrepaidModel] = [MyPrepaidModel]()
    lazy var returnArray : [MyPrepaidModel] = [MyPrepaidModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "我的预支"
    
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "记录", style: .done, target: self, action: #selector(pushToDetailPage))
        
        
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        self.view.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        self.tableView.register(UINib.init(nibName: "MyAdvanceCell", bundle: nil), forCellReuseIdentifier: "advance")
        self.getSalaryPrepaidRecord()
    }
    @objc fileprivate func pushToDetailPage(){
        let detail = MyPrepaidDetailViewController(nibName: "MyPrepaidDetailViewController", bundle: nil)
    self.navigationController?.pushViewController(detail, animated: true)
    }
    fileprivate func getSalaryPrepaidRecord(){
        
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
    LCAPiSubManager.request(.GetSalaryPrepaidRecord(AccountId: accountId)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
                
                guard (jsonString["State"]) as! String == "OK" else{
                    return
                }
               
                
                guard  let resultDic : NSDictionary = jsonString["Result"] as? NSDictionary else{
                    return
                }
                guard let prepaidDetailArray:NSArray = resultDic["PrepaidDetailList"] as? NSArray else{
                    return
                }
                guard let returnPrepaidArray:NSArray = resultDic["ReturnPrepaidDetailList"] as? NSArray else{
                    return
                }
            self.prepaidArray = Mapper<MyPrepaidModel>().mapArray(JSONObject: prepaidDetailArray)!
            self.returnArray = Mapper<MyPrepaidModel>().mapArray(JSONObject: returnPrepaidArray)!
            self.tableView.reloadData()
                
            }
        }
    }
}
extension MyPrepaidViewController:UITableViewDelegate,UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if prepaidArray.count == 0 && returnArray.count == 0 {
            self.tableView.isHidden = true
        }else{
            self.tableView.isHidden = false
        }
        
        if section == 0{
            return prepaidArray.count
        }
        return returnArray.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MyAdvanceCell = tableView.dequeueReusableCell(withIdentifier: "advance") as! MyAdvanceCell
        if indexPath.section == 0{
     cell.setupCellDataWithModel(self.prepaidArray[indexPath.row])
        }
        if indexPath.section == 1{
    cell.setupCellDataWithModel(self.returnArray[indexPath.row])
            
        }
       return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 30))
        let titleLabel = UILabel.init(frame: CGRect(x: 23, y: 10, width: 120, height: 21))
        headerView.addSubview(titleLabel)
        if section == 0 && self.prepaidArray.count>0{
            titleLabel.text = "待还"
        }else if section == 1 && self.returnArray.count>0{
            titleLabel.text = "已还"
        }
        return headerView
    }
}
