//
//  ETTextViewAlertController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/10/27.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import YYText

class ETTextViewAlertController: UIViewController {
    
    fileprivate var coverView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        view.isUserInteractionEnabled = true
        return view
    }()
    
    fileprivate var panelView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 1, height: 1)
        view.backgroundColor = UIColor.white
        return view
    }()
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "工资结算申请"
        label.textAlignment = .center
        return label
    }()
    
    fileprivate lazy var reasonTextView: YYTextView = {
        let textView = YYTextView()
        textView.layer.borderColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1).cgColor
        textView.layer.borderWidth = 1
        textView.font = UIFont(name: "PingFangSC-Regular", size: 16)
        //textView.placeholderText = "请填写申请理由..."
        
        return  textView
    }()
    
    private var warningLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "注：每个月最多只能主动发起1次工资结算，没有特殊情况，工资会按期自动结算！"
        label.font = UIFont(name: "PingFangSC-Regular", size: 12)
        label.textColor = colorWithRGB(r: 252, g: 78, b: 59)
        return label
    }()
    
    fileprivate var leftButton: UIButton = {
        let button = UIButton()
        button.setTitle("取消", for: .normal)
        button.titleLabel?.font = UIFont(name: "PingFangSC-Regular", size: 16)
        button.setTitleColor(UIColor.black, for: .normal)
        return button
    }()
    
    fileprivate var rightButton: UIButton = {
        let button = UIButton()
        button.setTitle("确认", for: .normal)
        button.titleLabel?.font = UIFont(name: "PingFangSC-Regular", size: 16)
        button.setTitleColor(UIColor.init(red: 16/255, green: 142/255, blue: 233/255, alpha: 1), for: .normal)
        return button
    }()

    private var okHandler: ((String)->Void)?
    
    init (_ message: String, okHandler: @escaping (String)-> Void){
        super.init(nibName: nil, bundle: nil)
        self.transitioningDelegate = self
        self.okHandler = okHandler
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.leftButton.addTarget(self, action: #selector(doLeftButtonAction), for: .touchUpInside)
        self.rightButton.addTarget(self, action: #selector(doRightButtonAction), for: .touchUpInside)
        self.coverView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(doTapCoverViewAction)))
        
        self.setupViews()
    }
    
    private func setupViews() {
        self.view.addSubview(self.coverView)
        self.coverView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        self.view.addSubview(self.panelView)
        self.panelView.snp.makeConstraints { (make) in
            make.left.equalTo(45)
            make.right.equalTo(-45)
            make.center.equalTo(self.view)
        }
        
        self.panelView.addSubview(self.titleLabel)
        self.titleLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.panelView)
            make.top.equalTo(self.panelView).offset(15)
        }
        
        self.panelView.addSubview(self.reasonTextView)
        self.reasonTextView.snp.makeConstraints { (make) in
            make.left.equalTo(self.panelView).offset(20)
            make.right.equalTo(self.panelView).offset(-20)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(15)
            make.height.equalTo(150)
        }
        
        self.panelView.addSubview(self.warningLabel)
        self.warningLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.reasonTextView)
            make.top.equalTo(self.reasonTextView.snp.bottom).offset(10)
            
        }
        
        let horizontalLine = UIView()
        horizontalLine.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        self.panelView.addSubview(horizontalLine)
        horizontalLine.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.panelView)
            make.top.equalTo(self.warningLabel.snp.bottom).offset(20)
            make.height.equalTo(1)
        }
        
        self.panelView.addSubview(self.leftButton)
        self.leftButton.snp.makeConstraints { (make) in
            make.left.bottom.equalTo(self.panelView)
            make.top.equalTo(horizontalLine.snp.bottom)
            make.height.equalTo(50)
        }
        
        let verticalLine = UIView()
        verticalLine.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        self.panelView.addSubview(verticalLine)
        verticalLine.snp.makeConstraints { (make) in
            make.top.equalTo(horizontalLine.snp.bottom)
            make.bottom.equalTo(self.panelView)
            make.width.equalTo(1)
            make.left.equalTo(self.leftButton.snp.right)
        }
        
        self.panelView.addSubview(self.rightButton)
        self.rightButton.snp.makeConstraints { (make) in
            make.right.bottom.equalTo(self.panelView)
            make.left.equalTo(verticalLine.snp.right)
            make.height.equalTo(self.leftButton)
            make.top.equalTo(horizontalLine.snp.bottom)
            make.width.equalTo(self.leftButton)
        }
    }
    
    @objc private func doLeftButtonAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func doRightButtonAction() {
        if self.okHandler != nil {
            self.okHandler!(self.reasonTextView.text ?? "")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func doTapCoverViewAction() {
        self.view.endEditing(true)
    }
}

extension ETTextViewAlertController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AlertControllerTransitionPresent()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AlertControllerTransitionDismiss()
    }
}

class AlertControllerTransitionPresent: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromView = transitionContext.view(forKey: .from)
        let toVc = transitionContext.viewController(forKey: .to) as! ETTextViewAlertController
        let container = transitionContext.containerView
        container.addSubview(toVc.view)
        
        let panelView = toVc.panelView
        let coverview = toVc.coverView
        coverview.backgroundColor = UIColor.black.withAlphaComponent(0.01)
        panelView.alpha = 0.3
        panelView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration:0.1, animations: {
            coverview.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            panelView.transform = CGAffineTransform.identity
            panelView.alpha = 1
        }) { (finish) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            toVc.view.addSubview(fromView!)
            toVc.view.sendSubview(toBack: fromView!)
        }
    }
}

class AlertControllerTransitionDismiss: NSObject,UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromVc = transitionContext.viewController(forKey: .from) as! ETTextViewAlertController
        
        //        let panelView = fromVc.panelView
        let coverview = fromVc.coverView
        UIView.animate(withDuration: 0.1, animations: {
            coverview.backgroundColor = UIColor.black.withAlphaComponent(0.01)
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}
