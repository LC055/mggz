//
//  AuthenDataViewController.swift
//  mggz
//
//  Created by QinWei on 2018/3/13.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Kingfisher
class AuthenDataViewController: UIViewController {
     fileprivate var userInfoModel: UserInfoModel?
     var cTStatus:CertificationStatus?
    @IBOutlet weak var headerH: NSLayoutConstraint!
    
    @IBOutlet weak var headerW: NSLayoutConstraint!
    
   
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var topH: NSLayoutConstraint!
    
    @IBOutlet weak var firstH: NSLayoutConstraint!
    
    @IBOutlet weak var secondH: NSLayoutConstraint!
    
    
    @IBOutlet weak var thirdH: NSLayoutConstraint!
    
    @IBOutlet weak var fiveH: NSLayoutConstraint!
    @IBOutlet weak var fourH: NSLayoutConstraint!
    @IBOutlet weak var bottomH: NSLayoutConstraint!
    
    
    @IBOutlet weak var headerView: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var sexLabel: UILabel!
    
    @IBOutlet weak var idCardLabel: UILabel!
    
    @IBOutlet weak var adressLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var posiveImage: UIImageView!
    
    @IBOutlet weak var reverseImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupAllUIHeight()
        self.getData()
    }
    
    
    
    @IBAction func statusButtonClick(_ sender: Any) {
        let certification = CertificationFirstPageViewController(nibName: "CertificationFirstPageViewController", bundle: nil)
 self.navigationController?.pushViewController(certification, animated: true)
    }
    fileprivate func setupAllUIHeight(){
        self.headerH.constant = 1.2*90*SCREEN_WIDTH/375
        self.headerW.constant = 1.2*90*SCREEN_WIDTH/375
        self.topH.constant = 1.2*134*SCREEN_WIDTH/375
        self.firstH.constant = 1.2*30*SCREEN_WIDTH/375
        self.secondH.constant = 1.2*30*SCREEN_WIDTH/375
        self.thirdH.constant = 1.2*30*SCREEN_WIDTH/375
        self.fourH.constant = 1.2*40*SCREEN_WIDTH/375
        self.fiveH.constant = 1.2*30*SCREEN_WIDTH/375
        self.bottomH.constant = 1.2*115*SCREEN_WIDTH/375
        
        self.headerView.layer.cornerRadius = self.headerH.constant/2
        self.headerView.layer.masksToBounds = true
        
        self.posiveImage.layer.cornerRadius = 5
        self.posiveImage.layer.masksToBounds = true
        
        self.reverseImage.layer.cornerRadius = 5
        self.reverseImage.layer.masksToBounds = true
        
        self.statusButton.layer.cornerRadius = 5
        self.statusButton.layer.masksToBounds = true
        
        if self.cTStatus == .certificationInProgress{
            self.statusButton.backgroundColor = UIColor.lightGray
            self.statusButton.isEnabled = false
            self.statusButton.setTitle("审核中", for: UIControlState.normal)
        }
        if self.cTStatus == .beReturn{
            self.statusButton.isEnabled = true
            self.statusButton.setTitle("重新认证", for: UIControlState.normal)
        }
    }
    func getData() {
        UserInfoModel.getMigrantWorkInfo { (response) in
            if response.success {
                self.userInfoModel = response.value
                self.setupAllUIData()
            }
            else {
                WWError(response.message)
            }
        }
    }
    fileprivate func setupAllUIData(){
        self.userName.text = self.userInfoModel?.username
        if let photoHrl = self.userInfoModel?.photoUrl {
            self.headerView.kf.setImage(with: URL.init(string: ImageUrlPrefix + photoHrl), placeholder: UIImage.init(named: "au_header"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        if self.userInfoModel?.sex == .boy {
            self.sexLabel.text = "男"
        }else{
            self.sexLabel.text = "女"
        }
        self.idCardLabel.text = self.userInfoModel?.idCard
        self.adressLabel.text = self.userInfoModel?.address
        
        
        if ((self.userInfoModel?.iDCardValidBeginDate) != nil) && ((self.userInfoModel?.iDCardValidEndDate) != nil){
            self.dateLabel.text = (self.userInfoModel?.iDCardValidBeginDate)!   + "至" + (self.userInfoModel?.iDCardValidEndDate)!
        }else{
            self.dateLabel.text = ""
        }
        
        if let posiveUrl = self.userInfoModel?.positiveIdCardUrl {
            
            self.posiveImage.kf.setImage(with: URL.init(string: ImageUrlPrefix + posiveUrl), placeholder: UIImage.init(named: "id_place"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        if let resverseUrl = self.userInfoModel?.reverseIdCardUrl {
            
            self.reverseImage.kf.setImage(with: URL.init(string: ImageUrlPrefix + resverseUrl), placeholder: UIImage.init(named: "id_place"), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
}
