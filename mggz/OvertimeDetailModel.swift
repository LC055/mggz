//
//  OvertimeDetailModel.swift
//  mggz
//
//  Created by QinWei on 2018/4/17.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class OvertimeDetailModel: Mappable {
    enum OvertimeStatus : Int {
        case inApply = 0
        case onOvertime = 1
        case haveOvertime = 2  //离职
        case noneOvertime = 3
        case invakid = -1 //已验收
        case inreturn = -2 //
    }
    
    
    var ID : String?
    var WorkDate : String?
    var DemandBaseMigrantWorkerID : String?
    var ApplyWorkDays : Float?
    var ApplyWorkHours : Float?
    var WorkBeginTime : String?
    var WorkEndTime : String?
    var ApplyState : Int?
    var Reason : String?
    var BackReason : String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ID       <- map["ID"]
        WorkDate       <- map["WorkDate"]
        DemandBaseMigrantWorkerID       <- map["DemandBaseMigrantWorkerID"]
        ApplyWorkDays       <- map["ApplyWorkDays"]
        ApplyWorkHours       <- map["ApplyWorkHours"]
        WorkBeginTime       <- map["WorkBeginTime"]
        WorkEndTime       <- map["WorkEndTime"]
        ApplyState       <- map["ApplyState"]
        Reason       <- map["Reason"]
        BackReason       <- map["BackReason"]
    }
}








