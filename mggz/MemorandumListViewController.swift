//
//  MemorandumListViewController.swift
//  mggz
//
//  Created by QinWei on 2018/4/10.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class MemorandumListViewController: UIViewController {
    var projectModel: ProjectModel?
    var selectDate: Date?
   // var popState:String?
  //  var needSaveData:String?
    
    
    @IBOutlet weak var projectView: UIView!
    
    @IBOutlet weak var projectName: UILabel!
    @IBOutlet weak var supplyButton: UIButton!
    
    @IBOutlet weak var memorandumButton: UIButton!
    fileprivate lazy var showView : UIView = {
        let showView = UIView(frame: CGRect(x: SCREEN_WIDTH/8, y: 144, width: SCREEN_WIDTH/4, height: 2))
        showView.backgroundColor = UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1)
        return showView
    }()
    
    fileprivate lazy var scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = UIColor.white
        scrollView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.scrollsToTop = false
        scrollView.bounces = false
        scrollView.isScrollEnabled = false
        scrollView.contentSize = CGSize(width: 2*SCREEN_WIDTH, height: 0)
        return scrollView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = dateToString(date: selectDate!)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "记事", style: .done, target: self, action: #selector(pushAbnormalTrack))
        self.projectName.text = "项目:暂无"
        if  let projectStr = self.projectModel?.projectName {
            self.projectName.text = "项目:" + projectStr
        }
  self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font:UIFont.systemFont(ofSize: 15)], for: .normal)
      
       
        self.view.backgroundColor = UIColor.white
        //self.title = dateToString(date: selectDate!)
        self.supplyButton.setTitleColor(UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1), for: .normal)
        
       self.memorandumButton.setTitleColor(UIColor.lightGray, for: .normal)
        
        self.view.addSubview(scrollView)
        self.setupChildController()
        self.addChildVcIntoScrollView()
        self.view.bringSubview(toFront: self.supplyButton)
        self.view.bringSubview(toFront: self.memorandumButton)
        self.view.bringSubview(toFront: self.projectView)
        self.view.addSubview(showView)
    }
    
    @objc fileprivate func pushAbnormalTrack(){
        let memorand = MemorandumViewController(nibName: "MemorandumViewController", bundle: nil)
        memorand.currentProject = self.projectModel
        memorand.selectDate = self.selectDate
      self.navigationController?.pushViewController(memorand, animated: true)
    }
    @IBAction func supplyButtonClick(_ sender: Any) {
        UIView.animate(withDuration: 0.2) {
            self.scrollView.contentOffset = CGPoint(x: 0, y: self.scrollView.contentOffset.y)
            self.supplyButton.setTitleColor(UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1), for: .normal)
        self.memorandumButton.setTitleColor(UIColor.lightGray, for: .normal)
            
            self.showView.frame = CGRect(x: SCREEN_WIDTH/8, y: self.showView.frame.origin.y, width: self.showView.frame.size.width, height: self.showView.frame.size.height)
        }
    }
    
    @IBAction func memorandumClick(_ sender: Any) {
        UIView.animate(withDuration: 0.2) {
            self.scrollView.contentOffset = CGPoint(x: SCREEN_WIDTH, y: self.scrollView.contentOffset.y)
           self.supplyButton.setTitleColor(UIColor.lightGray, for: .normal)
        self.memorandumButton.setTitleColor(UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1), for: .normal)
            
        self.showView.frame = CGRect(x: 5*SCREEN_WIDTH/8, y: self.showView.frame.origin.y, width: self.showView.frame.size.width, height: self.showView.frame.size.height)
        }
    }
    
    private func setupChildController(){
        
        let vc1 = RecordSupplyFormController()
        vc1.projectModel = self.projectModel
        vc1.selectDate = self.selectDate
        vc1.title = "补工"
        self.addChildViewController(vc1)
        
        let memorand = OvertimeMainViewController(nibName: "OvertimeMainViewController", bundle: nil)
        memorand.currentProject = self.projectModel
        memorand.selectDate = self.selectDate
        memorand.isCanApply = true
        memorand.title = "加班"
        memorand.isCalenlar = true
        self.addChildViewController(memorand)
    }
    private func addChildVcIntoScrollView(){
        for i in 0..<2 {
            let childVc = self.childViewControllers[i]
            if childVc.isViewLoaded {
                return
            }
            let childVcView = childVc.view
            let scrollViewW = CGFloat(i) * SCREEN_WIDTH
            childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
            scrollView.addSubview(childVcView!)
        }
    }
}

extension MemorandumListViewController : UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
}







