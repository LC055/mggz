//
//  OvertimeStartView.swift
//  mggz
//
//  Created by QinWei on 2018/4/17.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class OvertimeStartView: UIView {

    @IBOutlet weak var datePicker: UIDatePicker!
    

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sureButton: UIButton!
    
    var myFrame: CGRect?
    var dateStr:String?
    var detailDateStr:String?
    var selectedTime:((String,String) -> Void)? = nil
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
        self.dateStr = "17:30"
        self.detailDateStr = "17:30:00"
        
        
        self.datePicker.addTarget(self, action: #selector(dateValueChange(_:)), for: .valueChanged)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    @IBAction func sureButtonClick(_ sender: Any) {
        self.selectedTime!(self.dateStr!, self.detailDateStr!)
    }
    @objc fileprivate func dateValueChange(_ datePicker:UIDatePicker){
        self.dateStr = dateToTimeString(date: datePicker.date)
        self.detailDateStr =  dateToDetailTimeString(date: datePicker.date)
    }
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "OvertimeStartView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        return view
    }
}
