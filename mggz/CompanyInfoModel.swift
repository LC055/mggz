//
//  CompanyInfoModel.swift
//  mggz
//
//  Created by QinWei on 2018/3/13.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class CompanyInfoModel: Mappable {
    var CompanyID : String?
    var CompanyName : String?
    var DaySalary : Float?
    var HourSalary : Float?
    var ID : String?
    var MigrantWorkType : String?
    var SalaryAutoPeriod : String?
    var WorkTimeQuantum : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
        CompanyID       <- map["CompanyID"]
        CompanyName       <- map["CompanyName"]
        DaySalary       <- map["DaySalary"]
        HourSalary       <- map["HourSalary"]
        ID       <- map["ID"]
        MigrantWorkType       <- map["MigrantWorkType"]
        SalaryAutoPeriod       <- map["SalaryAutoPeriod"]
        WorkTimeQuantum       <- map["WorkTimeQuantum"] 
    }
}
