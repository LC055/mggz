//
//  NormalAndOvertimeCell.swift
//  mggz
//
//  Created by QinWei on 2018/4/16.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class NormalAndOvertimeCell: UITableViewCell {
   
    @IBOutlet weak var normalButton: UIButton!
    
    @IBOutlet weak var overtimeButton: UIButton!
    
    @IBOutlet weak var bottomView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
    }
    
}
