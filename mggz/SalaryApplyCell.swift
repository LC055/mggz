//
//  SalaryApplyCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/25.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class SalaryApplyCell: UITableViewCell {
    
    var panel = UIView()
    
    //标题
    var titleWithDateLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = wwFont_Medium(15)
        
        
        return label
    }()
    
    //结算天数
    var workingDaysLabel: UILabel = {
        let label = UILabel()
        label.text = "结算天数:"
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(15)
        
        return label
    }()
    
    var workingDaysDetailLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.text = "0天"
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(15)
        
        return label
    }()
    
    //结算工时数
    var workingHoursLabel: UILabel = {
        let label = UILabel()
        label.text = "结算工数:"
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(15)
        return label
    }()
    
    var workingHoursDetailLabel: UILabel = {
        let label = UILabel()
        label.text = "0小时"
        label.textAlignment = .right
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(15)
        return label
    }()
    
    
    //本次结算工资
    var accountAmountLabel: UILabel = {
        let label = UILabel()
        label.text = "本次约定结算工资:"
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(15)
        return label
    }()
    
    //本次实际结算工资
    var realAccountAmountLabel: UILabel = {
        let label = UILabel()
        label.text = "本次实际结算工资:"
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(15)
        return label
    }()
    var realAccountAmountDetailLabel: UILabel = {
        let label = UILabel()
        label.text = "0元"
        label.textColor = UIColor.mg_lightGray
        label.textAlignment = .right
        label.font = wwFont_Regular(15)
        return label
    }()
    
    
    var accountAmountDetailLabel: UILabel = {
        let label = UILabel()
        label.text = "0元"
        label.textColor = UIColor.mg_lightGray
        label.textAlignment = .right
        label.font = wwFont_Regular(15)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clear
        self.panel.backgroundColor = UIColor.white
        self.selectionStyle = .none
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        
        self.contentView.addSubview(self.panel)
        self.panel.snp.makeConstraints { (make) in
            make.right.left.top.equalTo(self.contentView)
            make.bottom.equalTo(self.contentView).offset(-10)
        }
        
        self.contentView.addSubview(self.titleWithDateLabel)
        self.titleWithDateLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.panel).offset(15)
            make.left.equalTo(self.panel).offset(20)
            make.height.equalTo(15)
        }
        
        let grayLine = UIView()
        grayLine.backgroundColor = UIColor.mg_backgroundGray
        self.panel.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleWithDateLabel.snp.bottom).offset(10)
            make.left.equalTo(self.panel).offset(20)
            make.right.equalTo(self.panel).offset(-10)
            make.height.equalTo(1)
        }
        
        self.panel.addSubview(self.workingDaysLabel)
        self.workingDaysLabel.snp.makeConstraints { (make) in
            make.left.equalTo(grayLine)
            make.top.equalTo(grayLine.snp.bottom).offset(15)
            make.width.equalTo(150)
        }
        
        self.panel.addSubview(workingDaysDetailLabel)
        self.workingDaysDetailLabel.snp.makeConstraints { (make) in
            make.right.equalTo(grayLine)
            make.left.equalTo(self.workingDaysLabel.snp.right)
            make.centerY.equalTo(self.workingDaysLabel)
        }
        
        self.panel.addSubview(self.workingHoursLabel)
        self.workingHoursLabel.snp.makeConstraints { (make) in
            make.left.equalTo(grayLine)
            make.top.equalTo(self.workingDaysLabel.snp.bottom).offset(10)
            make.width.equalTo(self.workingDaysLabel)
        }
        
        self.panel.addSubview(workingHoursDetailLabel)
        self.workingHoursDetailLabel.snp.makeConstraints { (make) in
            make.right.equalTo(grayLine)
            make.left.equalTo(self.workingHoursLabel.snp.right)
            make.centerY.equalTo(self.workingHoursLabel)
        }
        
        self.panel.addSubview(accountAmountLabel)
        self.accountAmountLabel.snp.makeConstraints { (make) in
            make.left.equalTo(grayLine)
            make.top.equalTo(self.workingHoursLabel.snp.bottom).offset(10)
            make.width.equalTo(self.workingDaysLabel)
        }
        
        self.panel.addSubview(accountAmountDetailLabel)
        self.accountAmountDetailLabel.snp.makeConstraints { (make) in
            make.right.equalTo(grayLine)
            make.left.equalTo(self.accountAmountLabel.snp.right)
            make.centerY.equalTo(self.accountAmountLabel)
        }
        
        self.panel.addSubview(realAccountAmountLabel)
        self.realAccountAmountLabel.snp.makeConstraints { (make) in
            make.left.equalTo(grayLine)
       make.top.equalTo(self.accountAmountLabel.snp.bottom).offset(10)
            make.width.equalTo(self.accountAmountLabel)
        }
        
        self.panel.addSubview(realAccountAmountDetailLabel)
        self.realAccountAmountDetailLabel.snp.makeConstraints { (make) in
            make.right.equalTo(grayLine)
        make.left.equalTo(self.realAccountAmountLabel.snp.right)
            make.centerY.equalTo(self.realAccountAmountLabel)
        }
        
    }
    
    func bind(_ model: SalaryApplyModel,workType:Int) {
        
        if workType==2{
            
            self.workingDaysDetailLabel.text = model.workingDays + "天"
            self.workingHoursDetailLabel.text = model.workingHours + "小时"
            self.accountAmountDetailLabel.text = model.protocolSalary + "元"
            self.realAccountAmountDetailLabel.text = model.realPaySalary + "元"
        }else{
            self.workingDaysDetailLabel.text = model.workingDays + "天"
            self.workingHoursDetailLabel.text = model.workedDays + "工"
            self.accountAmountDetailLabel.text = model.protocolSalary + "元"
            self.realAccountAmountDetailLabel.text = model.realPaySalary + "元"
            
            
        }
    }
}

