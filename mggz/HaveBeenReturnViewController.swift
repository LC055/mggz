//
//  HaveBeenReturnViewController.swift
//  mggz
//
//  Created by QinWei on 2018/3/21.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class HaveBeenReturnViewController: UIViewController {
    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var ctButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "实名认证"
        guard let resonStr = WWUser.sharedInstance.stateReason else {
            return
        }
        self.reasonLabel.text = resonStr
    }
    @IBAction func ctButtonClick(_ sender: Any) {
        let certification = CertificationFirstPageViewController(nibName: "CertificationFirstPageViewController", bundle: nil)
        
    self.navigationController?.pushViewController(certification, animated: true)
    }
}
