//
//  FillRecordCell.swift
//  mggz
//
//  Created by QinWei on 2018/3/19.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class FillRecordCell: UITableViewCell {

    @IBOutlet weak var fillNum: UILabel!
    
    @IBOutlet weak var comLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCellDataWithModel(_ model:FillRecordModel){
        if model.WorkDayType == 2{
            if (model.WorkHours != nil){
                self.fillNum.text = String(model.WorkHours!)
            }else{
                self.fillNum.text = "?"
            }
            self.comLabel.text = "小时"
        }else{
            if (model.WorkDays != nil){
                self.fillNum.text = String(model.WorkDays!)
            }else{
                self.fillNum.text = "?"
            }
            self.comLabel.text = "工"
        }
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
