//
//  PwdRestViewController.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/2.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class PwdRestViewController: UIViewController {
    
    @IBOutlet weak var pastPwdTextField: UITextField!
    
    @IBOutlet weak var newPwdTextField: UITextField!
    
    @IBOutlet weak var surePwdTextField: UITextField!
    
    @IBOutlet weak var passDisplay: UIButton!
    
    @IBOutlet weak var newDisplay: UIButton!
    
    @IBOutlet weak var sureDisplay: UIButton!
    
    @IBOutlet weak var submitButton: UIButton!
    private var ispastDisplay:Bool = true
    private var isnewDisplay:Bool = true
    private var issureDisplay:Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "修改密码"
        self.setupInitUI()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    func setupInitUI(){
        
        self.pastPwdTextField.autocorrectionType = .no
        self.pastPwdTextField.autocapitalizationType = .none
        self.pastPwdTextField.delegate = self
        self.newPwdTextField.autocorrectionType = .no
        self.newPwdTextField.autocapitalizationType = .none
        self.newPwdTextField.delegate = self
        self.surePwdTextField.autocorrectionType = .no
        self.surePwdTextField.autocapitalizationType = .none
        self.surePwdTextField.delegate = self
        submitButton.layer.cornerRadius = 6
        submitButton.layer.masksToBounds = true
        submitButton.layer.shadowOffset = CGSize(width: 1, height: 1)
        submitButton.layer.shadowOpacity = 0.3
    }
    
    @IBAction func forgetButtonClick(_ sender: Any) {
        let forgetVC = ForgetPwdNavigation()
        forgetVC.modalTransitionStyle = .flipHorizontal
        self.present(forgetVC, animated: true, completion: nil)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        pastPwdTextField.resignFirstResponder()
        newPwdTextField.resignFirstResponder()
        surePwdTextField.resignFirstResponder()
    }
    
    @IBAction func passDisplayClick(_ sender: Any) {
        if ispastDisplay == true {
            ispastDisplay = false
            pastPwdTextField.isSecureTextEntry = false
            passDisplay.setImage(UIImage.init(named: "login_openEye"), for: .normal)
            
        }else{
            ispastDisplay = true
            pastPwdTextField.isSecureTextEntry = true
            passDisplay.setImage(UIImage.init(named: "login_closeEye"), for: .normal)
        }
        
    }
    
    @IBAction func newDisplayClick(_ sender: Any) {
        if isnewDisplay == true {
            isnewDisplay=false
            newPwdTextField.isSecureTextEntry = false
            newDisplay.setImage(UIImage.init(named: "login_openEye"), for: .normal)
            
        }else{
            isnewDisplay = true
            newPwdTextField.isSecureTextEntry = true
            newDisplay.setImage(UIImage.init(named: "login_closeEye"), for: .normal)
            
        }
        
    }
    
    @IBAction func sureDisplayClick(_ sender: Any) {
        if issureDisplay == true {
            issureDisplay = false
            surePwdTextField.isSecureTextEntry = false
            sureDisplay.setImage(UIImage.init(named: "login_openEye"), for: .normal)
        }else{
            issureDisplay = true
            surePwdTextField.isSecureTextEntry = true
            sureDisplay.setImage(UIImage.init(named: "login_closeEye"), for: .normal)
        }
    }
    
    
    @IBAction func submitButtonClick(_ sender: Any) {
        guard let oldString = self.pastPwdTextField.text, oldString.count > 0 else {
            WWError("旧密码为空")
            return
        }
        guard let newString = self.newPwdTextField.text, newString.count > 0 else {
            WWError("新密码为空")
            return
        }
        guard let insureString = self.surePwdTextField.text, insureString.count > 0  else {
            WWError("确认密码为空")
            return
        }
        guard  newString == insureString else {
            WWError("两次新密码不同")
            return
        }
        //        guard  oldString != newString else {
        //            WWError("新密码和旧密码不能一样")
        //            return
        //        }
        guard newString.range(of: "&") == nil,insureString.range(of: "&") == nil else {
            WWError("密码必须为6-16位字母/数字组合！")
            return
        }
        
        
        WWBeginLoadingWithStatus("提交中...")
        UserModel.modifyPassword(oldString, newPassword: newString) { (response) in
            if response.success {
                
                WWSuccess("修改成功")
                self.navigationController?.popViewController(animated: true)
            }
            else {
                WWError(response.message)
            }
        }
        
    }

}
extension PwdRestViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        pastPwdTextField.resignFirstResponder()
        newPwdTextField.resignFirstResponder()
        surePwdTextField.resignFirstResponder()
        return true
    }
}
