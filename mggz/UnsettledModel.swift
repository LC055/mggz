//
//  UnsettledModel.swift
//  mggz
//
//  Created by QinWei on 2018/3/29.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class UnsettledModel: Mappable {
    var CreateTime : String?
    var SalaryNotPayed : Double?
    var WorkTime:Float?
    var ProjectName:String?
    var WorkDayType:Int?
    required init?(map: Map) {
        
    
    }
    func mapping(map: Map) {
        
        CreateTime       <- map["CreateTime"]
        ProjectName       <- map["ProjectName"]
        SalaryNotPayed       <- map["SalaryNotPayed"]
        WorkTime       <- map["WorkTime"]
        WorkDayType       <- map["WorkDayType"]
    }
}
