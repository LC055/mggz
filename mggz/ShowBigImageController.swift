//
//  ShowBigImageController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/13.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class ShowBigImageController: UIViewController,UIViewControllerTransitioningDelegate {
    
    fileprivate var bigImageView:UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    var image: UIImage?
    
    //小图相对于window的Rect
    var fromBounds: CGRect = CGRect.zero
    
    init(_ imageView: UIImageView) {
        super.init(nibName: nil, bundle: nil)
        
        let position = self.imageViewRectToWindow(imageView)
        
        self.fromBounds = position
        self.image = imageView.image
        self.transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black
        
        self.bigImageView.image = self.image
        self.bigImageView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(doTapAction)))
        
        self.view.addSubview(self.bigImageView)
        self.bigImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
    
    @objc func doTapAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        
    }
    
    //获取imageView相对于window的位置
    func imageViewRectToWindow(_ imageView: UIImageView) -> CGRect {
        let window = UIApplication.shared.delegate?.window!
        let position = imageView.convert(imageView.bounds, to: window)
        return position
    }
    
    //MARK:UIViewControllerTransitioningDelegate
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let aClass = BigImageControllerTransionPresent()
        aClass.fromBound = self.fromBounds
        return aClass
    }
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let dismissClass = BigImageControllerTransionDismiss()
        dismissClass.fromBound = fromBounds
        dismissClass.currentBound = self.bigImageView.frame
        return dismissClass
    }
    
}

class BigImageControllerTransionPresent: NSObject, UIViewControllerAnimatedTransitioning {
    var fromBound: CGRect = CGRect.zero
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        //toVC在present和dismiss中表示的是不同的。在present中表要present的VC，在dismiss中表示dismiss后跳去的vc
        let toVC = transitionContext.viewController(forKey: .to) as! ShowBigImageController
        //containerView是专门在transition过程出现的一个用来展示动画的view，这个view在transition动画结束后就会消失。
        let container = transitionContext.containerView
        container.addSubview(toVC.view)
        
        let aImageView = toVC.bigImageView
        aImageView.frame = self.fromBound
        
        UIView.animate(withDuration: 0.3, delay: 0.3, options: UIViewAnimationOptions.curveEaseIn, animations: {
            aImageView.frame = UIScreen.main.bounds
        }) { (finish) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}

class BigImageControllerTransionDismiss: NSObject, UIViewControllerAnimatedTransitioning {
    var fromBound: CGRect = CGRect.zero
    var currentBound: CGRect = CGRect.zero
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromVc = transitionContext.viewController(forKey: .from) as! ShowBigImageController //被dismiss的vc
        let toVc = transitionContext.viewController(forKey: .to)! //dismiss后跳向的vc
        let container = transitionContext.containerView
        
        let aToView = toVc.view!
        
        //coverView用来遮盖原始的小图。在缩放完成后取消遮盖，
        let coverView = UIView(frame: fromBound)
        coverView.backgroundColor = UIColor.white
        aToView.addSubview(coverView)
        container.addSubview(aToView)
        
        let animateView = fromVc.view!//进行动画的view
        let animateImageView = fromVc.bigImageView
        
        //animateView是当前要dismiss的vc的view,把它提到顶层并让背景清空，就可以实现『图片开始缩放，之前的页面马上可以看见的效果』
        animateView.backgroundColor = UIColor.clear
        container.bringSubview(toFront: animateView)
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            
            animateImageView.frame = self.fromBound
        }) { (finish) in
            //完成页面转换时调用。一般都在完成动画后调用
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            
            coverView.removeFromSuperview()
        }
    }
}
