//
//  LoanAmountSelectView.swift
//  mggz
//
//  Created by QinWei on 2018/2/11.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LoanAmountSelectView: UIView {

    @IBOutlet weak var onehundsButton: UIButton!
    
    @IBOutlet weak var twoHundsButton: UIButton!
    
    @IBOutlet weak var oneMonthButton: UIButton!
    
    @IBOutlet weak var twoMonthsButton: UIButton!
    
    @IBOutlet weak var threeMonthsButton: UIButton!
    
    @IBOutlet weak var makesureButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    fileprivate func setupAllButtonStatus(_ button:UIButton){
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
    }
    fileprivate func setupBottomButton(){
        self.makesureButton.layer.cornerRadius = 5
        self.makesureButton.layer.masksToBounds = true
        self.cancelButton.layer.cornerRadius = 5
        self.cancelButton.layer.masksToBounds = true
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "LoanAmountSelectView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        view.backgroundColor = UIColor.white
        self.makesureButton.isEnabled = false
        self.setupAllButtonStatus(self.onehundsButton)
        self.setupAllButtonStatus(self.twoHundsButton)
        self.setupAllButtonStatus(self.oneMonthButton)
        self.setupAllButtonStatus(self.twoMonthsButton)
        self.setupAllButtonStatus(self.threeMonthsButton)
        self.setupBottomButton()
        return view
    }
}
