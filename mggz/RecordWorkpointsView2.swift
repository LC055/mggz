//
//  RecordWorkpointsView2.swift
//  mggz
//
//  Created by QinWei on 2018/2/28.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class RecordWorkpointsView2: UIView {
    var myFrame: CGRect?
    var selectedPoints:((String) -> Void)? = nil
    var systemPoints:String?
    var workType:Int?
    var isRecord:Bool?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var recordPoints: UIButton!
    @IBOutlet weak var pickView: UIPickerView!
    fileprivate var contentView:UIView?
    fileprivate let workNumArray = ["0.5","0.75","1","1.5","2","3"]
//    fileprivate lazy var pickerView : UIPickerView = {
//        let pickerView = UIPickerView.init(frame: CGRect(x: 0, y: 50, width: 240, height: 250))
//        pickerView.backgroundColor = UIColor.clear
//        
//        pickerView.delegate = self
//        pickerView.dataSource = self
//        return pickerView
//    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        myFrame = frame
        self.setupSubviews()
       
        self.pickView.delegate = self
        self.pickView.dataSource = self
        self.pickView.reloadAllComponents()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setupSubviews()
    }
    func setupSubviews() {
        contentView = loadViewFromNib()
        addSubview(contentView!)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "RecordWorkpointsView2", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        return view
    }
}

extension RecordWorkpointsView2:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if workType == 2 {
            return 16
        }
       return 12
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 240
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if workType == 2{
            self.titleLabel.text = "请选择工时数"
            return ["0.5","1","1.5","2","2.5","3","3.5","4","4.5","5","5.5","6","6.5","7","7.5","8"][row]
        }
        self.titleLabel.text = "请选择工数"
      return ["0.25","0.5","0.75","1","1.25","1.5","1.75","2","2.25","2.5","2.75","3"][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if workType == 2{
            
            let selectPointStr = ["0.5","1","1.5","2","2.5","3","3.5","4","4.5","5","5.5","6","6.5","7","7.5","8"][row]
            self.selectedPoints?(selectPointStr)
            
        }else{
            
            let selectPointStr = ["0.25","0.5","0.75","1","1.25","1.5","1.75","2","2.25","2.5","2.75","3"][row]
            self.selectedPoints?(selectPointStr)
        }
    }
}























































