//
//  UITableView+Extension.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/25.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

/**
 向tableView 注册 UITableViewCell
 
 - parameter tableView: tableView
 - parameter cell:      要注册的类名
 */
func regClass(_ tableView: UITableView, cell: AnyClass)  {
    tableView.register(cell, forCellReuseIdentifier: "\(cell)")
}

/**
 从tableView缓存中取出对应类型的Cell
 如果缓存中没有，则重新创建一个
 
 - parameter tableView: tableView
 - parameter cell:      要返回的Cell类型
 - parameter indexPath: 位置
 
 - returns: 传入Cell类型的 实例对象
 */
func getCell<T: UITableViewCell>(_ tableView: UITableView, cell: T.Type, indexPath: IndexPath) -> T {
    return tableView.dequeueReusableCell(withIdentifier: "\(cell)", for: indexPath) as! T
}
