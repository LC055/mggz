//
//  OvertimeWorkView.swift
//  mggz
//
//  Created by QinWei on 2018/4/17.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class OvertimeWorkView: UIView {
    var selectedPoints:((String) -> Void)? = nil
    var workSurePoints:String?
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var sureButton: UIButton!
    
    let workData = ["0.25工","0.5工","0.75工","1工"]
    
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.workSurePoints = "0.5"
        self.setupSubviews()
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.selectRow(1, inComponent: 0, animated: true)
        self.pickerView.reloadAllComponents()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    @IBAction func makeSureButtonClick(_ sender: Any) {
        self.selectedPoints!(self.workSurePoints!)
    }
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "OvertimeWorkView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        return view
    }
    
}
extension OvertimeWorkView:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return workData.count
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 240
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return workData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      self.workSurePoints = ["0.25","0.5","0.75","1.0"][row]
    }
}
