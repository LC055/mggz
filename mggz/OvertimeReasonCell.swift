//
//  OvertimeReasonCell.swift
//  mggz
//
//  Created by QinWei on 2018/4/17.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class OvertimeReasonCell: UITableViewCell {
    var textViewDidChange: ((String) -> Void)?
    @IBOutlet weak var reasonTextView: QWTextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        //self.reasonTextView.toolbarPlaceholder = "请输入需要加班的原因..."
        //self.reasonTextView = UITextView.init(placeholder: "请输入需要加班的原因...", placeholderColor: UIColor.init(white: 0.4, alpha: 1))
        
        self.reasonTextView.placeHolder = "请输入需要加班的原因..."
        self.reasonTextView.delegate = self
        self.reasonTextView.layer.cornerRadius = 5
        self.reasonTextView.layer.masksToBounds = true
        self.reasonTextView.layer.borderWidth = 1
        self.reasonTextView.layer.borderColor = UIColor.mg_backgroundGray.cgColor
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension OvertimeReasonCell:UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        if let changeHandler = self.textViewDidChange {
            changeHandler(textView.text)
        }
    }
}
