//
//  CertificationReviewIdPhotoCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/7.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Kingfisher

class CertificationReviewIdPhotoCell: UITableViewCell {
    
    var positiveSideImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = colorWith255RGB(248, g: 248, b: 248)
        return imageView
    }()
    
    var reverseSideImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = colorWith255RGB(248, g: 248, b: 248)
        return imageView
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.contentView.addSubview(self.positiveSideImageView)
        self.positiveSideImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView).offset(20)
            make.right.equalTo(self.contentView.snp.centerX).offset(-10)
            make.width.equalTo(150)
            make.height.equalTo(80)
        }
        
        let positiveSideLabel = UILabel()
        positiveSideLabel.text = "身份证正面照"
        positiveSideLabel.font = wwFont_Regular(15)
        positiveSideLabel.textColor = UIColor.mg_lightGray
        self.contentView.addSubview(positiveSideLabel)
        positiveSideLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.positiveSideImageView.snp.bottom).offset(10)
            make.height.equalTo(30)
            make.centerX.equalTo(self.positiveSideImageView)
        }
        
        self.contentView.addSubview(self.reverseSideImageView)
        self.reverseSideImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView).offset(20)
            make.size.equalTo(self.positiveSideImageView)
            make.left.equalTo(self.contentView.snp.centerX).offset(20)
        }
        
        let reverseSideLabel = UILabel()
        reverseSideLabel.text = "身份证反面照"
        reverseSideLabel.font = wwFont_Regular(15)
        reverseSideLabel.textColor = UIColor.mg_lightGray
        self.contentView.addSubview(reverseSideLabel)
        reverseSideLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.reverseSideImageView.snp.bottom).offset(10)
            make.centerX.equalTo(self.reverseSideImageView)
        }
    }
    
    func bind(_ model: UserInfoModel) {
        if let url = model.positiveIdCardUrl {
            
            self.positiveSideImageView.kf.setImage(with: URL.init(string: ImageUrlPrefix + url), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        if let url = model.reverseIdCardUrl {
            self.reverseSideImageView.kf.setImage(with: URL.init(string: ImageUrlPrefix + url), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
        }
        
    }
    
}
