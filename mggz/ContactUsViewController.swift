//
//  ContactUsViewController.swift
//  mggz
//
//  Created by QinWei on 2018/4/2.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    
    @IBOutlet weak var suggestionButton: UIView!
    
    @IBOutlet weak var phoneButton: UIView!
    
    @IBOutlet weak var qqButton: UIView!
    
    @IBOutlet weak var qqLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "联系我们"
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(suggestionButtonClick))
        self.suggestionButton.addGestureRecognizer(tap1)
        self.qqButton.isUserInteractionEnabled = true
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(phoneButtonClick))
        self.phoneButton.addGestureRecognizer(tap2)
        self.qqLabel.isUserInteractionEnabled = true
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressClick(_:)))
        longPress.allowableMovement = 1.5
        self.qqLabel.addGestureRecognizer(longPress)
    }
    @objc fileprivate func longPressClick(_ longPress:UILongPressGestureRecognizer){
        self.qqLabel.becomeFirstResponder()
        let menu = UIMenuController.shared
        let item1 = UIMenuItem.init(title: "复制", action: #selector(copyText))
        menu.menuItems = [item1]
        menu.arrowDirection = .up
        menu.setTargetRect(self.qqLabel.bounds, in: self.qqLabel.superview!)
        menu.setMenuVisible(true, animated: true)
    }
    @objc fileprivate func copyText() {
        UIPasteboard.general.string = self.qqLabel.text
    }
    

    @objc fileprivate func suggestionButtonClick(){
        let vc = SuggestionController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc fileprivate func phoneButtonClick(){
        UIApplication.shared.openURL(NSURL(string: "tel:400-822-9555")! as URL)
    }
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copyText) {
            return true
        } else {
            return false
        }
    
      }
}










