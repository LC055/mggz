//
//  OvertimePlanModel.swift
//  mggz
//
//  Created by QinWei on 2018/4/17.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class OvertimePlanModel: Mappable {
    var ID : String?
    var WorkDate : String?
    var ApplyWorkDays : Float?
    var ApplyWorkHours : Float?
    var WorkBeginTime : String?
    var WorkEndTime : String?
    var ApplyState : Int?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        ID       <- map["ID"]
        WorkDate       <- map["WorkDate"]
        ApplyWorkDays       <- map["ApplyWorkDays"]
        ApplyWorkHours       <- map["ApplyWorkHours"]
        WorkBeginTime       <- map["WorkBeginTime"]
        ApplyState       <- map["ApplyState"]
        WorkEndTime       <- map["WorkEndTime"]
    }
    
}
