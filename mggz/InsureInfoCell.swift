//
//  InsureInfoCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/30.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class InsureInfoCell: UITableViewCell {
    
    static let labelFont:CGFloat = 17
    //状态
    var statusLabel: UILabel = {
       let label = UILabel()
        label.text = "状态："
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(labelFont)
        return label
    }()
    var statusDetailLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Regular(labelFont)
        label.text = "正常"
        return label
    }()
    
    //保险公司
    var insureCompanyLabel: UILabel = {
        let label = UILabel()
        label.text = "保险公司："
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(labelFont)
        return label
    }()
    var insureCompanyDetailLabel: UILabel = {
        let label = UILabel()
        label.text = "中国人寿"
        label.font = wwFont_Regular(labelFont)
        return label
    }()
    
    //保险产品
    var insureProductLabel: UILabel = {
        let label = UILabel()
        label.text = "保险产品："
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(labelFont)
        return label
    }()
    var insureProductDetailLabel: UILabel = {
        let label = UILabel()
        label.text = "意外铭贡献"
        label.font = wwFont_Regular(labelFont)
        return label
    }()
    
    //保单号
    var policyNoLabel: UILabel = {
        let label = UILabel()
        label.text = "保单号："
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(labelFont)
        return label
    }()
    var policyNoDetailLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Regular(labelFont)
        return label
    }()
    
    //起始投保时间
    var startTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "起始投保时间："
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(labelFont)
        return label
    }()
    var startTimeDetailLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Regular(labelFont)
        return label
    }()
    
    //起始投保时间
    var endTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "截止投保时间："
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(labelFont)
        return label
    }()
    var endTimeDetailLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Regular(labelFont)
        return label
    }()
    
    var panelView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 6
        return view
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.backgroundColor = UIColor.clear
        
        self.contentView.addSubview(self.panelView)
        self.panelView.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView).offset(10)
            make.left.equalTo(self.contentView).offset(10)
            make.right.equalTo(self.contentView).offset(-10)
            make.bottom.equalTo(self.contentView)
        }
        
        self.panelView.addSubview(self.statusLabel)
        self.statusLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.panelView).offset(20)
            make.top.equalTo(self.panelView).offset(10)
            make.width.equalTo(120)
            make.height.equalTo(30)
        }
        
        self.panelView.addSubview(self.statusDetailLabel)
        self.statusDetailLabel.snp.makeConstraints { (make) in
            make.right.equalTo(self.panelView)
            make.top.equalTo(self.statusLabel)
            make.left.equalTo(self.statusLabel.snp.right).offset(10)
            make.height.equalTo(self.statusLabel)
        }
        
        self.panelView.addSubview(self.insureCompanyLabel)
        self.insureCompanyLabel.snp.makeConstraints { (make) in
            make.width.height.left.equalTo(self.statusLabel)
            make.top.equalTo(self.statusLabel.snp.bottom)
        }
        
        self.panelView.addSubview(self.insureCompanyDetailLabel)
        self.insureCompanyDetailLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.statusDetailLabel)
            make.top.equalTo(self.statusDetailLabel.snp.bottom)
            make.height.equalTo(self.statusDetailLabel)
        }
        
        self.panelView.addSubview(self.insureProductLabel)
        self.insureProductLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.insureCompanyLabel.snp.bottom)
            make.height.left.width.equalTo(self.insureCompanyLabel)
        }
        
        self.panelView.addSubview(self.insureProductDetailLabel)
        self.insureProductDetailLabel.snp.makeConstraints { (make) in
            make.height.left.right.equalTo(self.insureCompanyDetailLabel)
            make.top.equalTo(self.insureCompanyDetailLabel.snp.bottom)
            
        }
        
        self.panelView.addSubview(self.policyNoLabel)
        self.policyNoLabel.snp.makeConstraints { (make) in
            make.width.height.left.equalTo(self.insureProductLabel)
            make.top.equalTo(self.insureProductLabel.snp.bottom)
        }
        self.panelView.addSubview(self.policyNoDetailLabel)
        self.policyNoDetailLabel.snp.makeConstraints { (make) in
            make.left.right.height.equalTo(self.insureProductDetailLabel)
            make.top.equalTo(self.insureProductDetailLabel.snp.bottom)
        }
        
        self.panelView.addSubview(self.startTimeLabel)
        self.startTimeLabel.snp.makeConstraints { (make) in
            make.height.left.width.equalTo(self.policyNoLabel)
            make.top.equalTo(self.policyNoLabel.snp.bottom)
        }
        self.panelView.addSubview(self.startTimeDetailLabel)
        self.startTimeDetailLabel.snp.makeConstraints { (make) in
            make.left.right.height.equalTo(self.policyNoDetailLabel)
            make.top.equalTo(self.policyNoDetailLabel.snp.bottom)
        }
        
        self.panelView.addSubview(self.endTimeLabel)
        self.endTimeLabel.snp.makeConstraints { (make) in
            make.height.left.width.equalTo(self.startTimeLabel)
            make.top.equalTo(self.startTimeLabel.snp.bottom)
        }
        self.panelView.addSubview(self.endTimeDetailLabel)
        self.endTimeDetailLabel.snp.makeConstraints { (make) in
            make.left.right.height.equalTo(self.startTimeDetailLabel)
            make.top.equalTo(self.startTimeDetailLabel.snp.bottom)
        }
    }
    
    func bind(_ model: InsureModel) {
        self.statusDetailLabel.text = model.isValide ? "正常" : "不正常"
        self.insureCompanyDetailLabel.text = model.insuranceCompany
        self.insureProductDetailLabel.text = model.insuranceProduct
        self.policyNoDetailLabel.text = model.insuranceNo
        if let date = model.beginDate {
            self.startTimeDetailLabel.text = dateToString(date: date)
        }
        if let date = model.endDate {
            self.endTimeDetailLabel.text = dateToString(date: date)
        }
        
    }
}
