//
//  ProjectNormalCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/11/24.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class ProjectNormalCell: UITableViewCell {

    @IBOutlet weak var projectTitleLabel: UILabel!
    @IBOutlet weak var daySalaryLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var companyImageView: UIImageView!
    @IBOutlet weak var addressImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.companyImageView.image = UIImage(named: "Icon_公司")!.withRenderingMode(.alwaysTemplate)
        self.addressImageView.image = UIImage(named: "Icon_地址")!.withRenderingMode(.alwaysTemplate)
    }

    func bind(_ model: ProjectModel) {
        self.projectTitleLabel.text = model.projectName
        self.companyLabel.text = model.companyName
        self.addressLabel.text = model.address
        
        let daySalaryFloat:Float!
        let daySalaryStr:String?
        let daySalaryNewFloat:Float!
        let daySalaryNewStr:String?
        if model.daySalary != nil {
            daySalaryFloat = Float(model.daySalary!)
            daySalaryStr = String(format: "%0.2f", daySalaryFloat)
        }
        else {
            daySalaryStr = "0"
        }
        
        
        if model.daySalaryNew != nil {
            daySalaryNewFloat = Float(model.daySalaryNew!)
            daySalaryNewStr = String(format: "%0.2f", daySalaryNewFloat)
        }
        else {
            daySalaryNewStr = "0"
        }
        
        //self.daySalaryLabel.text = "工资:\(Int(daySalaryFloat))元/天"
        if model.workDayType == 2 {
            self.daySalaryLabel.text = "工资:" + daySalaryStr! + "元/天"
        }else{
            self.daySalaryLabel.text = "工资:" + daySalaryNewStr! + "元/工"
        }
    }
    
}
