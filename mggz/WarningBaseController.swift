//
//  WarningBaseController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/4.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class WarningBaseController: UIViewController {

    var warningView: MGWarningView = { //头部的警告条
        let view = MGWarningView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = UIColor(red: 1 , green: 1, blue: 235/255, alpha: 1)
        return view
    }()
    
    var baseView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.warningView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(doTapAction)))
        
        self.setupViews()
    }
    
    private func setupViews() {
        self.view.addSubview(warningView)
        self.warningView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view).offset(64)
            make.left.right.equalTo(self.view)
            make.height.equalTo(0.1)
        }
        
        self.view.addSubview(self.baseView)
        self.baseView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.top.equalTo(self.warningView.snp.bottom)
            make.bottom.equalTo(self.view).offset(0)
        }
        
    }
    
    func setupWarningContent( status:CertificationStatus) {
        switch status {
        case .uncertified:
//            let submitVC = CertificationSubmitController.init(.certificate)
//            submitVC.hidesBottomBarWhenPushed = true
//            submitVC.navigationItem.hidesBackButton = true
//        self.navigationController?.pushViewController(submitVC, animated: true)
            return
        case .beReturn:
            self.warningView.backgroundColor = UIColor(red: 1, green: 0.96, blue: 0.89, alpha: 1)
            self.warningView.setWarningText("您的资料审核未通过，请点此重新提交", textColor: UIColor(red: 0.96, green: 0.65, blue: 0.14, alpha: 1), completion: {
                self.warningView.snp.remakeConstraints { (make) in
                    make.top.equalTo(self.view).offset(64)
                    make.left.right.equalTo(self.view)
                    make.height.equalTo(self.warningView.contentHeight != nil ? self.warningView.contentHeight! + 20 : 0.1)
                }
            })
        case .certificationInProgress:
            self.warningView.backgroundColor = UIColor(red: 1, green: 0.96, blue: 0.89, alpha: 1)
            self.warningView.setWarningText("因您的资料尚在审核中，部分功能无法使用", textColor: UIColor(red: 0.96, green: 0.65, blue: 0.14, alpha: 1), completion: {
                self.warningView.snp.remakeConstraints { (make) in
                    make.top.equalTo(self.view).offset(64)
                    make.left.right.equalTo(self.view)
                    make.height.equalTo(self.warningView.contentHeight != nil ? self.warningView.contentHeight! + 20 : 0.1)
                }
            })
        default:
            self.warningView.snp.remakeConstraints { (make) in
                make.top.equalTo(self.view).offset(64)
                make.left.right.equalTo(self.view)
                make.height.equalTo(0.1)
            }
        }

    }
    
    func doTapAction() {
        WWUser.sharedInstance.ensureCertificationStatus { (status) in
            if status == .beReturn {
                let beReturn = HaveBeenReturnViewController(nibName: "HaveBeenReturnViewController", bundle: nil)
                beReturn.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(beReturn, animated: true)
            }
            if status == .certificationInProgress {
                let authenData = AuthenDataViewController(nibName: "AuthenDataViewController", bundle: nil)
                authenData.cTStatus = status
                authenData.navigationItem.hidesBackButton = false
                authenData.hidesBottomBarWhenPushed = true
           self.navigationController?.pushViewController(authenData, animated: true)
            }
        }
    }
}
