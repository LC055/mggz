//
//  HomeProjectCell.swift
//  mggz
//
//  Created by QinWei on 2018/3/13.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class HomeProjectCell: UITableViewCell {

    
    @IBOutlet weak var adressImage: UIImageView!
    
    @IBOutlet weak var noDataImage: UIImageView!
    
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var projectName: UILabel!
    
    @IBOutlet weak var adressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
