//
//  LaborInfoCell.swift
//  mggz
//
//  Created by QinWei on 2018/3/12.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LaborInfoCell: UITableViewCell {

    @IBOutlet weak var companyName: UILabel!
    
    @IBOutlet weak var agreedWages: UILabel!
    
    @IBOutlet weak var settlementCycle: UILabel!
    
    @IBOutlet weak var workSort: UILabel!
    
    @IBOutlet weak var workTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
