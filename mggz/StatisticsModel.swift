//
//  StatisticsModel.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/24.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Alamofire

class StatisticsModel: NSObject {

}


// 出工统计
class StatisticsWorkingModel {
    var workingDays = "0"    //总出工天数
    var workingHours = "0"   //总出工工时
    var workingDaysNotPayed = "0"//未结算天数
    var workingHoursNotPayed = "0" //未结算工时数
    var payedSalaryAmount = "0" //已结算工资 ,1000,00.00格式
    var workedDays = "0" //已经工作工数
    
    
    
    required init(jsonData: [String: Any]) {
        if let workingDays = jsonData["WorkingDays"] {
            self.workingDays = "\(workingDays)"
        }
        if let workedDays = jsonData["WorkedDays"] {
            self.workedDays = "\(workedDays)"
        }
        if let workingHours = jsonData["WorkingHours"] as? Double {
            self.workingHours = String(format: "%0.2f", workingHours)
        }
        if let daysNotPayed = jsonData["WorkingDaysNotPayed"] {
            self.workingDaysNotPayed = "\(daysNotPayed)"
        }
        
        if let workingHoursNotPayed = jsonData["WorkingHoursNotPayed"] as? Double{
            self.workingHoursNotPayed = String(format: "%0.2f", workingHoursNotPayed)
        }
        
        if let payedSalaryAmount = jsonData["PayedSalaryAmount"] as? Float{
//            let amountNumber = NSNumber.init(value: payedSalaryAmount)
            let tempString = "\(payedSalaryAmount)"
            self.payedSalaryAmount = Tool.numberToMoney(tempString)!
        }
    }
    
    //获取民工的全部出工情况
    class func getStatisticsWorkingTotal(demandBaseMigrantWorkerID: String, completionHandler: @escaping (WWValueResponse<StatisticsWorkingModel>)-> Void) {
        let method = "MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWokerSignedStatisticsView.GetSignedStatisticsTotalList"
        getStatistiscsWorking(demandBaseMigrantWorkerID: demandBaseMigrantWorkerID, startTime: nil, lastTime: nil, method: method, completionHandler: completionHandler)
    }
    
    //获取民工某一月的出工情况
    class func getStatisticsWorkingAMonth(demandBaseMigrantWorkerID: String,startTime: String?, lastTime: String?, completionHandler: @escaping (WWValueResponse<StatisticsWorkingModel>)-> Void) {
        let method = "MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWokerSignedStatisticsView.GetSignedStatisticsMonthTotalList"
        getStatistiscsWorking(demandBaseMigrantWorkerID: demandBaseMigrantWorkerID, startTime: startTime, lastTime: lastTime, method: method, completionHandler: completionHandler)
    }
    
    
    private class func getStatistiscsWorking(demandBaseMigrantWorkerID: String, startTime: String?, lastTime: String?, method: String,completionHandler: @escaping (WWValueResponse<StatisticsWorkingModel>)-> Void) {
        let url = JsonAPIURL + "CallMethod"
        guard let httpHeader = mobile_cilent_headers() else {
            return
        }
        
        let workId = demandBaseMigrantWorkerID
        var params = [String: String]()
        if startTime == nil && lastTime == nil {
           params = ["Method": method, "DemandBaseMigrantWorkerID": workId]
        }
        else {
            params = ["Method": method, "DemandBaseMigrantWorkerID": workId, "StartDate": startTime!, "LastDate": lastTime!]
        }
        
        Alamofire.request(url, method: .post, parameters: params, headers: httpHeader).responseJSON { (response) in
            if response.result.isSuccess {
                if let value = response.result.value as? [[String: Any]]{
                    if value.count <= 0 {
                    completionHandler(WWValueResponse.init(success: true, message: "没有数据"))
                    }
                    else {
                        
                        let dic = value[0]
                        
                        let model = StatisticsWorkingModel(jsonData: dic)
                    completionHandler(WWValueResponse.init(value: model, success: true))
                    }
                    return
                }
                
                completionHandler(WWValueResponse.init(success: false, message: "数据格式错误"))
                
            }
            else {
                
                completionHandler(WWValueResponse.init(success: false, message: "网络错误"))
            }
        }
    }
}
