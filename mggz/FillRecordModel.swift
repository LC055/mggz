//
//  FillRecordModel.swift
//  mggz
//
//  Created by QinWei on 2018/3/20.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class FillRecordModel: Mappable {
    var WorkDayType : Int?
    var WorkDays : Float?
    var WorkHours : Float?
    var SupplementReason : String?
    var SignedDate : String?
    var OrderNo : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        WorkDayType       <- map["WorkDayType"]
        WorkDays       <- map["WorkDays"]
        WorkHours       <- map["WorkHours"]
        SupplementReason       <- map["SupplementReason"]
        SignedDate       <- map["SignedDate"]
        OrderNo       <- map["OrderNo"]
    }
    
}
