//
//  InsureModel.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/30.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Alamofire

class InsureModel: NSObject {
    var insuranceProduct: String? //保险产品
    var isValide = false //状态
    var insuranceNo: String? //保险单号
    var insuranceCompany: String? //保险公司
    var beginDate: Date? //投保时间
    var endDate: Date? //截止时间
    
    required init(_ jsonData: [String: AnyObject]) {
        if let insuranceProduct = jsonData["InsuranceProduct"] {
            self .insuranceProduct = insuranceProduct as? String
        }
        if let isValide = jsonData["IsValide"] as? NSNumber {
            if isValide == NSNumber.init(value: 1) {
                self.isValide = true
            }
            else {
                self.isValide = false
            }
        }
        
        if let insuranceNo = jsonData["InsuranceNo"] {
            self.insuranceNo = insuranceNo as? String
        }
        if let companyDic = jsonData["InsuranceCompany"] as? [String: AnyObject] {
            if let companyname = companyDic["CompanyName"] {
                self.insuranceCompany = companyname as? String
            }
        }
        if let beginDate = jsonData["BeginDate"] as? String {
            let date = dateStringWithTToDate(dateString: beginDate)
            self.beginDate = date
        }
        if let endData = jsonData["EndDate"] as? String {
            let date = dateStringWithTToDate(dateString: endData)
            self.endDate = date
        }
    }

    class func getInsure(completionHandler:@escaping (WWValueResponse<[InsureModel]>) -> Void) {
        let url = JsonAPIURL + "CallMethod"
        guard let httpHeader = mobile_cilent_headers() else {
            return
        }
        guard let model = WWUser.sharedInstance.userModel, let accountId = model.accountId,let companyId = model.companyId  else {
            return
        }
        
        let method = "KouFine.Handler.Core.AjaxOperate.FindPagingEntityData"
        let domain = "民工管理"
        let typeFullName = "MarTian.MigrantWorkerManage.Entity.MigrantWorkerInsurance"
        let orderManagerConfigId = "5ab42e4a-5902-6ce9-55c5-e1c3b7024517"
        let isQueryControl = "false"
        let sql = "CompanyMGID==@CompanyMGID"
        let pageName = "KouFine.Container.List"
        let controlName = ""
        let start = "0"
        let limit = "25"
        
        let item = ["Name":"@CompanyMGID", "Type": "Guid", "Value":companyId]
        let sqlParam = ["Items":[item]]
        let sqlParamString = getJSONStringFromDictionary(dictionary: sqlParam as NSDictionary)
        
        let params = ["Method":method, "domain":domain, "typeFullName": typeFullName, "orderManagerConfigId":orderManagerConfigId, "isQueryControl": isQueryControl, "sql":sql, "sqlParam":sqlParamString, "pageName":pageName, "controlName":controlName, "AccountId":accountId, "start": start, "limit": limit]
        Alamofire.request(url, method: .post, parameters: params, headers: httpHeader).responseJSON { (response) in
            if response.result.isSuccess {
                if let value = response.result.value as? [String: AnyObject] {
                    if let items = value["Items"] as? [[String: AnyObject]] {
                        var modelArr = [InsureModel]()
                        for item in items {
                            let model = InsureModel.init(item)
                            modelArr.append(model)
                        }
                        completionHandler(WWValueResponse.init(value: modelArr, success: true))
                        
                        return
                    }
                }
                completionHandler(WWValueResponse.init(success: false, message: "数据解析错误"))
            }
            else {
                
                completionHandler(WWValueResponse.init(success: false, message: "网络访问错误"))
            }
        }
    }
}
