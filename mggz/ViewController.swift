//
//  ViewController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/9.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    
    fileprivate var _tableView: UITableView!
    fileprivate var tableview: UITableView {
        get {
            if _tableView != nil {
                return _tableView
            }
            _tableView = UITableView()
            _tableView.delegate = self
            _tableView.dataSource = self
            _tableView.tableFooterView = UIView()
            _tableView.estimatedRowHeight = 44
            _tableView.rowHeight = UITableViewAutomaticDimension
            
            _tableView.register(UINib.init(nibName: "ProjectNormalCell", bundle: nil), forCellReuseIdentifier: "cell")
            
            return _tableView
        }
    }
    
    var contents = [[String: Any]]()
    var search:BMKGeoCodeSearch?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "主界面"
        self.view.backgroundColor = UIColor.white
        
        self.search = BMKGeoCodeSearch()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "点击", style: .done, target: self, action: #selector(doRightAction))
    self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font:UIFont.systemFont(ofSize: 15)], for: .normal)
        self.view.addSubview(self.tableview)
        self.tableview.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
    
    @objc func doRightAction() {
        
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 50
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProjectNormalCell

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

