//
//  SignHomeInfoModel.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/9.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class SignHomeInfoModel: Mappable {
    var WorkState : Int?
    var WorkerCount : Int?
    var WorkHours : String?
    var WorkDays : String?
    var OverworkTime : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        WorkState       <- map["WorkState"]
        WorkerCount       <- map["WorkerCount"]
        WorkHours       <- map["WorkHours"]
        WorkDays       <- map["WorkDays"]
        OverworkTime       <- map["OverworkTime"]
    }
}
