//
//  RecordSupplyListViewController.swift
//  mggz
//
//  Created by QinWei on 2018/2/28.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class RecordSupplyListViewController: UIViewController {
    
    
    @IBOutlet weak var companyView: UIView!
    
    @IBOutlet weak var companyLabel: UILabel!
    //var projectModel: ProjectModel?
    //var selectDate: Date?
    var companyID:String?
    var companyName:String?
    @IBOutlet weak var recordTitle: UILabel!
    
    @IBOutlet weak var trackTitle: UILabel!
    
    @IBOutlet weak var recordButton: UIView!
    
    @IBOutlet weak var trackButton: UIView!

    fileprivate func setupRecordAndTrackTarget(){
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(recordButtonClick(_:)))
        self.recordButton.addGestureRecognizer(tap1)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(trackButtonClick(_:)))
        self.trackButton.addGestureRecognizer(tap2)
    }
    @objc fileprivate func recordButtonClick(_ tap:UITapGestureRecognizer){
        UIView.animate(withDuration: 0.2) {
            self.scrollView.contentOffset = CGPoint(x: 0, y: self.scrollView.contentOffset.y)
        self.recordTitle.textColor = UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1)
        self.trackTitle.textColor = UIColor.lightGray
        self.showView.frame = CGRect(x: 0, y: self.showView.frame.origin.y, width: self.showView.frame.size.width, height: self.showView.frame.size.height)
        }
    }
    @objc fileprivate func trackButtonClick(_ tap:UITapGestureRecognizer){
        
        UIView.animate(withDuration: 0.2) {
            self.scrollView.contentOffset = CGPoint(x: SCREEN_WIDTH, y: self.scrollView.contentOffset.y)
            self.recordTitle.textColor = UIColor.lightGray
            self.trackTitle.textColor = UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1)
            
            self.showView.frame = CGRect(x: SCREEN_WIDTH/2, y: self.showView.frame.origin.y, width: self.showView.frame.size.width, height: self.showView.frame.size.height)
            
        }
    }
    fileprivate lazy var showView : UIView = {
        let showView = UIView(frame: CGRect(x: 0, y: 149, width: SCREEN_WIDTH/2, height: 2))
        showView.backgroundColor = UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1)
        return showView
    }()
    fileprivate lazy var scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = UIColor.white
        scrollView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.scrollsToTop = false
        scrollView.bounces = false
        scrollView.isScrollEnabled = false
        scrollView.contentSize = CGSize(width: 2*SCREEN_WIDTH, height: 0)
        return scrollView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "发放明细"
        self.companyLabel.text = self.companyName
        self.view.backgroundColor = UIColor.white
        //self.title = dateToString(date: selectDate!)
        self.recordTitle.textColor = UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1)
        self.trackTitle.textColor = UIColor.lightGray
        self.setupRecordAndTrackTarget()
        self.view.addSubview(scrollView)
        self.setupChildController()
        self.addChildVcIntoScrollView()
        self.view.bringSubview(toFront: recordButton)
        self.view.bringSubview(toFront: trackButton)
        self.view.addSubview(showView)
        self.view.bringSubview(toFront: self.companyView)
    }
    
    private func setupChildController(){
//        let vc1 = RecordSupplyFormController()
//        vc1.projectModel = self.projectModel
//        vc1.selectDate = self.selectDate
//        vc1.title = "签到记录"
        
        let salaryFlow = SalaryFlowDetailConttroller()
        salaryFlow.companyID = self.companyID
        salaryFlow.companyName = self.companyName
        //salaryFlow.title = ""
        self.addChildViewController(salaryFlow)
        
        let vc2 = UnSettledViewController(nibName: "UnSettledViewController", bundle: nil)
        vc2.companyID = self.companyID
        vc2.companyName = self.companyName
        self.addChildViewController(vc2)
    }
    private func addChildVcIntoScrollView(){
        for i in 0..<2 {
            let childVc = self.childViewControllers[i]
            if childVc.isViewLoaded {
                return
            }
            let childVcView = childVc.view
            let scrollViewW = CGFloat(i) * SCREEN_WIDTH
            childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
            scrollView.addSubview(childVcView!)
        }
    }

}
// MARK:- 代理方法
extension RecordSupplyListViewController : UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

    }
}
