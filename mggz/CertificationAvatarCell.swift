//
//  CertificationAvatarCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/5.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

import TZImagePickerController
import MobileCoreServices

class CertificationAvatarCell: UITableViewCell {
    
    var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 45
        imageView.layer.masksToBounds = true
        
        let aImageView = UIImageView()
        aImageView.image = UIImage(named: "background_hel")
        aImageView.contentMode = .scaleAspectFit
        imageView.addSubview(aImageView)
        aImageView.snp.makeConstraints({ (make) in
            make.center.equalTo(imageView)
            make.width.height.equalTo(40)
        })
        
        
        let coverView = UIView()
        coverView.backgroundColor = colorWith255RGBA(0, g: 0, b: 0, a: 0.6)
        imageView.addSubview(coverView)
        coverView.snp.makeConstraints({ (make) in
            make.edges.equalTo(imageView)
        })
        
        let label = UILabel()
        label.text = "上传"
        label.textColor = UIColor.white
        label.alpha = 1
        label.font = wwFont_Regular(16)
        coverView.addSubview(label)
        label.snp.makeConstraints({ (make) in
            make.center.equalTo(imageView)
        })
        
        return imageView
    }()
    
    var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "请上传您的个人真实头像"
        label.font = wwFont_Regular(12)
        label.textColor = UIColor.mg_lightGray
        return label
    }()
    
    var photoSelectionHandler: ((UIImage) -> Void)?
    
    var visiblePhoto: UIImage? {
        willSet {
            if newValue != nil {
                self.avatarImageView.image = newValue
                
                if let handler = self.photoSelectionHandler {
                    handler(newValue!)
                }
                
                for view in self.avatarImageView.subviews {//移除上面的子视图
                    view.removeFromSuperview()
                }
            }
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        
        self.avatarImageView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(doTapAction)))
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
        self.contentView.addSubview(self.avatarImageView)
        self.avatarImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView).offset(10)
            make.bottom.equalTo(self.contentView).offset(-40)
            make.centerX.equalTo(self.contentView)
            make.width.equalTo(self.avatarImageView.snp.height)
        }
        
        self.contentView.addSubview(self.descriptionLabel)
        self.descriptionLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.avatarImageView)
            make.top.equalTo(self.avatarImageView.snp.bottom).offset(8)
        }
    }
    
    func doTapAction() {
        var responder = self.next
        while !(responder is UIViewController) {
            responder = responder?.next
        }
        if responder is UIViewController {
            let vc = responder as! UIViewController
            
            let tool = PhotoSheet()
            tool.showPhotoSheet(viewController: vc) { [unowned self](images) in
                if images != nil && images!.count > 0 {
                    self.visiblePhoto = images![0]
                }
            }
        }
    }
    
//    
//    func getCurrentController() -> CertificationSubmitController {
//        var vc: CertificationSubmitController!
//        var responder = self.next
//        while !(responder is UIViewController) {
//            responder = responder?.next
//        }
//        if responder is CertificationSubmitController {
//            vc = responder as! CertificationSubmitController
//        }
//        
//        return vc
//    }
    
}

