//
//  ETSimpleAlertController.swift
//  SwiftDemo2
//
//  Created by ShareAnimation on 2017/10/27.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class ETSimpleAlertController: UIViewController {
    
    fileprivate var coverView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        return view
    }()
    
    fileprivate var panelView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 1, height: 1)
        view.backgroundColor = UIColor.white
        return view
    }()
    
    fileprivate var messageLabel : UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont(name: "PingFangSC-Regular", size: 18)
        label.textAlignment = .center
        return label
    }()
    
    fileprivate var leftButton: UIButton = {
        let button = UIButton()
        button.setTitle("取消", for: .normal)
        button.titleLabel?.font = UIFont(name: "PingFangSC-Regular", size: 16)
        button.setTitleColor(UIColor.black, for: .normal)
        return button
    }()

    fileprivate var rightButton: UIButton = {
        let button = UIButton()
        button.setTitle("确认", for: .normal)
        button.titleLabel?.font = UIFont(name: "PingFangSC-Regular", size: 16)
        button.setTitleColor(UIColor.init(red: 16/255, green: 142/255, blue: 233/255, alpha: 1), for: .normal)
        return button
    }()
    
    private var okHandler: (()->Void)?
    private var message: String?
    
    //当前alert是否正在显示
    private var isShowing:Bool = false
    
    init (_ message: String, okHandler: @escaping ()-> Void){
        super.init(nibName: nil, bundle: nil)
        self.transitioningDelegate = self
        self.okHandler = okHandler
        self.message = message
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        
        self.leftButton.addTarget(self, action: #selector(doLeftButtonAction), for: .touchUpInside)
        self.rightButton.addTarget(self, action: #selector(doRightButtonAction), for: .touchUpInside)

        self.setupViews()
        self.updateViews()
        
        self.isShowing = true
    }
    
    deinit {
        self.isShowing = false
    }
    
    private func setupViews() {
        self.view.addSubview(self.coverView)
        self.coverView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        self.view.addSubview(self.panelView)
        self.panelView.snp.makeConstraints { (make) in
            make.left.equalTo(45)
            make.right.equalTo(-45)
            make.center.equalTo(self.view)
        }
        
        self.panelView.addSubview(self.messageLabel)
        self.messageLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.panelView).offset(10)
            make.top.equalTo(self.panelView).offset(20)
            make.right.equalTo(self.panelView).offset(-10)
        }
        
        let horizontalLine = UIView()
        horizontalLine.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        self.panelView.addSubview(horizontalLine)
        horizontalLine.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.panelView)
            make.top.equalTo(self.messageLabel.snp.bottom).offset(20)
            make.height.equalTo(1)
        }
        
        self.panelView.addSubview(self.leftButton)
        self.leftButton.snp.makeConstraints { (make) in
            make.left.bottom.equalTo(self.panelView)
            make.top.equalTo(horizontalLine.snp.bottom)
            make.height.equalTo(50)
        }
        
        let verticalLine = UIView()
        verticalLine.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        self.panelView.addSubview(verticalLine)
        verticalLine.snp.makeConstraints { (make) in
            make.top.equalTo(horizontalLine.snp.bottom)
            make.bottom.equalTo(self.panelView)
            make.width.equalTo(1)
            make.left.equalTo(self.leftButton.snp.right)
        }
        
        self.panelView.addSubview(self.rightButton)
        self.rightButton.snp.makeConstraints { (make) in
            make.right.bottom.equalTo(self.panelView)
            make.left.equalTo(verticalLine.snp.right)
            make.height.equalTo(self.leftButton)
            make.top.equalTo(horizontalLine.snp.bottom)
            make.width.equalTo(self.leftButton)
        }
    }
    
    private func updateViews() {
        self.messageLabel.text = self.message
    }
    
    @objc private func doLeftButtonAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func doRightButtonAction() {
        if self.okHandler != nil {
            self.okHandler!()
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension ETSimpleAlertController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SimpleAlertControllerTransitionPresent()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SimpleAlertControllerTransitionDismiss()
    }
}

class SimpleAlertControllerTransitionPresent: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromView = transitionContext.view(forKey: .from)
        let toVc = transitionContext.viewController(forKey: .to) as! ETSimpleAlertController
        let container = transitionContext.containerView
        container.addSubview(toVc.view)
        
        let panelView = toVc.panelView
        let coverview = toVc.coverView
        coverview.backgroundColor = UIColor.black.withAlphaComponent(0.01)
        panelView.alpha = 0.3
        panelView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration:0.1, animations: {
            coverview.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            panelView.transform = CGAffineTransform.identity
            panelView.alpha = 1
        }) { (finish) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            toVc.view.addSubview(fromView!)
            toVc.view.sendSubview(toBack: fromView!)
        }
    }
}

class SimpleAlertControllerTransitionDismiss: NSObject,UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromVc = transitionContext.viewController(forKey: .from) as! ETSimpleAlertController
        
//        let panelView = fromVc.panelView
        let coverview = fromVc.coverView
        UIView.animate(withDuration: 0.1, animations: {
            coverview.backgroundColor = UIColor.black.withAlphaComponent(0.01)
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}
