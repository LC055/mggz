//
//  CertificationReviewTextFieldCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/7.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit


class CertificationReviewTextFieldCell: UITableViewCell, UITextFieldDelegate {
    var cellImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = UIColor.mg_lightGray
        return imageView
    }()
    
    var cellTextField: UITextField = {
        let textField = UITextField()
        textField.textColor = UIColor.mg_lightGray
        textField.font = wwFont_Regular(18)
        
        return textField
    }()
    
    var separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.mg_backgroundGray
        return view
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        
        self.cellTextField.delegate = self
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
        self.contentView.addSubview(self.cellImageView)
        self.cellImageView.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(20)
            make.centerY.equalTo(self.contentView)
            make.width.height.equalTo(20)
        }
        
        self.contentView.addSubview(self.cellTextField)
        self.cellTextField.snp.makeConstraints { (make) in
            make.left.equalTo(self.cellImageView.snp.right).offset(10)
            make.top.right.equalTo(self.contentView)
        }
        
        self.contentView.addSubview(self.separatorLine)
        self.separatorLine.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.contentView)
            make.top.equalTo(self.cellTextField.snp.bottom)
            make.height.equalTo(1)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}
