//
//  UserCommonCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/7.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class UserCommonCell: UITableViewCell {
    
    var panel = UIView()
    
    var cellImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 3
        return imageView
    }()
    
    var itemNameLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Regular(16)
        return label
    }()
    
    var itemDetailLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Regular(14)
        label.textColor = UIColor.mg_lightGray
        label.textAlignment = .right
        return label
    }()
    
    var itemSeperatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.mg_backgroundGray
        return view
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
        self.contentView.addSubview(self.cellImageView)
        self.cellImageView.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(20)
            make.top.equalTo(self.contentView).offset(10)
            make.bottom.equalTo(self.contentView).offset(-10)
            make.width.equalTo(self.cellImageView.snp.height)
        }
        
        self.contentView.addSubview(self.itemNameLabel)
        self.itemNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.cellImageView.snp.right).offset(20)
            make.centerY.equalTo(self.cellImageView)
        }
        
        self.contentView.addSubview(itemDetailLabel)
        self.itemDetailLabel.snp.makeConstraints { (make) in
//            make.left.equalTo(self.itemDetailLabel.snp.right)
            make.right.equalTo(self.contentView)
            make.centerY.equalTo(self.cellImageView)
            make.width.equalTo(100)
        }
        
        self.contentView.addSubview(itemSeperatorLine)
        self.itemSeperatorLine.snp.makeConstraints { (make) in
            make.left.equalTo(self.cellImageView)
            make.right.equalTo(self)
            make.bottom.equalTo(self.contentView)
            make.height.equalTo(1)
        }
    }
    
}
