//
//  NotiTimeSelectView.swift
//  mggz
//
//  Created by QinWei on 2018/4/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class NotiTimeSelectView: UIView {

    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var makeSureButton: UIButton!
    var workSurePoints:String?
    var selectedMinitinues:((String) -> Void)? = nil
    var cancelClick:(() -> Void)? = nil
    let workData = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"]
    @IBAction func cancelButtonClick(_ sender: Any) {
        self.cancelClick!()
    }
    
    @IBAction func makeSureButtonClick(_ sender: Any) {
        
        self.selectedMinitinues!(self.workSurePoints!)
    }
    
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.workSurePoints = "6"
        self.setupSubviews()
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.selectRow(5, inComponent: 0, animated: true)
        self.pickerView.reloadAllComponents()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "NotiTimeSelectView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        return view
    }
}
extension NotiTimeSelectView:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return workData.count
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 130
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return workData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.workSurePoints = workData[row]
    }
}
