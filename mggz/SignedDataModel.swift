//
//  SignedDataModel.swift
//  mggz
//
//  Created by QinWei on 2018/5/3.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class SignedDataModel: Mappable{
    var AggregateEntity : String?
    var AggregateEntityID : String?
    var ID : String?
    var Latitude : String?
    var Longitude : String?
    var Operate : Float?
    var Order : Float?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        AggregateEntity       <- map["AggregateEntity"]
        AggregateEntityID       <- map["AggregateEntityID"]
        ID       <- map["ID"]
        Latitude       <- map["Latitude"]
        Longitude       <- map["Longitude"]
        Operate       <- map["Operate"]
        Order       <- map["Order"]
    }
}








