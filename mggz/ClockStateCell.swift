//
//  ClockStateCell.swift
//  mggz
//
//  Created by QinWei on 2018/5/7.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import TXScrollLabelView
class ClockStateCell: UITableViewCell {
    fileprivate lazy var caveView:UIView = {
        let view = UIView.init()
        view.frame = CGRect(x: 75, y: 15, width: 11, height: 11)
        view.isHidden = true
        return view
    }()
    fileprivate lazy var caveLayer:CALayer = {
        let between : CGFloat = 1.0
        let radius = (12 - 2 * between)/3
        let shapeLayer = CAShapeLayer.init()
        shapeLayer.frame = CGRect(x: 0, y: (12-radius)/2, width: radius, height: radius)
        shapeLayer.path = UIBezierPath.init(ovalIn: CGRect(x: 0, y: 0, width: radius, height: radius)).cgPath
        shapeLayer.fillColor = UIColor.init(red: 0.40, green: 0.40, blue: 0.40, alpha: 0.40).cgColor
    shapeLayer.add(YUReplicatorAnimation.scaleAnimation1(), forKey: "scaleAnimation")
        let replicatorLayer = CAReplicatorLayer.init()
        replicatorLayer.frame = CGRect(x: 0, y: 0, width: 12, height: 12)
        replicatorLayer.instanceDelay = 0.4
        replicatorLayer.instanceCount = 3
        replicatorLayer.instanceTransform = CATransform3DMakeTranslation(between * 2 + radius, 0, 0)
        replicatorLayer.addSublayer(shapeLayer)
        return replicatorLayer
    }()
    fileprivate lazy var foreverLayer : CABasicAnimation = {
        let animation = CABasicAnimation.init(keyPath: "opacity")
        animation.fromValue = NSNumber.init(value: 1.0)
        animation.toValue = NSNumber.init(value: 0.3)
        animation.autoreverses = true
        animation.duration = 0.4
        animation.repeatCount = HUGE
        animation.fillMode = kCAFillModeForwards
        animation.isRemovedOnCompletion = false
        animation.timingFunction = CAMediaTimingFunction.init(name: kCAMediaTimingFunctionEaseIn)
        return animation
    }()
    var mwWorkState : String = "z"{
        willSet {
            if newValue == "0"{
                self.caveView.isHidden = true
                self.stateLabel.isHidden = false
               self.stateImage.isHidden = false
                self.stateLabel.text = "未签到"
                self.stateImage.image = UIImage(named: "Oval 12")
            self.stateImage.layer.add(self.foreverLayer, forKey: nil)
            }else if newValue == "1"{
                self.stateLabel.isHidden = false
                self.caveView.isHidden = false
                self.caveView.layer.addSublayer(caveLayer)
                self.stateImage.isHidden = false
                self.stateLabel.text = "务工中"
                self.stateImage.image = UIImage(named: "Oval 22")
            }else if newValue == "2"{
                self.caveView.isHidden = true
                self.stateLabel.isHidden = false
                self.stateImage.isHidden = false
                self.stateLabel.text = "暂离"
                self.stateImage.image = UIImage(named: "Oval 2")
            }else if newValue == "3"{
                
                self.caveView.isHidden = true
                self.stateLabel.isHidden = false
                self.stateImage.isHidden = false
                self.stateLabel.text = "已下班"
                self.stateImage.image = UIImage(named: "Oval23")
                
            }else if newValue == "4"{
                self.caveView.isHidden = true
                self.stateLabel.isHidden = false
                self.stateImage.isHidden = false
                self.stateLabel.text = "已下班(未考勤)"
                self.stateImage.image = UIImage(named: "Oval23")
            }else if newValue == "5"{
                self.stateLabel.isHidden = false
                self.caveView.isHidden = false
                self.caveView.layer.addSublayer(caveLayer)
                self.stateImage.isHidden = false
                self.stateLabel.text = "加班中"
                self.stateImage.image = UIImage(named: "Oval 22")
                
            }else if newValue == "6"{
                self.caveView.isHidden = true
                self.stateLabel.isHidden = false
                self.stateImage.isHidden = false
                self.stateLabel.text = "已下班(未记工)"
                self.stateImage.image = UIImage(named: "Oval 2")
            }else if newValue == "z"{
                self.caveView.isHidden = true
                self.stateLabel.isHidden = true
                self.stateImage.isHidden = true
            }
        }
    }
    @IBOutlet weak var stateImage: UIImageView!
    
    @IBOutlet weak var stateLabel: UILabel!
    
    var goToRecord:(() -> Void)? = nil
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var workmateLabel: UILabel!
    
    @IBOutlet weak var hoursLabel: UILabel!
    lazy var notiLabel:TXScrollLabelView = {
        let label = TXScrollLabelView.scroll(withTitle: "苹果是一家“了不起的”公司,在科技界的盈利能力无人能敌。", type: TXScrollLabelViewType.leftRight, velocity: 2, options: .curveEaseInOut)
        label?.scrollLabelViewDelegate = self
        label?.frame = CGRect(x: 20, y: 62, width: SCREEN_WIDTH-36, height: 18)
        label?.backgroundColor = UIColor.white
        label?.scrollTitleColor = UIColor.black
        label?.font = UIFont.systemFont(ofSize: 14)
        return label!
    }()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.contentView.addSubview(self.caveView)
        self.contentView.addSubview(self.notiLabel)
        self.notiLabel.beginScrolling()
    }
    
    override func layoutSubviews() {
        
    }
    @objc fileprivate func topTapClick(_ tap:UITapGestureRecognizer){
        self.goToRecord!()
    }
}
extension ClockStateCell:TXScrollLabelViewDelegate{
    func scrollLabelView(_ scrollLabelView: TXScrollLabelView!, didClickWithText text: String!, at index: Int) {
        
    }
}
