//
//  RecordSupplyListController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/22.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class RecordSupplyListController: BaseViewController {
    fileprivate lazy var noDataImage:UIImageView = {
        let noDataImage = UIImageView.init()
        noDataImage.image = UIImage(named: "Icon_NoData")
        return noDataImage
    }()
    fileprivate var _tableView: UITableView!
    fileprivate var tableView: UITableView {
        get {
            if _tableView != nil {
                return _tableView
            }
            _tableView = UITableView()
            _tableView.delegate = self
            _tableView.dataSource = self
            _tableView.tableFooterView = UIView()
            _tableView.showsVerticalScrollIndicator = false
            _tableView.backgroundColor = UIColor.mg_backgroundGray
            
            _tableView.register(RecordSupplyListCell.self, forCellReuseIdentifier: "\(RecordSupplyListCell.self)")
            
            return _tableView
        }
    }
    
    var currentProject: ProjectModel?   //当前的项目模型
    var recordSupplyApples: [RecordSupplyApply]? //申请的补签记录

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "申请记录"
        self.view.addSubview(self.noDataImage)
        self.noDataImage.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.size.equalTo(CGSize(width: 90, height: 90))
        }
        self.view.backgroundColor = UIColor.white
        
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "点击", style: .done, target: self, action: #selector(doRightAction))
        
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
            make.top.equalTo(self.view).offset(45)
        }
        
        //self.showLoadingView()
        self.getData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        WWProgressHUD.dismiss()
    }

    func getData() {
        guard let model = self.currentProject, let marketSupplyBaseID = model.marketSupplyBaseID else {
            return
        }
        
        UserModel.getGuid { (response) in
            if response.success {
                let guid = response.value!
                RecordSupplyApply.getRecordSupplyApplies( guid: guid, marketSupplyBaseID: marketSupplyBaseID){(response) in
                    if response.success {
                        //self.hideLoadingView()
                        self.recordSupplyApples = response.value
                        
                        self.tableView.reloadData()
                    }
                    else {
                       
                        self.showErrorView(state: response.message)
                    }
                    
                }
            }
            else {
                
            }
        }
        
    }

    
    func doRightAction(){
        
    }

}

extension RecordSupplyListController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = self.recordSupplyApples {
            if model.count > 0{
               self.tableView.isHidden = false
            }else{
                self.tableView.isHidden = true
            }
            return model.count
        }
        self.tableView.isHidden = true
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(RecordSupplyListCell.self)", for: indexPath) as! RecordSupplyListCell
        cell.accessoryType = .disclosureIndicator
        cell.bind(self.recordSupplyApples![indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = RecordSupplyDetailController()
        vc.recordApplyModel = self.recordSupplyApples![indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
