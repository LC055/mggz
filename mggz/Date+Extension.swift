//
//  Date+Extension.swift
//  SwiftDemo
//
//  Created by ShareAnimation on 2017/8/18.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

//Date对象存放的日期始终是UTC的标准时间，然后我需要的是GMT背景时间，这里相差8小时
//一般我们使用DateFormatter转为字符串的时候都会自动转为本地时区，所以不需要特殊处理
extension Date {
    
    //学习地址：https://stackoverflow.com/questions/33605816/first-and-last-day-of-the-current-month-in-swift
    
    //获取月份的第一天
    func startOfMonth() -> Date {
        let firstDayComponents = Calendar.current.dateComponents([.year, .month], from: self)
        let firstDay = Calendar.current.date(from: firstDayComponents)!

        return firstDay
    }
    
    //获取月份的最后一天
    func endOfMonth() -> Date {
        let lastDayComponents = DateComponents(month: 1, day: -1)
        let lastDay = Calendar.current.date(byAdding: lastDayComponents, to: self.startOfMonth())!
        return lastDay
    }
}
