//
//  MutipleAccountModel.swift
//  mggz
//
//  Created by QinWei on 2018/3/16.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class MutipleAccountModel: Mappable {
    var LealPersonIDCard : String?
    var UserID : String?
    var Name : String?
    var Portrait : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        LealPersonIDCard       <- map["LealPersonIDCard"]
        UserID       <- map["UserID"]
        Name       <- map["Name"]
        Portrait       <- map["Portrait"]
    }
    
}
