//
//  OvertimeStateCell.swift
//  mggz
//
//  Created by QinWei on 2018/4/17.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class OvertimeStateCell: UITableViewCell {
    
    //未完结状态的
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var smallWorkLabel: UILabel!
    @IBOutlet weak var topStateLabel: UILabel!
    @IBOutlet weak var overtimeLabel: UILabel!
    
    @IBOutlet weak var startTitle: UILabel!
    
    //已完结的
    @IBOutlet weak var overtimeFream: UILabel!
    @IBOutlet weak var workPointsLabel: UILabel!
    @IBOutlet weak var bottomStateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func setCellDataWithModel(_ model:OvertimeDetailModel){
        self.startLabel.isHidden = false
        self.smallWorkLabel.isHidden = false
        self.topStateLabel.isHidden = false
        self.overtimeLabel.isHidden = false
        self.startTitle.isHidden = false
        
        self.overtimeFream.isHidden = true
        self.workPointsLabel.isHidden = true
        self.bottomStateLabel.isHidden = true
        
        if (model.WorkBeginTime != nil){
            self.startLabel.text = self.getDateStrWithHHSS(dateStr: model.WorkBeginTime!)
        }
        if (model.ApplyWorkDays != nil){
            self.smallWorkLabel.text = String(model.ApplyWorkDays!) + "工"
        }
        if model.ApplyState == 0{
            self.topStateLabel.text = "申请中"
        }else if model.ApplyState == 1{
            self.topStateLabel.text = "待加班"
        }else if model.ApplyState == 2{
            self.startLabel.isHidden = true
            self.smallWorkLabel.isHidden = true
            self.topStateLabel.isHidden = true
            self.overtimeLabel.isHidden = true
            self.startTitle.isHidden = true
            
            self.overtimeFream.isHidden = false
            self.workPointsLabel.isHidden = false
            self.bottomStateLabel.isHidden = false
            self.bottomStateLabel.text = "已加班"
            if (model.ApplyWorkDays != nil){
                self.workPointsLabel.text = "记" + String(model.ApplyWorkDays!) + "工"
            }
            
            if (model.WorkBeginTime != nil) && (model.WorkEndTime != nil){
                let startTime = self.getDateStrWithHHSS(dateStr: model.WorkBeginTime!)
                let endTime = self.getDateStrWithHHSS(dateStr: model.WorkEndTime!)
                self.overtimeFream.text = startTime! + "~" + endTime!
            }
        }else if model.ApplyState == 3{
            self.topStateLabel.text = "未加班"
            self.topStateLabel.textColor = UIColor.init(red: 0.97, green: 0.43, blue: 0.38, alpha: 1)
        }else if model.ApplyState == -1{
            self.topStateLabel.text = "失效"
        }else if model.ApplyState == -2{
            self.topStateLabel.text = "被退回"
            self.topStateLabel.textColor = UIColor.init(red: 0.97, green: 0.43, blue: 0.38, alpha: 1)
        }
        
    }
    fileprivate func getDateStrWithHHSS(dateStr:String) -> String?{
        let derange = dateStr.range(of: "T")
        let suffixStr = dateStr.suffix(from: (derange?.upperBound)!)
        let index2 = suffixStr.index(suffixStr.startIndex, offsetBy: 5)
        let resultStr = suffixStr.prefix(upTo: index2)
       
        return String(resultStr)
    }
    
}
