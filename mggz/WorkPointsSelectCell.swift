//
//  WorkPointsSelectCell.swift
//  mggz
//
//  Created by QinWei on 2018/3/1.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class WorkPointsSelectCell: UITableViewCell {
    
    @IBOutlet weak var workPoints: UILabel!
    
    @IBOutlet weak var pointsSelect: UIButton!
    
    @IBOutlet weak var postLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
