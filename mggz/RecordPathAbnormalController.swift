//
//  RecordPathAbnormalController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/21.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

//轨迹异常
import UIKit
import MJRefresh

class RecordPathAbnormalController: UIViewController {
    fileprivate var _tableView: UITableView!
    
    fileprivate lazy var noDataImage:UIImageView = {
        let noDataImage = UIImageView.init()
        noDataImage.image = UIImage(named: "Icon_NoData")
        return noDataImage
    }()
    fileprivate lazy var noDataLabel:UILabel = {
        let label = UILabel.init()
        label.text = "没有异常轨迹!"
        label.font = wwFont_Regular(15)
        label.textAlignment = .center
        label.textColor = UIColor(red: 0.80, green: 0.80, blue: 0.80, alpha: 1)
        return label
    }()
    
    fileprivate var tableView: UITableView {
        get {
            if _tableView != nil {
                return _tableView
            }
            _tableView = UITableView()
            _tableView.delegate = self
            _tableView.dataSource = self
            _tableView.separatorStyle = .none
            
            _tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            regClass(_tableView, cell: RecordTrack_ExceptionCell.self)

            return _tableView
        }
    }
    
    
    
    
    
    var currentDate: Date? //当前页面的日期时间
    var currentPage:Int = 0
    
    var recordTrackModels = [RecordTrack]() //获取的异常轨迹数据
    
    /***传入的变量***/
    var projectModel: ProjectModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.mg_backgroundGray
        self.view.addSubview(self.noDataImage)
        self.noDataImage.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.size.equalTo(CGSize(width: 90, height: 90))
        }
        self.view.addSubview(self.noDataLabel)
        self.noDataLabel.snp.makeConstraints { (make) in
        make.top.equalTo(self.noDataImage.snp.bottom).offset(5)
            make.size.equalTo(CGSize(width: 150, height: 21))
            make.centerX.equalTo(self.noDataImage)
        }
        // Do any additional setup after loading the view.
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(self.view)
            make.top.equalTo(self.view).offset(64)
        }
        
        let header = MJRefreshNormalHeader { 
            self.getData()
        }
        header?.stateLabel.isHidden = true
        self.tableView.mj_header = header
        
        let footer = MJRefreshAutoNormalFooter { 
            self.getNextPage()
        }
        self.tableView.mj_footer = footer
        
        self.tableView.mj_header.beginRefreshing()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         WWProgressHUD.dismiss()
    }
    func getData() {
        guard self.currentDate != nil else {
            return
        }
        self.currentPage = 0
        
        let currentDateString = dateToString(date: self.currentDate!)
        let startOfDay = currentDateString + " " + "00:00:00"
        let endOfDay = currentDateString + " " + "23:59:59.998"
        
        if let marketSupplyBaseID = self.projectModel!.marketSupplyBaseID {
            RecordTrack.getRecordTrack(marketSupplyBaseID: marketSupplyBaseID,start: self.currentPage, startOfDay: startOfDay, endOfDay: endOfDay){(response) in
                if response.success {
                    self.recordTrackModels = response.value!
                    
                    self.tableView.reloadData()
                }
                else {
                    WWError(response.message)
                }

                self.tableView.mj_header.endRefreshing()
            }
        }
        else {
            self.tableView.mj_header.endRefreshing()
        }
        
    }
    
    func getNextPage() {
        guard self.recordTrackModels.count >= 25 else {
        self.tableView.mj_footer.endRefreshingWithNoMoreData()
            
            return
        }
        self.currentPage += 25
        
        let currentDateString = dateToString(date: self.currentDate!)
        let startOfDay = currentDateString + " " + "00:00:00"
        let endOfDay = currentDateString + " " + "23:59:59.998"
        
        if let marketSupplyBaseID = self.projectModel!.marketSupplyBaseID {
            RecordTrack.getRecordTrack(marketSupplyBaseID: marketSupplyBaseID,start: self.currentPage, startOfDay: startOfDay, endOfDay: endOfDay){(response) in
                if response.success {
                    if response.value!.count <= 0 {
                        self.tableView.mj_footer.endRefreshingWithNoMoreData()
                        return
                    }
                    self.recordTrackModels += response.value!
                    
                    self.tableView.reloadData()
                    self.tableView.mj_footer.endRefreshing()
                }
                else {
                    WWError(response.message)
                }
            }
        }
        else {
            self.tableView.mj_footer.endRefreshing()
        }

        
    }
}

extension RecordPathAbnormalController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.recordTrackModels.count < 25{
            self.tableView.mj_footer.isHidden = true
        }
        if self.recordTrackModels.count > 0{
            self.tableView.isHidden = false
        }else{
           self.tableView.isHidden = true
        }
        return self.recordTrackModels.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = getCell(tableView, cell: RecordTrack_ExceptionCell.self, indexPath: indexPath)
        cell.bind(self.recordTrackModels[indexPath.row])
        if indexPath.row == 0 {
            cell.upLine.isHidden = true
        }
        else {
            cell.upLine.isHidden = false
        }
        if indexPath.row == self.recordTrackModels.count - 1 {
            cell.downLine.isHidden = true
        }
        else {
            cell.downLine.isHidden = false
        }
        
        return cell
    }
}
