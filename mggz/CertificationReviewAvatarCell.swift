//
//  CertificationReviewAvatarCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/7.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import TZImagePickerController
import MobileCoreServices

class CertificationReviewAvatarCell: UITableViewCell,TZImagePickerControllerDelegate {
    
    var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isUserInteractionEnabled = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 60
        imageView.layer.masksToBounds = true
        
        let aImageView = UIImageView()
        aImageView.image = UIImage(named: "background_hel")
        aImageView.contentMode = .scaleAspectFit
        imageView.addSubview(aImageView)
        aImageView.snp.makeConstraints({ (make) in
            make.center.equalTo(imageView)
            make.width.height.equalTo(40)
        })
        
        let coverView = UIView()
        coverView.backgroundColor = colorWith255RGBA(0, g: 0, b: 0, a: 0.6)
        imageView.addSubview(coverView)
        coverView.snp.makeConstraints({ (make) in
            make.edges.equalTo(imageView)
        })
        
        return imageView
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {

        self.contentView.addSubview(self.avatarImageView)
        self.avatarImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView).offset(10)
            make.bottom.equalTo(self.contentView).offset(-10)
            make.centerX.equalTo(self.contentView)
            make.width.equalTo(self.avatarImageView.snp.height)
        }
    }
    
    func bind(_ model: UserInfoModel) {
        if let url = model.photoUrl {
            for view in self.avatarImageView.subviews {
                view.removeFromSuperview()
            }
            self.avatarImageView.kf.setImage(with: URL.init(string: ImageUrlPrefix + url), placeholder: UIImage(named: "UserAvatar.JPG"), options: nil, progressBlock: nil, completionHandler: nil)
        }
    }
    
    
}
