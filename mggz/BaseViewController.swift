//
//  BaseViewController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/10.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    //这里沿用V2的方式使用weak，但是我不理解为什么要用weak
    fileprivate weak var _loadingView: V2LoadingView?
    fileprivate weak var _errorView: UIView?
    
    func showLoadingView() {
        self.hideLoadingView()
        
        let loadingView = V2LoadingView()
        loadingView.backgroundColor = self.view.backgroundColor
        self.view.addSubview(loadingView)
        loadingView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        self._loadingView = loadingView
    }
    
    func showErrorView(state: String) {
        self.hideLoadingView()
        
        let errorView = UIView()
        errorView.backgroundColor = self.view.backgroundColor
        self.view.addSubview(errorView)
        errorView.snp.makeConstraints({ (make) in
            make.edges.equalTo(self.view)
        })
        let errorImageView = UIImageView(image: UIImage.init(named: "错误提示"))
        errorImageView.contentMode = .scaleAspectFit
        errorView.addSubview(errorImageView)
        errorImageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(errorView)
            make.height.width.equalTo(40)
            make.centerY.equalTo(errorView).offset(-30)
        }
        let errorLabel = UILabel()
        errorLabel.text = state
        errorLabel.font = UIFont.systemFont(ofSize: 16)
        errorLabel.textColor = colorWith255RGB(173, g: 173, b: 173)
        errorView.addSubview(errorLabel)
        errorLabel.snp.makeConstraints { (make) in
            make.top.equalTo(errorImageView.snp.bottom)
            make.centerX.equalTo(errorView)
        }
        
        self._errorView = errorView
    }
    func hideLoadingView() {
        self._loadingView?.removeFromSuperview()
    }
    fileprivate func hideErrorView() {
        self._errorView?.removeFromSuperview()
    }

}
