//
//  RecordWorkpointsView3.swift
//  mggz
//
//  Created by QinWei on 2018/3/6.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class RecordWorkpointsView3: UIView {
    var myFrame: CGRect?
    
    @IBOutlet weak var recordLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var workPoints: UILabel!
    
    @IBOutlet weak var makeSureButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "RecordWorkpointsView3", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        return view
    }
}
