//
//  WWResponse.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/4.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class WWResponse: NSObject {

    var success: Bool = false
    var message: String = "no message"
    
    init(success: Bool, message: String?) {
        super.init()
        self.success = success
        if let message = message {
            self.message = message
        }
    }
    
    init(success: Bool) {
        super.init()
        self.success = success
    }
}

class WWValueResponse<T>: WWResponse {
    var value: T?
    
    convenience init(value: T, success: Bool) {
        self.init(success: success)
        self.value = value
    }
    
    convenience init(value: T, message: String, success: Bool) {
        self.init(success: success, message: message)
        self.value = value
    }
}

enum JsonType {
    case string
    case array
    case dictionary
}
class WWValueAndTypeResponse<T>: WWResponse {
    var instanceType: JsonType?
    var value:T?
    
    convenience init(value: T, type:JsonType, success: Bool) {
        self.init(success: success)
        self.value = value
        self.instanceType = type
    }
    
    convenience init(value: T, type:JsonType, success: Bool ,message: String) {
        self.init(success: success, message: message)
        self.value = value
        self.instanceType = type
    }
    
}
