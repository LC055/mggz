//
//  AccountBalanceCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/24.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class AccountBalanceCell: UITableViewCell {
    
    //cell图标
    fileprivate var balanceImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Icon_账户余额")!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = colorWith255RGB(255, g: 206, b: 61)
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    
    //标题
    fileprivate var balanceTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "账户余额"
        label.textColor = UIColor.black
        label.font = wwFont_Medium(18)
        
        return label
    }()
    
    //额度
    fileprivate var balanceDetailLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.textColor = UIColor.mg_lightGray
        label.text = "0元"
        label.font = wwFont_Medium(18)
        return label
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.contentView.addSubview(self.balanceImageView)
        self.balanceImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView).offset(10)
            make.left.equalTo(self.contentView).offset(15)
            make.bottom.equalTo(self.contentView).offset(-10)
            make.height.equalTo(self.balanceImageView.snp.width)
        }
        
        self.contentView.addSubview(self.balanceTitleLabel)
        self.balanceTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.balanceImageView.snp.right).offset(10)
            make.centerY.equalTo(self.balanceImageView)
            make.width.equalTo(80)
        }
        
        self.contentView.addSubview(self.balanceDetailLabel)
        self.balanceDetailLabel.snp.makeConstraints { (make) in
            make.right.equalTo(self.contentView).offset(-10)
            make.centerY.equalTo(self.balanceTitleLabel)
            make.left.equalTo(self.balanceTitleLabel.snp.right)
        }
    }

    func bind(_ number: NSNumber) {
        if let moneyString = Tool.numberToMoney("\(number)") {
            self.balanceDetailLabel.text = moneyString + "元"
        }
    }
}
