//
//  SignedPromptView.swift
//  mggz
//
//  Created by QinWei on 2018/5/3.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class SignedPromptView: UIView {

    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "SignedPromptView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        view.alpha = 0.7
        view.layer.cornerRadius = 6
        view.layer.masksToBounds = true
        return view
    }
}
