//
//  NewVesionModel.swift
//  mggz
//
//  Created by QinWei on 2018/2/6.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class NewVesionModel: Mappable{
    var AppPath : String?
    var AppType : Int?
    var ClientType : Int?
    var CreateTime : String?
    var CreateUser : String?
    var CreateUserID : String?
    var ID : String?
    var IsForce : Bool?
    var LastUpdateTime : String?
    var LastUpdateUser : String?
    var LastUpdateUserID : String?
    var TimeStamp : String?
    var VersionCode : String?
    var VersionNum : Int?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
        AppPath       <- map["AppPath"]
        AppType       <- map["AppType"]
        ClientType       <- map["ClientType"]
        CreateTime       <- map["CreateTime"]
        CreateUser       <- map["CreateUser"]
        CreateUserID       <- map["CreateUserID"]
        ID       <- map["ID"]
        IsForce       <- map["IsForce"]
        LastUpdateTime       <- map["LastUpdateTime"]
        LastUpdateUser       <- map["LastUpdateUser"]
        LastUpdateUserID       <- map["LastUpdateUserID"]
        TimeStamp       <- map["TimeStamp"]
        VersionCode       <- map["VersionCode"]
        VersionNum       <- map["VersionNum"]
        
    }
}
