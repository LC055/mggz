//
//  MyPrepaidModel.swift
//  mggz
//
//  Created by QinWei on 2018/3/27.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class MyPrepaidModel: Mappable {
    var PrepaidAmount : Double?
    var CreateTime : String?
    var ExpiryDate : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        PrepaidAmount       <- map["PrepaidAmount"]
        CreateTime       <- map["CreateTime"]
        ExpiryDate       <- map["ExpiryDate"]
        
    }
}
