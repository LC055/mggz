//
//  GenderChooseController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/5.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class GenderChooseController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    fileprivate var _tableView: UITableView!
    fileprivate var tableView: UITableView {
        get {
            if _tableView != nil {
                return _tableView
            }
            _tableView = UITableView()
            _tableView.delegate = self
            _tableView.dataSource = self
            _tableView.tableFooterView = UIView()
            _tableView.separatorStyle = .none
            _tableView.showsVerticalScrollIndicator = false
            _tableView.isScrollEnabled = false
            _tableView.backgroundColor = UIColor.mg_backgroundGray
            
            regClass(_tableView, cell: UITableViewCell.self)
            
            return _tableView
        }
    }

    var genders = ["男", "女"]
    var selectionHandler:((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "选择性别"

        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        WWProgressHUD.dismiss()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = getCell(tableView, cell: UITableViewCell.self, indexPath: indexPath)
        cell.textLabel?.text = genders[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let handler = self.selectionHandler {
            handler(genders[indexPath.row])
            self.navigationController?.popViewController(animated: true)
        }
    }
}
