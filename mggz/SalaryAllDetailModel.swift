//
//  SalaryAllDetailModel.swift
//  mggz
//
//  Created by QinWei on 2018/3/14.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class SalaryAllDetailModel: Mappable {
    var CompanyID : String?
    var CompanyName : String?
    var SalaryPayed : Double?
    var SalaryNotPayed : Double?
    
    
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        CompanyID       <- map["CompanyID"]
        CompanyName       <- map["CompanyName"]
        SalaryPayed       <- map["SalaryPayed"]
        SalaryNotPayed       <- map["SalaryNotPayed"]
    }
}
