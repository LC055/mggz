//
//  RegularTool.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/7.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class RegularTool: NSObject {

    //是否是手机号
    class func isValidatePhoneNum(_ number: String) -> Bool {
        if number.count != 11 {
            return false
        }
        let pattern = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$"
        let regex = try! NSRegularExpression(pattern: pattern, options: [])
        let result = regex.matches(in: number, options: [], range: NSRange.init(location: 0, length: number.count))
        
        if result.count > 0 {
            return true
        }
        
        return false
    }
}
