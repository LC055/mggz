//
//  MyPrepaidDetailViewController.swift
//  mggz
//
//  Created by QinWei on 2018/3/27.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import MJRefresh
import Moya
import ObjectMapper
class MyPrepaidDetailViewController: UIViewController {
    fileprivate var totalPage:Int?
    fileprivate var currentPage = 1
   fileprivate lazy var itemArray : [PrepaidDetailModel] = [PrepaidDetailModel]()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "预支记录"
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 10)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        self.tableView.register(UINib.init(nibName: "PrepaidDetailCell", bundle: nil), forCellReuseIdentifier: "detail")
        
        let tableFooter = MJRefreshAutoNormalFooter { [unowned self] in
            self.getNextPage()
        }
        self.tableView.mj_footer = tableFooter
        
        self.getData()
    }
    func getData() {
        
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
        self.currentPage = 1
        LCAPiSubManager.request(.GetSalaryPrepaidRecordDetail(AccountId: accountId, pageIndex: 1, pageSize: 25)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
                
                guard (jsonString["State"]) as! String == "OK" else{
                    return
                }
               
                
                guard  let resultDic : NSDictionary = jsonString["Result"] as? NSDictionary else{
                    return
                }
                guard  let resultArray : NSArray = resultDic["PrepaidDetailList"] as? NSArray else{
                    return
                }
                if let totalList = resultDic["Total"] as? Int {
                    self.totalPage = (totalList/25)+1
                }
                self.itemArray = Mapper<PrepaidDetailModel>().mapArray(JSONObject: resultArray)!
               
                self.tableView.reloadData()
                
            }
        }
    }
    
    func getNextPage() {
        guard (self.totalPage != nil) else {
            return
        }
        if self.currentPage >= self.totalPage! {
        self.tableView.mj_footer.endRefreshingWithNoMoreData()
            return
        }
        
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
        self.currentPage += 1
       
        LCAPiSubManager.request(.GetSalaryPrepaidRecordDetail(AccountId: accountId, pageIndex: self.currentPage, pageSize: 25)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
                
                guard (jsonString["State"]) as! String == "OK" else{
                    return
                }
               
                
                guard  let resultDic : NSDictionary = jsonString["Result"] as? NSDictionary else{
                    return
                }
                guard  let resultArray : NSArray = resultDic["PrepaidDetailList"] as? NSArray else{
                    return
                }
                var salaryArray = [PrepaidDetailModel].init()
                salaryArray = Mapper<PrepaidDetailModel>().mapArray(JSONObject: resultArray)!
                if salaryArray.count <= 0 {
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
                    return
                }
                self.itemArray += salaryArray
                self.tableView.reloadData()
                self.tableView.mj_footer.endRefreshing()
            }
        }
    }


}
extension MyPrepaidDetailViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.itemArray.count < 25{
           self.tableView.mj_footer.isHidden = true
        }else{
            self.tableView.mj_footer.isHidden = false
        }
        return self.itemArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PrepaidDetailCell = tableView.dequeueReusableCell(withIdentifier: "detail") as! PrepaidDetailCell
    cell.setupCellDataWithModel(self.itemArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
