//
//  TrackService.swift
//  mggz
//
//  Created by ShareAnimation on 2017/10/10.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

//弃用
import UIKit

//class TrackService: NSObject {
//    static let sharedInstance = TrackService()
//
//
//    /// 服务是否启动。true表示启动
//    private(set) var isActive: Bool = false
//
//    fileprivate override init() {
//        super.init()
//
//        self.locationService.allowsBackgroundLocationUpdates = true //允许后天定位
//        self.locationService.pausesLocationUpdatesAutomatically = false//不允许系统停止定位，否则20分后就会停止
//        let locationManger = self.locationService.value(forKey: "locationManager") as? CLLocationManager
//        if locationManger != nil {
//            locationManger?.requestAlwaysAuthorization()
//        }
//    }
//
//    //给轨迹上传服务匹配项目。切换项目的时候要重新匹配
//    func config(_ recordPolicy: RecordPolicy, demandBaseMigrantWorkerId: String) {
//        self.recordPolicyModel = recordPolicy
//        self.demandBaseMigrantWorkerId = demandBaseMigrantWorkerId
//    }
//
//    fileprivate var recordPolicyModel: RecordPolicy!
//    fileprivate var demandBaseMigrantWorkerId : String!
//
//    //连点是否在指定范围内
//    var twoPointWithinTheSpecifiedRange:((Bool) -> Void)?
//
//    /***本地变量***/
//    fileprivate let locationService = BMKLocationService()
//    fileprivate var lastDate:Date! //上一次上传轨迹的时间点
//
//    func start() {
//
//        locationService.delegate = self
//        // self.isActive==true，表示现在就是start的状态。否者现在是stop的状态
//        if self.isActive != true {
//             self.lastDate = Date()
//            self.isActive = true
//            self.startLocation()
//            print("TrackService开始启动")
//        }
//        else {
//            print("TrackService已经启动")
//        }
//    }
//
//    func stop() {
////        locationService.delegate = nil
//        self.isActive = false
//    }
//
//    func startLocation() {
//        self.locationService.stopUserLocationService()
//        self.locationService.startUserLocationService()
//    }
//
//    func isTimePoint() -> Bool {
//        let currentInterval = fabs(Date().timeIntervalSince(self.lastDate!))
//
//        var preferredInterval : TimeInterval!
//        if let rate = recordPolicyModel.locationRate, let rateInt = Int(rate) {
//            preferredInterval = TimeInterval.init(rateInt * 60)
//        }
//        else {
//            preferredInterval = TimeInterval.init(15 * 60)
//        }
//
////        //测试，默认5分钟上传一次
////        preferredInterval = 10
//
//        if currentInterval >= preferredInterval {
//            self.lastDate = Date()
//            return true
//        }
//        return false
//    }
//
//    func trackDataForTest() {
//    }
//
//    func uploadRecordTrack(_ longitude: String, latitude: String, isException: Bool) {
//        var guid: String?
//
//        let dispatchGroup = DispatchGroup()
//
//        dispatchGroup.enter()
//        UserModel.getGuid { (response) in
//            if response.success {
//                guid = response.value
//            }
//            else {
//                WWError(response.message)
//            }
//            dispatchGroup.leave()
//        }
//
//        dispatchGroup.notify(queue: DispatchQueue.main) {
//            guard guid != nil else {
//                return
//            }
//            RecordTrack.uploadRecordTrack(guid!, demandBaseMigrantWorkerID: self.demandBaseMigrantWorkerId, isException: isException, longitude: longitude, latitude: latitude){(response) in
//                if response.success {
//                    print("轨迹上传成功")
//                }
//                else {
//                    print("轨迹上传失败")
//                }
//            }
//        }
//
//    }
//}
//
//extension TrackService: BMKLocationServiceDelegate {
//
//
//    func didUpdate(_ userLocation: BMKUserLocation!) {
//        //测试
////TrackServiceTest.sharedInstance.configLocationInfo(userLocation.location.coordinate.latitude, long: userLocation.location.coordinate.longitude)
//
//        let lon = userLocation.location.coordinate.longitude
//        let lat = userLocation.location.coordinate.latitude
//
//        //计算两点距离
//        if let recordLat = self.recordPolicyModel.latitude, let recordLon = self.recordPolicyModel.longitude, let signedRange = self.recordPolicyModel.signedRange {
//            if let recordLatDouble = Double(recordLat), let recordLonDouble = Double(recordLon){
//                let recordCoor = CLLocationCoordinate2D(latitude: recordLatDouble, longitude: recordLonDouble)
//
//                let point1 = BMKMapPointForCoordinate(userLocation.location.coordinate)
//                let point2 = BMKMapPointForCoordinate(recordCoor)
//                let distance = BMKMetersBetweenMapPoints(point1, point2)
//
//                //两点范围在指定范围内，则true，否则false
//                let inRange = Double(signedRange)! - distance > 0
//                if let closure = self.twoPointWithinTheSpecifiedRange {
//                    closure(inRange)
//                }
//
//                //定位是一致开着的，但是轨迹上传是根据策略来的
//                if isTimePoint() {
//                    self.uploadRecordTrack("\(lon)", latitude: "\(lat)",isException: !inRange)
//                }
//
//            }
//
//        }
//    }
//}

