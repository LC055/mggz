//
//  WithDrawDetailCell.swift
//  mggz
//
//  Created by QinWei on 2018/3/16.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class WithDrawDetailCell: UITableViewCell {

    var moneyLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Semibold(17)
        label.text = "+10,000.00"
        
        return label
    }()
    
    var dateAndTimeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.text = "2017-8-30 12:10:10"
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(14)
        return label
    }()
    
    //资金流动细节
    var moneyFlowDetailLabel: UILabel = {
        let label = UILabel()
        label.text = "利息收入"
       // label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Medium(17)
        label.numberOfLines = 0
        label.lineBreakMode = .byCharWrapping
        
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
        self.contentView.addSubview(self.moneyFlowDetailLabel)
        self.moneyFlowDetailLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView).offset(5)
            make.left.equalTo(self.contentView).offset(20)
            make.right.equalTo(self.contentView).offset(-15)
        }
        
        self.contentView.addSubview(self.dateAndTimeLabel)
        self.dateAndTimeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(20)
            make.bottom.equalTo(self.contentView).offset(-10)
            make.height.equalTo(20)
        }
        
        self.contentView.addSubview(self.moneyLabel)
        self.moneyLabel.snp.makeConstraints { (make) in
            make.height.equalTo(20)
          make.right.equalTo(self.contentView).offset(-15)
            make.centerY.equalTo(self.dateAndTimeLabel)
            
        }
    }
    
    func bind(_ model: SalaryFlowModel) {
        
        if let applyAmount = model.applyAmount {
            let tempString = "\(applyAmount)"
            if let format = Tool.numberToMoney(tempString) {
                if format.hasPrefix("-"){
                    self.moneyLabel.text = format
                }else{
                    self.moneyLabel.text = "+" + format
                }
            }
        }
        
        
        if let date = model.createTime {
            let dateString = dateTimeToString(date: date)
            self.dateAndTimeLabel.text = dateString
        }
        
        if model.moneyFlowDetail == "提现"{
            self.moneyFlowDetailLabel.textColor = UIColor.init(white: 155/255.0, alpha: 1)
            let detailIntro = model.moneyFlowDetail! + "(" + model.salaryMonths! + ")"
            let detailIntroAttri = NSMutableAttributedString.init(string: detailIntro)
        detailIntroAttri.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSRange(location: 0, length: 2))
            self.moneyFlowDetailLabel.attributedText = detailIntroAttri
        }else{
            self.moneyFlowDetailLabel.textColor = UIColor.black
        self.moneyFlowDetailLabel.text = model.moneyFlowDetail
        }
    }
    
}
