//
//  PhotoSheet.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/20.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import MobileCoreServices
import TZImagePickerController

class PhotoSheet: UIViewController,TZImagePickerControllerDelegate {
    var choosePhotoHandler: (([UIImage]?) -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        
    }
    func showPhotoSheet(_ maxImageCount:Int = 1 ,viewController: UIViewController,completionHandler:@escaping ([UIImage]?) -> Void) {
        self.addSelfToController(viewController)
        
        self.showActionSheet(maxImageCount: maxImageCount,completionHandler: completionHandler)
    }
    
    func showCTOnlyPhotoSheet(_ maxImageCount:Int = 1 ,viewController: UIViewController,completionHandler:@escaping ([UIImage]?) -> Void) {
        self.addSelfToController(viewController)
        self.showCTOnlyPhotoActionSheet(maxImageCount: maxImageCount, completionHandler: completionHandler)
    }
    
    private func showCTOnlyPhotoActionSheet(maxImageCount:Int,completionHandler:@escaping ([UIImage]?) -> Void) {
        self.choosePhotoHandler = completionHandler
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhotoAction = UIAlertAction(title: "拍照", style: .default) { (action) in
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = UIImagePickerControllerSourceType.camera
            imagePickerController.allowsEditing = true
            imagePickerController.showsCameraControls = true
            imagePickerController.cameraDevice = UIImagePickerControllerCameraDevice.rear
            imagePickerController.mediaTypes = [kUTTypeImage as String]
            
            self.present(imagePickerController, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "取消", style: .cancel){(action) in
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
        }
        alertController.addAction(takePhotoAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    
    private func addSelfToController(_ viewController: UIViewController) {
        viewController.addChildViewController(self)
        viewController.view.addSubview(self.view)
    }
    
    private func showActionSheet(maxImageCount:Int,completionHandler:@escaping ([UIImage]?) -> Void) {
        self.choosePhotoHandler = completionHandler
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhotoAction = UIAlertAction(title: "拍照", style: .default) { (action) in
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = UIImagePickerControllerSourceType.camera
            imagePickerController.allowsEditing = true
            imagePickerController.showsCameraControls = true
            imagePickerController.cameraDevice = UIImagePickerControllerCameraDevice.rear
            imagePickerController.mediaTypes = [kUTTypeImage as String]
            
            self.present(imagePickerController, animated: true, completion: nil)
        }
        
        let choicePhotoAction = UIAlertAction(title: "从相册选择", style: .default) { (action) in
            let imagePicker = TZImagePickerController.init(maxImagesCount: maxImageCount, delegate: self)
            imagePicker?.didFinishPickingPhotosHandle = {(photos, assets,isSelectOriginalPhoto) in
                if let handler = self.choosePhotoHandler {
                    handler(photos)
                }
                self.removeFromParentViewController()
                self.view.removeFromSuperview()
            }
            imagePicker?.imagePickerControllerDidCancelHandle = {
                self.removeFromParentViewController()
                self.view.removeFromSuperview()
            }
            self.present(imagePicker!, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "取消", style: .cancel){(action) in
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
        }
        alertController.addAction(takePhotoAction)
        alertController.addAction(choicePhotoAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

extension PhotoSheet:UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let photo = info[UIImagePickerControllerOriginalImage]
        
        if photo != nil {
            if let handler = self.choosePhotoHandler {
                handler([photo as! UIImage])
            }
        }
        
        self.dismiss(animated: true) { 
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true) {
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
        }
    }
}

