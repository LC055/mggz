//
//  WWProgressHUD.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/4.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import SVProgressHUD

open class WWProgressHUD: NSObject {
    open class func show() {
        SVProgressHUD.show(with: .none)
    }
    
    open class func showWithClearMask() {
        SVProgressHUD.show(with: .clear)
    }
    
    open class func dismiss() {
        SVProgressHUD.dismiss()
    }
    
    open class func showWithStatus(_ status:String!) {
        SVProgressHUD.dismiss(withDelay: 2)
        SVProgressHUD.show(withStatus: status)
    }
    
    open class func success(_ status:String!) {
        SVProgressHUD.dismiss(withDelay: 2)
        SVProgressHUD.showSuccess(withStatus: status)
    }
    
    open class func error(_ status:String!) {
        SVProgressHUD.dismiss(withDelay: 2)
        SVProgressHUD.showError(withStatus: status)
    }
    
    open class func inform(_ status:String!) {
        SVProgressHUD.dismiss(withDelay: 2)
        SVProgressHUD.showInfo(withStatus: status)
    }
}

public func WWSuccess(_ status:String!) {
    SVProgressHUD.dismiss(withDelay: 2)
    WWProgressHUD.success(status)
}

public func WWError(_ status:String!) {
    SVProgressHUD.dismiss(withDelay: 2)
    WWProgressHUD.error(status)
}

public func WWInform(_ status:String!) {
    SVProgressHUD.dismiss(withDelay: 2)
    WWProgressHUD.inform(status)
}

public func WWBeginLoading() {
    WWProgressHUD.show()
}

public func WWBeginLoadingWithStatus(_ status:String!) {
    WWProgressHUD.showWithStatus(status)
}

public func WWEndLoading() {
    WWProgressHUD.dismiss()
}
