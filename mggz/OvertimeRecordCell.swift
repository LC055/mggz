//
//  OvertimeRecordCell.swift
//  mggz
//
//  Created by QinWei on 2018/4/17.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class OvertimeRecordCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var stateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellDataWithModel(_ model:OvertimePlanModel){
        
        if (model.ApplyWorkDays != nil) {
            self.contentLabel.text = "申请加班" + String(model.ApplyWorkDays!) + "工"
        }
        if model.ApplyState == 0{
            self.stateLabel.text = "申请中"
        }else if model.ApplyState == 1{
            self.stateLabel.text = "待加班"
        }else if model.ApplyState == 2{
            self.stateLabel.text = "已加班"
        }else if model.ApplyState == 3{
            self.stateLabel.text = "未加班"
            self.stateLabel.textColor = UIColor.init(red: 0.97, green: 0.43, blue: 0.38, alpha: 1)
        }else if model.ApplyState == -1{
            self.stateLabel.text = "已失效"
        }else if model.ApplyState == -2{
            self.stateLabel.text = "被退回"
            self.stateLabel.textColor = UIColor.init(red: 0.97, green: 0.43, blue: 0.38, alpha: 1)
        }
        if let dateStr = model.WorkDate{
        let index1 = dateStr.index(of: "T")
            let realDate = String(dateStr.prefix(upTo: index1!))
        self.dateLabel.text = realDate
        }
    }
}
