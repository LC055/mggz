//
//  AmountPayView.swift
//  mggz
//
//  Created by QinWei on 2018/3/15.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class AmountPayView: UIView {

    
    @IBOutlet weak var myValidAccount: UILabel!
    
    @IBOutlet weak var amountTextField: UITextField!
    
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var withDrawButton: UIButton!
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "AmountPayView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        view.backgroundColor = UIColor.mg_backgroundGray
        self.withDrawButton.layer.cornerRadius = 20
        self.withDrawButton.layer.masksToBounds = true
        return view
    }
}
