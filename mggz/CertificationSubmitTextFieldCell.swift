//
//  CertificationSubmitTextFieldCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/4.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class CertificationSubmitTextFieldCell: UITableViewCell,UITextFieldDelegate {
    
    var cellIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = UIColor.mg_lightGray
        return imageView
    }()
    
    var cellTextField: UITextField = {
        let textField = UITextField()
        return textField
    }()
    
    var isCellTextFieldCanEdit = true
    
    var textFieldEditHandler:((String) -> Void)?

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.cellTextField.delegate = self
        self.cellTextField.addTarget(self, action: #selector(doTextFieldEditAction(_:)), for: .editingChanged)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.contentView.addSubview(cellIcon)
        self.cellIcon.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(15)
            make.width.height.equalTo(20)
            make.centerY.equalTo(self.contentView)
        }
        
        self.contentView.addSubview(cellTextField)
        self.cellTextField.snp.makeConstraints { (make) in
            make.top.bottom.right.equalTo(self.contentView)
            make.left.equalTo(self.cellIcon.snp.right).offset(10)
        }
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return self.isCellTextFieldCanEdit
    }
    
    @objc func doTextFieldEditAction(_ textField: UITextField) {
       // textField.text = textField.text?.replacingOccurrences(of: "x", with: "X")
        if let handler = self.textFieldEditHandler {
            handler(textField.text ?? "")
        }
    }
}
