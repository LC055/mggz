//
//  HaveBeenCTViewController.swift
//  mggz
//
//  Created by QinWei on 2018/3/14.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class HaveBeenCTViewController: UIViewController {
    fileprivate var userInfoModel: UserInfoModel?
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var idCard: NSLayoutConstraint!
    
    @IBOutlet weak var idCardNum: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "实名认证"
        self.getData()
        
    }
    func getData() {
        UserInfoModel.getMigrantWorkInfo { (response) in
            if response.success {
                self.userInfoModel = response.value
                
                self.userName.text = self.userInfoModel?.username
                
                guard ((self.userInfoModel?.idCard) != nil) else{
                    return
                }
                self.idCardNum.text = Tool.securityWithIDCard((self.userInfoModel?.idCard)!)
            }
            else {
                WWError(response.message)
            }
        }
    }
}
