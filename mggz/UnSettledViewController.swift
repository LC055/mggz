//
//  UnSettledViewController.swift
//  mggz
//
//  Created by QinWei on 2018/3/29.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
import Moya
import MJRefresh
class UnSettledViewController: UIViewController {
   fileprivate lazy var itemArray : [UnsettledModel] = [UnsettledModel]()
    var companyID:String?
    var companyName:String?
    fileprivate var totalPage:Int?
    fileprivate var currentPage = 1
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
     self.view.backgroundColor = UIColor.mg_backgroundGray
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.mg_backgroundGray
        self.tableView.estimatedRowHeight = 120
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 10)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.showsHorizontalScrollIndicator = false
        
        self.tableView.register(UINib.init(nibName: "UnsettledCell", bundle: nil), forCellReuseIdentifier: "unsettle")
        let tableFooter = MJRefreshAutoNormalFooter { [unowned self] in
            self.getNextPage()
        }
        self.tableView.mj_footer = tableFooter
        
        self.getData()
        

    }
    func getData() {
        
        guard (self.companyID != nil) else {
            return
        }
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
        self.currentPage = 1
        LCAPiSubManager.request(.GetSalaryNotPayedRecord(contructionCompanyID: companyID!, AccountId: accountId, pageIndex: 1, pageSize: 25)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
                
                guard (jsonString["State"]) as! String == "OK" else{
                    return
                }
               
                
                guard  let resultDic : NSDictionary = jsonString["Result"] as? NSDictionary else{
                    return
                }
                guard  let resultArray : NSArray = resultDic["ProjectList"] as? NSArray else{
                    return
                }
                if let totalList = resultDic["Total"] as? Int {
                    self.totalPage = (totalList/25)+1
                }
                self.itemArray = Mapper<UnsettledModel>().mapArray(JSONObject: resultArray)!
                self.tableView.reloadData()
                
            }
        }
    }
    
    func getNextPage() {
        guard (self.totalPage != nil) else {
            return
        }
        if self.currentPage >= self.totalPage! {
            self.tableView.mj_footer.endRefreshingWithNoMoreData()
            return
        }
        
        guard (self.companyID != nil) else {
            return
        }
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
        self.currentPage += 1
        
        LCAPiSubManager.request(.GetSalaryPayedRecord(contructionCompanyID: companyID!, AccountId: accountId, pageIndex: self.currentPage, pageSize: 25)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
                
                guard (jsonString["State"]) as! String == "OK" else{
                    return
                }
               
                
                guard  let resultDic : NSDictionary = jsonString["Result"] as? NSDictionary else{
                    return
                }
                guard  let resultArray : NSArray = resultDic["ProjectList"] as? NSArray else{
                    return
                }
                var salaryArray = [UnsettledModel].init()
                salaryArray = Mapper<UnsettledModel>().mapArray(JSONObject: resultArray)!
                if salaryArray.count <= 0 {
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
                    return
                }
                self.itemArray += salaryArray
                self.tableView.reloadData()
                self.tableView.mj_footer.endRefreshing()
            }
        }
    }
   
}
extension UnSettledViewController:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.itemArray.count == 0 {
            self.tableView.isHidden = true
        }else{
            self.tableView.isHidden = false
        }
        if self.itemArray.count < 25{
            self.tableView.mj_footer.isHidden = true
        }else{
            self.tableView.mj_footer.isHidden = false
        }
        
        
        return self.itemArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UnsettledCell = tableView.dequeueReusableCell(withIdentifier: "unsettle") as! UnsettledCell
   cell.setupCellDataWithModel(self.itemArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
