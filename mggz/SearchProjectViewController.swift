//
//  SearchProjectViewController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/11.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class SearchProjectViewController: UIViewController {
    fileprivate var _tableView: UITableView!
    fileprivate var tableView: UITableView {
        get {
            if _tableView != nil {
                return _tableView
            }
            _tableView = UITableView()
            _tableView.delegate = self
            _tableView.dataSource = self
            _tableView.tableFooterView = UIView()
            
            _tableView.register(ProjectCell.self, forCellReuseIdentifier: "\(ProjectCell.self)")
            
            return _tableView
        }
    }
    
    fileprivate var completion: ((ProjectModel) -> Void)?
    var searchProjectLists: [ProjectModel] = []     //搜索出来的projectList

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "搜索结果"

        self.view.addSubview(tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(self.view)
            make.top.equalTo(self.view).offset(-44)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //初始化。          好奇怪，第一次点击搜索框输入关键字得到搜索结果后，点击取消。然后又点击搜索框输入相同的关键字，不点击搜索就会显示上次的搜索结果。我只能使用下面的方法进行初始化。
        self.searchProjectLists = []
        self.tableView.reloadData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        WWProgressHUD.dismiss()
    }
    
    func actionHandler() {
        self.tableView.reloadData()
    }

    func searchAction(searchText: String, completion: @escaping (ProjectModel) -> Void) {
        self.completion = completion
        
        ProjectModel.projectList(pageIndex: 1, keyword: searchText, isShowApplying:true) { (response) in
            if response.success {
                if let value = response.value {
                    if value.count <= 0{
                       
                    }
                    else {
                        self.searchProjectLists = value
                        self.tableView.reloadData()
                    }
                }
            }
            else {
               
            }
        }

    }
    
    deinit {
       
    }

}
extension SearchProjectViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchProjectLists.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100 + 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(ProjectCell.self)", for: indexPath) as! ProjectCell
        cell.bind(self.searchProjectLists[indexPath.row])
        cell.callInHandler = {
            cell.projectStatus = .inApply
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! ProjectCell
        if cell.projectStatus != .onJob {
            WWInform("未在职的不能选择")
        }
        else {
            let model = self.searchProjectLists[indexPath.row]
            ProjectModel.updateProjectInformation(workerId: model.demandBaseMigrantWorkerID!) { [unowned self](response) in
                if response.success {
                    if let closure = self.completion {
                        closure(self.searchProjectLists[indexPath.row])
                    }
                    self.dismiss(animated: true, completion: nil)
                }
                else {
                    WWError(response.message)
                }
            }
        }

    }
}
