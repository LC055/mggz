//
//  OvertimeSwitchCell.swift
//  mggz
//
//  Created by QinWei on 2018/4/13.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class OvertimeSwitchCell: UITableViewCell {

    @IBOutlet weak var overtimeSwitch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func layoutSubviews() {
        
        self.overtimeSwitch.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}










