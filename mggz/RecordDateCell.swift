//
//  RecordDateCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/8.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import FSCalendar

class RecordDateCell: UITableViewCell,FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance {
    var noData: Bool = true {
        willSet {
            self.noData = newValue
            self.updateViews()
        }
    }
    fileprivate let gregorian: NSCalendar! = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian)


    lazy var calendar: MMCalendar = {
        let calendar = MMCalendar()
        calendar.backgroundColor = UIColor.white
        calendar.scrollEnabled = false
        calendar.calendarHeaderView.isUserInteractionEnabled = true
        calendar.appearance.todayColor = colorWith255RGB(73, g: 169, b: 238)
   calendar.calendarHeaderView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(doTapCalendarHeaderViewAction)))
        
        return calendar
    }()
    
    var blueExplainLabel: UILabel = {
        let label = UILabel()
        label.text = "正常"
        label.font = wwFont_Regular(14)
        label.textColor = colorWith255RGB(102, g: 102, b: 102)
        
        return label
    }()
    
    var pinkExplainLabel:UILabel = {
        let pinkExplainLabel = UILabel()
        pinkExplainLabel.text = "未记工"
        pinkExplainLabel.font = wwFont_Regular(14)
        pinkExplainLabel.textColor = colorWith255RGB(102, g: 102, b: 102)
        return pinkExplainLabel
    }()
    var overtimeLabel:UILabel = {
        let pinkExplainLabel = UILabel()
        pinkExplainLabel.text = "加班"
        pinkExplainLabel.font = wwFont_Regular(14)
        pinkExplainLabel.textColor = colorWith255RGB(102, g: 102, b: 102)
        return pinkExplainLabel
    }()
    //左右滑动月份时触发
    var currentMonthChangeHandler: ((Date) -> Void)?
    
//    let panel = UIView()
    
//    var showCalendar = false {
//        willSet {
//            self.showCalendar = newValue
//            self.calendar.isHidden = !newValue
//        }
//    }
    
    var selectDateHandler: ((Date) -> Void)?
    var recordSupplyLists: [RecordSupply]? {
        willSet {
            self.recordSupplyLists = newValue
            self.calendar.reloadData()
        }
    }
    var isOvertime:Bool?{
        didSet {
            self.calendar.reloadData()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.contentView.backgroundColor = UIColor.mg_backgroundGray
        
        self.setupViews()
    }
    
    func setupViews() {
//        panel.addSubview(self.cellButton)
//        self.cellButton.snp.makeConstraints({ (make) in
//            make.left.top.bottom.equalTo(panel)
//        })
//        
//        panel.addSubview(arrowImageView)
//        arrowImageView.snp.makeConstraints({ (make) in
//            make.left.equalTo(cellButton.snp.right)
//            make.right.equalTo(panel)
//            make.centerY.equalTo(panel)
//            make.width.height.equalTo(25)
//        })
        
//        
//        self.contentView.addSubview(panel)
//        panel.snp.makeConstraints({ (make) in
//            make.centerX.equalTo(self.contentView)
//            make.top.equalTo(self.contentView)
//            make.height.equalTo(50)
//        })
        
        self.calendar.dataSource = self
        self.calendar.delegate = self
        self.contentView.addSubview(self.calendar)
        self.calendar.snp.makeConstraints { (make) in
            make.top.right.left.equalTo(self.contentView)
        }


        let bottomPanel = UIView()
        self.contentView.addSubview(bottomPanel)
        bottomPanel.backgroundColor = UIColor.white
        bottomPanel.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.contentView)
            make.top.equalTo(self.calendar.snp.bottom).offset(10)
            make.height.equalTo(30)
            make.bottom.equalTo(self.contentView)
        }
        
        let blueRoundView = UIView()
        blueRoundView.backgroundColor = UIColor.mg_blue
        blueRoundView.layer.cornerRadius = 8
        bottomPanel.addSubview(blueRoundView)
        blueRoundView.snp.makeConstraints { (make) in
            make.left.equalTo(bottomPanel).offset(16)
            make.centerY.equalTo(bottomPanel)
            make.height.width.equalTo(16)
        }
        
        bottomPanel.addSubview(self.blueExplainLabel)
        self.blueExplainLabel.snp.makeConstraints { (make) in
          make.left.equalTo(blueRoundView.snp.right).offset(8)
            make.centerY.equalTo(bottomPanel)
        }
        
        let pinkRoundView = UIView()
        pinkRoundView.backgroundColor = colorWith255RGB(244, g: 110, b: 101)
        pinkRoundView.layer.cornerRadius = 8
        bottomPanel.addSubview(pinkRoundView)
        pinkRoundView.snp.makeConstraints { (make) in
        make.left.equalTo(blueExplainLabel.snp.right).offset(15)
            make.centerY.equalTo(bottomPanel)
            make.height.width.equalTo(blueRoundView)
        }
        
        bottomPanel.addSubview(self.pinkExplainLabel)
        self.pinkExplainLabel.snp.makeConstraints { (make) in
        make.left.equalTo(pinkRoundView.snp.right).offset(8)
            make.centerY.equalTo(bottomPanel)
        }
        
        let overtimeView = UIView()
        overtimeView.backgroundColor = colorWith255RGB(62, g: 207, b: 142)
        overtimeView.layer.cornerRadius = 8
        bottomPanel.addSubview(overtimeView)
        overtimeView.snp.makeConstraints { (make) in
        make.left.equalTo(pinkExplainLabel.snp.right).offset(15)
            make.centerY.equalTo(bottomPanel)
            make.height.width.equalTo(pinkRoundView)
        }
        bottomPanel.addSubview(self.overtimeLabel)
        self.overtimeLabel.snp.makeConstraints { (make) in
        make.left.equalTo(overtimeView.snp.right).offset(8)
            make.centerY.equalTo(bottomPanel)
        }
    }
    
    
    func updateViews() {
//        if self.noData {
//            self.arrowImageView.tintColor = UIColor.mg_noDataGray
//            self.cellButton.setTitleColor(UIColor.mg_noDataGray, for: .normal)
//        }
//        else {
//            
//            self.arrowImageView.tintColor = colorWith255RGB(16, g: 142, b: 233)
//            self.cellButton.setTitleColor(colorWith255RGB(16, g: 142, b: 233), for: .normal)
//        }
    }
    
    @objc func doTapCalendarHeaderViewAction() {
        var responder = self.next
        while !(responder is UIViewController) {
            responder = responder?.next
        }
        if responder is UIViewController {
            let vc = responder as! UIViewController
            
            let toVc = DateAlertController.init({ (date) in
                self.calendar.currentPage = date
            })
            toVc.date = self.calendar.currentPage
            vc.present(toVc, animated: true, completion: nil)
        }
    }
    
    //Delegate,点击日历上的日期item会调用
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
//        guard date < Date() else {//未来还没到，咋签到
//            QWTextonlyHud("日期超出,无法补工", target: self.contentView)
//            return
//        }
       
        if let closure = self.selectDateHandler {
            closure(date)
        }
    }
    
    //月份变化后触发
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        if let handler = self.currentMonthChangeHandler {
            handler(calendar.currentPage)
        }
    }

    
    //对应date的cell的背景色
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
       
        guard date < Date() else {
            if self.isOvertime == true {
            if let lists = self.recordSupplyLists {
                for item in lists {
                    if date == item.signedDate {
                        if item.isOverTimeWork == true{
                            return colorWith255RGB(62, g: 207, b: 142)
                        }
                    }
                }
            }
            }
            return UIColor.clear
        }
        
        let selectForm = DateFormatter()
        selectForm.dateFormat = "yyyyMMdd"
        if selectForm.string(from: date) == selectForm.string(from: Date()){
            
            return UIColor.clear
            
        }
        
        if let lists = self.recordSupplyLists {
            for item in lists {
                if date == item.signedDate {
                    switch item.exceptionState {
                    case .unSigned:
                        return nil
                    case .signed:
                        return colorWith255RGB(73, g: 169, b: 238)
                    case .other:
                        return colorWith255RGB(244, g: 110, b: 101)
                    }
                }
            }
        }
        return UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 1)
    }
    //修改指定日期的颜色
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        let selectForm = DateFormatter()
        selectForm.dateFormat = "yyyyMMdd"
        if selectForm.string(from: date) == selectForm.string(from: Date()){
            
            return UIColor.black
            
        }
        if let lists = self.recordSupplyLists {
            for item in lists {
                if date == item.signedDate {
                    switch item.exceptionState {
                    case .unSigned:
                        return nil
                    case .signed,.other:
                        return UIColor.white
                        
                    }
                }
            }
        }
        return colorWith255RGB(66, g: 66, b: 66)
    }

    //去掉选中色，titleSelectionColorFor指cell中文字色，fillSelectionColorFor指cell背景色
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {
        
        let selectForm = DateFormatter()
        selectForm.dateFormat = "yyyyMMdd"
        if selectForm.string(from: date) == selectForm.string(from: Date()){
            
            return UIColor.black
            
        }
        if let lists = self.recordSupplyLists {
            for item in lists {
                if date == item.signedDate {
                    switch item.exceptionState {
                    case .unSigned:
                        return UIColor.black
                    case .signed,.other:
                        return UIColor.white
                    }
                }
            }
        }
        return UIColor.black
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        guard date < Date() else {
            if self.isOvertime == true {
                if let lists = self.recordSupplyLists {
                    for item in lists {
                        if date == item.signedDate {
                            if item.isOverTimeWork == true{
                                return colorWith255RGB(62, g: 207, b: 142)
                            }
                        }
                    }
                }
            }
            return UIColor.clear
        }
        
        let selectForm = DateFormatter()
        selectForm.dateFormat = "yyyyMMdd"
        if selectForm.string(from: date) == selectForm.string(from: Date()){
            
            return UIColor.clear
            
        }
        if let lists = self.recordSupplyLists {
            for item in lists {
                if date == item.signedDate {
                    switch item.exceptionState {
                    case .unSigned:
                        return UIColor.clear
                    case .signed:
                        return colorWith255RGB(73, g: 169, b: 238)
                    case .other:
                        return colorWith255RGB(244, g: 110, b: 101)
                    }
                }
            }
        }
        
        return UIColor(red: 0.91, green: 0.91, blue: 0.91, alpha: 1)
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
        let selectForm = DateFormatter()
        selectForm.dateFormat = "yyyyMMdd"
        if selectForm.string(from: date) == selectForm.string(from: Date()){
            
            return colorWith255RGB(73, g: 169, b: 238)
        }
        return UIColor.clear
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderSelectionColorFor date: Date) -> UIColor? {
        let selectForm = DateFormatter()
        selectForm.dateFormat = "yyyyMMdd"
        if selectForm.string(from: date) == selectForm.string(from: Date()){
            
            return colorWith255RGB(73, g: 169, b: 238)
        }
        return UIColor.clear
 }
//    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
//        if self.isOvertime == true{
//            let day: Int! = self.gregorian.component(.day, from: date)
//            return [13,24].contains(day) ? 1 : 0
//        }
//        return 0
//    }
//    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
//
//        return [UIColor(red: 0.24, green: 0.81, blue: 0.56, alpha: 1)]
//    }
   
    func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
        
        guard date < Date() else {//未来还没到，咋签到
            return nil
        }
        
        let selectForm = DateFormatter()
        selectForm.dateFormat = "yyyyMMdd"
        if selectForm.string(from: date) == selectForm.string(from: Date()){
            return nil
        }
        if self.isOvertime == true{
        if let lists = self.recordSupplyLists {
            for item in lists {
                if date == item.signedDate {
                    if item.isOverTimeWork == true{
                        return UIImage(named: "staticts_or")
                    }else{
                       return nil
                    }
                }
            }
          }
        }
        return nil
    }
}
