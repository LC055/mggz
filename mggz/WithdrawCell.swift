//
//  WithdrawCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/29.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class WithdrawCell: UITableViewCell {
    var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Icon_选中")!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.mg_lightGray
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    var withdrawTitleLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Semibold(16)
        label.text = ""
        return label
    }()
    
    var withdrawDetailLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Regular(13)
        label.textColor = UIColor.mg_lightGray
        label.text = "宁波工程学院总承包劳务"
        return label
    }()
    
    var amountLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = wwFont_Semibold(17)
        label.text = "$1,300.00"
        label.textColor = colorWith255RGB(255, g: 191, b: 0)
        return label
    }()
    
    var isChosen = false {
        willSet {
            if newValue {
                self.selectImageView.image = UIImage(named: "Icon_选中")!.withRenderingMode(.alwaysTemplate)
                self.selectImageView.tintColor = colorWith255RGB(62, g: 207, b: 110)
            }
            else {
                self.selectImageView.image = UIImage(named: "Icon_未选中")!.withRenderingMode(.alwaysTemplate)
                self.selectImageView.tintColor = UIColor.mg_lightGray
            }
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.backgroundColor = UIColor.mg_backgroundGray
        let selectionView = UIView()
        selectionView.backgroundColor = UIColor.mg_backgroundGray
        self.selectedBackgroundView = selectionView
        
        let panel = UIView()
        panel.backgroundColor = UIColor.white
        self.contentView.addSubview(panel)
        panel.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(self.contentView)
            make.bottom.equalTo(self.contentView).offset(-10)
        }
        
        panel.addSubview(self.selectImageView)
        self.selectImageView.snp.makeConstraints { (make) in
            make.top.equalTo(panel).offset(10)
            make.left.equalTo(panel).offset(15)
            make.size.equalTo(CGSize.init(width: 26, height: 26))
        }
        
        panel.addSubview(self.withdrawTitleLabel)
        self.withdrawTitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.selectImageView.snp.right).offset(10)
            make.centerY.equalTo(self.selectImageView)
        }
        
        panel.addSubview(self.withdrawDetailLabel)
        self.withdrawDetailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.withdrawTitleLabel)
            make.top.equalTo(self.withdrawTitleLabel.snp.bottom).offset(10)
        }
        
        panel.addSubview(self.amountLabel)
        self.amountLabel.snp.makeConstraints { (make) in
            make.right.equalTo(panel).offset(-20)
            make.top.equalTo(self.withdrawTitleLabel)
            make.left.equalTo(self.withdrawTitleLabel.snp.right)
        }
    }
    
    func bind(_ model: SalaryWithdraw) {
        if let timeDate = model.createTime {
            
            let dateString = dateToString(date: timeDate)
            self.withdrawTitleLabel.text = dateString
        }
        
        self.withdrawDetailLabel.text = model.projectName
        
        if let amount = model.applyAmount {
            let tempString = "\(amount)"
            let amountMoneyFormat = "￥" + Tool.numberToMoney(tempString)!
            self.amountLabel.text = amountMoneyFormat
        }
    }
}
