//
//  JavascriptJSONjavascript.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/16.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import WebKit
class JavascriptJSONjavascript: NSObject {

    private static let webView = WKWebView()
    
    class func parse(jsonString: String, completionHandler: @escaping (Any?, Error?) -> Void) {
        self.webView.evaluateJavaScript("tmp = \(jsonString)", completionHandler: completionHandler)
    }
}
