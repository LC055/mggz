//
//  SalaryFlowDetailCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/24.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class SalaryFlowDetailCell: UITableViewCell {
    
    var moneyLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Semibold(16)
        label.text = "+10,000.00"
        
        return label
    }()
    
    var dateAndTimeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.text = "2017-8-30 12:10:10"
        label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Regular(14)
        return label
    }()
    fileprivate lazy var monthSalary:UILabel = {
        let monthSalary = UILabel()
        monthSalary.text = "2017年6月份工资(元)"
        monthSalary.font = wwFont_Medium(14)
        //monthSalary.textColor = UIColor(red: 0.40, green: 0.40, blue: 0.40, alpha: 1)
        return monthSalary
    }()
    
    
    fileprivate lazy var iconView:UIImageView = {
       let iconView = UIImageView()
        iconView.image = UIImage(named: "reth_project")
        return iconView
    }()
    
    
    
    //资金流动细节
    var moneyFlowDetailLabel: UILabel = {
        let label = UILabel()
        label.text = "利息收入"
        //label.textColor = UIColor.mg_lightGray
        label.font = wwFont_Medium(14)
        label.numberOfLines = 0
        label.lineBreakMode = .byCharWrapping
        
        return label
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.contentView.addSubview(self.iconView)
        self.iconView.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(16)
            make.top.equalTo(self.contentView).offset(7)
            make.size.equalTo(CGSize(width: 12, height: 12))
        }
    self.contentView.addSubview(self.moneyFlowDetailLabel)
        self.moneyFlowDetailLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView).offset(5)
        make.left.equalTo(self.iconView.snp.right).offset(3)
         make.right.equalTo(self.contentView).offset(-15)
        }
      self.contentView.addSubview(self.monthSalary)
      self.monthSalary.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(16)
        make.top.equalTo(self.moneyFlowDetailLabel.snp.bottom).offset(8)
            make.size.equalTo(CGSize(width: 200, height: 19))
        }
        
        
        
        
    self.contentView.addSubview(self.dateAndTimeLabel)
        self.dateAndTimeLabel.snp.makeConstraints { (make) in
          make.left.equalTo(self.contentView).offset(16)
      make.top.equalTo(self.monthSalary.snp.bottom).offset(5)
            make.height.equalTo(20)
        }
        
        self.contentView.addSubview(self.moneyLabel)
        self.moneyLabel.snp.makeConstraints { (make) in
            make.height.equalTo(20)
            make.right.equalTo(self.contentView).offset(-15)
            make.centerY.equalTo(self.dateAndTimeLabel)
            
        }
        
       
        
    
    }
    
    func bind(_ model: SalaryRecordModel) {
        
        if let applyAmount = model.ApplyAmount {
            let tempString = "\(applyAmount)"
            if let format = Tool.numberToMoney(tempString) {
                if format.hasPrefix("-"){
                    self.moneyLabel.text = format
                }else{
                    self.moneyLabel.text = "+" + format
                }
            }
        }
        
        if (model.CreateTime != nil){
            let dateString = model.CreateTime
            let date = dateStringWithTToDate(dateString: dateString!)
            if date != nil {
                let dateString = dateTimeToString(date: date!)
                self.dateAndTimeLabel.text = dateString
            }
        }
        self.moneyFlowDetailLabel.text = model.ProjectName
        self.monthSalary.text = model.ChangeType
    }
}
