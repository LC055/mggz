//
//  ProjectNoDataCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/10.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class ProjectNoDataCell: UITableViewCell {
    
    fileprivate var cellImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "Icon_NoData")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    fileprivate var cellLabel: UILabel = {
        let label = UILabel()
        label.text = "暂未选择在职项目，点击选择"
        label.textColor = colorWith255RGB(221, g: 221, b: 221)
        label.font = wwFont_Light(13)
        label.textAlignment = .center
        return label
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.contentView.addSubview(self.cellImageView)
        self.cellImageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.top.equalTo(self.contentView).offset(8)
        }
        
        self.contentView.addSubview(self.cellLabel)
        self.cellLabel.snp.makeConstraints { (make) in
            make.top.equalTo(cellImageView.snp.bottom).offset(5)
            make.centerX.equalTo(self.cellImageView)
            make.bottom.equalTo(self.contentView).offset(-8)
        }
    }

}
