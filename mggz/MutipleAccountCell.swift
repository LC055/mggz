//
//  MutipleAccountCell.swift
//  mggz
//
//  Created by QinWei on 2018/3/16.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class MutipleAccountCell: UITableViewCell {

    
    @IBOutlet weak var headerView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var idCardLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
