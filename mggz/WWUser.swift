//
//  WWUser.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/7.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
let kUserName = "me.fin.username"
class WWUser: NSObject {
    static let sharedInstance = WWUser()
    fileprivate override init() {
         super.init()
    }
    
    fileprivate var _userModel: UserModel?
    var userModel: UserModel? {
        get {
            return self._userModel
        }
        set {
            self._userModel = newValue
        }
    }
    
    var _token: String?
    var token: String? {
        set {
            self._token = newValue
        }
        get {
            if self._token == nil && self.userModel != nil  {
                if let accountId = self.userModel!.accountId , let key = self.userModel!.userKey {
                    UserModel.login(accountId: accountId, key: key, completionHandler: { (response) in
                        if response.success {
                            
                        }
                    })
                }
            }
            return self._token
        }
    }
    
    func  getToken(_ accountId: String, key: String,completionHandler:@escaping (Bool) -> Void) {
        UserModel.login(accountId: accountId, key: key, completionHandler: { (response) in
            if response.success {
                completionHandler(true)
            }
            else {
                completionHandler(false)
            }
        })
        
    }
    
    var certificationStatus: CertificationStatus? //认证状态
    var stateReason:String?
    /// accountId
    var accountId: String? {
        get {
            if let model = self.userModel, let accountId = model.accountId {
                return accountId
            }
            else {
               
                return nil
            }
        }
    }
    
    var companyID: String? {
        get {
            if let model = self.userModel, let companyID = model.companyId {
                return companyID
            }
            else {
                
                return nil
            }
        }
    }
    
    
    /// httpheader
    var mobile_cilent_headers:[String: String]? {
        get {
            if let token = self.token {
                return ["Authorization":"Bearer" + " " + token]
            }
            else {
               
                
                
                return nil
            }
        }
    }
    
    
    var isLogin: Bool {
        get {
            if self.userModel != nil {
                return true
            }
            else {
                return false
            }
        }
    }
    
    
    private var _ensureLoginWithHandler:((Bool) -> Void)?
    //确认当前登录状态
    func ensureLoginWithHandler(_ handler: @escaping (Bool) -> Void) {
        self._ensureLoginWithHandler = handler
        handler(isLogin)
        guard isLogin else {
            WWInform("没有登录！")
            return
        }
    }
    
    //提示：这个方法专门给SalaryHomeController使用。因为SalaryHomeController使用了左右滑动cell的框架
    //导致不能继承WarningBaseController，也就不能使用通用的确认认证状态的方法了。
    func ensureCertificationStatus(_ handler: (CertificationStatus) -> Void) {
        guard self.userModel != nil else {
            handler(.unKnow)
           
            return
        }
        
        if let status = self.certificationStatus {
        handler(status)
        }
    }
    
    func ensureCertificationStatus<T: WarningBaseController>(_ vc: T) {
        guard self.userModel != nil else {
           
            return
        }
        
        if let status = self.certificationStatus {
            vc.setupWarningContent(status: status)
        }
    }
    
    func loginOut() {
        self.userModel = nil
        LoginAuthenticationService.removeUsernameAndPassword()
        (UIApplication.shared.delegate as! AppDelegate).setTags(nil)
    }
    
}
