//
//  AnomalyTackCell.swift
//  mggz
//
//  Created by QinWei on 2018/4/16.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class AnomalyTackCell: UITableViewCell {
    
    
    @IBOutlet weak var trackNumLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func setCellDataWith(_ count:Int){
       
        self.trackNumLabel.text = String(count) + "次"
    
    }
}
