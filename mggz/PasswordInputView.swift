//
//  PasswordInputView.swift
//  SwiftDemo2
//
//  Created by ShareAnimation on 2017/9/13.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
func colorWithRGB(r:CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
    return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
}

class PasswordInputView: UIView {
    
    private let defaultColor = colorWithRGB(r: 217, g: 217, b: 217)
    
    private var labels = [UILabel]()
    
    private var viewWithd:CGFloat?
    private var viewHeight:CGFloat?
    
    var password: String? {
        willSet {
            if newValue != nil {
                guard newValue!.characters.count <= 6 else {
                    return
                }
                
                for label in self.labels {
                    label.text = ""
                }
                
                for (index, _) in newValue!.characters.enumerated() {
                    let label = self.labels[index]
                    label.text = "●"
                }
                
                if let handler = self.passwordInputHandler {
                    handler(newValue!)
                }
            }
        }
    }
    
     var textField: UITextField = {
        let textField = UITextField()
        textField.tintColor = UIColor.red
        textField.keyboardType = .numberPad
        return textField
    }()
    
    var setFirstResponder:Bool = false {
        willSet {
            if newValue {
                self.textField.becomeFirstResponder()
            }
        }
    }
    
    var passwordInputHandler:((String) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: CGRect.zero)
        
        self.textField.addTarget(self, action: #selector(textFieldEditAction(_:)), for: .editingChanged)
        
        for _ in 0...5 {
            let label = self.createLabel()
            self.labels.append(label)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        var labelWidth = (self.viewWithd! - 5)/6
        if labelWidth > viewHeight! {
            labelWidth = viewHeight!
        }
        
        let panelView = UIView()
        panelView.layer.borderColor = defaultColor.cgColor
        panelView.layer.borderWidth = 1
        panelView.bounds = CGRect(x: 0, y: 0, width: self.viewWithd!, height: labelWidth)
        panelView.center = CGPoint(x: self.viewWithd!/2, y: self.viewHeight!/2)
        self.addSubview(panelView)
        
        panelView.addSubview(self.textField)
        self.textField.frame = panelView.bounds
        
        let coverView = UIView()
        coverView.backgroundColor = UIColor.white
        panelView.addSubview(coverView)
        coverView.frame = panelView.bounds

        for (index, label) in self.labels.enumerated() {
            label.frame = CGRect(x: CGFloat(index) * (labelWidth + 1), y: 0, width: labelWidth, height: labelWidth)
            panelView.addSubview(label)
            if index != 0 {
                let grayLine = UIView()
                grayLine.backgroundColor = defaultColor
                grayLine.frame = CGRect(x: CGFloat(index) * labelWidth + (CGFloat(index)-1), y: 0, width: 1, height: labelWidth)
                panelView.addSubview(grayLine)
            }
        }
    }

    private func createLabel() -> UILabel {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }
    
    //清空当前密码
    func clear() {
        self.password = ""
        self.textField.text = ""
    }
    
    @objc func textFieldEditAction(_ textField: UITextField) {
        if let text = textField.text  {
            if text.characters.count <= 6 {
                self.password = text
//                self.passwordInputView.password = text
            }
            
            //限定6个字符
            if text.characters.count > 6 {
                let index = text.index(text.startIndex, offsetBy: 6)
                textField.text = text.substring(to: index)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.viewWithd = self.frame.width
        self.viewHeight = self.frame.height
        self.setupViews()
    }
}
