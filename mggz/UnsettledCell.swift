//
//  UnsettledCell.swift
//  mggz
//
//  Created by QinWei on 2018/3/29.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class UnsettledCell: UITableViewCell {

    @IBOutlet weak var projectName: UILabel!
    
    @IBOutlet weak var unsettledLabel: UILabel!
    
    @IBOutlet weak var salaryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setupCellDataWithModel(_ model:UnsettledModel){
         self.projectName.text = model.ProjectName ?? "暂无"
       
        if model.WorkDayType == 2{
            if (model.WorkTime != nil) {
                let timeStr = String(format: "%.2f",model.WorkTime!)
                self.unsettledLabel.text = timeStr + "小时未结算"
            }
            
        }else{
            if (model.WorkTime != nil) {
                let timeStr = String(format: "%.2f",model.WorkTime!)
                self.unsettledLabel.text = timeStr + "工未结算"
            }
            
        }
        if let amount = model.SalaryNotPayed{
            let amountObj = NSNumber.init(value: amount)
            let valueStr = Tool.numberToMoney("\(amountObj)")
            self.salaryLabel.text = valueStr
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
