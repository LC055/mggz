//
//  RemoteNotificationModel.swift
//  mggz
//
//  Created by ShareAnimation on 2017/10/19.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Alamofire

class RemoteNotificationModel: NSObject {

    class func registerClient(_ completionHandler: @escaping (WWResponse) -> Void) {
        guard let httpHeader = mobile_cilent_headers() else{
            return
        }
        guard let accountId = WWUser.sharedInstance.accountId else {
            return
        }
        guard let registerId = JPUSHService.registrationID() else {
            return
        }
        guard let deviceUDID = LoginAuthenticationService.getUDIDWithKeychain(jpushID: registerId) else {
            return
        }
        
        
        let method = "KouFine.Common.MessageHub.Handler.AjaxAppClient.RegisterClient"
        let clientType = "1"
        let productType = "民工端"
        
        let params = ["Method":method, "accountId":accountId, "clientID": registerId,"clientType":clientType, "productType":productType, "AccountId": accountId,"deviceID":deviceUDID]
        print(params)
        Alamofire.request(API_CALL_METHOD, method: .post, parameters: params, headers: httpHeader).responseString { (response) in
            if response.result.isSuccess {
                
                if response.result.value == "OK" {
                    completionHandler(WWResponse.init(success: true))
                }
                else {
                    completionHandler(WWResponse.init(success: false, message: "注册客户端出错"))
                }
            }
            else {
                completionHandler(WWResponse.init(success: false, message: "网络出错"))
            }
        }
    }
    
    class func removeClient(_ completionHandler:@escaping (WWResponse) -> Void) {
        guard let httpHeader = mobile_cilent_headers() else{
            return
        }
        guard let accountId = WWUser.sharedInstance.accountId else {
            return
        }
        guard let registerId = JPUSHService.registrationID() else {
            return
        }
        let method = "KouFine.Common.MessageHub.Handler.AjaxAppClient.LogoffClient"
        
        let params = ["Method": method, "accountId": accountId, "clientID": registerId,  "AccountId":accountId,"productType":"民工端"]
        Alamofire.request(API_CALL_METHOD, method: .post, parameters: params, headers: httpHeader).responseString { (response) in
            if response.result.isSuccess {
                if response.result.value == "OK" {
                completionHandler(WWResponse.init(success: true))
                }
                else {
                completionHandler(WWResponse.init(success: false, message: "注册推送ID失败"))
                }
            }
            else {
                completionHandler(WWResponse.init(success: false, message: "网络访问错误"))
            }
        }
    }
}
