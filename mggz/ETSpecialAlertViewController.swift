//
//  ETSpecialAlertViewController.swift
//  mggz
//
//  Created by QinWei on 2018/4/25.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class ETSpecialAlertViewController: UIViewController {

    fileprivate var coverView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        return view
    }()
    
    fileprivate var panelView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 1, height: 1)
        view.backgroundColor = UIColor.white
        return view
    }()
    
    fileprivate var messageLabel : UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = wwFont_Regular(15)
        //label.textAlignment = .center
        return label
    }()
    
    fileprivate var leftButton: UIButton = {
        let button = UIButton()
        button.setTitle("确认", for: .normal)
        button.titleLabel?.font = UIFont(name: "PingFangSC-Regular", size: 16)
        
        button.setTitleColor(UIColor.init(red: 0.97, green: 0.43, blue: 0.38, alpha: 1), for: .normal)
        return button
    }()
    
    fileprivate var rightButton: UIButton = {
        let button = UIButton()
        button.setTitle("取消", for: .normal)
        button.titleLabel?.font = UIFont(name: "PingFangSC-Regular", size: 16)
        button.setTitleColor(UIColor.init(white: 0.4, alpha: 1), for: .normal)
        return button
    }()
    
    fileprivate lazy var cancelButton:UIButton = {
       let button = UIButton.init()
       button.setImage(UIImage.init(named: "alert_default"), for: .normal)
       return button
    }()
    
    fileprivate lazy var titleLabel:UILabel = {
        let label = UILabel.init()
        label.text = "提醒"
        label.textAlignment = .center
        label.font = wwFont_Medium(17)
        return label
    }()
    
    private var okHandler: (()->Void)?
    private var sort: String?
    
    //当前alert是否正在显示
    private var isShowing:Bool = false
    fileprivate var demandBaseMigrantWorkerID:String?
    init (_ sort: String,demandBaseMigrantWorkerID:String, okHandler: @escaping ()-> Void){
        super.init(nibName: nil, bundle: nil)
        self.transitioningDelegate = self
        self.okHandler = okHandler
        self.sort = sort
        self.demandBaseMigrantWorkerID = demandBaseMigrantWorkerID
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        self.cancelButton.addTarget(self, action: #selector(cancelButtonAction), for: .touchUpInside)
        self.leftButton.addTarget(self, action: #selector(doRightButtonAction), for: .touchUpInside)
        self.rightButton.addTarget(self, action: #selector(doLeftButtonAction), for: .touchUpInside)
        
        self.setupViews()
        self.updateViews()
        
        self.isShowing = true
    }
    
    deinit {
        self.isShowing = false
    }
    
    private func setupViews() {
        self.view.addSubview(self.coverView)
        self.coverView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        self.view.addSubview(self.panelView)
        self.panelView.snp.makeConstraints { (make) in
            make.left.equalTo(45)
            make.right.equalTo(-45)
            make.center.equalTo(self.view)
        }
        self.panelView.addSubview(cancelButton)
        self.cancelButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.panelView).offset(0)
            make.right.equalTo(self.panelView).offset(0)
            make.height.equalTo(30)
            make.width.equalTo(30)
        }
        self.panelView.addSubview(self.titleLabel)
        self.titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.panelView).offset(50)
            make.top.equalTo(self.panelView).offset(16)
            make.right.equalTo(self.panelView).offset(-50)
            make.height.equalTo(23)
        }
        
        self.panelView.addSubview(self.messageLabel)
        self.messageLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.panelView).offset(10)
        make.top.equalTo(self.titleLabel.snp.bottom).offset(10)
            make.right.equalTo(self.panelView).offset(-10)
            make.height.equalTo(50)
        }
        
        let horizontalLine = UIView()
        horizontalLine.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        self.panelView.addSubview(horizontalLine)
        horizontalLine.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.panelView)
            make.top.equalTo(self.messageLabel.snp.bottom).offset(20)
            make.height.equalTo(1)
        }
        
        self.panelView.addSubview(self.leftButton)
        self.leftButton.snp.makeConstraints { (make) in
            make.left.bottom.equalTo(self.panelView)
            make.top.equalTo(horizontalLine.snp.bottom)
            make.height.equalTo(50)
        }
        
        let verticalLine = UIView()
        verticalLine.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        self.panelView.addSubview(verticalLine)
        verticalLine.snp.makeConstraints { (make) in
            make.top.equalTo(horizontalLine.snp.bottom)
            make.bottom.equalTo(self.panelView)
            make.width.equalTo(1)
            make.left.equalTo(self.leftButton.snp.right)
        }
        
        self.panelView.addSubview(self.rightButton)
        self.rightButton.snp.makeConstraints { (make) in
            make.right.bottom.equalTo(self.panelView)
            make.left.equalTo(verticalLine.snp.right)
            make.height.equalTo(self.leftButton)
            make.top.equalTo(horizontalLine.snp.bottom)
            make.width.equalTo(self.leftButton)
        }
    }
    
    private func updateViews() {
        if sort == "1"{
            self.messageLabel.text = "考勤时间快到了,赶紧进场签到吧!"
            self.rightButton.setTitle("不再提醒", for: .normal)
            self.leftButton.setTitle("进场签到", for: .normal)
        }else if sort == "2"{
            self.messageLabel.text = "收工时间了,不要忘记签出记工哦!"
            self.rightButton.setTitle("不再提醒", for: .normal)
            self.leftButton.setTitle("签出记工", for: .normal)
            
        }else if sort == "3"{
           self.messageLabel.text = "加班时间快到了,赶紧进场签个到吧!"
            self.rightButton.setTitle("不再提醒", for: .normal)
            self.leftButton.setTitle("进场签到", for: .normal)
            
        }
    }
    
    @objc private func doLeftButtonAction() {
        
        if sort == "1"{
    CommonUsed.setMigrantWorkerSignedAlert(demandBaseMigrantWorkerID: demandBaseMigrantWorkerID!, alertSort: "0") { (state) in
            WWSuccess("不再提醒")
        }
            
            
        }else if sort == "2"{
    CommonUsed.setMigrantWorkerSignedAlert(demandBaseMigrantWorkerID: demandBaseMigrantWorkerID!, alertSort: "1") { (state) in
                WWSuccess("不再提醒")
        }
            
        }else{
    CommonUsed.setMigrantWorkerSignedAlert(demandBaseMigrantWorkerID: demandBaseMigrantWorkerID!, alertSort: "2") { (state) in
                WWSuccess("不再提醒")
            }
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func doRightButtonAction() {
        if self.okHandler != nil {
            self.okHandler!()
        }
        if sort == "1"{
        self.signedIn { (isSuccess) in
            if isSuccess {
                //                self.updateAutoSignedInState()
                WWSuccess("签入成功!")
                
            }
            else {
                WWError("快速签入失败!")
            }
           
            self.dismiss(animated: true, completion: nil)
        }
        }else if sort == "2"{
            self.signedOut()
            self.dismiss(animated: true, completion: nil)
        }else{
            self.signedIn { (isSuccess) in
                if isSuccess {
                    //                self.updateAutoSignedInState()
                    WWSuccess("签入成功!")
                    
                }
                else {
                    WWError("快速签入失败!")
                }
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    //快速签入
    //快速签入
    fileprivate func signedIn(_ completion:@escaping (Bool) -> Void) {
        //        WWBeginLoadingWithStatus("自动签入中")
        var guid: String?
        
        let _dispatchGroup = DispatchGroup()
        
        _dispatchGroup.enter()
        UserModel.getGuid { (response) in
            if response.success {
                guid = response.value
            }
            else{
            }
            _dispatchGroup.leave()
        }
        
        _dispatchGroup.notify(queue: DispatchQueue.main) {
            guard guid != nil else {
                
                completion(false)
                return
            }
            guard self.demandBaseMigrantWorkerID != nil else{
                completion(false)
                return
            }
            RecordModel.fastRecord(guid:guid!,demandBaseMigrantWorkerId: self.demandBaseMigrantWorkerID!, signedType: SignedType.signedIn){(response) in
                if response.success {
                    
                    completion(true)
                    NotificationCenter.default.post(name: NSNotification.Name.init(kNotificationAlertSignedInSignedOut), object: nil)
                }
                else {
                    
                    completion(false)
                }
            }
        }
    }
    //快速签出
    fileprivate func signedOut() {
        WWBeginLoadingWithStatus("正在签出")
        var guid: String?
        let _dispatchGroup = DispatchGroup()
        
        _dispatchGroup.enter()
        UserModel.getGuid { (response) in
            if response.success {
                guid = response.value
            }
            else{
                WWError(response.message)
            }
            _dispatchGroup.leave()
        }
        
        _dispatchGroup.notify(queue: DispatchQueue.main) {
            guard guid != nil else {
                WWError("guid是空的")
                return
            }
            guard self.demandBaseMigrantWorkerID != nil else{
                return
            }
        RecordModel.fastRecord(guid:guid!,demandBaseMigrantWorkerId: self.demandBaseMigrantWorkerID!, signedType: SignedType.signedOut){(response) in
                if response.success {
                    WWSuccess("签出成功")
                    NotificationCenter.default.post(name: NSNotification.Name.init(kNotificationAlertSignedInSignedOut), object: nil)
                     self.dismiss(animated: true, completion: nil)
                }
                else {
                    WWError(response.message)
                }
            }
        }
    }
    
    
    
    @objc fileprivate func cancelButtonAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
extension ETSpecialAlertViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ETSpecialAlertViewControllerTransitionPresent()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ETSpecialAlertViewControllerTransitionDismiss()
    }
}

class ETSpecialAlertViewControllerTransitionPresent: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromView = transitionContext.view(forKey: .from)
        let toVc = transitionContext.viewController(forKey: .to) as! ETSpecialAlertViewController
        let container = transitionContext.containerView
        container.addSubview(toVc.view)
        
        let panelView = toVc.panelView
        let coverview = toVc.coverView
        coverview.backgroundColor = UIColor.black.withAlphaComponent(0.01)
        panelView.alpha = 0.3
        panelView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration:0.1, animations: {
            coverview.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            panelView.transform = CGAffineTransform.identity
            panelView.alpha = 1
        }) { (finish) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            toVc.view.addSubview(fromView!)
            toVc.view.sendSubview(toBack: fromView!)
        }
    }
}

class ETSpecialAlertViewControllerTransitionDismiss: NSObject,UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromVc = transitionContext.viewController(forKey: .from) as! ETSpecialAlertViewController
        
        //        let panelView = fromVc.panelView
        let coverview = fromVc.coverView
        UIView.animate(withDuration: 0.1, animations: {
            coverview.backgroundColor = UIColor.black.withAlphaComponent(0.01)
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}
