//
//  NotificationMapple.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/19.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class NotificationMapple: Mappable{
    var `Type` : String?
    var Params : NSDictionary?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        Type       <- map["Type"]
        Params       <- map["Params"]
    }
}
