//
//  RecordSupplyDisplayController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/21.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class RecordSupplyDisplayController: YZDisplayViewController {
    
    var projectModel: ProjectModel?
    var selectDate: Date?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.mg_backgroundGray
        self.title = dateToString(date: selectDate!)
        self.setupAllViewController()

        //滑块颜色
        self.setUpTitleEffect { (titleScrollViewColor, norColor, selColor, titleFont, titleHeight, titleWidth) in

            norColor?.pointee = colorWith255RGB(153, g: 153, b: 153)
            selColor?.pointee = UIColor.mg_blue
        }
        
        //滑块底部的横线
        self.setUpUnderLineEffect { (isUnderLineDelayScroll, underLineH, underLineColor, isUnderLineEqualTitleWidth) in
            underLineColor?.pointee = UIColor.mg_blue
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "异常轨迹", style: .done, target: self, action: #selector(pushAbnormalTrack))
    self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font:UIFont.systemFont(ofSize: 15)], for: .normal)
    }
    @objc fileprivate func pushAbnormalTrack(){
        let vc2 = RecordPathAbnormalController()
        vc2.title = "轨迹异常"
        vc2.currentDate = self.selectDate
        vc2.projectModel = self.projectModel
        self.navigationController?.pushViewController(vc2, animated: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        WWProgressHUD.dismiss()
    }
    func setupAllViewController() {
        let vc1 = RecordSupplyFormController()
        vc1.projectModel = self.projectModel
        vc1.selectDate = self.selectDate
        vc1.title = "补工"
        self.addChildViewController(vc1)
        
        let memorand = MemorandumViewController(nibName: "MemorandumViewController", bundle: nil)
        memorand.title = "记事"
        self.addChildViewController(memorand)
        
    }

}
