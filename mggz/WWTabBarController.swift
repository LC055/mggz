//
//  WWTabBarController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/7.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class WWTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        UINavigationBar.appearance().tintColor = colorWith255RGB(51, g: 51, b: 51)
        let userInfo = MWMineViewController()
        userInfo.addSignStateNotificationObserve()
        let userInfoNav = UINavigationController(rootViewController: userInfo)
        
        let recordVC = RecordHomeController()
        let recordNav = UINavigationController(rootViewController: recordVC)
        
//        let statisticsVc = StatisticsHomeController()
//        let statisticsNav = UINavigationController(rootViewController: statisticsVc)
        
        let salaryVc = AccountBalanceController()
        let salaryNav = UINavigationController(rootViewController: salaryVc)
        
        let itemTitles = ["考勤", "工资", "我"]
        let itemImages = ["签到-default", "工资-default","我的-default"]
    self.setViewControllers([recordNav,salaryNav ,userInfoNav], animated: true)
        for (index, item) in self.tabBar.items!.enumerated() {
            
            item.title = itemTitles[index]
            item.image = UIImage(named: itemImages[index])
            
        }
    }
    
}








