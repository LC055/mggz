//
//  EnumsModel.swift
//  mggz
//
//  Created by ShareAnimation on 2017/11/2.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

/// 收到jpush消息后，弹框提示的类型
enum AlertShowType: String {
    case autoSignedOut = "民工工资-自动签出"
    case autoSignedIn = "民工工资-自动签入"
    case trajectoryAnomaly = "民工工资-轨迹异常"
    case applyCallIn = "民工工资-调入申请"
    case addWorker = "民工工资-拉入民工"
    case salaryPayed = "民工工资-工资发放"
    case signInWarn = "民工工资-签入提醒"
    case signOutWarn = "民工工资-签出提醒"
    case overtimeWarn = "民工工资-加班提醒"
    case leaveIceWarn = "民工工资-离场提醒"
    case downLine = "民工工资-下线通知"
    case payroll = "民工工资-工资发放预告"
    case paymentSure = "民工工资-工资确认"
    case wagePayment = "民工工资-工资垫付"
}

//自动签入时的选项。ETSignedInAlertController
enum AlertSignedInOption: Int {
    case once = 0   //只收到一次并直接帮我签入
    case everyHalfHour //每半小时再提醒我一次
    case ignore //忽略并且之后不再提醒
}
