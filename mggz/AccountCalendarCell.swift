//
//  AccountCalendarCell
//  mggz
//
//  Created by ShareAnimation on 2017/8/8.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import FSCalendar
import YYText

class AccountCalendarCell: UITableViewCell,FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance {
    var noData: Bool = true {
        willSet {
            self.noData = newValue
            self.updateViews()
        }
    }
    
//    var cellButton: UIButton = {
//        let button = UIButton()
//        button.setTitle("2017-06-20", for: .normal)
//        button.isEnabled = false
//        return button
//    }()
//    
//    var arrowImageView:UIImageView = {
//        let arrowImageView = UIImageView()
//        arrowImageView.contentMode = .scaleAspectFit
//        arrowImageView.image = UIImage.init(named: "more_unfold")?.withRenderingMode(.alwaysTemplate)
//        return arrowImageView
//    }()

    lazy var calendar: MMCalendar = {
        let calendar = MMCalendar()
//        calendar.allowsSelection = false
        calendar.backgroundColor = UIColor.white
        calendar.scrollEnabled = false
        calendar.calendarHeaderView.isUserInteractionEnabled = true
        calendar.calendarHeaderView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(doTapCalendarHeaderViewAction)))
        return calendar
    }()
    
    var blueExplainLabel: UILabel = {
        let label = UILabel()
        label.text = "正常"
        label.font = wwFont_Regular(14)
        label.textColor = colorWith255RGB(102, g: 102, b: 102)
        
        return label
    }()
    
    var pinkExplainLabel:UILabel = {
        let pinkExplainLabel = UILabel()
        pinkExplainLabel.text = "异常"
        pinkExplainLabel.font = wwFont_Regular(14)
        pinkExplainLabel.textColor = colorWith255RGB(102, g: 102, b: 102)
        return pinkExplainLabel
    }()
    
    //左右滑动月份时触发
    var currentMonthChangeHandler: ((Date) -> Void)?
    
//    let panel = UIView()
    
//    var showCalendar = false {
//        willSet {
//            self.showCalendar = newValue
//            self.calendar.isHidden = !newValue
//        }
//    }
    
    var selectDateHandler: ((Date) -> Void)?
    var recordSupplyLists: [RecordSupply]? {
        willSet {
            self.recordSupplyLists = newValue
            self.calendar.reloadData()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.contentView.backgroundColor = UIColor.mg_backgroundGray
        
        self.setupViews()
    }
    
    func setupViews() {
//        panel.addSubview(self.cellButton)
//        self.cellButton.snp.makeConstraints({ (make) in
//            make.left.top.bottom.equalTo(panel)
//        })
//        
//        panel.addSubview(arrowImageView)
//        arrowImageView.snp.makeConstraints({ (make) in
//            make.left.equalTo(cellButton.snp.right)
//            make.right.equalTo(panel)
//            make.centerY.equalTo(panel)
//            make.width.height.equalTo(25)
//        })
        
//        
//        self.contentView.addSubview(panel)
//        panel.snp.makeConstraints({ (make) in
//            make.centerX.equalTo(self.contentView)
//            make.top.equalTo(self.contentView)
//            make.height.equalTo(50)
//        })
        
        self.calendar.dataSource = self
        self.calendar.delegate = self
        self.contentView.addSubview(self.calendar)
        self.calendar.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 360)
//        self.calendar.snp.makeConstraints { (make) in
//            make.top.right.left.equalTo(self.contentView)
//        }


        let bottomPanel = UIView()
        self.contentView.addSubview(bottomPanel)
        bottomPanel.backgroundColor = UIColor.white
        bottomPanel.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.contentView)
            make.top.equalTo(self.calendar.snp.bottom).offset(10)
            make.height.equalTo(30)
            make.bottom.equalTo(self.contentView)
        }
        
        let blueRoundView = UIView()
        blueRoundView.backgroundColor = UIColor.mg_blue
        blueRoundView.layer.cornerRadius = 8
        bottomPanel.addSubview(blueRoundView)
        blueRoundView.snp.makeConstraints { (make) in
            make.left.equalTo(bottomPanel).offset(30)
            make.centerY.equalTo(bottomPanel)
            make.height.width.equalTo(16)
        }
        
        
       
        bottomPanel.addSubview(self.blueExplainLabel)
        self.blueExplainLabel.snp.makeConstraints { (make) in
            make.left.equalTo(blueRoundView.snp.right).offset(8)
            make.centerY.equalTo(bottomPanel)
        }
        
        let pinkRoundView = UIView()
        pinkRoundView.backgroundColor = UIColor.mg_pink
        pinkRoundView.layer.cornerRadius = 8
        bottomPanel.addSubview(pinkRoundView)
        pinkRoundView.snp.makeConstraints { (make) in
            make.left.equalTo(blueExplainLabel.snp.right).offset(15)
            make.centerY.equalTo(bottomPanel)
            make.height.width.equalTo(blueRoundView)
        }
        
        bottomPanel.addSubview(self.pinkExplainLabel)
        self.pinkExplainLabel.snp.makeConstraints { (make) in
            make.left.equalTo(pinkRoundView.snp.right).offset(8)
            make.centerY.equalTo(bottomPanel)
        }

        let settlementButton = YYLabel()
        settlementButton.isUserInteractionEnabled = true
        settlementButton.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(doSettlementAction)))
        let attributedString = NSMutableAttributedString(string: "申请结算工资")
        let settlementImageView = UIImageView(frame: CGRect.init(x: 0, y: 0, width: 20, height: 20))
        settlementImageView.image = UIImage(named: "Icon_右箭头")!.withRenderingMode(.alwaysTemplate)
        settlementImageView.tintColor = UIColor.mg_blue
        let attachAttributed = NSAttributedString.yy_attachmentString(withContent: settlementImageView, contentMode: .scaleAspectFit, attachmentSize: CGSize.init(width: 20, height: 20), alignTo: wwFont_Regular(16), alignment: .center)
        attributedString.append(attachAttributed)
        attributedString.yy_font = wwFont_Regular(16)
        attributedString.yy_alignment = .center
        attributedString.yy_color = UIColor.mg_blue
        settlementButton.attributedText = attributedString
//        bottomPanel.addSubview(settlementButton)
//        settlementButton.snp.makeConstraints { (make) in
//            make.top.bottom.equalTo(bottomPanel)
//            make.right.equalTo(bottomPanel).offset(-20)
//            make.width.equalTo(120)
//        }
    }
    
    func updateViews() {
    }
    
    @objc func doTapCalendarHeaderViewAction() {
        var responder = self.next
        while !(responder is UIViewController) {
            responder = responder?.next
        }
        if responder is UIViewController {
            let vc = responder as! UIViewController
            
            let toVc = DateAlertController.init({ (date) in
                self.calendar.currentPage = date
            })
            toVc.date = self.calendar.currentPage
            vc.present(toVc, animated: true, completion: nil)
        }
    }
    
    @objc func doSettlementAction() {
        let alertController = ETTextViewAlertController.init("") {(resultString) in
            
        }
        Tool.topViewController().present(alertController, animated: true, completion: nil)
    }
    
    //Delegate,点击日历上的日期item会调用
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        if let closure = self.selectDateHandler {
            closure(date)
        }
    }
    
    //月份变化后触发
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        if let handler = self.currentMonthChangeHandler {
            handler(calendar.currentPage)
        }
    }

    
    //对应date的cell的背景色
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        if let lists = self.recordSupplyLists {
            for item in lists {
                if date == item.signedDate {
                    if item.isPayed {
                        return colorWith255RGB(73, g: 169, b: 238)
                    }
                    else {
                        return colorWith255RGB(244, g: 110, b: 101)
                    }
                }
            }
        }
        return nil
    }
    //修改指定日期的颜色
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        if let lists = self.recordSupplyLists {
            for item in lists {
                if date == item.signedDate {
                    return UIColor.white
                }
            }
        }
        return colorWith255RGB(66, g: 66, b: 66)
    }

    //去掉选中色，titleSelectionColorFor指cell中文字色，fillSelectionColorFor指cell背景色
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {
        if let lists = self.recordSupplyLists {
            for item in lists {
                if date == item.signedDate {
                    return UIColor.white

                }
            }
        }
        return UIColor.black
    }
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        if let lists = self.recordSupplyLists {
            for item in lists {
                if date == item.signedDate {
                    if item.isPayed {
                        return colorWith255RGB(73, g: 169, b: 238)
                    }
                    else {
                        return colorWith255RGB(244, g: 110, b: 101)
                    }
                }
            }
        }
        return UIColor.clear
    }


}
