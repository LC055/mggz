//
//  ETAdvancePayViewController.swift
//  mggz
//
//  Created by QinWei on 2018/5/30.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class ETAdvancePayViewController: UIViewController {

    fileprivate var coverView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        return view
    }()
    
    fileprivate lazy var panelView:WorkerSalaryFPreView = {
        let view = WorkerSalaryFPreView.init(frame: CGRect.zero)
        return view
    }()
    
    
    
    private var okHandler: (()->Void)?
    private var message: String?
    
    //当前alert是否正在显示
    private var isShowing:Bool = false
    
    init (_ message: String, okHandler: @escaping ()-> Void){
        super.init(nibName: nil, bundle: nil)
        self.transitioningDelegate = self
        self.okHandler = okHandler
        self.message = message
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.setupViews()
        self.updateViews()
        
        self.isShowing = true
    }
    deinit {
        self.isShowing = false
    }
    
    private func setupViews() {
        
        self.view.addSubview(self.coverView)
        self.coverView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        self.panelView.companyLabel.sizeToFit()
        self.view.addSubview(self.panelView)
        let topH = self.panelView.companyLabel.frame.height + 206
        self.panelView.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.centerY.equalTo(self.view)
            make.height.equalTo(topH)
        }
    }
    
    private func updateViews() {
        
    }
    
    @objc private func doLeftButtonAction() {
        self.dismiss(animated: true, completion: nil)
    }
    @objc private func doRightButtonAction() {
        if self.okHandler != nil {
            self.okHandler!()
        }
        self.dismiss(animated: true, completion: nil)
    }
}
extension ETAdvancePayViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SimpleAlertControllerTransitionPresent()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SimpleAlertControllerTransitionDismiss()
    }
}

class ETAdvancePayViewControllerTransitionPresent: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromView = transitionContext.view(forKey: .from)
        let toVc = transitionContext.viewController(forKey: .to) as! ETAdvancePayViewController
        let container = transitionContext.containerView
        container.addSubview(toVc.view)
        
        let panelView = toVc.panelView
        let coverview = toVc.coverView
        coverview.backgroundColor = UIColor.black.withAlphaComponent(0.01)
        panelView.alpha = 0.3
        panelView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        UIView.animate(withDuration:0.1, animations: {
            coverview.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            panelView.transform = CGAffineTransform.identity
            panelView.alpha = 1
        }) { (finish) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            toVc.view.addSubview(fromView!)
            toVc.view.sendSubview(toBack: fromView!)
        }
    }
}

class ETAdvancePayViewControllerTransitionDismiss: NSObject,UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromVc = transitionContext.viewController(forKey: .from) as! ETAdvancePayViewController
        
        // let panelView = fromVc.panelView
        let coverview = fromVc.coverView
        UIView.animate(withDuration: 0.1, animations: {
            coverview.backgroundColor = UIColor.black.withAlphaComponent(0.01)
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}
