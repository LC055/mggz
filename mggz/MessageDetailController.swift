//
//  MessageDetailController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/26.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class MessageDetailController: UIViewController {
    
    var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    //传入参数
    var descriptionString: String? {
        willSet {
            if newValue != nil {
                self.descriptionLabel.text = newValue!
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "消息详情"
        self.view.backgroundColor = UIColor.white

        self.view.addSubview(descriptionLabel)
        self.descriptionLabel.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
        }
    }


}
