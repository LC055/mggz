//
//  AppDefine.swift
//  mggz
//
//  Created by ShareAnimation on 2017/8/4.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

let SCREEN_WIDTH = UIScreen.main.bounds.size.width;
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height;

//正式环境

let URL_Header = "http://app.winshe.cn/"
let URL_Json_Header = "http://a.winshe.cn:8020"
let API_HOME_TEST = "http://a.winshe.cn:8020"
let LC_DetailUrl = "http://a.winshe.cn:8020/api/Core/CallMethod"
let LC_IMAGE_URL = "http://app.winshe.cn/upload/images/"
let certificationDetailUrl = "http://app.winshe.cn/upload/Files/%E6%B3%A8%E5%86%8C%E5%8D%8F%E8%AE%AE/%E5%A4%AA%E5%85%AC%E6%B0%91%E5%B7%A5%E6%B3%A8%E5%86%8C%E5%8D%8F%E8%AE%AE.html"
let CheckNewVasionUrl = "http://app.winshe.cn/WebService.asmx/GetAppVersion2?ClientType=1&productType=2"
let MutipleAccountUrl = "http://app.winshe.cn/WebService.asmx/GetMultiUserAccountList?"
let DealWithMutipleAccountUrl = "http://app.winshe.cn/WebService.asmx/SetMultiUserAccount?"
let VidifySecurityUrl = "http://app.winshe.cn/WebService.asmx/CheckPhoneVerifyCode"
 let ReRegisterAccountUrl = "http://120.27.136.165:8050/WebService.asmx/ReRegisterMigrantWorker"


//测试环境


/*let URL_Header = "http://120.27.136.165:8050/"
let URL_Json_Header = "http://120.27.136.165:8020"
let API_HOME_TEST = "http://120.27.136.165:8020"
let LC_DetailUrl = "http://120.27.136.165:8020/api/Core/CallMethod"
let LC_IMAGE_URL = "http://120.27.136.165:8050/upload/images/"
let certificationDetailUrl = "http://120.27.136.165:8050/upload/Files/%E6%B3%A8%E5%86%8C%E5%8D%8F%E8%AE%AE/%E5%A4%AA%E5%85%AC%E6%B0%91%E5%B7%A5%E6%B3%A8%E5%86%8C%E5%8D%8F%E8%AE%AE.html"
let CheckNewVasionUrl = "http://120.27.136.165:8050/WebService.asmx/GetAppVersion2?ClientType=1&productType=2"
let MutipleAccountUrl = "http://120.27.136.165:8050/WebService.asmx/GetMultiUserAccountList?"
let DealWithMutipleAccountUrl = "http://120.27.136.165:8050/WebService.asmx/SetMultiUserAccount?"
let VidifySecurityUrl = "http://120.27.136.165:8050/WebService.asmx/CheckPhoneVerifyCode"
let ReRegisterAccountUrl = "http://120.27.136.165:8050/WebService.asmx/ReRegisterMigrantWorker"*/
 

/**关于预支费率的接口***/

let PrepaidExpenseUrl = "http://app.winshe.cn/upload/Files/%E6%B3%A8%E5%86%8C%E5%8D%8F%E8%AE%AE/%E5%85%B3%E4%BA%8E%E6%89%8B%E7%BB%AD%E8%B4%B9%E7%9A%84%E8%AF%B4%E6%98%8E.html"
//中间路径
let imageKeyUrl = "upload/images/"
let WebServiceURL = URL_Header + "WebService.asmx"
let JsonAPIURL = URL_Json_Header + "/api/Core/"
let ImageUrlPrefix = URL_Header + imageKeyUrl //获取图片信息的url前缀
//测试环境的基本路径
//中缀
let API_INFIX_CALLMETHOD = "/api/Core/"

//基本上的访问接口
let API_CALL_METHOD = API_HOME_TEST + API_INFIX_CALLMETHOD + "CallMethod"
let LCBaiduMapKeySubmit = "vUoFraoTWAZGnghnq43LMFG29XROqhq5"
let LCBaiduMapKey = "KzzK3sCCAKZFeq51mVulwh2NNRZzAOwl"
/******通知******/
let kNotificationSelectedProjectChanged = "kNotificationSelectedProjectChanged"
let kNotificationUserInfoChanged = "kNotificationUserInfoChanged"
let kNotificationWithdrawComplete = "kNotificationWithdrawComplete"//提现成功
let kNotificationAlertSignedInSignedOut = "kNotificationAlertSignedInSignedOut"//弹框快速签入签出成功时


/**************关于包工头项目的全局常量***********************/

let LCMainProjectNotification = "LCMainProjectNotification"
let LCNotificationMainRefresh = "LCNotificationMainRefresh"

//民工端的通知
let MWNotificationMainRefresh = "MWNotificationMainRefresh"
let MWSignStateNotification = "MWSignStateNotification"
//百度云的文字识别
let BaiduYunAppID = "10555491"
let BaiduYunAPIKey = "mTxh5ViATivG1ggBUNotvIGX"
let BaiduYunSecretKey = "hdPaynU2ZcaxpfIEWsZ9CNNpXncCpNfA"
//UDID存储
let UdidKeyWord = "mgUDID"
let firstScreenW : CGFloat = 320
let secondScreenW : CGFloat = 375
let thirdScreenW : CGFloat = 414
class AppDefine: NSObject {

}
//获取http header
func mobile_cilent_headers() -> [String: String]? {
    if let token = WWUser.sharedInstance.token {
        return ["Authorization":"Bearer" + " " + token]
    }
    else {
        
        return nil
    }
}

func isFirstUse() -> Bool {
    let userDefault = UserDefaults.standard
    if let isFirstUse = userDefault.object(forKey: "isFirstUse") as? Bool {
        if isFirstUse {
            userDefault.set(false, forKey: "isFirstUse")
            return true
        }
        else {
            return false
        }
    }
    else {
        userDefault.set(false, forKey: "isFirstUse")
        return true
    }
}

//是否有新通知未读
var isNewNotificationUnread: Bool {
    set {
        let userdefault = UserDefaults.standard
        userdefault.set(newValue, forKey: "khasNewNotificationUnread")
    }
    get {
        let userdefault = UserDefaults.standard
        if let boolValue = userdefault.object(forKey: "khasNewNotificationUnread") as? Bool {
            return boolValue
        }
        else {
            return false
        }
    }

}

func getCurrentDateString() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let date = Date()
    let dateString = formatter.string(from: date)
    return dateString
}

// 字典转字符串
func getJSONStringFromDictionary(dictionary:NSDictionary) -> String {
    if (!JSONSerialization.isValidJSONObject(dictionary)) {
        
        return ""
    }
    let data : NSData! = try? JSONSerialization.data(withJSONObject: dictionary, options: []) as NSData!
    let JSONString = NSString(data:data as Data,encoding: String.Encoding.utf8.rawValue)
    return JSONString! as String
}

//json字符串转字典
func getDictionaryFromJsonString(jsonString: String) -> [String: AnyObject]? {
    let jsonData = jsonString.data(using: .utf8)!
    let dic = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
    return dic as? [String : AnyObject]
}

func mwDateToString(date: Date) -> String{
    let format = DateFormatter()
    format.dateFormat = "MM月dd日"
    let dateString = format.string(from: date)
    return dateString
}


//date格式转string yyyy-MM-dd
func dateToString(date: Date) -> String{
    let format = DateFormatter()
    format.dateFormat = "yyyy-MM-dd"
    let dateString = format.string(from: date)
    return dateString
}

func dateToTimeString(date: Date) -> String{
    let format = DateFormatter()
    format.dateFormat = "HH:mm"
    let dateString = format.string(from: date)
    return dateString
}

func dateToDetailTimeString(date: Date) -> String{
    let format = DateFormatter()
    format.dateFormat = "HH:mm:ss"
    let dateString = format.string(from: date)
    return dateString
}
//字典转字符串
func putDictionaryToJSONString(dictionary:NSDictionary) -> String {
    if (!JSONSerialization.isValidJSONObject(dictionary)) {
        return ""
    }
    let data : NSData! = try! JSONSerialization.data(withJSONObject: dictionary, options: []) as NSData?
    let JSONString = NSString(data:data as Data,encoding: String.Encoding.utf8.rawValue)
    return JSONString! as String
    
}
//date格式转Stirng yyyy-MM-dd HH:mm:ss
func dateTimeToString(date: Date) -> String {
    let format = DateFormatter()
    format.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let dateString = format.string(from: date)
    return dateString
}

//date格式转String，yyyy-MM-dd HH:mm格式
func dateToStringWithNoSecond(date: Date) -> String {
    let format = DateFormatter()
    format.dateFormat = "yyyy-MM-dd HH:mm"
    let dateString = format.string(from: date)
    return dateString
}

//string转date {
func stringToDate(dateString: String) -> Date? {
    let formate = DateFormatter()
    formate.dateFormat = "yyyy-MM-dd"
    let date = formate.date(from: dateString)

    return date
}

func stringToTimeDate(dateString: String) -> Date? {
    let formate = DateFormatter()
    formate.dateFormat = "HH:mm"
    let date = formate.date(from: dateString)
    return date
}
func stringToTimeHMSDate(dateString: String) -> Date? {
    let formate = DateFormatter()
    formate.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let date = formate.date(from: dateString)
    return date
}
/*
//后台传过来的带"T"的时间字符串转Date
func dateStringWithTToDate(dateString: String) -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
   // dateFormatter.locale = NSLocale.init(localeIdentifier: "zh_CN") as Locale?
    let date1 = dateFormatter.date(from: dateString)
    if date1 == nil {
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
      //  dateFormatter.locale = NSLocale.init(localeIdentifier: "zh_CN") as Locale?
        let date2 = dateFormatter.date(from: dateString)
        return worldTimeToChinaTime(date: date2!)
    }
    return worldTimeToChinaTime(date: date1!)
}
 */
func dateStringWithTToDate(dateString: String) -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    let date1 = dateFormatter.date(from: dateString)
    if date1 == nil {
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date2 = dateFormatter.date(from: dateString)
        return date2
    }
    return date1
}
func worldTimeToChinaTime(date:Date)-> Date? {
    let timeZone = NSTimeZone.system
    let interval  = timeZone.secondsFromGMT(for: date)
    let localDate = date.addingTimeInterval(TimeInterval(interval))
    return localDate
}



//
////数字转金钱格式
//func numberToMoney(_ numbers: NSNumber) -> String? {
//    let moneyFormatter = NumberFormatter()
//    moneyFormatter.numberStyle = .decimal
//    return moneyFormatter.string(from: numbers)
//}

//获取银行卡尾号4位
func lastFourNumbers(_ numberString: String) -> String {
    if numberString.characters.count <= 4 {
        return numberString
    }
    let index = numberString.index(numberString.endIndex, offsetBy: -4)
    let lastFourNum = numberString.substring(from: index)
    return lastFourNum
}

//图片转Data
func compressImage(_ image: UIImage) -> Data {
    let size = scaleSize(image.size)
    UIGraphicsBeginImageContext(size)
    image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    let maxFileSize = 500 * 1024
    var compressionRatio: CGFloat = 0.7
    let maxCompressionRatio: CGFloat = 0.1
    
    var imageData = UIImageJPEGRepresentation(scaledImage!, compressionRatio)
    
    while imageData!.count > maxFileSize && compressionRatio > maxCompressionRatio {
        compressionRatio -= 0.1
        imageData = UIImageJPEGRepresentation(image, compressionRatio)
    }
    
    return imageData!
}

func scaleSize(_ sourceSize: CGSize) -> CGSize {
    let width = sourceSize.width
    let height = sourceSize.height
    if width >= height {
        return CGSize(width: 800, height: 800 * height / width)
    }
    else {
        return CGSize(width: 800 * width / height, height: 800)
    }
}

func wwFont_Semibold(_ fontSize: CGFloat) -> UIFont {
    return wwFont(fontSize, fontName: "Semibold")
}

func wwFont_Regular(_ fontSize: CGFloat) -> UIFont {
    return wwFont(fontSize, fontName: "Regular")
}

func wwFont_Light(_ fontSize: CGFloat) -> UIFont {
    return wwFont(fontSize, fontName: "Light")
}

func wwFont_Medium(_ fontSize: CGFloat) -> UIFont {
    return wwFont(fontSize, fontName: "Medium")
}

fileprivate func wwFont(_ fontSize: CGFloat, fontName: String) -> UIFont {
    
    return UIFont(name: "PingFangSC-" + fontName, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
}
// MARK:- <自定义方法>

