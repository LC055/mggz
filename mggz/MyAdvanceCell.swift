//
//  MyAdvanceCell.swift
//  mggz
//
//  Created by QinWei on 2018/3/26.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class MyAdvanceCell: UITableViewCell {

    @IBOutlet weak var prepaidLabel: UILabel!
    
    @IBOutlet weak var fromTime: UILabel!
    
    @IBOutlet weak var endTime: UILabel!
    
    @IBOutlet weak var leftView: UIView!
    
    @IBOutlet weak var rightView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func setupCellDataWithModel(_ model:MyPrepaidModel) {
        
        if SCREEN_WIDTH <= CGFloat(320) {
            self.prepaidLabel.font = UIFont.systemFont(ofSize: 17)
        }
        self.contentView.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        self.leftView.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        self.rightView.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        
        if let startTime = model.CreateTime{
            let index = startTime.index(startTime.startIndex, offsetBy: 10)
            self.fromTime.text = startTime.substring(to: index)
        }
        if let finishTime = model.ExpiryDate{
            let index = finishTime.index(finishTime.startIndex, offsetBy: 10)
            self.endTime.text = finishTime.substring(to: index)
        }
        
        if let amount = model.PrepaidAmount{
            let amountObj = NSNumber.init(value: amount)
            let valueStr = Tool.numberToMoney("\(amountObj)")
            self.prepaidLabel.text = "预支" + valueStr! + "元"
        }
    }
}








