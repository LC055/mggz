//
//  PassWordSetViewController.swift
//  mggz
//
//  Created by QinWei on 2018/3/19.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class PassWordSetViewController: UIViewController {
    
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var bottomView: UIView!
    let titleArray = ["重置登录密码","重置交易密码"]
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "密码设置"
        self.view.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        let topTap = UITapGestureRecognizer(target: self, action: #selector(topViewClick))
        topView.addGestureRecognizer(topTap)
        
        let bottomTap = UITapGestureRecognizer(target: self, action: #selector(bottomViewClick))
        bottomView.addGestureRecognizer(bottomTap)
        
    }
    
    @objc fileprivate func topViewClick(){
        let vc = PwdRestViewController(nibName: "PwdRestViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @objc fileprivate func bottomViewClick(){
        let vc = SetPaymentPasswordController()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

