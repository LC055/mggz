//
//  NotificationController.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/30.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class NotificationController: UIViewController {
    
    fileprivate var _tableView: UITableView!
    fileprivate var tableView: UITableView {
        get {
            if _tableView != nil {
                return _tableView
            }
            _tableView = UITableView()
            _tableView.delegate = self
            _tableView.dataSource = self
            _tableView.backgroundColor = UIColor.clear
            _tableView.separatorStyle = .none
            _tableView.tableFooterView = UIView()
            _tableView.showsVerticalScrollIndicator = false
            
            regClass(_tableView, cell: UITableViewCell.self)
            
            return _tableView
        }
    }

    var isNotificationOk = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.mg_backgroundGray
        
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        self.currentNotificationSetting()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        WWProgressHUD.dismiss()
    }
    
    private func currentNotificationSetting() {
        if (UIDevice.current.systemVersion as NSString).floatValue >= 8 {
            if let setting = UIApplication.shared.currentUserNotificationSettings {
                if setting.types == UIUserNotificationType.init(rawValue: 0) {
                    
                    self.isNotificationOk = false
                }
                else {
                    self.isNotificationOk = true
                }
                self.tableView.reloadData()
            }
            
        }
    }
}

extension NotificationController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .value1, reuseIdentifier: "\(UITableViewCell.self)")
        cell.selectionStyle = .none
        cell.textLabel?.text = "新消息通知"
        if self.isNotificationOk {
            cell.detailTextLabel?.text = "推送已经设置"
            cell.accessoryType = .none
        }
        else {
            cell.detailTextLabel?.text = "去设置"
            cell.accessoryType = .disclosureIndicator
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !self.isNotificationOk {
            UIApplication.shared.openURL(URL.init(string: UIApplicationOpenSettingsURLString)!)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        
        let label = UILabel()
        label.text = "请在iPhone的\"设置\" - \"通知\"中进行修改。"
        label.font = wwFont_Regular(13)
        view.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.centerY.equalTo(view)
            make.left.equalTo(view).offset(15)
        }
        
        return view
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
}
