//
//  PrepaidDetailCell.swift
//  mggz
//
//  Created by QinWei on 2018/3/27.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class PrepaidDetailCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var salaryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setupCellDataWithModel(_ model:PrepaidDetailModel){
       
        if let amount = model.PrepaidAmount{
            let amountObj = NSNumber.init(value: amount)
            let valueStr = Tool.numberToMoney("\(amountObj)")
            if valueStr!.contains("-"){
                self.salaryLabel.text = valueStr
            }else{
                self.salaryLabel.text = "+" + valueStr!
            }
        }
        self.titleLabel.text = model.PrepaidType ?? "暂无"

        if let prepaidTime = model.PrepaidTime{
            let prepaidStr = prepaidTime.replacingOccurrences(of: "T", with: "  ")
            let index = prepaidStr.index(prepaidStr.startIndex, offsetBy: 20)
            self.timeLabel.text = prepaidStr.substring(to: index)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
