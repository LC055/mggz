//
//  LoginAuthenticationService.swift
//  mggz
//
//  Created by ShareAnimation on 2017/10/17.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import KeychainAccess
let kUserDefaultUsename = "kUserDefaultUsename"
let kUserDefaultPassword = "kUserDefaultPassword"

let kLastUserDefaultUsename = "kLastUserDefaultUsename"
let kLastUserDefaultPassword = "kLastUserDefaultPassword"

class LoginAuthenticationService: NSObject {

    class func save(username: String, password: String) {
        UserDefaults.standard.setValue(username, forKey: kUserDefaultUsename)
        UserDefaults.standard.setValue(password, forKey: kUserDefaultPassword)
        UserDefaults.standard.synchronize()
        LoginAuthenticationService.saveLast(username: username, password: password)
    }
    class func setupIngore(username: String) {
        UserDefaults.standard.setValue(username, forKey: "ingore")
    }
    class func getupIngore() -> (String?) {
        let ingoreStr = UserDefaults.standard.string(forKey: "ingore")
        return ingoreStr
    }
    
    class func setupShockSound(shock: String) {
        UserDefaults.standard.setValue(shock, forKey: "shock")
    }
    
    class func getupShockSound() -> (String?) {
        let shock = UserDefaults.standard.string(forKey: "shock")
        return shock
    }
    
    class func setupRingtone(ring: String) {
        UserDefaults.standard.setValue(ring, forKey: "ring")
    }
    
    class func getupRingtone() -> (String?) {
        let ring = UserDefaults.standard.string(forKey: "ring")
        return ring
    }
    
    class func setupIsRangeData(range: String) {
        UserDefaults.standard.setValue(range, forKey: "range")
    }
    
    class func getupIsRangeData() -> (String?) {
        let range = UserDefaults.standard.string(forKey: "range")
        return range
    }
    class func setupcurrentDate(range: String) {
        UserDefaults.standard.setValue(range, forKey: "currentDate")
    }
    
    class func getupcurrentDate() -> (String?) {
        let range = UserDefaults.standard.string(forKey: "currentDate")
        return range
    }
    
    class func getUDIDWithKeychain(jpushID:String) ->(String?) {
       let keychain = Keychain(service: "com.winshe.jtg.mggz")
        if let deviceMac = try? keychain.get(UdidKeyWord) {
            if deviceMac == nil{
                var deviceUDID:String?
                deviceUDID = UIDevice.current.identifierForVendor?.uuidString
                
                if deviceUDID == nil{
                    deviceUDID = jpushID
                }
                let selectUDID = deviceUDID?.replacingOccurrences(of: "-", with: "")
                keychain[UdidKeyWord] = selectUDID
                return selectUDID
            }
            return deviceMac
        } else {
            var deviceUDID:String?
            deviceUDID = UIDevice.current.identifierForVendor?.uuidString
            
            if deviceUDID == nil{
                deviceUDID = jpushID
            }
            let selectUDID = deviceUDID?.replacingOccurrences(of: "-", with: "")
            keychain[UdidKeyWord] = selectUDID
            return selectUDID
        }
    }
    
    
    class func removeUsernameAndPassword() {
        //UserDefaults.standard.removeObject(forKey: kUserDefaultUsename)
        UserDefaults.standard.removeObject(forKey: kLastUserDefaultPassword)
    }
    
    //上一次使用的用户名和密码
    class func saveLast(username: String, password: String) {
        UserDefaults.standard.setValue(username, forKey: kLastUserDefaultUsename)
        UserDefaults.standard.setValue(password, forKey: kLastUserDefaultPassword)
        UserDefaults.standard.synchronize()
    }
    
    class func getLastUsernameAndPassword() -> (String?, String?) {
        let username = UserDefaults.standard.string(forKey: kLastUserDefaultUsename)
        let password = UserDefaults.standard.string(forKey: kLastUserDefaultPassword)
        return (username, password)
    }
   
    
    
    
    class func verifyLoginStatus(_ completion: ((WWResponse) -> Void)?) {
        let username = UserDefaults.standard.string(forKey: kUserDefaultUsename)
        let password = UserDefaults.standard.string(forKey: kUserDefaultPassword)
        
        if username == nil || password == nil {
           
            if completion != nil {
                completion!(WWResponse.init(success: false, message: "没有登录记录"))
            }
        }
        else {
            //WWBeginLoadingWithStatus("检查登录状态")
            UserModel.login(username: username!, password: password!, completionHandler: { (response) in
                if response.success {
                   
                    if completion != nil {
                        completion!(WWResponse.init(success: true))
                    }
                    
                }
                else {
                    
                    if completion != nil {
                        completion!(WWResponse.init(success: false, message: response.message))
                    }
                }
               // WWEndLoading()
            })
        }
    }
}
