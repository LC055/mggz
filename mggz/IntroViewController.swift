//
//  IntroViewController.swift
//  SwiftDemo2
//
//  Created by ShareAnimation on 2017/9/30.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import EAIntroView
import YYText
import CoreLocation
class IntroViewController: UIViewController,EAIntroDelegate {
    
    var bottomPanel: UIView = {
        let view = UIView()
        return view
    }()
    var locationManager:CLLocationManager?
    lazy var registerButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.white
        button.setTitle("免费注册", for: .normal)
        button.setTitleColor(colorWithRGB(r: 61, g: 151, b: 255), for: .normal)
        button.titleLabel?.font = UIFont(name: "PingFangSC-Regular", size: 16)
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize(width: 1, height: 1)
        button.layer.shadowRadius = 2
        button.layer.shadowOpacity = 0.3
        button.layer.cornerRadius = 20
        button.addTarget(self, action: #selector(doRegisterAction), for: .touchUpInside)
        return button
    }()
    
    lazy var loginLabel: YYLabel = {
        let label = YYLabel()
        
        let yyText = NSMutableAttributedString(string: "已有账号请 登录")
        yyText.yy_setAttribute(NSAttributedStringKey.underlineStyle.rawValue, value: NSNumber.init(value: NSUnderlineStyle.styleSingle.rawValue), range: NSRange.init(location: 6, length: 2))
        yyText.yy_color = colorWithRGB(r: 153, g: 153, b: 153)
        yyText.yy_setTextHighlight(NSRange.init(location: 6, length: 2), color: colorWith255RGB(16, g: 142, b: 233), backgroundColor: UIColor.clear) { [unowned self](view, text, range, rect) in
            self.doLoginAction()
        }
        yyText.yy_alignment = .center
        yyText.yy_font = UIFont(name: "PingFangSC-Regular", size: 15)
        label.attributedText = yyText
        
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
       
        //初始化铃声与震动
        LoginAuthenticationService.setupShockSound(shock: "1")
        LoginAuthenticationService.setupRingtone(ring: "1")
        
        self.locationManager = CLLocationManager.init()
        self.locationManager?.delegate = self
        
    self.locationManager?.pausesLocationUpdatesAutomatically = false
    self.locationManager?.allowsBackgroundLocationUpdates = true
        self.locationManager?.requestAlwaysAuthorization()
        self.locationManager?.startUpdatingLocation()
        self.showIntroView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        WWProgressHUD.dismiss()
    }
    func showIntroView() {
        let page1 = EAIntroPage()
        page1.bgImage = UIImage(named: "boot_page1")
        let page2 = EAIntroPage()
        page2.bgImage = UIImage(named: "boot_page2")
        let page3 = EAIntroPage()
        page3.bgImage = UIImage(named: "boot_page3")
        
        let intro = EAIntroView.init(frame: self.view.bounds, andPages: [page1,page2,page3])
        intro?.delegate = self
        intro?.skipButton.isHidden = true
        intro?.swipeToExit = false
        intro?.pageControlY = 135
        intro?.pageControl.pageIndicatorTintColor = colorWithRGB(r: 240, g: 247, b: 255)
        intro?.pageControl.currentPageIndicatorTintColor = colorWithRGB(r: 188, g: 219, b: 255)
        
        intro?.show(in: self.view, animateDuration: 0.1)
        let viewHeight = intro!.bounds.height
        let viewWidth = intro!.bounds.width
        intro?.addSubview(self.bottomPanel)
        self.bottomPanel.frame = CGRect(x: 0, y: viewHeight-200, width: viewWidth, height: 200)
        
        self.bottomPanel.addSubview(self.registerButton)
        self.registerButton.frame = CGRect(x: 0, y: 0, width: 150, height: 40)
        self.registerButton.center = CGPoint(x: self.bottomPanel.bounds.width/2, y: (self.bottomPanel.bounds.height/2)+15)
        
        self.bottomPanel.addSubview(self.loginLabel)
        self.loginLabel.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        let loginLabelCenterY = (self.bottomPanel.bounds.height - self.registerButton.frame.maxY)/2
        self.loginLabel.center = CGPoint(x: self.bottomPanel.center.x, y: self.registerButton.frame.maxY + loginLabelCenterY)
    }
    
    @objc func doRegisterAction() {
        let nav = RegisterNavigation()
        nav.modalTransitionStyle = .flipHorizontal
        self.present(nav, animated: true, completion: nil)
    }
    
    func doLoginAction() {
        let window = UIApplication.shared.delegate!.window!
        let tabVC = WWTabBarController()
        window?.rootViewController = tabVC
        window?.makeKeyAndVisible()
    }
}
extension IntroViewController:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    }
}
