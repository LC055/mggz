//
//  RecordTrack_ExceptionCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/10/9.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class RecordTrack_ExceptionCell: UITableViewCell {
    
    var roundIconView : UIView = {
        let view = UIView()
        view.backgroundColor = colorWithRGB(r: 217, g: 217, b: 217)
        view.layer.cornerRadius = 4
        
        return view
    }()
    
    var upLine: UIView = {
        let view = UIView()
        view.backgroundColor = colorWithRGB(r: 217, g: 217, b: 217)
        
        return view
    }()
    
    var downLine: UIView = {
        let view = UIView()
        view.backgroundColor = colorWithRGB(r: 217, g: 217, b: 217)
        return view
    }()
    
    var descriptionLabel:UILabel = {
        let label = UILabel()
        label.text = "轨迹上报时间"
        label.font = wwFont_Regular(15)
        label.textColor = UIColor.mg_lightGray
        return label
    }()
    
    //小时:分  label
    var exceptionTimeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.mg_lightGray
        label.text = "08:30"
        label.font = wwFont_Regular(15)
        return label
    }()
    
    var exceptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.red
        label.textAlignment = .center
        label.text = "异常"
        label.font = wwFont_Regular(15)
        return label
    }()

    var checkButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage.init(named: "Icon_轨迹异常"), for: .normal)
        button.setTitle("点击查看", for: .normal)
        button.titleLabel?.font = wwFont_Regular(14)
        button.setTitleColor(UIColor.mg_lightGray, for: .normal)
        button.layer.cornerRadius = 12
        button.backgroundColor = UIColor.white
        button.layer.shadowOffset = CGSize(width: 1, height: 1)
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOpacity = 0.3
        
        return button
    }()
    
    /***传入的变量***/
    var currentTrackModel: RecordTrack?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        
        self.checkButton.addTarget(self, action: #selector(doCheckButtonAction), for: .touchUpInside)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        self.contentView.addSubview(roundIconView)
        self.roundIconView.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(10)
            make.centerY.equalTo(self.contentView)
            make.width.height.equalTo(8)
        }
        
        self.contentView.addSubview(self.upLine)
        self.upLine.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.roundIconView.snp.top)
            make.centerX.equalTo(self.roundIconView)
            make.top.equalTo(self.contentView)
            make.width.equalTo(1)
        }
        
        self.contentView.addSubview(self.downLine)
        self.downLine.snp.makeConstraints { (make) in
            make.top.equalTo(self.roundIconView.snp.bottom)
            make.centerX.equalTo(self.roundIconView)
            make.bottom.equalTo(self.contentView)
            make.width.equalTo(1)
        }
        
        self.contentView.addSubview(self.descriptionLabel)
        self.descriptionLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.roundIconView.snp.right).offset(5)
            make.centerY.equalTo(self.roundIconView)
            make.width.equalTo(100)
        }
        
        self.contentView.addSubview(self.exceptionTimeLabel)
        self.exceptionTimeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.descriptionLabel.snp.right).offset(5)
            make.centerY.equalTo(self.descriptionLabel)
            make.width.equalTo(60)
        }
        
        self.contentView.addSubview(self.exceptionLabel)
        self.exceptionLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.exceptionTimeLabel.snp.right)
            make.centerY.equalTo(self.exceptionTimeLabel)
        }
        
        self.contentView.addSubview(self.checkButton)
        self.checkButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.contentView).offset(-10)
            make.left.equalTo(self.exceptionLabel.snp.right)
            make.centerY.equalTo(self.contentView)
            make.width.equalTo(80)
            make.height.equalTo(30)
        }
    }
    
    func doCheckButtonAction() {
        
        
        var responder = self.next
        while !(responder is UIViewController) {
            responder = responder?.next
        }
        if responder is UIViewController {
            if let vc = responder as? RecordPathAbnormalController {
                
                let toVc = TrackErrowDetailController()
                toVc.currentProject = vc.projectModel
                toVc.currentTrackModel = self.currentTrackModel
                vc.navigationController?.pushViewController(toVc, animated: true)
            }

        }
    }
    
    func bind(_ model: RecordTrack) {
        self.currentTrackModel = model
        if let date = model.locateTime {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let dateString = formatter.string(from: date)
            
            self.exceptionTimeLabel.text = dateString
        }
    }
}
