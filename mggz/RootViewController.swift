//
//  RootViewController.swift
//  SwiftDemo2
//
//  Created by ShareAnimation on 2017/10/17.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

//    var backgrounImageView: UIImageView = {
//        let imageView = UIImageView()
//        imageView.image = UIImage(named: "启动页.png")
//        imageView.contentMode = .scaleAspectFit
//        return imageView
//    }()
    
    var activeIndicator: UIActivityIndicatorView = {
        let activeIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        return activeIndicator
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
       // self.view.addSubview(self.backgrounImageView)
//        self.backgrounImageView.snp.makeConstraints { (make) in
//            make.edges.equalTo(self.view)
//        }
        
        self.view.addSubview(self.activeIndicator)
        self.activeIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
        }
        self.activeIndicator.startAnimating()
        
    LoginAuthenticationService.verifyLoginStatus({ (response) in
            if response.success {
                let vc = WWTabBarController()
            UIApplication.shared.delegate?.window!?.rootViewController = vc
              //  let accountSelect = AccountSelectViewController(nibName: "AccountSelectViewController", bundle: nil)
                //UIApplication.shared.delegate?.window!?.rootViewController = accountSelect
            }
            else {
                let vc = LoginViewController()
               // let accountSelect = AccountSelectViewController(nibName: "AccountSelectViewController", bundle: nil)
      UIApplication.shared.delegate?.window!?.rootViewController = vc
            }
        })
        
    }
}
