//
//  LCSubApiManager.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/2/3.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
// LCSubApiManager

import Foundation
import Moya
import SwiftyJSON
enum LCSubApiManager{
    //工程列表
    case GetMigrantWorkerInfo(MigrantWorkerID:String,DemandBaseMigrantWorkerID:String,AccountId:String)
    //现金贷接口
    case GetLoanData(orderId:String,jsonData:String,AccountId:String)
    case GetVersonData(AccountId:String)
    case GETLCNewGuid(number:Int,AccountId:String)
    
    case CheckPayPwdExits(AccountId:String)
    case GetLoanStatusData(AccountId:String)
    case GetSalaryCardCountData(AccountId:String)
    case GetMyWorkTracksData(AccountId:String)
    case GetWorkDayType(AccountId:String,contructionCompanyID:String)
    case GetMigrantWorkerSignedDays(demandBaseMigrantWorkerID:String,AccountId:String)
    case SetMigrantWorkerSignedDays(demandBaseMigrantWorkerID:String,signedWorkDays:NSString,workDays:String,AccountId:String)
    case GetMigrantWorkerCompanyInfo(AccountId:String,CompanyID:String)
    case GetProjectSalaryPayList(AccountId:String,pageIndex:Int,pageSize:Int)
    case GetSalaryPayedRecord(contructionCompanyID:String,AccountId:String,pageIndex:Int,pageSize:Int)
    case GetAccountAmount(AccountId:String,CompanyID:String)
    case GetEntityOrderByOrderID(AccountId:String,orderID:String)
    case GetWorkTimeState(demandBaseMigrantWorkerID:String,AccountId:String)
    case GetWebConfigSalaryPrepaidParams(AccountId:String)
    case GetSalaryPrepaidAmount(AccountId:String)
    case GetSalaryPrepaidRecord(AccountId:String)
    case GetSalaryPrepaidRecordDetail(AccountId:String,pageIndex:Int,pageSize:Int)
    case SetSalaryPrepaid(prepaidAmount:String,AccountId:String)
    case GetSalaryPrepaidCount(AccountId:String)
    //发放明细未结算
    case GetSalaryNotPayedRecord(contructionCompanyID:String,AccountId:String,pageIndex:Int,pageSize:Int)
    case ReRegisterMigrantWorker(phoneNumber:String,verifycode:String,passWord:String,AccountId:String)
    case AddSalaryCard(SalaryCardBankAccount:String,CardholderName:String,BankInfoID:String,AccountId:String)
    case GetMigrantWorkerSignedRemark(migrantWorkerID:String,signedDay:String,AccountId:String)
    case SetMigrantWorkerSignedRemark(migrantWorkerID:String,signedDay:String,AccountId:String,remark:String)
    case ApplicationOvertimeAction(orderId:String,jsonData:String,AccountId:String,approvalNote:String)
    case GetOvertimeApplyRecord(demandBaseMigrantWorkerID:String,AccountId:String)
    case GetOvertimeApplyRecordHistory(demandBaseMigrantWorkerID:String,pageIndex:Int,pageSize:Int,AccountId:String)
    case GetOvertimeApplyInfo(demandBaseMigrantWorkerID:String,workDate:String,AccountId:String)
    //获取系统时间
    case GetSystemTime(AccountId:String)
    //或许员工打卡的阶段性时间
    case GetSignStageDate(AccountId:String,demandBaseMigrantWorkerID:String)
    //获取加班时间段范围
    case GetOverTimeQuantaum(demandBaseMigrantWorkerID:String,workDate:String,AccountId:String)
    case SetSignedMessageSet(isSignedMessage:Bool,minutesLimit:String,AccountId:String)
    case GetSignedMessageSet(AccountId:String)
    case GetSignedHalfHourData(demandBaseMigrantWorkerID:String,AccountId:String)
    case CheckOtherProjectSignedIn(demandBaseMigrantWorkerID:String,AccountId:String)
    case CheckOtherProjectSignedOut(demandBaseMigrantWorkerID:String,AccountId:String)
    case SetMigrantWorkerSignedAlert(demandBaseMigrantWorkerID:String,alertType:String,signedDate:String,AccountId:String)
    //上传轨迹接口
    case UploadTrajectoryData(orderId:String,jsonData:String,AccountId:String)
    //考勤页面文案
    case GetSignHomeInfo(demandBaseMigrantWorkerID:String,AccountId:String)
    //工资申诉与同意接口
    case SetWorkerSalaryAdvanceConsult(advanceDetailID:String,appealResult:Int,AccountId:String)
    case SetWorkerSalaryAdvanceConfirmResult(advanceDetailID:String,confirmResult:Int,AccountId:String)
    case GetMyData
}
private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

//let LCAPiMainManager = MoyaProvider<LCAPiManager>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])
let LCAPiSubManager = MoyaProvider<LCSubApiManager>()
extension LCSubApiManager : TargetType{
    public var baseURL: URL {
        return URL.init(string: URL_Json_Header)!
    }
    
    public var path: String {
        switch self {
        case .GetMigrantWorkerInfo,.GetLoanData,.GetVersonData,.GETLCNewGuid,.CheckPayPwdExits,.GetLoanStatusData,.GetSalaryCardCountData,.GetMyWorkTracksData,.GetWorkDayType,.GetMigrantWorkerSignedDays,.SetMigrantWorkerSignedDays,.GetMigrantWorkerCompanyInfo,.GetProjectSalaryPayList,.GetSalaryPayedRecord,.GetAccountAmount,.GetEntityOrderByOrderID,.GetWorkTimeState,.GetSalaryPrepaidAmount,.GetWebConfigSalaryPrepaidParams,.GetSalaryPrepaidRecord,.GetSalaryPrepaidRecordDetail,.SetSalaryPrepaid,.GetSalaryPrepaidCount,.GetSalaryNotPayedRecord,.ReRegisterMigrantWorker,.AddSalaryCard,.GetMigrantWorkerSignedRemark,.SetMigrantWorkerSignedRemark,.ApplicationOvertimeAction,.GetOvertimeApplyRecordHistory,.GetOvertimeApplyInfo,.GetOvertimeApplyRecord,.GetSystemTime,.GetOverTimeQuantaum,.SetSignedMessageSet,.GetSignedMessageSet,.GetSignedHalfHourData,.GetSignStageDate,.CheckOtherProjectSignedIn,.CheckOtherProjectSignedOut,.SetMigrantWorkerSignedAlert,.UploadTrajectoryData,.GetSignHomeInfo,.SetWorkerSalaryAdvanceConsult,.SetWorkerSalaryAdvanceConfirmResult:
            return API_INFIX_CALLMETHOD + "CallMethod"
        default:
            return API_INFIX_CALLMETHOD + "CallMethod"
        }
    }
    
    public var method: Moya.Method {
        return .post
    }
    
    public  var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
    
    public var task: Task {
        switch self {
        case .GetMigrantWorkerInfo(let MigrantWorkerID, let DemandBaseMigrantWorkerID, let AccountId):
            return .requestParameters(parameters: ["MigrantWorkerID":MigrantWorkerID,"DemandBaseMigrantWorkerID":DemandBaseMigrantWorkerID,"AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkInfo.GetMigrantWorkInfo"], encoding: JSONEncoding.default)
        case .GetLoanData(let orderId, let jsonData, let AccountId):
            return .requestParameters(parameters: ["pageName":"KouFine.Container.Form","orderConfigId":"09FD3207-AB69-49AE-AFBE-6A36060E1F0C","orderId":orderId,"jsonData":jsonData,"approvalNote":"提交审批","AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxDBHandler.Submit"], encoding: JSONEncoding.default)
        case .GetVersonData(let AccountId):
            return .requestParameters(parameters: ["clientType":1,"AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxCommon.GetAppVersion"], encoding: JSONEncoding.default)
        case .GETLCNewGuid(let number, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.BasicHandler.AjaxCommonApp.GetNewGuid","number":number,"AccountId":AccountId], encoding: JSONEncoding.default)
        case .CheckPayPwdExits(let AccountId):
             return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxCommon.CheckPayPwdExists","AccountId":AccountId], encoding: JSONEncoding.default)
        case .GetSystemTime(let AccountId):
             return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxCommon.GetSystemTime","AccountId":AccountId], encoding: JSONEncoding.default)
        case .GetSignStageDate(let AccountId,let demandBaseMigrantWorkerID):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxCommon.GetSignDate","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID], encoding: JSONEncoding.default)
        case .GetLoanStatusData(let AccountId):
             return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerLoan.GetApplyLoanState","AccountId":AccountId], encoding: JSONEncoding.default)
        case .GetSalaryCardCountData(let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryPayApply.GetSalaryCardCount","AccountId":AccountId], encoding: JSONEncoding.default)
        case .GetMyWorkTracksData(let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerLocation.GetMigrantWorkerLocationList","AccountId":AccountId], encoding: JSONEncoding.default)
        case .GetWorkDayType(let AccountId, let contructionCompanyID):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxConstructionBasicSet.GetWorkDayType","AccountId":AccountId,"contructionCompanyID":contructionCompanyID], encoding: JSONEncoding.default)
        
        case .SetMigrantWorkerSignedDays(let demandBaseMigrantWorkerID, let signedWorkDays, let workDays, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSignedDays.SetMigrantWorkerSignedDays","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID,"signedWorkDays":signedWorkDays,"workDays":workDays], encoding: JSONEncoding.default)
        case .GetMigrantWorkerSignedDays(let demandBaseMigrantWorkerID, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSignedDays.GetMigrantWorkerSignedDays","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID], encoding: JSONEncoding.default)
        case .GetMigrantWorkerCompanyInfo(let AccountId, let CompanyID):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxConstructionTeamDetail.GetMigrantWorkerCompanyInfo","AccountId":AccountId,"CompanyID":CompanyID], encoding: JSONEncoding.default)
        case .GetProjectSalaryPayList(let AccountId, let pageIndex, let pageSize):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorker.GetProjectSalaryPayList","AccountId":AccountId,"pageIndex":pageIndex,"pageSize":pageSize], encoding: JSONEncoding.default)
        case .GetSalaryPayedRecord(let contructionCompanyID, let AccountId, let pageIndex, let pageSize):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryPayApply.GetSalaryPayedRecord","AccountId":AccountId,"pageIndex":pageIndex,"pageSize":pageSize,"contructionCompanyID":contructionCompanyID], encoding: JSONEncoding.default)
        case .GetAccountAmount(let AccountId, let CompanyID):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryPayApply.GetAccountAmount","AccountId":AccountId,"companyID":CompanyID], encoding: JSONEncoding.default)
        case .GetEntityOrderByOrderID(let AccountId, let orderID):
            return .requestParameters(parameters: ["Method":"KouFine.Handler.Core.AjaxOperate.GetEntityOrderByOrderID","AccountId":AccountId,"pageName":"MarTian.Edit.MinGongBuQianXinXi","domain":"民工管理","typeFullName":"MarTian.MigrantWorkerManage.Entity.DemandBaseMigrantWorkerSignedSupplement","orderConfigId":"6c2eff7d-fbb8-b727-ae1c-04fcdb47aabe","orderID":orderID], encoding: JSONEncoding.default)
        case .GetWorkTimeState(let demandBaseMigrantWorkerID, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSigned.GetWorkTimeState","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID], encoding: JSONEncoding.default)
        case .GetSalaryPrepaidAmount(let AccountId):
             return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryPrepaid.GetSalaryPrepaidAmount","AccountId":AccountId], encoding: JSONEncoding.default)
        case .GetWebConfigSalaryPrepaidParams(let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryPrepaid.GetWebConfigSalaryPrepaidParams","AccountId":AccountId], encoding: JSONEncoding.default)
        case .GetSalaryPrepaidRecord(let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryPrepaid.GetSalaryPrepaidRecord","AccountId":AccountId], encoding: JSONEncoding.default)
        case .GetSalaryPrepaidRecordDetail(let AccountId, let pageIndex, let pageSize):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryPrepaid.GetSalaryPrepaidRecordDetail","AccountId":AccountId,"pageIndex":pageIndex,"pageSize":pageSize], encoding: JSONEncoding.default)
        case .SetSalaryPrepaid(let prepaidAmount, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryPrepaid.SetSalaryPrepaid","AccountId":AccountId,"prepaidAmount":prepaidAmount], encoding: JSONEncoding.default)
        case .GetSalaryPrepaidCount(let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryPrepaid.GetSalaryPrepaidCount","AccountId":AccountId], encoding: JSONEncoding.default)
      case .GetSalaryNotPayedRecord(let contructionCompanyID, let AccountId, let pageIndex, let pageSize):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryPayApply.GetSalaryNotPayedRecord","AccountId":AccountId,"pageIndex":pageIndex,"pageSize":pageSize,"contructionCompanyID":contructionCompanyID], encoding: JSONEncoding.default)
        case .ReRegisterMigrantWorker(let phoneNumber, let verifycode, let passWord, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMGAppeal.ReRegisterMigrantWorker","AccountId":AccountId,"phoneNumber":phoneNumber,"verifycode":verifycode,"passWord":passWord], encoding: JSONEncoding.default)
        case .AddSalaryCard(let SalaryCardBankAccount, let CardholderName, let BankInfoID, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryPayApply.AddSalaryCard","AccountId":AccountId,"SalaryCardBankAccount":SalaryCardBankAccount,"CardholderName":CardholderName,"BankInfoID":BankInfoID], encoding: JSONEncoding.default)
        case .GetMigrantWorkerSignedRemark(let migrantWorkerID, let signedDay, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSigned.GetMigrantWorkerSignedRemark","AccountId":AccountId,"migrantWorkerID":migrantWorkerID,"signedDay":signedDay], encoding: JSONEncoding.default)
        case .SetMigrantWorkerSignedRemark(let migrantWorkerID, let signedDay, let AccountId, let remark):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSigned.SetMigrantWorkerSignedRemark","AccountId":AccountId,"migrantWorkerID":migrantWorkerID,"signedDay":signedDay,"remark":remark], encoding: JSONEncoding.default)
        case .ApplicationOvertimeAction(let orderId, let jsonData, let AccountId, let approvalNote):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDBHandler.Submit","AccountId":AccountId,"pageName":"KouFine.Container.Form","orderConfigId":"8efb1163-74b5-b61b-d7e0-b821a266bb40","orderId":orderId,"jsonData":jsonData,"approvalNote":approvalNote], encoding: JSONEncoding.default)
        case .GetOvertimeApplyRecordHistory(let demandBaseMigrantWorkerID, let pageIndex, let pageSize, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerOvertimeApply.GetOvertimeApplyRecordHistory","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID,"pageIndex":pageIndex,"pageSize":pageSize], encoding: JSONEncoding.default)
        case .GetOvertimeApplyInfo(let demandBaseMigrantWorkerID, let workDate, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerOvertimeApply.GetOvertimeApplyInfo","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID,"workDate":workDate], encoding: JSONEncoding.default)
        case .GetOvertimeApplyRecord(let demandBaseMigrantWorkerID, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerOvertimeApply.GetOvertimeApplyRecord","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID], encoding: JSONEncoding.default)
        case .GetOverTimeQuantaum(let demandBaseMigrantWorkerID, let workDate, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkTimeQuantum.GetOverTimeQuantaum","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID,"workDate":workDate], encoding: JSONEncoding.default)
        case .SetSignedMessageSet(let isSignedMessage, let minutesLimit, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSigned.SetSignedMessageSet","AccountId":AccountId,"isSignedMessage":isSignedMessage,"minutesLimit":minutesLimit], encoding: JSONEncoding.default)
        case .GetSignedMessageSet(let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSigned.GetSignedMessageSet","AccountId":AccountId], encoding: JSONEncoding.default)
        case .GetSignedHalfHourData(let demandBaseMigrantWorkerID, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSigned.GetSignedMessage","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID], encoding: JSONEncoding.default)
        case .CheckOtherProjectSignedIn(let demandBaseMigrantWorkerID, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSigned.CheckOtherProjectSignedIn","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID], encoding: JSONEncoding.default)
        case .CheckOtherProjectSignedOut(let demandBaseMigrantWorkerID, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSigned.CheckOtherProjectSignedOut","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID], encoding: JSONEncoding.default)
        case .SetMigrantWorkerSignedAlert(let demandBaseMigrantWorkerID, let alertType, let signedDate, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSigned.SetMigrantWorkerSignedAlert","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID,"alertType":alertType,"signedDate":signedDate], encoding: JSONEncoding.default)
        case .UploadTrajectoryData(let orderId, let jsonData, let AccountId):
            return .requestParameters(parameters: ["Method":"KouFine.Handler.Core.AjaxOperate.SaveOrderData","AccountId":AccountId,"pageName":"KouFine.Container.Form","orderConfigId":"2BA1B75D-88F0-0856-15A9-6A4C0A13933B","orderId":orderId,"jsonData":jsonData,"operate":"1"], encoding: JSONEncoding.default)
        case .GetSignHomeInfo(let demandBaseMigrantWorkerID, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSigned.GetSignHomeInfo","AccountId":AccountId,"demandBaseMigrantWorkerID":demandBaseMigrantWorkerID], encoding: JSONEncoding.default)
        case .SetWorkerSalaryAdvanceConsult(let advanceDetailID, let appealResult, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryAdvance.SetWorkerSalaryAdvanceConsult","AccountId":AccountId,"advanceDetailID":advanceDetailID,"appealResult":appealResult], encoding: JSONEncoding.default)
        case .SetWorkerSalaryAdvanceConfirmResult(let advanceDetailID, let confirmResult, let AccountId):
            return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerSalaryAdvance.SetWorkerSalaryAdvanceConfirmResult","AccountId":AccountId,"advanceDetailID":advanceDetailID,"confirmResult":confirmResult], encoding: JSONEncoding.default)
        default:
            return .requestPlain
        }
    }
    public  var headers: [String : String]? {
        let httpHeader = mobile_cilent_headers()
        return httpHeader
    }
    
}

























