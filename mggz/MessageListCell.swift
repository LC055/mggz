//
//  MessageListCell.swift
//  mggz
//
//  Created by ShareAnimation on 2017/9/1.
//  Copyright © 2017年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class MessageListCell: UITableViewCell {
    var panelView = UIView() //用来装除了时间控件的内容
    
    var dateAndTimeLabel : UILabel = {
        let label = UILabel()
        label.text = "2017-5-20 18:00"
        label.font = wwFont_Medium(14)
        label.backgroundColor = colorWith255RGB(233, g: 233, b: 233)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.layer.cornerRadius = 10
        label.layer.masksToBounds = true
        return label
    }()
    
    var cellTitleLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Regular(18)
        label.numberOfLines = 0
        return label
    }()
    
    var cellDetailLabel: UILabel = {
        let label = UILabel()
        label.font = wwFont_Regular(15)
        label.textColor = UIColor.mg_lightGray
        label.numberOfLines = 0
        return label
    }()
    
    var cellSeparatorView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.init(named: "Icon_右箭头")!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.mg_lightGray
        imageView.contentMode = .scaleAspectFit
        
        return imageView
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.panelView.backgroundColor = UIColor.white
        self.backgroundColor = UIColor.clear
        let selectedBackroundView = UIView()
        selectedBackroundView.backgroundColor = UIColor.mg_backgroundGray
        self.selectedBackgroundView = selectedBackroundView
        
        self.contentView.addSubview(self.dateAndTimeLabel)
        self.dateAndTimeLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.contentView)
            make.width.equalTo(130)
            make.height.equalTo(28)
            make.top.equalTo(self.contentView).offset(10)
        }
        
        self.contentView.addSubview(self.panelView)
        self.panelView.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(10)
            make.right.equalTo(self.contentView).offset(-10)
            make.top.equalTo(self.dateAndTimeLabel.snp.bottom).offset(8)
            make.bottom.equalTo(self.contentView)
        }
        
        self.panelView.addSubview(self.cellSeparatorView)
        self.cellSeparatorView.snp.makeConstraints { (make) in
            make.top.bottom.right.equalTo(self.panelView)
            make.width.equalTo(25)
        }
        
        self.panelView.addSubview(self.cellTitleLabel)
        self.cellTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.panelView).offset(6)
            make.left.equalTo(self.panelView).offset(10)
            make.right.equalTo(self.cellSeparatorView.snp.left)
        }
        
        self.panelView.addSubview(self.cellDetailLabel)
        self.cellDetailLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.cellTitleLabel.snp.bottom).offset(5)
            make.left.equalTo(self.cellTitleLabel)
            make.right.equalTo(self.cellSeparatorView.snp.left)
            make.bottom.equalTo(self.panelView).offset(-5)
        }
        
    }
    
    func bind(_ model: NotificationModel) {
        if let date = model.createTime {
            let dateString = dateToStringWithNoSecond(date: date)
            self.dateAndTimeLabel.text = dateString
        }
        if (model.messageTitle?.contains("工资提现通知"))! || (model.messageTitle?.contains("工资发放通知"))!{
            self.cellSeparatorView.isHidden = true
        }else{
            self.cellSeparatorView.isHidden = false
        }
        self.cellTitleLabel.text = model.messageTitle
        self.cellDetailLabel.text = model.messageDescription
    }
}

