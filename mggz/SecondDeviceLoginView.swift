//
//  SecondDeviceLoginView.swift
//  mggz
//
//  Created by QinWei on 2018/5/3.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class SecondDeviceLoginView: UIView {

    @IBOutlet weak var firstView: UIView!
    
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var securityField: UITextField!
    
    @IBOutlet weak var securityButton: UIButton!
    
    @IBOutlet weak var sureButton: UIButton!
    
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "SecondDeviceLoginView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        
        view.backgroundColor = UIColor.white
        self.firstView.layer.cornerRadius = 5
        self.firstView.layer.masksToBounds = true
        self.firstView.layer.borderWidth = 1
        self.firstView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.secondView.layer.cornerRadius = 5
        self.secondView.layer.masksToBounds = true
        self.secondView.layer.borderWidth = 1
        self.secondView.layer.borderColor = UIColor.lightGray.cgColor
        self.sureButton.isEnabled = false
        return view
    }
}
