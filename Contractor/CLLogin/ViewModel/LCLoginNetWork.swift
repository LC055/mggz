//
//  LCLoginNetWork.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/6.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCLoginNetWork: NSObject {

    
    
    
    
    
    
    
    
    
    
    
    
    
}
extension LCLoginNetWork{
    //登录第一步
    class func login (username: String, password: String, completionHandler: @escaping (WWValueResponse<String>) -> Void) {
        
        let params = ["loginName":username, "password":password,"deviceID":"", "companyType": "劳务公司"]
        let actionUrl = "http://app.winshe.cn/UserLoginForApp"
        let actionHeader = "UserLoginForApp"
        LCWebService.reqWebService(WebServiceURL + "?op=UserLoginForAppNew", action: actionUrl,actionHeader:actionHeader, params: params){ (response) in
            
            if response.success {
                let dic = response.value as! [String: AnyObject]
                
                let aStatus = StatusStruct(rootJson: dic)
                if aStatus.success {
                    let userModel = UserModel.init(dic)
                    WWUser.sharedInstance.userModel = userModel
                    let accountId = dic["AccountId"] as! String
                    let key = dic["Key"] as! String
                    //LoginAuthenticationService.save(username: username, password: password)
                    
                    getToken(accountId: accountId, key: key, completionHandler: completionHandler)
                    return
                }
                completionHandler(WWValueResponse.init(success: aStatus.success, message: aStatus.statusString))
                return
            }
            completionHandler(WWValueResponse.init(success: false, message: "网络错误"))
        }
        
    }
    
    //登录第二步
    class func getToken(accountId: String, key: String, completionHandler: @escaping (WWValueResponse<String>) -> Void) {
        let params = ["accountId":accountId, "key": key]
        
        let actionUrl = "http://app.winshe.cn/GetWebApiInfoWithKey"
        let actionHeader = "GetWebApiInfoWithKey"
        LCWebService.reqWebService(WebServiceURL, action: actionUrl, actionHeader: actionHeader, params: params) { (response) in
            if response.success {
                let dic = response.value as! [String: AnyObject]
                
                let status = StatusStruct(rootJson: dic)
                if status.success {
                    let token = dic["Token"] as! String
                    WWUser.sharedInstance.token = token
                }
                completionHandler(WWValueResponse.init(success: status.success, message: status.statusString))
                return
            }
            
            completionHandler(WWValueResponse.init(success: false, message: "网络错误"))
        }
    }
}
