//
//  AccountSelectViewController.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/3.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class AccountSelectViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func commonWorkerClcik(_ sender: Any) {
        LoginAuthenticationService.verifyLoginStatus({ (response) in
            if response.success {
            let vc = WWTabBarController()
            UIApplication.shared.delegate?.window!?.rootViewController = vc
            }
            else {
            let vc = LoginViewController()
        UIApplication.shared.delegate?.window!?.rootViewController = vc
            }
        })
    }
    @IBAction func commonfront(_ sender: Any) {
        self.commonWorkerClcik((Any).self)
    }
    @IBAction func contractorClick(_ sender: Any) {
        let contractor = ContractorLoginViewController(nibName: "ContractorLoginViewController", bundle: nil)
        UIApplication.shared.delegate?.window!?.rootViewController = contractor
    }
    @IBAction func contractorfront(_ sender: Any) {
        self.contractorClick((Any).self)
    }
}
