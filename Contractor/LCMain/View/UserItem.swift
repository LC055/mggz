//
//  UserItem.swift
//  mggz
//
//  Created by Apple on 2018/4/23.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class UserItem: UITableViewCell {

    @IBOutlet weak var headview: UIImageView!
    @IBOutlet weak var nameview: UILabel!
    
    var dataitem:DWItem?{//定位异常 设置
        get{
            return nil
        }
        set(newValue){
            do{
                var i = Int((newValue?.ExceptionCount)!)
                let name = "\((newValue?.Name)!)"+"("+"\(Int((newValue?.ExceptionCount)!))"+")"
                self.nameview.text = name
                
                if newValue?.PhotoPath == nil || (newValue?.PhotoPath?.isEmpty)!{
                    self.headview.image = UIImage(named: "scene_touxiang")
                }else{
                    let path = "\(LC_IMAGE_URL)\((newValue?.PhotoPath)!)"
                    self.headview.kf.setImage(with: URL.init(string: path), placeholder: UIImage(named: "scene_touxiang"), options: nil, progressBlock: nil, completionHandler: nil)
                }
                
            }catch{
                print(error)
            }
            
            
        }
    }
    
    var chidaoItem:DWItem?{
        get{
            return nil
        }
        set(newValue){
            var countNum = Int((newValue?.SignedExceptionCount)!)
            let name = "\((newValue?.Name)!)"+"("+"\(countNum)"+")"
            self.nameview.text = name
            if newValue?.PhotoPath == nil || (newValue?.PhotoPath?.isEmpty)!{
                self.headview.image = UIImage(named: "scene_touxiang")
            }else{
                let path = "\(LC_IMAGE_URL)\((newValue?.PhotoPath)!)"
                self.headview.kf.setImage(with: URL.init(string: path), placeholder: UIImage(named: "scene_touxiang"), options: nil, progressBlock: nil, completionHandler: nil)
                
                
            }
        }
    }
    
    override func layoutSubviews() {
        let f = self.headview.bounds.size.width
        self.headview.layer.cornerRadius = f/2
        self.headview.layer.masksToBounds = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.LightGreyBG
        self.nameview.textColor = UIColor.LightBlack
        self.nameview.font = UIFont.systemFont(ofSize: 17)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected{
            setSelectStatus()
        }else{
            self.normal()
        }
    }
    
    func setSelectStatus(){
        self.contentView.backgroundColor = UIColor.white
        self.nameview.textColor = UIColor.DeepBlack
        //self.name.font = UIFont.systemFont(ofSize: 19)
        var text = self.nameview.text
        var start = text?.range(of: "(", options: String.CompareOptions.literal)
        var end = text?.range(of: ")", options: String.CompareOptions.backwards)
        
        var t = (start?.lowerBound.encodedOffset)!+1
        var e = end?.lowerBound.encodedOffset
        
        let attr = NSMutableAttributedString(string: self.nameview.text!)
        attr.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: NSMakeRange(t, (e!-t)))
        attr.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 19), range: NSMakeRange(0,(self.nameview.text?.count)!))
        self.nameview.attributedText = attr
    }
    
    func normal(){
        self.nameview.textColor = UIColor.LightBlack
        self.nameview.font = UIFont.systemFont(ofSize: 17)
    }
    
}
