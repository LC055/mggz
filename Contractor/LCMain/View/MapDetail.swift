//
//  MapDetail.swift
//  mggz
//
//  Created by Apple on 2018/5/5.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import SnapKit

class MapDetail: UIView {
    
    lazy var headImg:UIImageView = {
        var view = UIImageView(image: UIImage(named: "scene_touxiang"))
        view.layer.cornerRadius = 25/2
        view.layer.masksToBounds = true
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    lazy var name:UILabel = {
       var view = UILabel()
       view.textColor = UIColor.LightBlack
        view.textAlignment = .left
        return view
    }()
    
    lazy var gjlabel:UIButton = {//轨迹异常
        var view = UIButton()
        view.setTitle("查看轨迹>", for: UIControlState.normal)
        view.setTitleColor(UIColor.DeepBlack, for: UIControlState.normal)
        view.titleLabel?.textAlignment = .right
        return view
    }()
    
    lazy var jllabel:UILabel = {//异常记录
        var view = UILabel()
        view.textAlignment = .left
        view.textColor = UIColor.red
        return view
    }()
    
    lazy var addImg:UIImageView = {
        var view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "addressPoint")
        return view
    }()
    lazy var addressview:UILabel = {
       var view = UILabel()
        view.textAlignment = .left
        view.numberOfLines = 0
        view.sizeToFit()
        view.textColor = UIColor.LightBlack
        return view
    }()
    
    lazy var timeview:UIImageView = {
       var view = UIImageView(image: UIImage(named: "timegrey"))
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    lazy var timelabel:UILabel = {
        var view = UILabel()
        view.textAlignment = .left
        view.textColor = UIColor.LightBlack
        return view
    }()
    
    lazy var bottomlayout:UIView = {
       var view = UIView()
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initView()
    }
    
    func initView(){
        self.name.text = "张三"
        self.jllabel.text = "异常记录"
        self.addressview.text = "宁啊实打实的"
        self.timelabel.text = "2011-01-01"
        
        
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.addSubview(self.name)
        self.addSubview(self.headImg)
        self.addSubview(self.gjlabel)
        self.addSubview(self.jllabel)
        self.bottomlayout.addSubview(self.addImg)
        self.bottomlayout.addSubview(self.addressview)
        self.bottomlayout.addSubview(self.timeview)
        self.bottomlayout.addSubview(self.timelabel)
        self.addSubview(bottomlayout)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.headImg.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left).offset(10)
            make.top.equalTo(self.snp.top).offset(15)
            make.width.height.equalTo(30)
        }
        self.name.snp.updateConstraints { (make) in
            make.left.equalTo(self.headImg.snp.right).offset(5)
            make.top.equalTo(self.snp.top).offset(15)
        }
        self.gjlabel.snp.updateConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(15)
            make.right.equalTo(self.snp.right).offset(-10)
        }
        
        self.jllabel.snp.updateConstraints { (make) in
            make.left.equalTo(self).offset(10)
            make.top.equalTo(self.headImg.snp.bottom).offset(10)
        }
        
        self.bottomlayout.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left).offset(10)
            make.right.equalTo(self.snp.right).offset(10)
            make.bottom.equalTo(self.snp.bottom)
            make.top.equalTo(self.jllabel.snp.bottom)
        }
        
        self.addImg.snp.updateConstraints { (make) in
            make.width.height.equalTo(15)
            make.left.equalTo(self.bottomlayout.snp.left)
            make.centerY.equalTo(self.bottomlayout)
        }
        self.timelabel.snp.updateConstraints { (make) in
            make.right.equalTo(self.bottomlayout.snp.right).offset(-10)
            make.centerY.equalTo(self.bottomlayout)
            make.width.equalTo(70)
        }
        self.timeview.snp.updateConstraints { (make) in
            make.right.equalTo(self.timelabel.snp.left).offset(-5)
            make.centerY.equalTo(self.bottomlayout)
            make.width.height.equalTo(15)
        }
        
        self.addressview.snp.updateConstraints { (make) in
            make.left.equalTo(self.addImg.snp.right)
            make.right.equalTo(self.timeview.snp.left)
            make.top.equalTo(self.bottomlayout.snp.top).offset(10)
            make.bottom.equalTo(self.bottomlayout.snp.bottom).offset(-10)
        }
        
        
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
