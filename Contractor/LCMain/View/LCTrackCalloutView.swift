//
//  LCTrackCalloutView.swift
//  mggz
//
//  Created by QinWei on 2018/1/19.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCTrackCalloutView: UIView {

    @IBOutlet weak var stateLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "LCTrackCalloutView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

}
