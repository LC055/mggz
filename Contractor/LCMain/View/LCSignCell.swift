//
//  LCSignCell.swift
//  mggz
//
//  Created by QinWei on 2018/1/5.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Kingfisher
class LCSignCell: UITableViewCell {
    
    enum BoardColor:Int{
        case Green = 0
        case Red = 1
        case Grey = 2
    }

    
    @IBOutlet weak var workerType: UILabel!
    
    @IBOutlet weak var mwName: UILabel!
    
    @IBOutlet weak var adressLabel: UILabel!
    
    @IBOutlet weak var phoneNum: UILabel!
    
    @IBOutlet weak var checkButton: UIButton!
    
    @IBOutlet weak var queqinview: LabelView!
    @IBOutlet weak var iconView: TextImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func isQueQing(_ flag:Bool){
        if flag{
            queqinview.isHidden = false
            checkButton.isHidden = true
            checkButton.isEnabled = false
        }else{
            queqinview.isHidden = true
            checkButton.isHidden = false
            checkButton.isEnabled = true
        }
    }
    
    public func setCqListItem(model :MWCurrentLocationModel,_boardType boardType:BoardColor){
        self.adressLabel.text = model.Location?["Address"] as? String ?? "暂无位置信息"
        if let imagePath = model.MigrantWorker!["Photo"] as? [String: AnyObject]{
            let path : String = imagePath["Path"] as? String ?? "scene_touxiang"
            
            if model.SignedResult?.rawValue == 0{//查看轨迹
                self.iconView.label = nil
                self.isQueQing(false)
                switch boardType{
                case .Green:
                    self.iconView.broadColor = UIColor.green
                    break
                case .Red:
                    self.iconView.broadColor = UIColor.red
                    break
                case .Grey:
                    self.iconView.broadColor = UIColor.gray
                    break
                default:
                    break
                }
                
            }else if model.SignedResult?.rawValue == 1{//查看轨迹 已经下班
                self.iconView.label = "已下班"
                self.iconView.broadColor = nil
                self.isQueQing(false)
            }else{//查看轨迹 false
                self.iconView.label = nil
                self.iconView.broadColor = nil
                if model.IsAbsence!{//缺勤
                    self.isQueQing(true)
                }else{//未缺勤
                    self.isQueQing(false)
                }
                
            }
            
            
            //self.iconView.layer.borderWidth = 4
            self.iconView.kf.setImage(with: URL.init(string: (ImageUrlPrefix + path)), placeholder: UIImage(named: "scene_touxiang"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        self.workerType.text = ((model.MigrantWorker!["MigrantWorkType"] as! Dictionary<String,Any>)["WorkType"] as! String) ?? ""
        
        
        
        
        //if let resultDic = jsonString["Result"] as? [String: AnyObject] {
        //   if let itemDic = resultDic["Items"] as? [[String: Any]] {
        
        if let companyMG = model.MigrantWorker!["CompanyMG"] as? [String: AnyObject]{
            self.mwName.text = companyMG["CompanyName"] as? String
            self.phoneNum.text = companyMG["LealPersonPhone"] as? String
            
            
        }
        if let address = model.Location!["Address"] as? String{
            self.adressLabel.text = address
        }
    }
    
    
    public func setupDataWith(model :MWCurrentLocationModel,flag flag:Bool){
    
        self.adressLabel.text = model.Location?["Address"] as? String ?? "暂无位置信息"
        if let imagePath = model.MigrantWorker!["Photo"] as? [String: AnyObject]{
            let path : String = imagePath["Path"] as? String ?? "scene_touxiang"
            
            if model.SignedResult?.rawValue == 0{//查看轨迹
                self.iconView.label = nil
                self.isQueQing(false)
                if model.IsOnline! {
                    if flag{
                        self.iconView.broadColor = UIColor.green//layer.borderColor = UIColor.green.cgColor
                    }else{
                        self.iconView.broadColor = UIColor.red//layer.borderColor = UIColor.red.cgColor
                    }
                }else{
                    self.iconView.broadColor = UIColor.gray//layer.borderColor = UIColor.gray.cgColor
                }
                
            }else if model.SignedResult?.rawValue == 1{//查看轨迹 已经下班
                self.iconView.label = "已下班"
                self.iconView.broadColor = nil
                self.isQueQing(false)
            }else{//查看轨迹 false
                self.iconView.label = nil
                self.iconView.broadColor = nil
                if model.IsAbsence!{//缺勤
                    self.isQueQing(true)
                }else{//未缺勤
                    self.isQueQing(false)
                }
                
            }
            
            
            //self.iconView.layer.borderWidth = 4
            self.iconView.kf.setImage(with: URL.init(string: (ImageUrlPrefix + path)), placeholder: UIImage(named: "scene_touxiang"), options: nil, progressBlock: nil, completionHandler: nil)
        }
        
        self.workerType.text = ((model.MigrantWorker!["MigrantWorkType"] as! Dictionary<String,Any>)["WorkType"] as! String) ?? ""
        
        
        
        
        //if let resultDic = jsonString["Result"] as? [String: AnyObject] {
        //   if let itemDic = resultDic["Items"] as? [[String: Any]] {
        
        if let companyMG = model.MigrantWorker!["CompanyMG"] as? [String: AnyObject]{
            self.mwName.text = companyMG["CompanyName"] as? String
            self.phoneNum.text = companyMG["LealPersonPhone"] as? String
            
            
        }
        if let address = model.Location!["Address"] as? String{
            self.adressLabel.text = address
        }
    }
    
}
