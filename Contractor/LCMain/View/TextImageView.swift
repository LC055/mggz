//
//  TextImageView.swift
//  mggz
//
//  Created by Apple on 2018/5/8.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class TextImageView: UIImageView {

    var broadColor:UIColor? {
        get{
            return nil
        }
        set(newValue){
            if newValue != nil{
                self.layer.borderWidth = 2
                self.layer.borderColor = newValue?.cgColor
            }else{
                self.layer.borderWidth = 0
                self.layer.borderColor = UIColor.white.cgColor
            }
            
        }
    }
    
    var _label:String?
    
    var label:String?{
        get{
            return _label
        }
        set(newValue){
            _label = newValue
            if newValue != nil{
                self.titleView.text = newValue
                self.layer.borderWidth = 0
                self.headImgeView.layer.addSublayer(self.greylayer)
            }else{
                self.layer.borderWidth = 0
                self.titleView.text = ""
                guard let items = self.headImgeView.layer.sublayers else{
                    return
                }
                for item in items{
                    if item == self.greylayer{
                        item.removeFromSuperlayer()
                    }
                }
            }
            
            
        }
    }
    
    lazy var headImgeView:UIImageView = {
        var view = UIImageView(frame: self.frame)
        view.image = UIImage(named: "120.jpg")
        return view
    }()
    
    lazy var titleView:UILabel = {
        var view = UILabel(frame: self.frame)
        view.textAlignment = .center
        view.font = UIFont.systemFont(ofSize: 12.0)
        view.text = ""
        view.textColor = UIColor.white
        return view
    }()
    
    
    lazy var greylayer:CALayer = {
        var customlayer = CALayer()
        customlayer.backgroundColor = UIColor.gray.cgColor
        customlayer.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        customlayer.opacity = 0.6
        
        return customlayer
    }()
    
    lazy var textlayer:CATextLayer = {
        var customlayer = CATextLayer()
        customlayer.bounds = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        customlayer.fontSize = 12.0
        customlayer.isWrapped = true
        customlayer.alignmentMode = kCAAlignmentCenter
        customlayer.position = CGPoint(x: self.bounds.width, y: self.bounds.height)
        customlayer.contentsScale = UIScreen.main.scale
        customlayer.string = "1212"
        customlayer.foregroundColor = UIColor.blue.cgColor
        return customlayer
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initView()
    }
    
    override func layoutSubviews() {
        self.headImgeView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        self.titleView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        self.layer.cornerRadius = self.bounds.width/2
        self.layer.masksToBounds = true
    }
    
    func initView(){
        self.addSubview(self.headImgeView)
        self.addSubview(self.titleView)
        
        self.bringSubview(toFront: self.titleView)
        self.backgroundColor = UIColor.white
    }
    
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = self.bounds.width/2
        layer.masksToBounds = true
    }
    
    override func draw(_ layer: CALayer, in ctx: CGContext) {
        print("draw--CALayer")
        
        layer.cornerRadius = self.bounds.width/2
        layer.masksToBounds = true
    }

}
