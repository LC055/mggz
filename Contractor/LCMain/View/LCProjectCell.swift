//
//  LCProjectCell.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/6.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCProjectCell: UITableViewCell {

    @IBOutlet weak var projectName: UILabel!
    
    @IBOutlet weak var projectCode: UILabel!
    
    @IBOutlet weak var allWorker: UILabel!
    
    @IBOutlet weak var onlineWorker: UILabel!
    
    @IBOutlet weak var attendance: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    public func setupDataWith(model :LCProjectListModel){
        if model.IsAppSelected{
            self.projectName.textColor = UIColor.init(red: 0.24, green: 0.59, blue: 1, alpha: 1)
        }else{
            self.projectName.textColor = UIColor.black
        }
        self.projectName.text = model.ProjectName
        self.projectCode.text = model.OrderNo
        self.allWorker.text = String.init(format: "%d", model.OnGuardCount!)
        self.onlineWorker.text = String.init(format: "%d", model.OnLineCount!)
        //self.attendance.text = String.init(format: "%d", model.DutyRate!)
        self.attendance.text = model.DutyRate
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
