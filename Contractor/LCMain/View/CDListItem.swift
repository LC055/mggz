//
//  CDListItem.swift
//  mggz
//
//  Created by Apple on 2018/4/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class CDListItem: UITableViewCell {

    @IBOutlet weak var timelabel: UILabel!
    @IBOutlet weak var statuslabel: UILabel!
    
    var dataitem:ChiDaoItemResult?{
        get{
            return nil
        }
        set(newValue){
            self.timelabel.text = newValue?.SignedDay?.timeYearDate() as! String
            self.statuslabel.text = newValue?.SignedState as! String
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.timelabel.textColor = UIColor.LightBlack
        self.statuslabel.textColor = UIColor.LightBlack
        self.timelabel.font = UIFont.systemFont(ofSize: 15)
        self.statuslabel.font = UIFont.systemFont(ofSize: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
