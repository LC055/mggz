//
//  PresentTitleView.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/23.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import YYText

class PresentTitleView: UIView {
    var title:String?
    var introText:NSMutableAttributedString?
    var close:NormalClick?
    
    var line:UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.DeepBlackLight
        return view
    }()
    
    var closeView:UIButton = {
       var view = UIButton(type: .custom)
        view.setImage(UIImage(named: "closeimg"), for: .normal)
        view.imageView?.contentMode = UIViewContentMode.scaleAspectFill
        view.contentVerticalAlignment = .fill
        view.contentHorizontalAlignment = .fill
        view.addTarget(self, action: #selector(viewclick(_:)), for: .touchUpInside)
        return view
    }()
    
    var titleView:YYLabel = {
        var view = YYLabel()
        
        view.textColor = UIColor.DeepBlack
        view.font = UIFont.systemFont(ofSize: 18)
        view.textAlignment = .center
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(frame: CGRect,_title title:String,_close close:@escaping NormalClick) {
        self.init(frame: frame)
        self.title = title
        self.close = close
        self.initView()
    }
    
    @objc func viewclick(_ sender:UIButton){
        self.close!()
    }
    
    override func layoutSubviews() {
        
        var size = CGSize(width: ScreenWidth, height: CGFloat.greatestFiniteMagnitude)
        var layout = YYTextLayout.init(containerSize: CGSize(width: ScreenWidth, height: CGFloat.greatestFiniteMagnitude), text: self.introText!)
        self.titleView.textLayout = layout
        let textHeight = layout?.textBoundingSize.height
        
        self.titleView.snp.updateConstraints { (make) in
            make.centerY.equalTo(self).offset(20-textHeight!/2)
            make.centerX.equalTo(self)
        }
        
        self.closeView.snp.updateConstraints { (make) in
            make.height.width.equalTo(18)
            make.centerY.equalTo(self.titleView)
            make.left.equalTo(10)
        }
        
        self.line.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.height.equalTo(0.8)
            make.bottom.equalTo(self.snp.bottom)
        }
    }
    
    func initView(){
        self.introText = NSMutableAttributedString(string: self.title!, attributes: [NSAttributedStringKey.font:UIFont.boldSystemFont(ofSize: 17)])
        //self.titleView.attributedText = self.introText
        self.backgroundColor = UIColor.white
        self.addSubview(self.closeView)
        self.addSubview(self.titleView)
        self.addSubview(self.line)
        
    }
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
