//
//  QbItemView.swift
//  mggz
//
//  Created by Apple on 2018/4/25.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class QbItemView: UITableViewCell {

    @IBOutlet weak var projectname: UILabel!
    @IBOutlet weak var payAmount: UILabel!
    @IBOutlet weak var salaryMonths: UILabel!
    @IBOutlet weak var lastUpdateTime: UILabel!
    
    var itemData:QBItemDetail? {
        get{
            return nil
        }
        set(newValue){
            var moneyTxt = String(format:"\((newValue?.PayAmount ?? 0) > 0 ? "+":"")%.2f",newValue?.PayAmount ?? 0)
            self.projectname.text = newValue?.ProjectName ?? ""
            self.payAmount.attributedText = moneyTxt.getMoneyAttr()//"\((newValue?.PayAmount ?? 0))"
            self.salaryMonths.text = "\((newValue?.SalaryMonths ?? ""))"
            var t = "\((newValue?.LastUpdateTime ?? ""))"
            if t != ""{
                self.lastUpdateTime.text = t.timeYearDate()
            }else{
                self.lastUpdateTime.text = ""
            }
            //self.lastUpdateTime.text = "\((newValue?.LastUpdateTime ?? ""))"
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
