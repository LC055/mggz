//
//  ZhuhuCell.swift
//  mggz
//
//  Created by Apple on 2018/4/26.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class ZhuhuCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var money: UILabel!
    @IBOutlet weak var time: UILabel!
    var attr:NSMutableAttributedString?
    
    var dataItem:ZhListitem?{
        get{
            return nil
        }
        set(newValue){
            self.title.text = newValue?.SalaryMonths ?? ""
            var moneyTxt = (newValue?.PayAmount ?? 0) > 0 ? "+\((newValue?.PayAmount ?? 0).roundTo(2))":"-\((newValue?.PayAmount ?? 0).roundTo(2))"
            /*self.attr = NSMutableAttributedString(string: moneyTxt)
            self.attr?.addAttribute(.font, value: UIFont(name: "Arial-BoldMT", size: 20), range: NSMakeRange(0, moneyTxt.count))*/
            self.money.attributedText = moneyTxt.getMoneyAttr()//self.attr
            self.time.text = newValue?.LastUpdateTime?.timeYearDate()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
