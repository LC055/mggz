//
//  DwListItem.swift
//  mggz
//
//  Created by Apple on 2018/4/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class DwListItem: UITableViewCell {
    @IBOutlet weak var timelable: UILabel!
    @IBOutlet weak var content: UILabel!
    
    var dataItem:DWListResut?{
        get{
            return nil
        }
        set(newValue){
            self.timelable.text = newValue?.LocateDay?.timeYearAndMonth()
            if newValue?.ExceptionState == "轨迹异常"{
                self.content.text = "\((newValue?.ExceptionState)!)\(Int((newValue?.ExceptionCount)!))次"
            }else{
                self.content.text = "\((newValue?.ExceptionState)!)\(Int((newValue?.ExceptionCount)!))小时"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.timelable.textColor = UIColor.LightBlack
        self.content.textColor = UIColor.LightBlack
        self.timelable.font = UIFont.systemFont(ofSize: 15)
        self.content.font = UIFont.systemFont(ofSize: 15)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
