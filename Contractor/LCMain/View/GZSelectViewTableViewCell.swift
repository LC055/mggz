//
//  GZSelectViewTableViewCell.swift
//  mggz
//
//  Created by Apple on 2018/4/21.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class GZSelectViewTableViewCell: UITableViewCell {
    var _itemdata:(String,String)?
    var itemdata:(String,String)?{
        set(newValue){
            self._itemdata = newValue
            if newValue?.0 == "" && newValue?.1 == "余额明细"{
                self.titleValue.textColor = UIColor.red
                let tap = UITapGestureRecognizer(target: self, action: #selector(chaxun(_:)))
                self.titleValue.isUserInteractionEnabled = true
                self.titleValue.addGestureRecognizer(tap)
                
            }
            title.text = (newValue?.0)!
            titleValue.text = (newValue?.1)!
        }
        get{
            return _itemdata
        }
    }
    
    var deleget:NormalClick?
    

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var titleValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func chaxun(_ tap:UIGestureRecognizer){
        guard let t = deleget else{
            return
        }
        t()
    }
    
}
