//
//  WKQDetail.swift
//  mggz
//
//  Created by Apple on 2018/4/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class WKQDetail: UITableViewCell {

    @IBOutlet weak var headimg: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var type: UILabel!
    
    var dataitem:WKQResult?{
        get{
            return nil
        }
        set(newValue){
            self.name.text = newValue?.Name
            self.type.text = newValue?.WorkType
            
            if newValue?.PhotoPath == nil || (newValue?.PhotoPath?.isEmpty)!{
                self.headimg.image = UIImage(named: "scene_touxiang")
            }else{
                    if !(isScrollview!){
                        let path = "\(LC_IMAGE_URL)\((newValue?.PhotoPath)!)"
                        self.headimg.kf.setImage(with: URL.init(string: path), placeholder: UIImage(named: "scene_touxiang"), options: nil, progressBlock: nil, completionHandler: nil)
                        
                    }
                    
            }
        }
    }
    
    var isScrollview:Bool? = false
    override func awakeFromNib() {
        super.awakeFromNib()
        let f = self.headimg.bounds.size.width
        self.headimg.layer.cornerRadius = f/2
        self.headimg.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
