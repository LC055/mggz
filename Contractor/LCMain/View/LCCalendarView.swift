//
//  LCCalendarView.swift
//  mggz
//
//  Created by QinWei on 2018/1/16.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCCalendarView: UIView {
    var didSelectIndex:((_ model:LCProjectListModel)->Void)?
    var isShow : Bool?
    var menuSize : CGSize?
    class func initLCCalendarView(size:CGSize)->LCCalendarView{
        
        let frame = CGRect(x:0,y:0,width:size.width,height:size.height)
        let menu = LCCalendarView.init(frame:frame)
        menu.menuSize = size
        return menu
    }
    override init(frame: CGRect) {
        
        /**
         这时候 frame 的 height 设置为 0 ，是为了加载缓慢弹出的动画……
         */
        let  initialFrame = CGRect(x:frame.origin.x,y:frame.origin.y,width:frame.size.width,height:0.0)
        isShow = false
        super.init(frame: initialFrame)
        isHidden = true
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func popupMenu(orginPoint:CGPoint){
        
        if self.isShow == true {
            return
        }
        self.isShow = true
        self.isHidden = false
       // menuTab?.isHidden = false
        self.frame.origin = orginPoint
       // self.menuTab?.reloadData()
        self.superview?.bringSubview(toFront: self)
      //  menuTab?.frame.size.height = 0.0
        /**
         这里是 弹出的动画
         */
        UIView.animate(withDuration: 0.2, animations: {
            self.frame.size.height = (self.menuSize!.height)
        }) { (finish) in
        }
    }
    
    
    func packUpMenu() {
        
        if self.isShow == false {
            return
        }
        self.isShow = false
        
        UIView.animate(withDuration: 0.2, animations: {
            
            //self.menuTab?.frame.size.height = 0.0
            self.frame.size.height = 0.0
        }) { (finish) in
            
            self.isHidden = true
           // self.menuTab?.isHidden = true
        }
    }
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
