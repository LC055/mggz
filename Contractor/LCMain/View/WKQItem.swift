//
//  WKQItem.swift
//  mggz
//
//  Created by Apple on 2018/4/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class WKQItem: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    
    var dataitem:WKQTotalResult?{
        get{
            return nil
        }
        set(newValue){
            var title = "\((newValue?.SignedDay?.timeMonthAndDate())!)"+"("+"\(Int((newValue?.NonSignedCount)!))"+")"
            self.name.text = title
            
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.LightGreyBG
        self.name.textColor = UIColor.LightBlack
        self.name.font = UIFont.systemFont(ofSize: 17)
    }
    
    func setSelectStatus(){
        self.contentView.backgroundColor = UIColor.white
        self.name.textColor = UIColor.DeepBlack
        //self.name.font = UIFont.systemFont(ofSize: 19)
        var text = self.name.text
        var start = text?.range(of: "(", options: String.CompareOptions.literal)
        var end = text?.range(of: ")", options: String.CompareOptions.backwards)
        
        var t = (start?.lowerBound.encodedOffset)!+1
        var e = end?.lowerBound.encodedOffset
        
        let attr = NSMutableAttributedString(string: self.name.text!)
        attr.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: NSMakeRange(t, (e!-t)))
        attr.addAttribute(NSAttributedStringKey.font, value: UIFont.systemFont(ofSize: 19), range: NSMakeRange(0,(self.name.text?.count)!))
        self.name.attributedText = attr
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        print("--->\(selected)")
        if selected{
            self.setSelectStatus()
        }else{
            self.name.textColor = UIColor.LightBlack
            self.name.font = UIFont.systemFont(ofSize: 17)
        }

        // Configure the view for the selected state
    }
    
}
