//
//  EmptyView.swift
//  mggz
//
//  Created by Apple on 2018/4/21.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class EmptyView: UIView {
    
    var _statusTxt:String?
    var statusTxt:String?{
        get{
            return self._statusTxt
        }
        set(newValue){
            self._statusTxt = newValue
            self.status.text = newValue
        }
    }
    
    var top:Float = 0.0

    lazy var imgview:UIImageView = {
        var imgview = UIImageView(image: UIImage(named: "空状态"))
        imgview.contentMode = .scaleAspectFill
        imgview.clipsToBounds = true
        imgview.contentScaleFactor = UIScreen.main.scale
        return imgview
       
    }()
    
    lazy var status:UILabel = {
        var labelview = UILabel()
        labelview.text = self._statusTxt
        labelview.textColor = UIColor.DeepBlack
        labelview.textAlignment = .center
        return labelview
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    convenience init(frame: CGRect,_title title:String,_top top:Float = 0.0) {
        self.init(frame: frame)
        self.statusTxt = title
        self.top = top
        initView()
    }
    
    func initView(){
        self.backgroundColor = UIColor.LightGreyBG
        self.addSubview(imgview)
        self.addSubview(status)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imgview.snp.updateConstraints { (maker) in
            if self.top == 0{
                maker.center.equalTo(self)
            }else{
                maker.top.equalTo(self.snp.top).offset(self.top)
                maker.centerX.equalTo(self)
            }
            
            
            maker.width.equalTo(100)
            maker.height.equalTo(100)
        }
        
        self.status.snp.updateConstraints { (maker) in
            maker.top.equalTo(self.imgview.snp.bottom).offset(20)
            maker.centerX.equalTo(self)
            maker.left.equalTo(self.snp.left)
            maker.right.equalTo(self.snp.right)
            maker.height.equalTo(40)
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
