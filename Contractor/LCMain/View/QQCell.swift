//
//  QQCell.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class QQCell: UITableViewCell {
    @IBOutlet weak var headview: TextImageView!
    
    @IBOutlet weak var telview: UILabel!
    @IBOutlet weak var addressview: UILabel!
    @IBOutlet weak var nameview: UILabel!
    
    
    var itemdata:MWCurrentLocationModel?{
        get{
            return nil
        }
        set(newValue){
            
            self.headview.kf.setImage(with: URL.init(string: (ImageUrlPrefix + ((newValue?.MigrantWorker!["Photo"] as? NSDictionary)?.object(forKey: "Path") as? String ?? ""))), placeholder: UIImage(named: "scene_touxiang"), options: nil, progressBlock: nil, completionHandler: nil)
            
            self.nameview.text = String.init(format: "%@(%@)", (newValue?.MigrantWorker!["CompanyMG"] as? NSDictionary)?.object(forKey: "CompanyName") as? String ?? "",(newValue?.MigrantWorker!["MigrantWorkType"] as? NSDictionary)?.object(forKey: "WorkType") as? String ?? "")
            self.addressview.text = String(format: "坐标位置：%@", newValue?.Location?.object(forKey: "Address") as? String ?? "暂无位置信息")
            self.telview.text = String(format: "联系电话：%@", (newValue?.MigrantWorker!["CompanyMG"] as? NSDictionary)?.object(forKey: "LealPersonPhone") as? String ?? "")
            //self.nameView.text = (newValue?.MigrantWorker?.CompanyMG?.CompanyName ?? "") + "("+(newValue?.MigrantWorker?.MigrantWorkType?.WorkType ?? "")+")"
            //self.addressview.text = newValue?.Loca
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
