//
//  TextImgeView.swift
//  mggz
//
//  Created by Apple on 2018/5/8.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class TextImgeView: UIImageView {
    
    lazy var greylayer:CALayer={
        var layer = CALayer()
        layer.backgroundColor = UIColor.yellow.cgColor
        layer.opacity = 0.5
        layer.position = CGPoint(x: self.bounds.width/2, y: self.bounds.height/2)
        layer.bounds = self.bounds
        
        return layer
    }()
    
    override func layoutSubviews() {
        self.drawView()
    }
    
    func drawView(){
        self.layer.addSublayer(self.greylayer)
        self.layer.cornerRadius = self.bounds.width / 2
        self.layer.masksToBounds = true
    }
    
    override func draw(_ rect: CGRect) {
        print("drawrect")
    }
    
    override func draw(_ layer: CALayer, in ctx: CGContext) {
        print("drawCALayer")
    }
 

}
