//
//  GZItem.swift
//  mggz
//
//  Created by Apple on 2018/4/23.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class GZItem: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var rightTitle: UILabel!
    @IBOutlet weak var money: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var content: UIView!
    
    @IBOutlet weak var line: UIView!
    var itemData:GZTotalItem?{
        get{
            return nil
        }
        set(newValue){
            self.title.textColor = UIColor.LightBlack
            self.title.text = newValue?.SalaryMonths ?? ""
            var moneyTxt = "\((newValue?.PayAmount ?? 0).roundTo(2))元"
            self.money.attributedText = moneyTxt.getMoneyAttr(26.0)
            if newValue!.DeductionCount == nil || newValue!.DeductionCount! == 0 || newValue?.SalaryState == "待发"{
                self.rightTitle.text = ""
            }else{
                self.rightTitle.text = "\((newValue?.DeductionCount ?? 0).roundTo(0))笔减扣"
            }
            self.rightTitle.textColor = UIColor.LightBlack
            
            self.status.text = (newValue?.SalaryState)!
            if newValue?.SalaryState == "已发"{
                self.status.textColor = UIColor.LightGreen
            }else if newValue?.SalaryState == "超期未发"{
                self.status.textColor = UIColor.LightRed
            }else{
                self.status.textColor = UIColor.LightBlack
            }
            
            self.time.textColor = UIColor.LightBlack
            
            if newValue?.LastUpdateTime == nil || newValue?.LastUpdateTime == ""{
                self.time.text = ""
            }else{
                self.time.text = newValue?.LastUpdateTime?.timeSimple()
            }
            
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        print("--->\(self.line.bounds.width)")
        self.line.drawline(10, _lineSpacing: 2, _lineColor: UIColor.DeepBlack)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.content.shadowEnable = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
