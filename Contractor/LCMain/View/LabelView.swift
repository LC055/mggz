//
//  LabelView.swift
//  mggz
//
//  Created by Apple on 2018/5/8.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LabelView: UIView {

    let lineWidth:CGFloat = 20.0
    let textSize:CGFloat = 12.0
    var label:NSString = "缺勤"
    
    override func awakeFromNib() {
        print("awakeFromNib")
    }
    
    override func layoutSubviews() {
        print("layoutSubviews")
    }
    
    override func draw(_ rect: CGRect) {
        print("draw")
        
        self.backgroundColor = UIColor.white
        var context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(18.0)
        
        context?.setStrokeColor(UIColor.red.cgColor)
        context?.move(to: CGPoint(x: self.bounds.width/2, y: -(0.52*self.lineWidth)))
        context?.addLine(to: CGPoint(x: self.bounds.width+(0.52*self.lineWidth), y: self.bounds.height/2))
        context?.strokePath()
        
        //var font = UIFont(name: "Arial", size: 15.0)
        //self.label.draw(at:CGPoint(x: self.bounds.width/2, y: self.bounds.height/2), withAttributes: [NSAttributedStringKey.font : UIFont(name: "Arial", size: 15.0)])
        //var b = CGSize(width: 100.0, height: Double(MAXFLOAT))
        
        var lablesize = self.label.boundingRect(with: CGSize(width: 100.0, height: Double(MAXFLOAT)), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: CGFloat(12.0))], context: nil)
        
        var textlayer = CATextLayer()
        textlayer.string = self.label
        textlayer.bounds = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        textlayer.fontSize = self.textSize
        textlayer.isWrapped = true
        textlayer.alignmentMode = kCAAlignmentLeft
        textlayer.foregroundColor = UIColor.white.cgColor
        textlayer.contentsScale = UIScreen.main.scale
        textlayer.position = CGPoint(x: self.bounds.width/4*3+lablesize.height/2*cos(CGFloat(45)), y: self.bounds.height/4*3-lablesize.height/2*cos(CGFloat(45)))
        textlayer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(M_PI/180.0*45.0)))
        //var r = CGAffineTransformMakeRotation(CGFloat(M_PI/180.0*45.0))
        textlayer.transform = CATransform3DRotate(textlayer.transform, CGFloat(M_PI/180.0*45.0), 0, 0, 0)//CGAffineTransformMakeRotation(CGFloat(0.25))
        self.layer.addSublayer(textlayer)
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */

}
