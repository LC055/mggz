//
//  ChiDaoItem.swift
//  mggz
//
//  Created by Apple on 2018/4/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct ChiDaoItem:Codable{
    var State:String?
    var Result:[ChiDaoItemResult]?
}

struct ChiDaoItemResult:Codable{
    var SignedDay:String?
    var SignedState:String?
}
