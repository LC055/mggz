//
//  ZhList.swift
//  mggz
//
//  Created by Apple on 2018/4/27.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct ZhList:Codable{
    var State:String?
    var Result:ZhListResult?
}

struct ZhListResult:Codable{
    var SalaryList:[ZhListitem]?
    var Total:Double?
}

struct ZhListitem:Codable{
    
    var LastUpdateTime:String?
    var SalaryMonths:String?
    var PayAmount:Double?
}
