//
//  KaoQingTotalDetail.swift
//  mggz
//
//  Created by Apple on 2018/4/20.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct KaoQingTotalDetail:Codable{
    var State:String?
    var Result:KaoQingTotalDetailResult?
}

struct KaoQingTotalDetailResult:Codable{
    var TotalSignedExceptionCount:Int?
    var TotalLocationExceptionCount:Int?
    var TotalSignedNonCount:Int?
}
