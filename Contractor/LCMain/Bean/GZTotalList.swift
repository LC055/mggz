//
//  GZTotalList.swift
//  mggz
//
//  Created by Apple on 2018/4/23.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct GZTotalList : Codable{
    var State:String?
    var Result:GZTotalListResult!
}

struct GZTotalListResult:Codable{
    var Total:Int?
    var SalaryList:[GZTotalItem]!
}

struct GZTotalItem : Codable{
    var LastUpdateTime:String?
    var SalaryMonths:String?
    var SalaryState:String?
    var PayAmount:Double?
    var DeductionCount:Double?
}
