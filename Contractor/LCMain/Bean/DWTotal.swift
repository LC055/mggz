//
//  DWTotal.swift
//  mggz
//
//  Created by Apple on 2018/4/23.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct DWTotal : Codable{
    var State:String?
    var Result:[DWItem]?
}

struct DWItem:Codable {
    var DemandBaseMigrantWorkerID:String?
    var PhotoPath:String?
    var Name:String?
    var ExceptionCount:Float?
    var LealPersonPhone:String?
    var SignedExceptionCount:Double?
}
