//
//  GZTotal.swift
//  mggz
//
//  Created by Apple on 2018/4/21.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct GZTotal:Codable{
    var State:String?
    var Result:GZTotalDetail?
}

struct GZTotalDetail:Codable{
    var ResidualAmount:Double?
    var IsSignSalary:Bool?
    var IsFlat:Bool?
    var ApplyedSalaryAmount:Double?
    var NonApplySalaryAmount:Double?
    var PayedSalaryAmount:Double?
    var AdvaceSalaryAmount:Double?
    var NonPaySalaryAmount:Double?
    var NonBalanceSalaryAmount:Double?
}
