//
//  QBItem.swift
//  mggz
//
//  Created by Apple on 2018/4/25.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct QBItem:Codable{
    var State:String?
    var Result:QBList?
}

struct QBList:Codable{
    var SalaryList:[QBItemDetail]?
}

struct QBItemDetail:Codable{
    var ProjectName:String?
    var LastUpdateTime:String?
    var SalaryMonths:String?
    var PayAmount:Double?
}
