//
//  KaoQingTotal.swift
//  mggz
//
//  Created by Apple on 2018/4/20.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct KaoQingTotal:Codable{
    var State:String?
    var Result:KaoQingTotalResult?
}

struct KaoQingTotalResult:Codable{
    var TotalDays:Int?
    var TotalWorkDays:String?
    var TotalWorkHours:String?
    var TotalWorkers:Int?
    var TotalNotPayedWorkDays:String?
    var TotalNotPayedWorkHours:String?
    var WorkDayType:Int?
}
