//
//  WKQList.swift
//  mggz
//
//  Created by Apple on 2018/4/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct WKQList:Codable {
    var State:String?
    var Result:[WKQResult]?
}

struct WKQResult:Codable{
    var Name:String?
    var DemandBaseMigrantWorkerID:String?
    var PhotoPath:String?
    var WorkType:String?
    var LealPersonPhone:String?
}
