//
//  ZhTotal.swift
//  mggz
//
//  Created by Apple on 2018/4/26.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct ZhTotal:Codable{
    var State:String?
    var Result:ZhResult?
}

struct ZhResult:Codable{
    var ResidualAmount:Double?
    var ProjectName:String?
}
