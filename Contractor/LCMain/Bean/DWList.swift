//
//  DWList.swift
//  mggz
//
//  Created by Apple on 2018/4/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct DWList:Codable{
    var State:String?
    var Result:[DWListResut]?
}

struct DWListResut:Codable{
    var LocateDay:String?
    var ExceptionCount:Float?
    var ExceptionState:String?
}


