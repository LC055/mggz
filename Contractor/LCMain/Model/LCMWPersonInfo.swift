//
//  LCMWPersonInfo.swift
//  mggz
//
//  Created by QinWei on 2018/1/15.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class LCMWPersonInfo: Mappable {
    var IsWorkTimeLack : Bool?
    var LocateLossHours : Float?
    var MigrantWorkerNo : String?
    var Name : String?
    var Path : String?
    var SignedInTime : String?
    var SignedOutTime : String?
    required init?(map: Map) {
    
        
    }
    func mapping(map: Map) {
        IsWorkTimeLack       <- map["IsWorkTimeLack"]
        LocateLossHours       <- map["LocateLossHours"]
        MigrantWorkerNo       <- map["MigrantWorkerNo"]
        Name       <- map["Name"]
        Path       <- map["Path"]
        SignedInTime       <- map["SignedInTime"]
        SignedOutTime       <- map["SignedOutTime"]
    }
}
