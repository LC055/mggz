//
//  LCMWTrackModel.swift
//  mggz
//
//  Created by QinWei on 2018/1/12.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class LCMWTrackModel: Mappable {
    
    var Longitude : String?
    var Latitude : String?
    var Address : String?
    var IsLoss : Bool?
    var LossBeginTime : String?
    var LossEndTime : String?
    var IsStayOut : Bool?
    var StayOutBeginTime : String?
    var StayOutEndTime : String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Longitude       <- map["Longitude"]
        Latitude       <- map["Latitude"]
        Address       <- map["Address"]
        IsLoss       <- map["IsLoss"]
        LossBeginTime       <- map["LossBeginTime"]
        LossEndTime       <- map["LossEndTime"]
        IsStayOut       <- map["IsStayOut"]
        StayOutBeginTime       <- map["StayOutBeginTime"]
        StayOutEndTime       <- map["StayOutEndTime"]
    }
}

class LCMWTrackPersonModel: Mappable{
    
    var Path : String?
    var MigrantWorkerNo : String?
    var Name : String?
    var SignedInTime : String?
    var SignedOutTime : String?
    var IsWorkTimeLack : Bool?
    var LocateLossHours : Double?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Path       <- map["Path"]
        MigrantWorkerNo       <- map["MigrantWorkerNo"]
        Name       <- map["Name"]
        SignedInTime       <- map["SignedInTime"]
        SignedOutTime       <- map["SignedOutTime"]
        IsWorkTimeLack       <- map["IsWorkTimeLack"]
        LocateLossHours       <- map["LocateLossHours"]
    }
}


