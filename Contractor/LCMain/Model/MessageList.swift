//
//  MessageList.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/23.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct MessageList:Codable{
    var Result:MessageResult?
}

struct MessageResult:Codable{
    var Items:[MessageItem]?
}

struct MessageItem:Codable{
    var ID:String?
    var OrderDataID:String?
    var CreateTime:String?
    var Title:String?
    var Description:String?
    var IsDetail:Bool?
    var JsonParams:String?
}
