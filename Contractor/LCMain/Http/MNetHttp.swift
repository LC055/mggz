//
//  MNetHttp.swift
//  mggz
//
//  Created by Apple on 2018/4/20.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Alamofire

private let http = MNetHttp()
class MNetHttp: NSObject {
    
    enum MethodType:String {
        case KQTotal = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerStatistics.GetProjectSignedStatistics"
        case KQTotalDetail = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerStatistics.GetProjectMonthSignedStatistics"
        case GZTotal = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerStatistics.GetSalaryStatistics"
        case GZTotalList = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerStatistics.GetSalaryStatisticsDetail"
        case DWTotal = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerStatistics.GetLocationExceptionStatistics"
        case DWDetailList = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerStatistics.GetLocationExceptionStatisticsDetail"
        case ChiDaoTotal = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerStatistics.GetSignedExceptionStatistics"
        case ChiDaoDetail = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerStatistics.GetSignedExceptionStatisticsDetail"
        case KaoQingTotal = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerStatistics.GetNonSignedStatistics"
        case WKaoQingList = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerStatistics.GetNonSignedStatisticsDetail"
        case QIANBAOList = "MarTian.MigrantWorkerManage.Handler.AjaxSpecialAccount.GetSpecialAccountHold"
        case QIANBAOListDetail = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerStatistics.GetSpecialAccountHoldDetail"
        case ZhTotal = "MarTian.MigrantWorkerManage.Handler.AjaxSpecialAccount.GetSpecialAccount"
        case ZhDetail = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerStatistics.GetSpecialAccountDetail"
        
        case GZGJ = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerLocation.GetMigrantWorkerLocation"
        case ADDBZ = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkGroup.GetWorkGroup"//添加班组
        case SETWORKER = "MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorker.SetDemandBaseMigrantWorkerGroup"//设置添加班组
        case KQTIMESET = "MarTian.MigrantWorkerManage.Handler.AjaxDBHandler.SaveData"//考勤时间段保存   员工入职的条件"
        case BLACKPERSON = "MarTian.MigrantWorkerManage.Handler.AjaxConstructionTeamDetail.GetMigrantWorker"//黑名单列表
        case DELEPERSON = "MarTian.MigrantWorkerManage.Handler.AjaxConstructionTeamDetail.SetMigrantWorkerStop"//删除人员
        case ADDBLACKPERSON = "MarTian.MigrantWorkerManage.Handler.AjaxConstructionTeamDetail.SetMigrantWorkerToBlankList"//加入黑名单
        case PROJECTLIST = "MarTian.MigrantWorkerManage.Handler.AjaxMarketDemandBase.GetMarketDemandBase"//项目列表
        case ADDTOPROJECT = "MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorker.SetDemandBaseMigrantWorker"//添加到项目中
        case REGISTSFZH = "MarTian.MigrantWorkerManage.Handler.AjaxConstructionTeamDetail.CheckWorkingState"//包工头注册民工身份证
        case GZLIST = "MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkType.GetMigrantWorkType"//工种列表
        case KQSJLIST = "MarTian.MigrantWorkerManage.Handler.AjaxDBHandler.FindPagingOrderData"//考勤时间list
        case GHYZ = "MarTian.MigrantWorkerManage.Handler.AjaxConstructionTeamDetail.CheckMigrantWorkerNo"  //工号验证
        case JGFS = "MarTian.MigrantWorkerManage.Handler.AjaxConstructionBasicSet.GetWorkDayType"//计工方式
        case QueckRegister = "MarTian.MigrantWorkerManage.Handler.AjaxConstructionTeamDetail.QueckRegister"//入职基本身份信息
        case PayDay = "MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSignedTactics.GetSalaryTacticsByID"//包工头支付时间获取
        
        case SaveGzJS = "MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorkerSignedTactics.SaveSalaryTactics" //保存工资结算设置
        
        case DEVIDEREGISE = "KouFine.Common.MessageHub.Handler.AjaxAppClient.RegisterClient"//设备注册
        case MESSAGE = "MarTian.MigrantWorkerManage.Handler.AjaxProcNotice.GetProcNoticeForApp"//代办消息
        case BlackPersonOut = "MarTian.MigrantWorkerManage.Handler.AjaxConstructionTeamDetail.SetMigrantWorkerOutBlankList"//移出黑名单
        case MessageDetail = "KouFine.Common.MessageHub.Handler.AjaxProcNotice.GetProcNoticeJsonParams" //消息详情
        case MessageStatus = "MarTian.MigrantWorkerManage.Handler.AjaxCommon.GetOrderDataWorkFlowState"//消息状态
        
        case MessageAgress = "KouFine.Handler.Core.AjaxOperate.Approve" // 同意
        case MessageDisAgress = "KouFine.Handler.Core.AjaxOperate.Back" //不同意
        case LogoffClient = "KouFine.Common.MessageHub.Handler.AjaxAppClient.LogoffClient"//注销客户端
        
    }
    
    class var sharedInstance:MNetHttp{
        return http
    }
    
    func postSingle <T:Decodable> (_ url:String,method method:MethodType,params params:inout [String:Any],type type:T.Type,success success:@escaping SuccessResponse,error errorResponse:@escaping ErrorResponse){
        guard let httpHeader = WWUser.sharedInstance.mobile_cilent_headers else{
            return
        }
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
        params["AccountId"] = accountId
        params["accountId"] = accountId
        params["Method"] = method.rawValue
        
        
        
        
        print("\(params)")
        Alamofire.request(url, method: .post, parameters: params, headers: httpHeader).responseData { (response) in
            if response.result.isSuccess{
                do{
                    guard var data = response.result.value else{
                        print("数据空")
                        return
                    }
                    var re = (String.init(data: response.result.value!, encoding: String.Encoding.utf8))!
                    print("\(re)")
                    //var resStr = String.init(data: data, encoding: String.Encoding.utf8)
                    //print("\(resStr)")
                    let dicData = try JSONSerialization.jsonObject(with: response.result.value!, options: .allowFragments) as! Dictionary<String,Any>
                    //let r = (String.init(data: response.result.value!, encoding: String.Encoding.utf8))?.replacingOccurrences(of: "/", with: "")
                    
                    let r = re.replacingOccurrences(of: "/", with: "", options: String.CompareOptions.literal, range: nil)
                    //print("\(r)")
                    if dicData["State"] as? String == "OK" || dicData["Status"] as? String == "OK"{
                        let result = try JSONDecoder().decode(type, from: response.result.value!)
                        success(result,dicData)
                    }else{
                        errorResponse(ErrorType.StateFail(dicData))
                    }
                    
                }
                catch {
                    var re = (String.init(data: response.result.value!, encoding: String.Encoding.utf8))!
                    errorResponse(ErrorType.StateFail(re))
                }
                
            }else{
                errorResponse(ErrorType.HTTPERROR)
            }
        }
        
        
        
        
    }
    

}
