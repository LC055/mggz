//
//  MWCurrentLocationModel.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/8.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class MWCurrentLocationModel: Mappable{
    enum signedResult:Int {
        case 未签到 = -1
        case 签入  = 0
        case 签出  = 1
    }
    var ID : String?
    var MigrantWorkerNo : String?
    var MigrantWorker:NSDictionary?
    var Photo : NSDictionary?
    var MarketSupplyBase : NSDictionary?
    var IsOnline : Bool?
    var SignedResult : signedResult?
    var IsAbsence : Bool?
    var Location : NSDictionary?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        ID       <- map["ID"]
        MigrantWorkerNo       <- map["MigrantWorkerNo"]
        MigrantWorker       <- map["MigrantWorker"]
        Photo       <- map["Photo"]
        MarketSupplyBase       <- map["MarketSupplyBase"]
        IsOnline       <- map["IsOnline"]
        SignedResult       <- map["SignedResult"]
        IsAbsence       <- map["IsAbsence"]
        Location       <- map["Location"]
    }
}

















