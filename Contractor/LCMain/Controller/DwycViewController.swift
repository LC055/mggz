//
//  DwycViewController.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/23.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

//定位异常
class DwycViewController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    var datalist = [MWCurrentLocationModel]()
    var datavariable:PublishSubject<[MWCurrentLocationModel]> = PublishSubject()
    var emptyview:EmptyView?
    
    @objc func gjBtn(_ btn:UIButton){
        
        var vc = WorkGuiJiControllerViewController(nibName: "WorkGuiJiControllerViewController", bundle: nil)
        //vc.MarketSupplyBaseID = self.MigrantWorkerID ?? ""
        vc.DemandBaseMigrantWorkerID = self.datalist[btn.tag ?? 0].ID
        
        if self.datalist[btn.tag ?? 0].Location == nil || self.datalist[btn.tag ?? 0].Location?.object(forKey: "LocateTime") as? String == nil{
            vc.LocateTime = ""
        }else{
            vc.LocateTime = self.datalist[btn.tag ?? 0].Location?.object(forKey: "LocateTime") as? String ?? ""
        }
        
        vc.MarketSupplyBaseID = ProjectId ?? ""
        vc.ProjectRange = ProjectMapPoint
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableview.register(UINib(nibName: "DwycCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.tableview.tableFooterView = UIView(frame: CGRect.zero)
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 110;
        self.datavariable.asObservable().bind(to: self.tableview.rx.items){
            (_,row,element) in
            var cell = self.tableview.dequeueReusableCell(withIdentifier: "cell", for: IndexPath(row: row, section: 0)) as? DwycCell
            
            if cell == nil{
                cell = DwycCell(style: .default, reuseIdentifier: "cell")
            }
            cell?.gjiview.tag = row
            cell?.gjiview.addTarget(self, action: #selector(self.gjBtn(_:)), for: .touchUpInside)
            cell?.itemdata = element
            cell?.selectionStyle = .none
            return cell!
        }
        
        self.tableview.rx.itemSelected.subscribe({
            index in
            self.tableview.deselectRow(at: index.element!, animated: true)
            var mwDetail = MWDetailInfoViewController(nibName: "MWDetailInfoViewController", bundle: nil)
            
            mwDetail.DemandBaseMigrantWorkerID = self.datalist[index.element?.row ?? 0].ID
            mwDetail.currentProjectName = ProjectName ?? ""
            mwDetail.currentProjectId = ProjectId ?? ""
            mwDetail.isZG = true
            mwDetail.ProjectRange = ProjectMapPoint
            if self.datalist[index.element?.row ?? 0].Location == nil || self.datalist[index.element?.row ?? 0].Location?.object(forKey: "LocateTime") as? String == nil{
                mwDetail.LocateTime = ""
            }else{
                mwDetail.LocateTime = self.datalist[index.element?.row ?? 0].Location?.object(forKey: "LocateTime") as? String ?? ""
            }
            
            
            self.navigationController?.pushViewController(mwDetail, animated: true)
        })

        // Do any additional setup after loading the view.
    }
    
    func addData(_ data:[MWCurrentLocationModel]){
        self.datalist.removeAll()
        self.datalist.append(contentsOf: data)
        self.datavariable.onNext(self.datalist)
        if data == nil || data.count == 0{
            self.addEmptyView("暂无更多数据")
        }else{
            self.removeEmptyView()
        }
    }
    
    func removeEmptyView(){
        if self.emptyview != nil{
            self.emptyview?.removeFromSuperview()
            self.emptyview = nil
        }
    }
    
    
    func addEmptyView(_ status:String = "网络异常"){
        if self.emptyview != nil{
            self.emptyview?.removeFromSuperview()
            self.emptyview = nil
        }
        self.emptyview = EmptyView(frame: CGRect.zero, _title: status, _top: Float(ScreenHeight/5))
        self.view.addSubview(self.emptyview!)
        self.emptyview?.snp.updateConstraints({ (make) in
            make.top.equalTo(self.tableview.snp.top)
            make.left.equalTo(self.tableview.snp.left)
            make.right.equalTo(self.tableview.snp.right)
            make.bottom.equalTo(self.tableview.snp.bottom)
        })
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
