//
//  LCStatisticsController.swift
//  mggz
//
//  Created by Apple on 2018/4/19.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCStatisticsController: BaseLcController {
    
    @IBOutlet weak var gongzi: UILabel!
    @IBOutlet weak var kaoqing: UILabel!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var projectName: UILabel!
    var img:UIImageView?
    
    @IBOutlet weak var slideviewConstraint: NSLayoutConstraint!
    lazy fileprivate var tap:UITapGestureRecognizer = {
        var p = UITapGestureRecognizer(target: self, action: #selector(segmentClick))
        p.numberOfTapsRequired = 1
        p.numberOfTouchesRequired = 1
        return p
    }()
    
    lazy fileprivate var tap2:UITapGestureRecognizer = {
        var p = UITapGestureRecognizer(target: self, action: #selector(segmentClick))
        p.numberOfTapsRequired = 1
        p.numberOfTouchesRequired = 1
        return p
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let img = UIColor.LightGreyBG.createImageWithColor()
        self.navigationController?.navigationBar.setBackgroundImage(img, for: .default)
        self.navigationController?.navigationBar.shadowImage = img
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIColor.white.createImageWithColor(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIColor.gray.withAlphaComponent(0.5).createImageWithColor()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "统计"
        self.kaoqing.text = "考勤"
        self.gongzi.text = "工资"
        self.projectName.text = ProjectName ?? ""
        // Do any additional setup after loading the view.
        initView()
        setupChildController()
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addChildVcIntoScrollView()
    }
    
    
    func initView(){
        scrollview.backgroundColor = UIColor.white
        scrollview.showsHorizontalScrollIndicator = false
        scrollview.showsVerticalScrollIndicator = false
        scrollview.isPagingEnabled = true
        scrollview.scrollsToTop = false
        scrollview.bounces = false
        scrollview.isScrollEnabled = false
        scrollview.contentSize = CGSize(width: 2*SCREEN_WIDTH, height: 0)
        self.kaoqing.isUserInteractionEnabled = true
        self.kaoqing.isMultipleTouchEnabled = true
        self.gongzi.isUserInteractionEnabled = true
        self.gongzi.isMultipleTouchEnabled = true
        self.kaoqing.tag = 0
        self.gongzi.tag = 1
        
        self.kaoqing.addGestureRecognizer(tap)
        self.gongzi.addGestureRecognizer(tap2)
    }
    
    private func setupChildController(){
        let scene = KQViewController(nibName: "KQViewController", bundle: nil)
        self.addChildViewController(scene)
        let lcList = GZViewController(nibName: "GZViewController", bundle: nil)
        self.addChildViewController(lcList)
    }
    private func addChildVcIntoScrollView(){
        for i in 0..<2 {
            let childVc = self.childViewControllers[i]
            if childVc.isViewLoaded {
                return
            }
            let childVcView = childVc.view
            let scrollViewW = CGFloat(i) * SCREEN_WIDTH
            if i==0{
                childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
            }else{
                childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: SCREEN_WIDTH, height: self.scrollview.frame.height)
            }
            scrollview.addSubview(childVcView!)
            
        }
    }
    
    @objc fileprivate func segmentClick(_ sender:UIGestureRecognizer){
        if sender.view?.tag == 0{
            /*UIView.animate(withDuration: 1, animations: {
                self.scrollview.contentOffset = CGPoint(x: 0, y: self.scrollview.contentOffset.y)
            })*/
            
            if self.slideviewConstraint.constant != 0{
                self.slideviewConstraint.constant = 0
                UIView.animate(withDuration: 0.3, animations: {
                    self.scrollview.contentOffset = CGPoint(x: 0, y: self.scrollview.contentOffset.y)
                    self.view.layoutIfNeeded()
                })
            }
            
        }else{
            /*UIView.animate(withDuration: 1, animations: {
                
            })*/
            
            if self.slideviewConstraint.constant == 0{
                self.slideviewConstraint.constant = ScreenWidth/2
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.scrollview.contentOffset = CGPoint(x: SCREEN_WIDTH, y: self.scrollview.contentOffset.y)
                    self.view.layoutIfNeeded()
                })
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
