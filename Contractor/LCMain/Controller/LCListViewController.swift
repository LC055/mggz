//
//  LCListViewController.swift
//  mggz
//
//  Created by QinWei on 2018/1/4.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCListViewController: UIViewController {

    @IBOutlet weak var signButton: UIButton!
    
    @IBOutlet weak var offLineButton: UIButton!
    fileprivate lazy var showView : UIView = {
        let showView = UIView(frame: CGRect(x: 0, y: 77, width: SCREEN_WIDTH/2, height: 2))
        showView.backgroundColor = UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1)
        return showView
    }()
    
    fileprivate lazy var showViewBg : UIView = {
        let showView = UIView(frame: CGRect(x: 0, y: 78, width: SCREEN_WIDTH, height: 1))
        showView.backgroundColor = UIColor.gray.withAlphaComponent(0.4)
        return showView
    }()
    
    fileprivate lazy var scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = UIColor.white
        scrollView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.scrollsToTop = false
        scrollView.bounces = false
        scrollView.isScrollEnabled = false
        scrollView.contentSize = CGSize(width: 2*SCREEN_WIDTH, height: 0)
        return scrollView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(scrollView)
        self.setupChildController()
        self.addChildVcIntoScrollView()
        self.view.bringSubview(toFront: signButton)
        self.view.bringSubview(toFront: offLineButton)
        self.view.addSubview(self.showViewBg)
        self.view.addSubview(self.showView)
    }
    
    private func setupChildController(){
        let sign = LCSignViewController(nibName: "LCSignViewController", bundle: nil)
        sign.signCount = {(value) in
            self.signButton.setTitle("出勤(" + String(value)+")", for: .normal)
        }
        self.addChildViewController(sign)
        let offline = LCOffLineViewController(nibName: "LCOffLineViewController", bundle: nil)
        offline.offlineCount = {(value) in
            self.offLineButton.setTitle("未签到(" + String(value)+")", for: .normal)
        }
        self.addChildViewController(offline)
    }
    private func addChildVcIntoScrollView(){
        for i in 0..<2 {
            let childVc = self.childViewControllers[i]
            if childVc.isViewLoaded {
                return
            }
            let childVcView = childVc.view
            let scrollViewW = CGFloat(i) * SCREEN_WIDTH
            childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
            scrollView.addSubview(childVcView!)
        }
    }
    
    @IBAction func signClick(_ sender: Any) {
        
        UIView.animate(withDuration: 0.2) {
            self.scrollView.contentOffset = CGPoint(x: 0, y: self.scrollView.contentOffset.y)
            self.signButton .setTitleColor(UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1), for: .normal)
            self.offLineButton.setTitleColor(UIColor.black, for: .normal)
            self.showView.frame = CGRect(x: 0, y: self.showView.frame.origin.y, width: self.showView.frame.size.width, height: self.showView.frame.size.height)
        }
    }
    @IBAction func offlineClick(_ sender: Any) {
        let showX = SCREEN_WIDTH/2
        
        UIView.animate(withDuration: 0.2) {
            
           self.scrollView.contentOffset = CGPoint(x: SCREEN_WIDTH, y: self.scrollView.contentOffset.y)
            self.signButton .setTitleColor(UIColor.black, for: .normal)
            self.offLineButton.setTitleColor(UIColor.init(red: 61/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1), for: .normal)
            self.showView.frame = CGRect(x: showX, y: self.showView.frame.origin.y, width: self.showView.frame.size.width, height: self.showView.frame.size.height)
        }
        
    }
}
// MARK:- 代理方法
extension LCListViewController : UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

    }
}






