//
//  YCJBController.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/23.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class YCJBController: UIViewController {
    @IBOutlet var scrollview: UIScrollView!
    @IBOutlet var projectName: UILabel!
    @IBOutlet var dwsbview: UILabel!
    @IBOutlet var qqingview: UILabel!
    
    @IBOutlet weak var slideviewConstraint: NSLayoutConstraint!
    var scene:DwycViewController?
    var lcList:QQController?
    
    var downLine = [TimeItem]()
    var absence = [TimeItem]()
    
    /*override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.LightGreyBG
        self.navigationController?.navigationBar.clipsToBounds = true
        self.view.backgroundColor = UIColor.LightGreyBG
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.clipsToBounds = false
        self.navigationController?.navigationBar.barTintColor = UIColor.LightGrey
    }*/
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "异常警报"
        self.projectName.text = ProjectName ?? ""
        self.dwsbview.text = "定位失败"
        self.qqingview.text = "缺勤"
        self.projectName.tag = -1
        self.dwsbview.tag = 0
        self.qqingview.tag = 1
        
        
        self.scrollview.showsHorizontalScrollIndicator = false
        self.scrollview.showsVerticalScrollIndicator = false
        self.scrollview.isPagingEnabled = true
        self.scrollview.scrollsToTop = false
        self.scrollview.bounces = false
        self.scrollview.isScrollEnabled = false
        
        self.addTapClick(self.dwsbview)
        self.addTapClick(self.qqingview)
        self.addTapClick(self.projectName)
        self.setupChildController()
        self.addChildVcIntoScrollView()
        self.getYcData()
    }
    
    func addTapClick(_ view:UILabel){
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewClick(_:)))
        tap.numberOfTouchesRequired = 1
        tap.numberOfTapsRequired = 1
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tap)
    }
    
    func viewClick(_ sender:UIGestureRecognizer){
        switch sender.view?.tag {
        case 0?:
            if self.slideviewConstraint.constant != 0{
                self.slideviewConstraint.constant = 0
                UIView.animate(withDuration: 0.3, animations: {
                    self.scrollview.contentOffset = CGPoint(x: 0, y: self.scrollview.contentOffset.y)
                    //self.view.layoutIfNeeded()
                })
            }
            break
        case 1?:
            if self.slideviewConstraint.constant == 0{
                self.slideviewConstraint.constant = ScreenWidth/2
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.scrollview.contentOffset = CGPoint(x: SCREEN_WIDTH, y: self.scrollview.contentOffset.y)
                    //self.view.layoutIfNeeded()
                    
                    
                })
            }
            break
        default:
            var vc = LCStatisticsController(nibName: "LCStatisticsController", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
            break
        }
    }
    
    private func setupChildController(){
        self.scene = DwycViewController(nibName: "DwycViewController", bundle: nil)
        self.addChildViewController(self.scene!)
        self.lcList = QQController(nibName: "QQController", bundle: nil)
        self.addChildViewController(self.lcList!)
    }
    private func addChildVcIntoScrollView(){
        for i in 0..<2 {
            let childVc = self.childViewControllers[i]
            if childVc.isViewLoaded {
                return
            }
            let childVcView = childVc.view
            let scrollViewW = CGFloat(i) * SCREEN_WIDTH
            if i==0{
                childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: SCREEN_WIDTH, height: self.scrollview.frame.height)
            }else{
                childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: SCREEN_WIDTH, height: self.scrollview.frame.height)
            }
            self.scrollview.addSubview(childVcView!)
        }
    }
    
    
    func getYcData(){
        QWTextWithStatusHud("正在加载...", target: self.view)
        var params:[String:Any] = ["start":"0","limit":"2000","sql":"SignedResult >= -1","sqlParam":"{}","supplybaseid":ProjectId ?? "","pageName":"MarTian.List.MinGongGuiJi","orderManagerConfigId":"5e797081-b073-6495-65ae-c2f524dc3345"]
        MNetHttp.sharedInstance.postSingle(API_CALL_METHOD, method: .KQSJLIST, params: &params, type: KQSJList.self, success: { (bean,_) in
            QWHudDiss(self.view)
            guard var data = bean as? KQSJList else{
                return
            }
            data.Result?.Items?.forEach({ (item) in
                if item.SignedResult == -1 && (item.IsAbsence ?? false){
                    self.absence.append(item)
                }else if item.SignedResult == 0 && (item.IsOnline ?? false){
                    self.downLine.append(item)
                }
            })
            //self.scene?.addData(self.downLine)
            //self.lcList?.addData(self.absence)
            self.dwsbview.text = String(format: "定位异常(%d)", self.downLine.count)//"定位失败"
            self.qqingview.text = String(format: "缺勤(%d)", self.absence.count)//"缺勤"
        }) { (error) in
            QWHudDiss(self.view)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
