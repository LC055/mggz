//
//  CompanyAboutController.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/30.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import WebKit

class CompanyAboutController: UIViewController,WKUIDelegate,WKNavigationDelegate{
    var webview:WKWebView = {
        var view = WKWebView()
        
        return view
    }()
    
    var urlEncode:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.webview)
        self.webview.uiDelegate = self
        self.webview.navigationDelegate = self
        let urlstr = String.init(format: "%@%@", URL_Header,"upload/files/注册协议/太公民工企业端服务协议.html")
        self.urlEncode = urlstr.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        //URL.init(string: URL_Header+"/upload/files/注册协议/太公民工企业端服务协议.html")
        
        let request = URLRequest(url: URL(string: self.urlEncode!)!)
        self.webview.load(request)
        

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        self.webview.snp.updateConstraints { (make) in
            make.left.equalTo(self.view.snp.left)
            make.top.equalTo(self.view.snp.top)
            make.right.equalTo(self.view.snp.right)
            make.bottom.equalTo(self.view.snp.bottom)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
        var t = webView.url?.absoluteString
        if t == self.urlEncode{
            webView.evaluateJavaScript(String.init(format: "%@", "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '300%'"), completionHandler: nil)
        }else{
            webView.evaluateJavaScript(String.init(format: "%@", "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '100%'"), completionHandler: nil)
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
