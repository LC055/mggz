//
//  WarnController.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class WarnController: BaseLcController {
    
    
    @IBOutlet var scrollview: UIScrollView!
    @IBOutlet var projectName: UILabel!
    @IBOutlet var dwsbview: UILabel!
    @IBOutlet var qqingview: UILabel!
    
    @IBOutlet weak var slideviewConstraint: NSLayoutConstraint!

    var scene:DwycViewController?
    var lcList:QQController?
    
    var downLine = [TimeItem]()
    var absence = [TimeItem]()
    
    lazy var warnDwArray:[MWCurrentLocationModel] = [MWCurrentLocationModel]()
    lazy var warnQQArray:[MWCurrentLocationModel] = [MWCurrentLocationModel]()
    
    var selectPosition = 0
    
    /*override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.LightGreyBG
        self.navigationController?.navigationBar.clipsToBounds = true
        self.view.backgroundColor = UIColor.LightGreyBG
        self.scrollview.contentOffset = CGPoint(x: SCREEN_WIDTH * CGFloat(self.selectPosition), y: self.scrollview.contentOffset.y)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.clipsToBounds = false
        self.navigationController?.navigationBar.barTintColor = UIColor.LightGrey
    }*/
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "异常警报"
        self.projectName.text = ProjectName ?? ""
        self.dwsbview.text = "定位失败"
        self.qqingview.text = "缺勤"
        self.projectName.tag = -1
        self.dwsbview.tag = 0
        self.qqingview.tag = 1
        
        
        self.scrollview.backgroundColor = UIColor.white
        self.scrollview.showsHorizontalScrollIndicator = false
        self.scrollview.showsVerticalScrollIndicator = false
        self.scrollview.isPagingEnabled = true
        self.scrollview.scrollsToTop = false
        self.scrollview.bounces = false
        self.scrollview.isScrollEnabled = false
        self.scrollview.contentSize = CGSize(width: 2*SCREEN_WIDTH, height: 0)
        
        self.addTapClick(self.dwsbview)
        self.addTapClick(self.qqingview)
        self.addTapClick(self.projectName)
        self.setupChildController()
        self.addChildVcIntoScrollView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(getWarnData(_:)), name: NSNotification.Name(rawValue: WarnPersonNotification), object: nil)
    }
    
    @objc func getWarnData(_ notification:Notification){
        var data = notification.object as? ([MWCurrentLocationModel],[MWCurrentLocationModel])
        self.warnDwArray.removeAll()
        self.warnQQArray.removeAll()
        self.warnDwArray.append(contentsOf: data?.0 ?? [MWCurrentLocationModel]())
        self.warnQQArray.append(contentsOf: data?.1 ?? [MWCurrentLocationModel]())
        self.dwsbview.text = String(format: "定位异常(%d)", self.warnDwArray.count)//"定位失败"
        self.qqingview.text = String(format: "缺勤(%d)", self.warnQQArray.count)//"缺勤"
        self.scene?.addData(self.warnDwArray)
        self.lcList?.addData(self.warnQQArray)
    }
    
    func addTapClick(_ view:UILabel){
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewClick(_:)))
        tap.numberOfTouchesRequired = 1
        tap.numberOfTapsRequired = 1
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tap)
    }
    
    func viewClick(_ sender:UIGestureRecognizer){
        switch sender.view?.tag {
        case 0?:
            self.selectPosition = 0
            if self.slideviewConstraint.constant != 0{
                self.slideviewConstraint.constant = 0
                UIView.animate(withDuration: 0.3, animations: {
                    self.scrollview.contentOffset = CGPoint(x: 0, y: self.scrollview.contentOffset.y)
                    //self.view.layoutIfNeeded()
                })
            }
            break
        case 1?:
            self.selectPosition = 1
            if self.slideviewConstraint.constant == 0{
                self.slideviewConstraint.constant = ScreenWidth/2
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.scrollview.contentOffset = CGPoint(x: SCREEN_WIDTH, y: self.scrollview.contentOffset.y)
                    //self.view.layoutIfNeeded()
                    
                    
                })
            }
            break
        default:
            var vc = LCStatisticsController(nibName: "LCStatisticsController", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
            break
        }
    }
    
    private func setupChildController(){
        self.scene = DwycViewController(nibName: "DwycViewController", bundle: nil)
        self.addChildViewController(self.scene!)
        self.lcList = QQController(nibName: "QQController", bundle: nil)
        self.addChildViewController(self.lcList!)
    }
    private func addChildVcIntoScrollView(){
        for i in 0..<2 {
            let childVc = self.childViewControllers[i]
            if childVc.isViewLoaded {
                return
            }
            let childVcView = childVc.view
            let scrollViewW = CGFloat(i) * SCREEN_WIDTH
            if i==0{
                childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: self.scrollview.bounds.width, height: self.scrollview.frame.height)
            }else{
                childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: self.scrollview.bounds.width, height: self.scrollview.frame.height)
            }
            self.scrollview.addSubview(childVcView!)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        /*self.scrollview.subviews.forEach { (item) in
            item.removeFromSuperview()
        }
        for i in 0..<2 {
            let childVc = self.childViewControllers[i]
            if childVc.isViewLoaded {
                return
            }
            let childVcView = childVc.view
            let scrollViewW = CGFloat(i) * SCREEN_WIDTH
            if i==0{
                childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: self.scrollview.bounds.width, height: self.scrollview.frame.height)
            }else{
                childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: self.scrollview.bounds.width, height: self.scrollview.frame.height)
            }
            self.scrollview.addSubview(childVcView!)
        }*/
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
