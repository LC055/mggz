//
//  LCDingWeiController.swift
//  mggz
//
//  Created by Apple on 2018/4/23.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import SVProgressHUD
import RxSwift
import RxCocoa

class LCDingWeiController:BaseLcController{//UIViewController
    
    var month:String?
    var phonenumber:String? = ""
    var number:Int?
    var monthStr:String?
    

    @IBOutlet weak var monthview: UILabel!
    @IBOutlet weak var dwtotalview: UITableView!
    @IBOutlet weak var detailistview: UITableView!
    @IBOutlet weak var line: UIView!
    @IBOutlet weak var projectName: UILabel!
    
    @IBOutlet weak var tellayout: UIView!
    var tellabel: UILabel?
    var telImg:UIImageView?
    
    var emptyview:EmptyView?
    
    lazy var gradientLayer:CAGradientLayer = {
        var layer = CAGradientLayer()
        layer.colors = [UIColor.LightBeautifutGreen80.cgColor,UIColor.LightBeautifutGreen.cgColor]
        layer.locations = [0.4,1]
        layer.startPoint = CGPoint(x: 0, y: 0)
        layer.endPoint = CGPoint(x:0,y:1)
        return layer
    }()
    
    lazy var callTap:UITapGestureRecognizer = {
        var tap = UITapGestureRecognizer(target: self, action: #selector(call(_:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        return tap
    }()
    
    var datavariable:PublishSubject<[DWItem]> = PublishSubject()
    var datatotal:[DWItem] = [DWItem]()
    
    var workerlist:PublishSubject<[DWListResut]> = PublishSubject()
    
    var workeridvariable:PublishSubject<String> = PublishSubject()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let img = UIColor.LightGreyBG.createImageWithColor()
        self.navigationController?.navigationBar.setBackgroundImage(img, for: .default)
        self.navigationController?.navigationBar.shadowImage = img
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIColor.white.createImageWithColor(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIColor.gray.withAlphaComponent(0.5).createImageWithColor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "定位异常"
        
        self.tellabel = UILabel()
        self.tellabel?.textColor = UIColor.white
        self.tellabel?.text = ""
        self.tellabel?.font = UIFont.systemFont(ofSize: 20.0)
        
        self.telImg = UIImageView()
        self.telImg?.image = UIImage(named: "联系")
        self.telImg?.contentMode = .scaleAspectFill
        
        self.tellayout.backgroundColor = UIColor.clear
        self.tellayout.isUserInteractionEnabled = true
        self.tellayout.addGestureRecognizer(self.callTap)
        
        self.projectName.text = ProjectName ?? ""
        self.dwtotalview.backgroundColor = UIColor.LightGrey
        self.monthview.text = self.monthStr
        self.dwtotalview.register(UINib(nibName: "UserItem", bundle: nil), forCellReuseIdentifier: "cell")
        self.dwtotalview.tableFooterView = UIView(frame: CGRect.zero)
        self.dwtotalview.separatorStyle = .none
        
        self.detailistview.register(UINib(nibName: "DwListItem", bundle: nil), forCellReuseIdentifier: "DwListItem")
        self.detailistview.separatorStyle = .singleLine
        self.detailistview.tableFooterView = UIView(frame: CGRect.zero)
        
        self.detailistview.bounces = false
        self.dwtotalview.bounces = false
        
        
        
        self.datavariable.asObservable().bind(to: self.dwtotalview.rx.items){
            (_,row,element) in
            let cell = self.dwtotalview.dequeueReusableCell(withIdentifier: "cell", for: IndexPath(row : row, section : 0)) as! UserItem
            cell.dataitem = element
            return cell
        }
        
        self.workerlist.asObservable().bind(to: self.detailistview.rx.items){
            (_,row,element) in
            let cell = self.detailistview.dequeueReusableCell(withIdentifier: "DwListItem", for: IndexPath(row : row, section : 0)) as! DwListItem
            cell.dataItem = element
            cell.selectionStyle = .none
            return cell
            
        }
        
        self.dwtotalview.rx.modelSelected(DWItem.self).subscribe { (event) in
            SVProgressHUD.show(withStatus: "正在加载")
            self.getListDetail((event.element?.DemandBaseMigrantWorkerID)!)
            self.tellabel?.text = event.element?.LealPersonPhone ?? ""
            self.phonenumber = event.element?.LealPersonPhone ?? ""
        }
        
        self.workeridvariable.subscribe { (workerid) in
            print("\(workerid.element!)")
            self.getListDetail(workerid.element!)
        }
        
        if self.number == 0{
            self.addEmptyView("当月没有定位异常数据")
        }else{
            getTotalData()
        }
        
        //self.gradientLayer.frame = self.tellayout.layer.bounds
        print("\(self.tellayout.layer.bounds)")
        self.tellayout.layer.addSublayer(self.gradientLayer)
        
        self.tellayout.addSubview(self.tellabel!)
        self.tellayout.addSubview(self.telImg!)
        
        self.telImg?.snp.updateConstraints({ (make) in
            make.centerY.equalTo(self.tellayout)
            make.left.equalTo(self.tellayout.snp.left).offset(ScreenWidth/8)
            make.size.equalTo(CGSize(width: 28, height: 28))
        })
        
        self.tellabel?.snp.updateConstraints({ (make) in
            make.centerY.equalTo(self.tellayout)
            //make.centerX.equalTo(self.tellayout)
            make.left.equalTo((self.telImg?.snp.right)!).offset(5)
        })
        

        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.gradientLayer.frame = self.tellayout.layer.bounds
    }
    
    func getTotalData(){
        SVProgressHUD.show(withStatus: "正在加载")
        var params:[String:Any] = ["month":self.month!,"constructionProjectID":ProjectId as! String]
        MNetHttp.sharedInstance.postSingle(API_CALL_METHOD, method: .DWTotal, params: &params, type: DWTotal.self, success: { (bean,_) in
            guard let dataBean = bean as? DWTotal else{
                print("数据有问题")
                return
            }
            guard (dataBean.Result?.count)! > 0 else{
                print("没有数据")
                return
            }
            self.datatotal.append(contentsOf: dataBean.Result!)
            self.datavariable.onNext(self.datatotal)
            let workerid = dataBean.Result?.first?.DemandBaseMigrantWorkerID!
            self.workeridvariable.onNext(workerid!)
            self.dwtotalview.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: UITableViewScrollPosition.top)
            self.tellabel?.text = dataBean.Result?.first?.LealPersonPhone ?? ""
            self.phonenumber = dataBean.Result?.first?.LealPersonPhone ?? ""
        }) { (error) in
            switch error{
            case .DATAERROR:
                self.addEmptyView("数据异常")
                break;
            case .DATAFAIL:
                break;
            case .HTTPERROR:
                self.addEmptyView("网络异常")
                break;
            case .StateFail(let obj):
                guard let data = obj as? [String:Any] else{
                    self.addEmptyView("数据异常")
                    return
                }
                self.addEmptyView(data["Msg"] as? String ?? "")
                break
            }
            SVProgressHUD.dismiss()
            
        }
    }
    
    
    func getListDetail(_ workerId:String){
        var params:[String:Any] = ["month":self.month!,"constructionProjectID":ProjectId as! String,"demandBaseMigrantWorkerID":workerId]
        MNetHttp.sharedInstance.postSingle(API_CALL_METHOD, method: .DWDetailList, params: &params, type: DWList.self, success: { (bean,_) in
            guard let data = bean as? DWList else{
                return
            }
            
            self.workerlist.onNext(data.Result!)
            SVProgressHUD.dismiss()
        }) { (error) in
            switch error{
            case .DATAERROR:
                self.addEmptyView("数据异常")
                break;
            case .DATAFAIL:
                break;
            case .HTTPERROR:
                self.addEmptyView("网络异常")
                break;
            case .StateFail(let obj):
                guard let data = obj as? [String:Any] else{
                    self.addEmptyView("数据异常")
                    return
                }
                self.addEmptyView(data["Msg"] as? String ?? "")
                break
            }
            SVProgressHUD.dismiss()
        }
    }
    
    func addEmptyView(_ status:String = "网络异常"){
        if self.emptyview != nil{
            self.emptyview?.removeFromSuperview()
            self.emptyview = nil
        }
        self.emptyview = EmptyView(frame: CGRect.zero, _title: status, _top: Float(ScreenHeight/5))
        self.view.addSubview(self.emptyview!)
        self.emptyview?.snp.updateConstraints({ (make) in
            make.top.equalTo(self.line.snp.bottom)
            make.left.equalTo(self.view.snp.left)
            make.right.equalTo(self.view.snp.right)
            make.bottom.equalTo(self.view.snp.bottom)
        })
    }
    
    @objc func call(_ sender:UIGestureRecognizer){
        if !((self.phonenumber?.isEmpty)!){
            UIApplication.shared.openURL(URL(string: "telprompt:\(self.phonenumber ?? "")")!)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
