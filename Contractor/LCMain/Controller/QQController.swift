//
//  QQController.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/23.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

//缺勤
class QQController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    var emptyview:EmptyView?
    var datalist = [MWCurrentLocationModel]()
    var datavariable:PublishSubject<[MWCurrentLocationModel]> = PublishSubject()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableview.tableFooterView = UIView(frame: CGRect.zero)
        self.tableview.register(UINib(nibName: "QQCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 110;
        self.datavariable.asObservable().bind(to: self.tableview.rx.items){
            (_,row,element) in
            var cell = self.tableview.dequeueReusableCell(withIdentifier: "cell", for: IndexPath(row: row, section: 0)) as? QQCell
            
            if cell == nil{
                cell = QQCell(style: .default, reuseIdentifier: "cell")
            }
            cell?.selectionStyle = .none
            cell?.itemdata = element
            return cell!
        }
        
        self.tableview.rx.itemSelected.subscribe({
            index in
            //self.tableview.deselectRow(at: index.element!, animated: true)
            var mwDetail = MWDetailInfoViewController(nibName: "MWDetailInfoViewController", bundle: nil)
            
            mwDetail.DemandBaseMigrantWorkerID = self.datalist[index.element?.row ?? 0].ID
            mwDetail.currentProjectName = ProjectName ?? ""
            mwDetail.currentProjectId = ProjectId ?? ""
            mwDetail.isZG = true
            mwDetail.ProjectRange = ProjectMapPoint
            if self.datalist[index.element?.row ?? 0].Location == nil || self.datalist[index.element?.row ?? 0].Location?.object(forKey: "LocateTime") as? String == nil{
                mwDetail.LocateTime = ""
            }else{
                mwDetail.LocateTime = self.datalist[index.element?.row ?? 0].Location?.object(forKey: "LocateTime") as? String ?? ""
            }
            
            
            self.navigationController?.pushViewController(mwDetail, animated: true)
        })
    }
    
    func addData(_ data:[MWCurrentLocationModel]){
        self.datalist.removeAll()
        self.datalist.append(contentsOf: data)
        self.datavariable.onNext(self.datalist)
        
        if data == nil || data.count == 0{
            self.addEmptyView("暂无更多数据")
        }else{
            self.removeEmptyView()
        }
    }
    
    func addEmptyView(_ status:String = "网络异常"){
        if self.emptyview != nil{
            self.emptyview?.removeFromSuperview()
            self.emptyview = nil
        }
        self.emptyview = EmptyView(frame: CGRect.zero, _title: status, _top: Float(ScreenHeight/5))
        self.view.addSubview(self.emptyview!)
        self.emptyview?.snp.updateConstraints({ (make) in
            make.top.equalTo(self.tableview.snp.top)
            make.left.equalTo(self.tableview.snp.left)
            make.right.equalTo(self.tableview.snp.right)
            make.bottom.equalTo(self.tableview.snp.bottom)
        })
    }
    
    func removeEmptyView(){
        if self.emptyview != nil{
            self.emptyview?.removeFromSuperview()
            self.emptyview = nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
