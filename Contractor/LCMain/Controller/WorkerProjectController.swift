//
//  WorkerProjectController.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/6/2.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class WorkerProjectController: UIViewController {
    var demandBaseList = [NSDictionary]()
    var maxItem:Int = 5
    var itemHeight:CGFloat = 44
    var backData:TypeClick?
    
    func setData(_ data:[NSDictionary]){
        self.demandBaseList.removeAll()
        self.demandBaseList.append(contentsOf: data)
        self.datavariable.onNext(demandBaseList)
        if data.count > maxItem{
            self.tableview.snp.updateConstraints({ (make) in
                make.height.equalTo(CGFloat(maxItem)*self.itemHeight)
            })
        }else{
            self.tableview.snp.updateConstraints({ (make) in
                make.height.equalTo(CGFloat(data.count)*self.itemHeight)
            })
        }
    }
    
    var datavariable:PublishSubject<[NSDictionary]> = PublishSubject()
    
    lazy var contentview:UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.gray.withAlphaComponent(0.6)
        view.addSubview(self.tableview)
        view.addSubview(self.closeBtn)
        return view
    }()
    
    lazy var tableview:UITableView = {
        var view = UITableView(frame: CGRect.zero, style: .plain)
        view.separatorStyle = .singleLine
        view.rowHeight = UITableViewAutomaticDimension
        view.estimatedRowHeight = 50
        view.bounces = false
        view.tableFooterView = UIView(frame: CGRect.zero)
        view.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        view.angle = 5
        return view
    }()
    
    lazy var closeBtn:UIButton = {
        var view = UIButton(type: .custom)
        view.setImage(UIImage(named: "dialogClose"), for: UIControlState.normal)
        view.addTarget(self, action: #selector(closeClick(_:)), for: .touchUpInside)
        return view
    }()
    
    @objc func closeClick(_ sender:UIButton){
        self.dismiss(animated:
            false, completion: nil)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.contentview)
        self.contentview.snp.updateConstraints { (make) in
            make.top.equalTo(self.view.snp.top)
            make.left.equalTo(self.view.snp.left)
            make.right.equalTo(self.view.snp.right)
            make.bottom.equalTo(self.view.snp.bottom)
        }
        
        self.tableview.snp.updateConstraints { (make) in
            make.top.equalTo(self.contentview.snp.top).offset(100)
            make.width.equalTo(self.contentview).multipliedBy(0.6)
            make.height.equalTo(0)
            make.centerX.equalTo(self.contentview)
        }
        
        self.closeBtn.snp.updateConstraints { (make) in
            make.width.height.equalTo(40)
            make.centerX.equalTo(self.contentview)
            make.top.equalTo(self.tableview.snp.bottom).offset(20)
        }
        
        self.datavariable.asObservable().bind(to: self.tableview.rx.items){
            (_,row,element) in
            //var cell = self.tableview.dequeueReusableCell(withIdentifier: "cell", for: IndexPath(row: item, section: 0))
            var cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell.textLabel?.text = element["ProjectName"] as? String ?? ""
            cell.textLabel?.textColor = UIColor.DeepBlack
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15.0)
            cell.textLabel?.numberOfLines = 0
            //cell.textLabel?.numberOfLines = 0
            //cell.detailTextLabel?.text = element["ProjectName"] as? String ?? ""
            self.tableview.deselectRow(at: IndexPath(row: row, section: 0), animated: true)
            return cell
        }
        
        self.tableview.rx.itemSelected.subscribe({
            index in
            self.tableview.deselectRow(at: index.element!, animated: true)
            self.dismiss(animated: false, completion: {
                guard let t = self.backData else{
                    return
                }
                t((index.element?.row)!)
            })
        })

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
