//
//  BalanceController.swift
//  mggz
//
//  Created by Apple on 2018/4/25.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class BalanceController: UIViewController {

    @IBOutlet weak var zhuanhu: UILabel!
    @IBOutlet weak var qianbao: UILabel!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var projectname: UILabel!
    var img:UIImageView?
    
    @IBOutlet weak var sildeConstraint: NSLayoutConstraint!
    lazy fileprivate var tap:UITapGestureRecognizer = {
        var p = UITapGestureRecognizer(target: self, action: #selector(segmentClick))
        p.numberOfTapsRequired = 1
        p.numberOfTouchesRequired = 1
        return p
    }()
    
    lazy fileprivate var tap2:UITapGestureRecognizer = {
        var p = UITapGestureRecognizer(target: self, action: #selector(segmentClick))
        p.numberOfTapsRequired = 1
        p.numberOfTouchesRequired = 1
        return p
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.LightGreyBG
        self.navigationController?.navigationBar.clipsToBounds = true
        self.view.backgroundColor = UIColor.LightGreyBG
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.clipsToBounds = false
        self.navigationController?.navigationBar.barTintColor = UIColor.LightGrey
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "余额明细"
        self.projectname.text = ProjectName ?? ""
        self.qianbao.text = "钱包"
        self.zhuanhu.text = "专户"
        // Do any additional setup after loading the view.
        initView()
        setupChildController()
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addChildVcIntoScrollView()
    }
    
    
    func initView(){
        scrollview.backgroundColor = UIColor.white
        scrollview.showsHorizontalScrollIndicator = false
        scrollview.showsVerticalScrollIndicator = false
        scrollview.isPagingEnabled = true
        scrollview.scrollsToTop = false
        scrollview.bounces = false
        scrollview.isScrollEnabled = false
        scrollview.contentSize = CGSize(width: 2*SCREEN_WIDTH, height: 0)
        self.qianbao.isUserInteractionEnabled = true
        self.qianbao.isMultipleTouchEnabled = true
        self.zhuanhu.isUserInteractionEnabled = true
        self.zhuanhu.isMultipleTouchEnabled = true
        self.qianbao.tag = 0
        self.zhuanhu.tag = 1
        
        self.qianbao.addGestureRecognizer(tap)
        self.zhuanhu.addGestureRecognizer(tap2)
    }
    
    private func setupChildController(){
        let scene = QBController(nibName: "QBController", bundle: nil)
        self.addChildViewController(scene)
        let lcList = ZhListController(nibName: "ZhListController", bundle: nil)
        self.addChildViewController(lcList)
    }
    private func addChildVcIntoScrollView(){
        for i in 0..<2 {
            let childVc = self.childViewControllers[i]
            if childVc.isViewLoaded {
                return
            }
            let childVcView = childVc.view
            let scrollViewW = CGFloat(i) * SCREEN_WIDTH
            childVcView?.frame = CGRect(x: scrollViewW, y: 0, width: SCREEN_WIDTH, height: self.scrollview.frame.height)
            scrollview.addSubview(childVcView!)
            
        }
    }
    
    @objc fileprivate func segmentClick(_ sender:UIGestureRecognizer){
        if sender.view?.tag == 0{
            self.sildeConstraint.constant = 0
            UIView.animate(withDuration: 0.3, animations: {
                self.scrollview.contentOffset = CGPoint(x: 0, y: self.scrollview.contentOffset.y)
                self.view.layoutIfNeeded()
            })
        }else{
            self.sildeConstraint.constant = ScreenWidth/2
            UIView.animate(withDuration: 0.3, animations: {
                self.scrollview.contentOffset = CGPoint(x: SCREEN_WIDTH, y: self.scrollview.contentOffset.y)
                self.view.layoutIfNeeded()
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
