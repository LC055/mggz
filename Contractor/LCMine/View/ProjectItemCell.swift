//
//  ProjectItemCell.swift
//  mggz
//
//  Created by Apple on 2018/5/16.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class ProjectItemCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var orderId: UILabel!
    
    @IBOutlet weak var num1: UILabel!
    @IBOutlet weak var num2: UILabel!
    @IBOutlet weak var num3: UILabel!
    
    var data:ProjectItem?{
        get{
            return nil
        }
        set(newValue){
            self.name.text = newValue?.ProjectName ?? ""
            self.orderId.text = "劳务包编号:\((newValue?.OrderNo ?? ""))"
            self.num1.text = "\(Int(newValue?.OnGuardCount ?? 0))"
            self.num2.text = "\(Int(newValue?.OnLineCount ?? 0))"
            self.num3.text = "\(newValue?.DutyRate ?? "")"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
