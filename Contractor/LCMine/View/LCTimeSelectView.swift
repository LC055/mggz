//
//  LCTimeSelectView.swift
//  mggz
//
//  Created by QinWei on 2018/1/30.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCTimeSelectView: UIView {
    var myFrame: CGRect?
    
    @IBOutlet weak var amTimeButton: UIButton!
    
    
    @IBOutlet weak var pmTimeButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "LCTimeSelectView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        
        return view
    }
    

}
