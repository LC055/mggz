//
//  ClassCheckPersonCell.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/10.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Kingfisher
class ClassCheckPersonCell: BaseCell {

    @IBOutlet weak var mainMarkView: UIImageView!
    @IBOutlet weak var headerView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var sexLabel: UILabel!
    
    @IBOutlet weak var functionLabel: UILabel!
    
    @IBOutlet weak var diaPhone: UIButton!
    
    @IBOutlet weak var ageLabel: UILabel!
    
    @IBOutlet weak var checkView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func setupDataWith(model :LCClassDetailModel){
          let path : String = model.PhotoPath ?? "scene_touxiang"
        self.headerView.layer.cornerRadius = self.headerView.frame.size.width/2;
        self.headerView.layer.masksToBounds = true
        self.headerView.kf.setImage(with: URL.init(string: (ImageUrlPrefix + path)), placeholder: UIImage(named: "scene_touxiang"), options: nil, progressBlock: nil, completionHandler: nil)
        self.nameLabel.text = model.Name
        if model.IsGroupHeadman == true{
            self.mainMarkView.isHidden = false
        }
        if  model.Sex == 0 {
            self.sexLabel.text = "男"
        }else{
            self.sexLabel.text = "女"
        }
        
        if let ageStr = model.Age{
            self.ageLabel.text = String(ageStr) + "岁"
        }else{
            self.ageLabel.text = "暂无"
        }
        self.functionLabel.text = model.WorkType ?? "暂无"
    }
    public func setupMembersDataWith(model :LCClassMembersModel){
        self.diaPhone.isHidden = true
        let path:String = model.PhotoPath ?? "scene_touxiang"
        self.headerView.kf.setImage(with: URL.init(string: (ImageUrlPrefix + path)), placeholder: UIImage(named: "scene_touxiang"), options: nil, progressBlock: nil, completionHandler: nil)
        self.nameLabel.text = model.Name
        if let ageStr = model.Age{
            self.ageLabel.text = String(ageStr) + "岁"
        }else{
            self.ageLabel.text = "暂无"
        }
        if  model.Sex == 0 {
            self.sexLabel.text = "男"
        }else{
            self.sexLabel.text = "女"
        }
        self.functionLabel.text = model.WorkType ?? "暂无"
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        self.headerView.layer.cornerRadius = self.headerView.frame.width/2
        self.headerView.layer.masksToBounds = true
    }
}
