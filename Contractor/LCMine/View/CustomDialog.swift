//
//  CustomDialog.swift
//  mggz
//
//  Created by Apple on 2018/5/14.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit



class CustomDialog: UIView {
    
    var autoClose = true
    var widthmultipliedBy = 0.7
    
    lazy var titleLabel:UILabel = {
        var view = UILabel()
        view.textColor = UIColor.black
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        view.text = "dialog"
        self.dialogview.addSubview(view)
        return view
    }()
    
    lazy var topview:UIView = {
        var window = UIApplication.shared.keyWindow
        return (window?.subviews.last)!
    }()
    
    lazy var tap:UITapGestureRecognizer = {
        var t = UITapGestureRecognizer(target: self, action: #selector(dismiss))
        t.numberOfTapsRequired = 1
        t.numberOfTouchesRequired = 1
        return t
    }()
    
    
    
    lazy var dialogview:UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.white
        //view.isUserInteractionEnabled = true
        //view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dialogviewClick)))
        return view
    }()
    
    lazy var contentStatus:UILabel = {
        var view = UILabel()
        view.backgroundColor = UIColor.white
        view.textColor = UIColor.LightBlack
        view.textAlignment = .center
        view.numberOfLines = 0
        self.dialogview.addSubview(view)
        return view
    }()
    
    var customView:UIView?
    var customHeight:CGFloat?
    var statusTxt:String?
    
    
    lazy var line1:UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.LightGrey
        self.dialogview.addSubview(view)
        return view
    }()
    
    /*lazy var line2:UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.LightBlack
        self.dialogview.addSubview(view)
        return view
    }()*/
    
    lazy var canbtn:UIButton = {
        var view = UIButton()
        view.setTitle("取消", for: UIControlState.normal)
        view.titleLabel?.contentMode = .center
        view.setTitleColor(UIColor.LightBlack, for: UIControlState.normal)
        self.dialogview.addSubview(view)
        view.tag = 0
        view.addTarget(self, action: #selector(clickbtn(_:)), for: UIControlEvents.touchUpInside)
        return view
    }()
    
    lazy var submitbtn:UIButton = {
        var view = UIButton()
        view.setTitle("确定", for: UIControlState.normal)
        view.titleLabel?.contentMode = .center
        view.setTitleColor(UIColor.LightGreen, for: UIControlState.normal)
        self.dialogview.addSubview(view)
        view.tag = 1
        view.addTarget(self, action: #selector(clickbtn(_:)), for: UIControlEvents.touchUpInside)
        return view
    }()
    
    var submitclick:DialogSubmit?
    var cancelclick:DialogCancel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initView()
    }
    
    convenience init(frame: CGRect,_title title:String,_status status:String,_widthmultipliedBy widthmultipliedBy:Double = 0.7,_submitclick submitclick:@escaping DialogSubmit){
        self.init(frame: frame)
        self.widthmultipliedBy = widthmultipliedBy
        self.titleLabel.text = title
        self.contentStatus.text = status
        self.submitclick = submitclick
    }
    
    
    convenience init(frame: CGRect,_title title:String,_customview customview:UIView,_customHeight customHeight:CGFloat,_widthmultipliedBy widthmultipliedBy:Double = 0.7,_cancelclick cancelclick:@escaping DialogCancel,_submitclick submitclick:@escaping DialogSubmit){
        self.init(frame: frame)
        self.widthmultipliedBy = widthmultipliedBy
        self.titleLabel.text = title
        self.submitclick = submitclick
        self.cancelclick = cancelclick
        self.customView = customview
        self.customHeight = customHeight
        self.dialogview.addSubview(self.customView!)
    }
    //customView
    
    convenience init(frame: CGRect,_title title:String,_customview customview:UIView,_customHeight customHeight:CGFloat,_widthmultipliedBy widthmultipliedBy:Double = 0.7,_submitclick submitclick:@escaping DialogSubmit){
        self.init(frame: frame)
        self.widthmultipliedBy = widthmultipliedBy
        self.titleLabel.text = title
        self.submitclick = submitclick
        self.customHeight = customHeight
        self.customView = customview
        self.dialogview.addSubview(self.customView!)
    }
    
    func initView(){
        self.isUserInteractionEnabled = true
        self.backgroundColor = UIColor(red: 100/255, green: 100/255, blue: 100/255, alpha: 0.6)
        self.titleLabel.backgroundColor = UIColor.white
        self.addSubview(self.dialogview)
        self.isUserInteractionEnabled = true
        //self.isUserInteractionEnabled = true
        //self.addGestureRecognizer(self.tap)
        //self.titleLabel.addGestureRecognizer(self.txt)
    }
    
    func show(){
        if self.topview != nil{
            self.topview.addSubview(self)
        }
        
    }
    
    @objc func clickbtn(_ btn:UIButton){
        switch btn.tag {
        case 0:
            if self.cancelclick == nil{
                self.dismiss()
            }else{
                self.cancelclick!()
            }
            
            break
        case 1:
            guard var click = self.submitclick else{
                self.dismiss()
                return
            }
            click()
            break
        default:
            self.dismiss()
            break
        }
        
    }
    @objc func dialogviewClick(){
        print("dialogviewClick点击")
    }
    
    @objc func dismiss(){
        self.removeFromSuperview()
    }
    
    @objc func txtTap(){
        print("label点击")
    }
    
    override func layoutSubviews() {
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.titleLabel.snp.updateConstraints { (make) in
            make.left.equalTo(self.dialogview.snp.left)
            make.top.equalTo(self.dialogview.snp.top)
            make.right.equalTo(self.dialogview.snp.right)
            make.height.equalTo(40)
        }
        
        if self.customView != nil{
            self.customView?.snp.updateConstraints { (make) in
                make.top.equalTo(self.titleLabel.snp.bottom)
                make.left.equalTo(self.dialogview.snp.left)
                make.right.equalTo(self.dialogview.snp.right)
                make.height.equalTo(self.customHeight!)
            }
            
            self.line1.snp.updateConstraints { (make) in
                make.top.equalTo((self.customView?.snp.bottom)!)
                make.left.equalTo(self.dialogview.snp.left)
                make.right.equalTo(self.dialogview.snp.right)
                make.height.equalTo(1)
            }
        }else{
            self.contentStatus.snp.updateConstraints { (make) in
                make.top.equalTo(self.titleLabel.snp.bottom)
                make.left.equalTo(self.dialogview.snp.left).offset(20)
                make.right.equalTo(self.dialogview.snp.right).offset(-20)
                make.height.greaterThanOrEqualTo(50)
            }
            
            self.line1.snp.updateConstraints { (make) in
                make.top.equalTo(self.contentStatus.snp.bottom)
                make.left.equalTo(self.dialogview.snp.left)
                make.right.equalTo(self.dialogview.snp.right)
                make.height.equalTo(1)
            }
        }
        
        
        
        self.canbtn.snp.updateConstraints { (make) in
            make.left.equalTo(self.dialogview.snp.left)
            make.top.equalTo(self.line1.snp.bottom)
            make.width.equalTo(self.dialogview.snp.width).multipliedBy(0.5)
            make.bottom.equalTo(self.dialogview.snp.bottom)
            make.height.equalTo(40)
        }
        self.submitbtn.snp.updateConstraints { (make) in
            make.left.equalTo(self.canbtn.snp.right)
            make.right.equalTo(self.dialogview.snp.right)
            make.top.equalTo(self.line1.snp.bottom)
            make.height.equalTo(40)
        }
        
        //self.submitclick.snp
        
        self.dialogview.snp.updateConstraints { (make) in
            make.center.equalTo(self)
            make.width.equalTo(self).multipliedBy(self.widthmultipliedBy)
            //make.left.equalTo(self.snp.left).offset(20)
            //make.right.equalTo(self.snp.right).offset(-20)
        }
        
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
