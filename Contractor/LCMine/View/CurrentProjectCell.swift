//
//  CurrentProjectCell.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/5.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class CurrentProjectCell: BaseCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var projectName: UILabel!
    
    @IBOutlet weak var codeLabel: UILabel!
    
    @IBOutlet weak var noProject: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
