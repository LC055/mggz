//
//  SelectListDialog.swift
//  mggz
//
//  Created by Apple on 2018/5/18.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SelectListDialog: UIViewController {
    
    let rowHeight = CGFloat(40.0)
    var textArray = [String]()
    var datavariable:PublishSubject<[(String,String,String)]> = PublishSubject()
    var selectBack:TypeClick?
    var selectIndex:Int? = -1
    
    var titleStr:String{
        get{
            return ""
        }
        set(newValue){
            self.headView.text = newValue ?? ""
        }
    }
    
    var widthDelay = 0.7
    
    
    
    func setContentWidth(_ w:Double){
        self.tableview.snp.updateConstraints { (make) in
            make.width.equalTo(self.contentview).multipliedBy(w)
        }
        self.headView.snp.updateConstraints { (make) in
            make.width.equalTo(self.contentview).multipliedBy(w)
        }
        
        self.line.snp.updateConstraints { (make) in
            make.width.equalTo(self.contentview).multipliedBy(w)
        }
    }
    
    var tap:UITapGestureRecognizer = {
        var t = UITapGestureRecognizer(target: self, action: #selector(dismissDialog(_:)))
        t.numberOfTapsRequired = 1
        t.numberOfTouchesRequired = 1
        return t
    }()
    
    var cancelBtn:UIButton = {
        var view = UIButton()
        view.setTitle("取消", for: .normal)
        view.setTitleColor(UIColor.DeepBlack, for: .normal)
        view.backgroundColor = UIColor.white
        view.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        view.tag = 0
        return view
    }()
    
    var submitBtn:UIButton = {
        var view = UIButton()
        view.setTitle("确定", for: .normal)
        view.setTitleColor(UIColor.LightGreen, for: .normal)
        view.backgroundColor = UIColor.white
        view.titleLabel?.textAlignment = .center
        view.addTarget(self, action: #selector(btnClick(_:)), for: .touchUpInside)
        view.tag = 1
        return view
    }()
    
    var line:UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.LightGreyBG
        return view
    }()
    
    lazy var headView:UILabel = {
       var view = UILabel()
        view.text = "标题"
        view.textAlignment = .center
        view.backgroundColor = UIColor.white
       return view
    }()
    
    lazy var tableview:UITableView = {
        var view = UITableView()
        view.rowHeight = rowHeight
        view.bounces = false
        view.register(UINib(nibName: "SelectDialogCell", bundle: nil), forCellReuseIdentifier: "cell")
        return view
    }()
    
    
    @objc func btnClick(_ sender:UIButton){
        
        switch sender.tag {
        case 1:
            if self.selectIndex == -1{
                QWTextonlyHud("请先选择", target: self.view)
                return
            }
            guard let click = self.selectBack else{
                return
            }
            click(self.selectIndex!)
            self.dismiss(animated: false, completion: nil)
            break
            
        default:
            self.dismiss(animated: false, completion: nil)
            break
        }
    }
    
    
    lazy var contentview:UIView = {
        var view = UIView()
        return view
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red: 150/255, green: 150/255, blue: 150/255, alpha: 0.7)
        self.contentview.addSubview(self.tableview)
        self.contentview.addSubview(self.headView)
        self.contentview.addSubview(self.line)
        self.contentview.addSubview(self.cancelBtn)
        self.contentview.addSubview(self.submitBtn)
        self.view.addSubview(self.contentview)
        self.contentview.layer.cornerRadius = 10
        self.contentview.layer.masksToBounds = true
        
        
        self.datavariable.asObservable().bind(to: self.tableview.rx.items){
            (_,item,element) in
            
            var cell = self.tableview.dequeueReusableCell(withIdentifier: "cell", for: IndexPath(row: item, section: 0)) as? SelectDialogCell
            if cell == nil{
                cell = SelectDialogCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
            }
            cell?.data = element
            return cell!
        }
        
        self.tableview.rx.itemSelected.subscribe{
            (event) in
            print("\(event.element?.row)")
            self.selectIndex = event.element?.row
            
        }
        
        self.contentview.snp.updateConstraints { (make) in
            make.left.equalTo(self.view.snp.left)
            make.right.equalTo(self.view.snp.right)
            make.top.equalTo(self.view.snp.top)
            make.bottom.equalTo(self.view.snp.bottom)
        }
        
        self.tableview.snp.updateConstraints { (make) in
            make.center.equalTo(self.contentview)
            make.width.equalTo(self.contentview).multipliedBy(self.widthDelay)
            make.height.equalTo(0)
        }
        self.headView.snp.updateConstraints { (make) in
            make.width.equalTo(self.contentview).multipliedBy(self.widthDelay)
            make.height.equalTo(40)
            make.centerX.equalTo(self.contentview)
            make.bottom.equalTo(self.tableview.snp.top)
        }
        
        self.line.snp.updateConstraints { (make) in
            make.width.equalTo(self.contentview).multipliedBy(self.widthDelay)
            make.top.equalTo(self.tableview.snp.bottom)
            make.centerX.equalTo(self.contentview)
            make.height.equalTo(0.8)
        }
        
        self.cancelBtn.snp.updateConstraints { (make) in
            make.top.equalTo(self.line.snp.bottom)
            make.width.equalTo(self.tableview).multipliedBy(0.5)
            make.left.equalTo(self.tableview.snp.left)
            make.height.equalTo(40)
        }
        self.submitBtn.snp.updateConstraints { (make) in
            make.top.equalTo(self.line.snp.bottom)
            make.width.equalTo(self.tableview).multipliedBy(0.5)
            make.height.equalTo(40)
            make.right.equalTo(self.tableview.snp.right)
        }
        
    }
    
    
    
    func setData(_ data:[(String,String,String)]){
        self.datavariable.onNext(data)
        if data.count < 8 {
            self.tableview.snp.updateConstraints({ (make) in
                make.center.equalTo(self.contentview)
                make.width.equalTo(self.contentview).multipliedBy(self.widthDelay)
                make.height.equalTo(CGFloat(data.count) * self.rowHeight)
            })
        }else{
            self.tableview.snp.updateConstraints({ (make) in
                make.height.equalTo(CGFloat(8) * self.rowHeight)
                make.center.equalTo(self.contentview)
                make.width.equalTo(self.contentview).multipliedBy(self.widthDelay)
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func dismissDialog(_ sender:UIGestureRecognizer){
        self.dismiss(animated: false, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
