//
//  MsgCell.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/23.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class MsgCell: UITableViewCell {
    @IBOutlet weak var titleview: UILabel!
    
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var timeview: UILabel!
    
    var itemdata:MessageItem?{
        get{
            return nil
        }
        set(newValue){
            self.titleview.text = newValue?.Title ?? ""
            self.content.text = newValue?.Description ?? ""
            if newValue?.CreateTime?.count ?? 0 > 16 {
                self.timeview.text = (newValue?.CreateTime ?? "").replacingOccurrences(of: "T", with: " ").getSubstring(0, end: 16)
            }else{
                self.timeview.text = newValue?.CreateTime ?? ""
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
