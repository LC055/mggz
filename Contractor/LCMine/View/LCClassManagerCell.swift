//
//  LCClassManagerCell.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/10.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCClassManagerCell: UITableViewCell {

    @IBOutlet weak var describleLabel: UILabel!
    @IBOutlet weak var className: UILabel!
    @IBOutlet weak var centerView: UIView!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var classLeader: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    override func layoutSubviews() {
        self.centerView.layer.cornerRadius = 5
        self.centerView.layer.masksToBounds = true
    }
    public func setupDataWith(model :LCClassListModel){
        self.className.text = model.WorkGroupName
        if let account = model.WorkGroupNumber {
            self.countLabel.text = String(account)
        }else{
           self.countLabel.text = "0"
        }
        
        self.timeLabel.text = model.WorkTimeQuantum
        self.classLeader.text = model.GroupHeadmanName
        self.describleLabel.text = model.Notes
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}











