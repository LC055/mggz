//
//  GuiJiMapDetailView.swift
//  mggz
//
//  Created by Apple on 2018/5/11.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class GuiJiMapDetailView: UIView {
    
    lazy var titleContent:UILabel = {
       var view = UILabel()
        view.text = "停留时间"
        view.textColor = UIColor.DeepBlack
        return view
    }()
    lazy var line:UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.LightGrey
        return view
    }()
    
    lazy var timeImgView:UIImageView = {
        var view = UIImageView()
        view.image = UIImage(named: "timegrey")
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    lazy var startTime:UILabel = {
        var view = UILabel()
        view.textColor = UIColor.LightBlack
        view.textAlignment = .center
        return view
    }()
    
    lazy var fg:UILabel = {
        var view = UILabel()
        view.textColor = UIColor.LightBlack
        view.textAlignment = .center
        view.text = "--"
        return view
    }()
    
    lazy var endTime:UILabel = {
        var view = UILabel()
        view.textColor = UIColor.LightBlack
        view.textAlignment = .center
        return view
    }()
    
    lazy var timelayout:UIView = {
        var view = UIView()
        view.addSubview(self.timeImgView)
        view.addSubview(self.startTime)
        view.addSubview(self.fg)
        view.addSubview(self.endTime)
        return view
    }()
    
    
    var data:Location?{
        get{
            return nil
        }
        set(newValue){
            if newValue?.StayOutBeginTime != nil && newValue?.StayOutEndTime != nil{
                self.startTime.text = newValue!.StayOutBeginTime?.getSubstring(11, end: 16)
                self.endTime.text = newValue?.StayOutEndTime?.getSubstring(11, end: 16)
                var t = ((newValue!.StayOutEndTime?.getSubstring(11, end: 13) as! NSString).doubleValue) - ((newValue!.StayOutBeginTime?.getSubstring(11, end: 13) as! NSString).doubleValue)
                var h = ((newValue?.StayOutEndTime?.getSubstring(14, end: 16) as! NSString).doubleValue) - ((newValue?.StayOutBeginTime?.getSubstring(14, end: 16) as! NSString).doubleValue)
                if h < 0{
                    t = t - 1
                    h = h + 60
                }
                self.titleContent.text = "停留\(Int(t))小时\(Int(h))分钟"
            }
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initView()
    }
    
    func initView(){
        self.addSubview(self.titleContent)
        self.addSubview(self.line)
        self.addSubview(self.timelayout)
        self.angle = 20
    }
    
    override func layoutSubviews() {
        self.titleContent.snp.updateConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(10)
            make.left.equalTo(self.snp.left).offset(10)
            make.right.equalTo(self.snp.right).offset(10)
        }
        self.line.snp.updateConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(self.titleContent.snp.bottom).offset(10)
            make.height.equalTo(0.8)
        }
        self.timelayout.snp.updateConstraints { (make) in
            make.top.equalTo(self.line.snp.bottom)
            make.right.equalTo(self.snp.right)
            make.left.equalTo(self.snp.left)
            make.bottom.equalTo(self.snp.bottom)
        }
        self.timeImgView.snp.updateConstraints { (make) in
            make.width.height.equalTo(18)
            make.centerY.equalTo(self.timelayout)
            make.left.equalTo(self.timelayout.snp.left).offset(10)
        }
        self.startTime.snp.updateConstraints { (make) in
            make.left.equalTo(self.timeImgView.snp.right).offset(5)
            make.centerY.equalTo(self.timelayout)
        }
        self.fg.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.timelayout)
            make.left.equalTo(self.startTime.snp.right).offset(2)
        }
        self.endTime.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.timelayout)
            make.left.equalTo(self.fg.snp.right).offset(2)
        }
        
    }
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.*/
    override func draw(_ rect: CGRect) {
        print("draw")
    }
}
