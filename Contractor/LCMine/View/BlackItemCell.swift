//
//  BlackItemCell.swift
//  mggz
//
//  Created by Apple on 2018/5/15.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class BlackItemCell: UITableViewCell {

    @IBOutlet weak var headView: TextImageView!
    @IBOutlet weak var nameview: UILabel!
    @IBOutlet weak var sexview: UILabel!
    @IBOutlet weak var ageview: UILabel!
    @IBOutlet weak var worktypeview: UILabel!
    
    var item:BlackItem?{
        get{
            return nil
        }
        set(newValue){
            self.headView.kf.setImage(with: URL.init(string: (ImageUrlPrefix + (newValue?.PhotoPath ?? ""))), placeholder: UIImage(named: "scene_touxiang"), options: nil, progressBlock: nil, completionHandler: nil)
            self.nameview.text = newValue?.CompanyName ?? ""
            self.sexview.text = (newValue?.Sex ?? 0) == 0 ? "男":"女"
            self.ageview.text = "\(Int((newValue?.Age ?? 0)))岁"
            self.worktypeview.text = newValue?.WorkType ?? ""
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
