//
//  SelectCellTableViewCell.swift
//  mggz
//
//  Created by Apple on 2018/5/14.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class SelectCellTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var time: UILabel!
    
    var data:ADDBZItem?{
        get{
            return nil
        }
        set(newValue){
            self.name.text = newValue?.WorkGroupName ?? ""
            self.time.text = newValue?.WorkTimeQuantum ?? ""
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
