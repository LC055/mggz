//
//  LCBasicSetCell.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/10.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCBasicSetCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!

    @IBOutlet weak var titleLabel: UILabel!
    
    var imgname:String?{
        get{
            return nil
        }
        set(newValue){
            var img = UIImage(named: newValue!)
            self.iconView.image = img
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
