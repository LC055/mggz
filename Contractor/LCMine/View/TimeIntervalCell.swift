//
//  TimeIntervalCell.swift
//  mggz
//
//  Created by QinWei on 2018/1/29.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class TimeIntervalCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var submitLabel: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   public func setupModelData(_ model:TimeIntervalModel){
      let startStr = model.BeginTime
    
      let start = startStr?.index((startStr?.startIndex)!, offsetBy: 11)
      let end = startStr?.index((startStr?.endIndex)!, offsetBy: -3)
     let startRange = Range<String.Index>(uncheckedBounds: (lower: start!, upper: end!))
     let startTime = startStr?.substring(with: startRange)
    
    
    let endStr = model.EndTime
    let start1 = endStr?.index((endStr?.startIndex)!, offsetBy: 11)
    let end1 = endStr?.index((endStr?.endIndex)!, offsetBy: -3)
    let endRange = Range<String.Index>(uncheckedBounds: (lower: start1!, upper: end1!))
    let endTime = endStr?.substring(with: endRange)
    
    if model.IsDefault == true{
        self.titleLabel.text = "默认时段"
    }else{
        self.titleLabel.text = "时段"+String(model.Order!)
    }
    if model.IsLastDay == true{
        self.submitLabel.text = startTime! + "——" + "第二天" + endTime!
    }else{
        self.submitLabel.text = startTime!+"——"+endTime!
    }
    }
}









