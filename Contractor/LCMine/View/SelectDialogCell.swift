//
//  SelectDialogCell.swift
//  mggz
//
//  Created by Apple on 2018/5/18.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class SelectDialogCell: UITableViewCell {
    
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var subtitleView: UILabel!
    
    var data:(String,String,String)?{
        get{
            return nil
        }
        set(newValue){
            self.titleView.text = newValue?.0 ?? ""
            self.subtitleView.text = newValue?.1 ?? ""
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
