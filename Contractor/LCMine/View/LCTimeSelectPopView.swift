//
//  LCTimeSelectPopView.swift
//  mggz
//
//  Created by QinWei on 2018/1/19.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCTimeSelectPopView: UIView {
    var timeArray : [LCAtteendceTimeModel]?{
        willSet {
            self.timeArray = newValue
        }
    }
    var timeSelectSuccess:((LCAtteendceTimeModel,String) -> Void)? = nil
    @IBOutlet weak var pushNewButton: UIButton!
    
    @IBOutlet weak var sureButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var timeTableView: UITableView!
    
    @IBOutlet weak var bottomView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    func setupSubviews() {
        let contentView = loadViewFromNib()
        //self.bottomView.layer.borderWidth = 1
        //self.bottomView.layer.borderColor = UIColor.lightGray.cgColor
        self.timeTableView.delegate = self
        self.timeTableView.dataSource = self
        self.timeTableView.register(UITableViewCell.self, forCellReuseIdentifier: "time")
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "LCTimeSelectPopView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
}
extension LCTimeSelectPopView:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.timeArray!.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //var cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "time")!
            var cell = UITableViewCell(style: UITableViewCellStyle.value2, reuseIdentifier: "time")
        
        let model = self.timeArray![indexPath.row]
        cell.textLabel?.text = model.WorkTimeQuantumName ?? ""
        if model.IsDefault == true{
            let startBefore = model.BeginTime
            let defaultstr = ""
            //let startIndex = startBefore?.index((startBefore?.startIndex)!, offsetBy: 11)
            let startStr = startBefore?.getSubstring(11, end: 16)
            let endBefore = model.EndTime
            //let endIndex = endBefore?.index((endBefore?.startIndex)!, offsetBy: 11)
            let endStr = endBefore?.getSubstring(11, end: 16)
            if model.IsLastDay == true{
                let titleStr = defaultstr+startStr!+"—"+"第二天"+endStr!
                cell.detailTextLabel?.text = titleStr
            }else{
                let titleStr = defaultstr+startStr!+"—"+endStr!
                cell.detailTextLabel?.text = titleStr
            }
        }else{
            let startBefore = model.BeginTime
            //let startIndex = startBefore?.index((startBefore?.startIndex)!, offsetBy: 11)
            let startStr = startBefore?.getSubstring(11, end: 16)
            let endBefore = model.EndTime
            //let endIndex = endBefore?.index((endBefore?.startIndex)!, offsetBy: 11)
            let endStr = endBefore?.getSubstring(11, end: 16)
            if model.IsLastDay == true{
                let titleStr = startStr!+"—"+"第二天" + endStr!
                cell.detailTextLabel?.text = titleStr
            }else{
                let titleStr = startStr!+"—"+endStr!
                cell.detailTextLabel?.text = titleStr
            }
        }
        cell.detailTextLabel?.textColor = UIColor.DeepBlack
        cell.textLabel?.textColor = UIColor.DeepBlack
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 43
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let str = String.init(format: "%@ %@", cell?.textLabel?.text ?? "",cell?.detailTextLabel?.text ?? "")
        timeSelectSuccess!(self.timeArray![indexPath.row],str)
    }
    
}
