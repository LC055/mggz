//
//  LCClassMembersModel.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/20.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class LCClassMembersModel: Mappable {
    var Age : Int?
    var DemandBaseMigrantWorkerID : String?
    var Name : String?
    var Order : Int?
    var PhotoPath : String?
    var Sex : Int?
    var WorkType : String?
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        Age       <- map["Age"]
        DemandBaseMigrantWorkerID       <- map["DemandBaseMigrantWorkerID"]
        Name       <- map["Name"]
        Order       <- map["Order"]
        PhotoPath       <- map["PhotoPath"]
        Sex       <- map["Sex"]
        WorkType       <- map["WorkType"]
    }
}
