//
//  ADDBZ.swift
//  mggz
//
//  Created by Apple on 2018/5/11.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct ADDBZ:Codable {
    var State:String?
    var Result:[ADDBZItem]?
}

struct ADDBZItem:Codable{
    var ID:String?
    var WorkGroupName:String?
    var WorkGroupNumber:Double?
    var Notes:String?
    var WorkTimeQuantumID:String?
    var WorkTimeQuantum:String?
    var IsLastDay:Bool?
    var WorkHours:Double?
    var GroupHeadmanName:String?
}
