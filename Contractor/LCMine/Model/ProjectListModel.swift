//
//  ProjectModel.swift
//  mggz
//
//  Created by Apple on 2018/5/16.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct ProjectListModel:Codable{
    var Result:ProjectResult?
}

struct ProjectResult:Codable{
    var Items:[ProjectItem]?
}

struct ProjectItem:Codable{
    var MarketSupplyBaseID:String?
    var ProjectName:String?
    var OrderNo:String?//编号
    var OnGuardCount:Double?
    var OnLineCount:Double?
    var DutyRate:String?
}
