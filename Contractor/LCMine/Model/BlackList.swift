//
//  BlackList.swift
//  mggz
//
//  Created by Apple on 2018/5/15.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct BlackList:Codable{
    var State:String?
    var Result:BlackArray?
}

struct BlackArray:Codable{
    var Items:[BlackItem]?
}

struct BlackItem:Codable {
    var MigrantWorkerID:String?
    var PhotoPath:String?
    var CompanyName:String?
    var Age:Double?
    var Sex:Double?
    var WorkType:String?
    var OnWorking:Bool?
    var CreateTime:String?
}
