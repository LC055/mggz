//
//  LCAtteendceTimeModel.swift
//  mggz
//
//  Created by QinWei on 2018/1/20.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class LCAtteendceTimeModel: Mappable {
    var ID : String?
    var BeginTime : String?
    var EndTime : String?
    var IsLastDay : Bool?
    var IsDefault : Bool?
    var WorkTimeQuantumName : String?
    required init?(map: Map) {
        
        
    }
    
    func mapping(map: Map) {
        ID       <- map["ID"]
        BeginTime       <- map["BeginTime"]
        EndTime       <- map["EndTime"]
        IsLastDay       <- map["IsLastDay"]
        IsDefault       <- map["IsDefault"]
        WorkTimeQuantumName       <- map["WorkTimeQuantumName"]
    }
}
