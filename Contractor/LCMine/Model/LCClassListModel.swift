//
//  LCClassListModel.swift
//  mggz
//
//  Created by QinWei on 2018/1/18.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class LCClassListModel: Mappable {
    var ID : String?
    var WorkGroupName : String?
    var WorkGroupNumber : Int?
    var Notes : String?
    var WorkTimeQuantum : String?
    var WorkTimeQuantumID : String?
    var IsLastDay : Bool?
    var GroupHeadmanName : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        ID       <- map["ID"]
        WorkGroupName       <- map["WorkGroupName"]
        WorkGroupNumber       <- map["WorkGroupNumber"]
        Notes       <- map["Notes"]
        WorkTimeQuantum       <- map["WorkTimeQuantum"]
        IsLastDay       <- map["IsLastDay"]
        GroupHeadmanName       <- map["GroupHeadmanName"]
        WorkTimeQuantumID       <- map["WorkTimeQuantumID"]
    }
}
