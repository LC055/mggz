//
//  PayDayModel.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/5/21.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation


struct PayDayModel:Codable{
    var State:String?
    var Result:PayDayResult?
}

struct PayDayResult:Codable{
    var OrderId:String?
    var SalaryAutoPeriod:Double?
    var SalaryPayTime:String?
    var PayDay:Double?
    var PayTime:String?
    var MarketSupplyBaseID:String?
    
}
