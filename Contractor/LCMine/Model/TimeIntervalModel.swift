//
//  TimeIntervalModel.swift
//  mggz
//
//  Created by QinWei on 2018/1/29.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class TimeIntervalModel: Mappable {
    var BeginTime : String?
    var EndTime : String?
    var ID : String?
    var IsDefault : Bool?
    var IsLastDay : Bool?
    var Order : Int?
    var WorkHours : Float?
    var WorkTimeQuantumName : String?
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        BeginTime       <- map["BeginTime"]
        EndTime       <- map["EndTime"]
        ID       <- map["ID"]
        IsDefault       <- map["IsDefault"]
        IsLastDay       <- map["IsLastDay"]
        Order       <- map["Order"]
        WorkHours       <- map["WorkHours"]
        WorkTimeQuantumName       <- map["WorkTimeQuantumName"]
    }
    
    
    
}
