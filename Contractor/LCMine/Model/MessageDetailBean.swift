//
//  MessageDetailBean.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/6/4.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct MessageDetailBean:Codable {
    var State:String?
    var Result:MessageDetailResult?
}

struct MessageDetailResult:Codable {
    //var Type:String?
    var Params:MessageParams?
}

struct MessageParams:Codable{
    var SignedDate,WorkHours,AlreadyWorkDays,WorkDays,SupplementReason,ContactPhone,DemandBaseMigrantWorkerID,OrderNo,ProjectName,WorkDate,WorkBeginTime,ApplyWorkDays,Reason:String?
}
