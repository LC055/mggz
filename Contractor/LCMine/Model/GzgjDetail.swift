//
//  GzgjDetail.swift
//  mggz
//
//  Created by Apple on 2018/5/9.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct GzgjDetail:Codable {
    
    var State:String?
    var Result:GzgjDetailResult?
    
    
}

struct GzgjDetailResult:Codable{
    var MigrantWorker:GzgjDetailMigrantWorker?
    var LocationList:[Location]?
}

struct GzgjDetailMigrantWorker:Codable{
    var Path:String?
    var Name:String?
    var SignedInTime:String?
    var SignedOutTime:String?
    var IsWorkTimeLack:Bool?
    var LocateLossHours:Double?
}

struct Location:Codable{
    var Longitude:String?
    var Latitude:String?
    var Address:String?
    var IsLoss:Bool?
    var LossBeginTime:String?
    var LossEndTime:String?
    var IsStayOut:Bool?
    var StayOutBeginTime:String?
    var StayOutEndTime:String?
}
