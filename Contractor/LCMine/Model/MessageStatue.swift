//
//  MessageStatue.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/6/4.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct MessageStatue:Codable{
    var State:String?
    var Result:MessageStatusResult?
}

struct MessageStatusResult:Codable{
    var PageName:String?
    var OrderConfigId:String?
    var OrderId:String
    var JsonData:String?
    var WorkflowState:String?
}
