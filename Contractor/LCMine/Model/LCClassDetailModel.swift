//
//  LCClassDetailModel.swift
//  mggz
//
//  Created by QinWei on 2018/1/18.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class LCClassDetailModel: Mappable{
    var ID : String?
    var AggregateEntityID : String?
    var DemandBaseMigrantWorkerID : String?
    var IsGroupHeadman : Bool?
    var Sex : Int?
    var Age : Int?
    var MigrantWorkTypeID : String?
    var Name : String?
    var WorkType : String?
    var WorkTypeOrder : Int?
    var CellPhoneNumber : String?
    var PhotoPath : String?
    
    
    required init?(map: Map) {
        
        
    }
    
    func mapping(map: Map) {
        ID       <- map["ID"]
        AggregateEntityID       <- map["AggregateEntityID"]
        DemandBaseMigrantWorkerID       <- map["DemandBaseMigrantWorkerID"]
        IsGroupHeadman       <- map["IsGroupHeadman"]
        Sex       <- map["Sex"]
        Age       <- map["Age"]
        MigrantWorkTypeID       <- map["MigrantWorkTypeID"]
        WorkType       <- map["WorkType"]
        WorkTypeOrder       <- map["WorkTypeOrder"]
        Name       <- map["Name"]
        CellPhoneNumber       <- map["CellPhoneNumber"]
        PhotoPath       <- map["PhotoPath"]
        
    }
}
