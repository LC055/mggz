//
//  MWDetailinfoModel.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/2/3.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class MWDetailinfoModel: Mappable{
    var Address : String?
    var Age : Int?
    var ChangeDaySalary:Double?
    var DaySalary:Double?
    var ChangeHourSalary : Double?
    var HourSalary : String?
    var IDCard : String?
    var MarketSupplyBaseID : String?
    var MigrantWorkType : NSDictionary?
    var MigrantWorkerID : String?
    var MigrantWorkerNo : String?
    var Phone : String?
    var PhotoPath : String?
    var Sex : Int?
    var WorkGroup : NSDictionary?
    var WorkHours : String?
    var WorkTimeQuantum : NSDictionary?
    var Name : String?
    var demandBaseList : [NSDictionary]?
    var DemandBaseMigrantWorkerID:String?
    var ProjectName:String?
    
    
    required init?(map: Map) {
        
        
    }
    func mapping(map: Map) {
        Address       <- map["Address"]
        Age       <- map["Age"]
        ChangeHourSalary       <- map["ChangeHourSalary"]
        ChangeDaySalary       <- map["ChangeDaySalary"]
        HourSalary       <- map["HourSalary"]
        IDCard       <- map["IDCard"]
        MarketSupplyBaseID       <- map["MarketSupplyBaseID"]
        MigrantWorkType       <- map["MigrantWorkType"]
        MigrantWorkerID       <- map["MigrantWorkerID"]
        MigrantWorkerNo       <- map["MigrantWorkerNo"]
        Phone       <- map["Phone"]
        PhotoPath       <- map["PhotoPath"]
        Sex       <- map["Sex"]
        WorkGroup       <- map["WorkGroup"]
        WorkHours       <- map["WorkHours"]
        WorkTimeQuantum       <- map["WorkTimeQuantum"]
        Name       <- map["Name"]
        DaySalary       <- map["DaySalary"]
        demandBaseList     <- map["demandBaseList"]
        ProjectName     <- map["ProjectName"]
    }
}
