//
//  LCClassManagerViewController.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/10.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Moya
import ObjectMapper
class LCClassManagerViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    lazy var itemArray : [LCClassListModel] = [LCClassListModel]()
    var currentProject: LCProjectListModel? {
        willSet {
            self.currentProject = newValue
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupClassData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = currentProject?.ProjectName
        self.setupNavBar()
        self.setupClassData()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //self.tableView.separatorStyle = .none
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 10)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.showsHorizontalScrollIndicator = false
        self.tableView.register(UINib.init(nibName: "LCClassManagerCell", bundle: nil), forCellReuseIdentifier: "class")
    }
    
    fileprivate func setupNavBar(){
        self.navigationItem.title = "班组管理"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "新建", fontSize: 17, self, #selector(setupNewClass))
    }
    
    @objc fileprivate func setupNewClass(){
        let newClass = LCNewClassViewController(nibName: "LCNewClassViewController", bundle: nil)
        newClass.currentProject = self.currentProject
        newClass.newClassSuccess = {(value) in
            self.setupClassData()
            self.tableView.reloadData()
        }
        self.navigationController?.pushViewController(newClass, animated: true)
    }
    //开始请求数据
    fileprivate func setupClassData(){
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
        guard let MarketSupplyBaseID = currentProject?.MarketSupplyBaseID else{
            return
        }
        WWProgressHUD.showWithStatus("数据载入中...")
    LCAPiMainManager.request(.CheckClassList(MarketSupplyBaseID: MarketSupplyBaseID, AccountId: accountId)) { (result) in
        if case let .success(response) = result {
            guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                return
            }
            guard (jsonString["State"]) as! String == "OK" else{
                return
            }
            WWProgressHUD.dismiss()
            print(jsonString)
            let resultArray : NSArray = jsonString["Result"] as! NSArray
            self.itemArray = Mapper<LCClassListModel>().mapArray(JSONObject: resultArray)!
            print(self.itemArray)
            self.tableView.reloadData()
        }
        
      }
        
    }
}
extension LCClassManagerViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 5))
        return headerView
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.itemArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : LCClassManagerCell = tableView.dequeueReusableCell(withIdentifier: "class") as! LCClassManagerCell
        cell.setupDataWith(model: self.itemArray[indexPath.section])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detail = LCClassDetailViewController(nibName: "LCClassDetailViewController", bundle: nil)
        detail.currentClass = self.itemArray[indexPath.section]
        detail.currentProject = self.currentProject
        self.navigationController?.pushViewController(detail, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 132
    }
}
