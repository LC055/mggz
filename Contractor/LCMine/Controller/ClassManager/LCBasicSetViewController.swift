//
//  LCBasicSetViewController.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/10.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCBasicSetViewController: UIViewController {
    let titleArray = ["班组管理","民工工资结算设置"]
    let imglist = ["bzrw","gzt"]
    @IBOutlet weak var tableView: UITableView!
    var currentProject: LCProjectListModel? {
        willSet {
            self.currentProject = newValue
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "项目基本设置"
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.separatorStyle = .singleLine
        self.tableView.bounces = false
        view.backgroundColor = UIColor.LightGreyBG
        self.tableView.register(UINib.init(nibName: "LCBasicSetCell", bundle: nil), forCellReuseIdentifier: "basic")
        
    }

}
extension LCBasicSetViewController : UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titleArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : LCBasicSetCell = tableView.dequeueReusableCell(withIdentifier: "basic") as! LCBasicSetCell
        cell.titleLabel.text  = self.titleArray[indexPath.row]
        cell.imgname = self.imglist[indexPath.row]
        cell.selectionStyle = .gray
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0{
            let classManager = LCClassManagerViewController(nibName: "LCClassManagerViewController", bundle: nil)
            classManager.currentProject = currentProject;
        self.navigationController?.pushViewController(classManager, animated: true)
        }else{
           let salarySet = SalaryDateSetViewController(nibName: "SalaryDateSetViewController", bundle: nil)
          self.navigationController?.pushViewController(salarySet, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
}
