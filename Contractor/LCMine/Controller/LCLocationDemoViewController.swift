//
//  LCLocationDemoViewController.swift
//  mggz
//
//  Created by QinWei on 2018/1/11.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCLocationDemoViewController: UIViewController {

    fileprivate var mapView : BMKMapView = {
        let mapView = BMKMapView(frame: CGRect(x: 0, y: 64, width: SCREEN_WIDTH, height: SCREEN_HEIGHT-64))
        mapView.zoomLevel = 17
        mapView.showsUserLocation = true
        mapView.showMapScaleBar = true
        return mapView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(mapView)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mapView.viewWillAppear()
        mapView.delegate = self
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mapView.delegate = nil
        mapView.viewWillDisappear()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
extension LCLocationDemoViewController : BMKMapViewDelegate,BMKLocationServiceDelegate{
    
    func didUpdateUserHeading(_ userLocation: BMKUserLocation!) {
        
    }
    func didUpdate(_ userLocation: BMKUserLocation!) {
        
    }
    
    func mapViewDidFinishLoading(_ mapView: BMKMapView!) {
        
    }
    
}


























