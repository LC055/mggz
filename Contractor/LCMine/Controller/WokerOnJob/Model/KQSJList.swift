//
//  KQSJList.swift
//  mggz
//
//  Created by Apple on 2018/5/18.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation

struct KQSJList:Codable{
    var State:String?
    var Result:KQSJResult?
}

struct KQSJResult:Codable{
    var Items:[TimeItem]?
}


struct TimeItem:Codable{
    var ID:String?
    var WorkTimeQuantumName:String?
    var BeginTime:String?
    var EndTime:String?
    var IsLastDay:Bool?
    
    var MigrantWorker:MigrantWorker?
    var IsOnline:Bool?
    var IsAbsence:Bool?
    var ConstructionProject:ConstructionProject?
    var SignedResult:Double?
    var Location:LocationWorker?
    
}

struct MigrantWorker:Codable{
    var CompanyMG:CompanyMG?
    var MigrantWorkType:MigrantWorkType?
    var Photo:Photo?
}

struct CompanyMG:Codable{
    var CompanyName:String?
    var LealPersonPhone:String?
}
struct MigrantWorkType:Codable {
    var WorkType:String?
    var ID:String?
}
struct Photo:Codable{
    var Path:String?
}

struct ConstructionProject:Codable {
    var ProjectName:String?
}

struct LocationWorker:Codable{
    var OrderNo:String?
    var ID:String?
    var Longitude:String?
    var Latitude:String?
    var LocateTime:String?
    var Address:String?
}
