//
//  HeadViewController.swift
//  mggz
//
//  Created by Apple on 2018/6/11.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class HeadViewController: UIViewController {
    
    var headview:UIImageView = {
        var img = UIImageView()
        img.isUserInteractionEnabled = false
        img.contentMode = .scaleAspectFill
        return img
    }()
    
    var contentView:UIView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.contentView = UIView(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight))
        self.contentView?.isUserInteractionEnabled = true
        self.contentView?.backgroundColor = UIColor.black
        self.contentView?.addSubview(self.headview)
        
        self.view.addSubview(self.contentView!)
        

        // Do any additional setup after loading the view.
    }
    
    func setHeadImg(imgpath:String){
        self.headview.kf.setImage(with: URL.init(string:imgpath), placeholder: nil, options: nil, progressBlock: nil) { (img, error, nil, imgurl) in
            var scale = (img?.size.width ?? CGFloat(0)) / (img?.size.height ?? CGFloat(0))
            var imgHeight = ScreenWidth / scale
            self.headview.snp.updateConstraints({ (make) in
                make.center.equalTo(self.view)
                make.width.equalTo(ScreenWidth)
                make.height.equalTo(imgHeight)
            })
            
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
