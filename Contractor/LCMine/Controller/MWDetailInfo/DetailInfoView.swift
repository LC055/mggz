//
//  DetailInfoView.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/2/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class DetailInfoView: UIView {

    @IBOutlet weak var postHeight: NSLayoutConstraint!
    @IBOutlet weak var joinHeight: NSLayoutConstraint!
    @IBOutlet weak var joinProject: UIButton!
    
    @IBOutlet weak var postAdjust: UIButton!
    
    @IBOutlet weak var quitDelete: UIButton!
    
    @IBOutlet weak var joinBlackList: UIButton!
    
    var isBlack:Bool?{
        get{
            return false
        }
        set(newValue){
            if newValue ?? false{
                self.postHeight.constant = 0
                self.joinHeight.constant = 0
            }else{
                self.postHeight.constant = 33
                self.joinHeight.constant = 33
            }
            
        }
    }
    
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        
        let nib = UINib(nibName: "DetailInfoView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        view.backgroundColor = UIColor.purple
        view.layer.cornerRadius = 6
        view.layer.masksToBounds = true
        return view
    }
}
