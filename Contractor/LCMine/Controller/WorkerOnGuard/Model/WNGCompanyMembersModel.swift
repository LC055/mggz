//
//  WNGCompanyMembersModel.swift
//  mggz
//
//  Created by QinWei on 2018/1/29.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class WNGCompanyMembersModel: Mappable {
    var Age : Int?
    var CompanyName : String?
    var MigrantWorkerID : String?
    var OnWorking : Bool?
    var PhotoPath : String?
    var Sex : Int?
    var WorkType : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        Age       <- map["Age"]
        CompanyName       <- map["CompanyName"]
        MigrantWorkerID       <- map["MigrantWorkerID"]
        OnWorking       <- map["OnWorking"]
        PhotoPath       <- map["PhotoPath"]
        Sex       <- map["Sex"]
        WorkType       <- map["WorkType"]
    }
}
