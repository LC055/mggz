//
//  WNGMemberModel.swift
//  mggz
//
//  Created by QinWei on 2018/1/25.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class WNGMemberModel: Mappable {
    var Age : Int?
    var CellPhone : String?
    var DemandBaseMigrantWorkerID : String?
    var Name : String?
    var PhotoPath : String?
    var Sex : Int?
    var WorkGroupName : String?
    var WorkGroupOrder : Int?
    var WorkType : String?
    var WorkTypeOrder : Int?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        Age       <- map["Age"]
        CellPhone       <- map["CellPhone"]
        DemandBaseMigrantWorkerID       <- map["DemandBaseMigrantWorkerID"]
        Name       <- map["Name"]
        PhotoPath       <- map["PhotoPath"]
        Sex       <- map["Sex"]
        WorkGroupName       <- map["WorkGroupName"]
        WorkGroupOrder       <- map["WorkGroupOrder"]
        WorkType       <- map["WorkType"]
        WorkTypeOrder       <- map["WorkTypeOrder"]
    }
    
    
    
    
}
