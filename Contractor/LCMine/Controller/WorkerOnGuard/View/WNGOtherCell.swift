//
//  WNGOtherCell.swift
//  mggz
//
//  Created by QinWei on 2018/1/17.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class WNGOtherCell: UITableViewCell {

    @IBOutlet weak var personIcon: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var sexLabel: UILabel!
    
    @IBOutlet weak var ageLabel: UILabel!
    
    @IBOutlet weak var stateLabel: UILabel!
    
    @IBOutlet weak var functionLabel: UILabel!
    
    @IBOutlet weak var coverView: UIImageView!
    
    
    
    func setupCompanyOnlineWorkerDataWithModel(_ model:WNGCompanyMembersModel) {
        nameLabel.text = model.CompanyName
        if model.Sex == 0 {
            self.sexLabel.text = "男"
        }else if model.Sex==1{
            self.sexLabel.text = "女"
        }else{
            sexLabel.text = "暂无"
        }
        if (model.Age != nil) {
            ageLabel.text = String(model.Age!)+"岁"
        }else{
            ageLabel.text = "暂无"
        }
        functionLabel.text = model.WorkType ?? "暂无"
        //self.isUserInteractionEnabled = true
        if model.OnWorking == false{
            stateLabel.text = "待分配"
        }else{
            stateLabel.text = ""
        }
        let path = model.PhotoPath ?? "scene_touxiang"
        self.personIcon.kf.setImage(with: URL.init(string: (ImageUrlPrefix + path)), placeholder: UIImage(named: "scene_touxiang"), options: nil, progressBlock: nil, completionHandler: nil)
        self.personIcon.layer.cornerRadius = 20
        self.personIcon.layer.masksToBounds = true
        self.coverView.layer.cornerRadius = 20
        self.coverView.layer.masksToBounds = true
    }
    
    func setupCompanyOnJobWorkerDataWithModel(_ model:WNGCompanyMembersModel) {
        nameLabel.text = model.CompanyName
        if model.Sex == 0 {
            self.sexLabel.text = "男"
        }else if model.Sex==1{
            self.sexLabel.text = "女"
        }else{
            sexLabel.text = "暂无"
        }
        if (model.Age != nil) {
            ageLabel.text = String(model.Age!)+"岁"
        }else{
            ageLabel.text = "暂无"
        }
        functionLabel.text = model.WorkType ?? "暂无"
        if model.OnWorking == false{
            //self.isUserInteractionEnabled = true
            //stateLabel.isHidden = false
            stateLabel.text = "待分配"
        }else{
            //self.isUserInteractionEnabled = false
            //stateLabel.isHidden = true
            stateLabel.text = ""
        }
        let path = model.PhotoPath ?? "scene_touxiang"
        self.personIcon.kf.setImage(with: URL.init(string: (ImageUrlPrefix + path)), placeholder: UIImage(named: "scene_touxiang"), options: nil, progressBlock: nil, completionHandler: nil)
        self.personIcon.layer.cornerRadius = 20
        self.personIcon.layer.masksToBounds = true
        self.coverView.layer.cornerRadius = 20
        self.coverView.layer.masksToBounds = true
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.coverView.alpha = 0.7

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
