//
//  WNGAddCell.swift
//  mggz
//
//  Created by QinWei on 2018/1/17.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import Kingfisher
class WNGAddCell: UITableViewCell {

    
    @IBOutlet weak var dagouWidth: NSLayoutConstraint!
    @IBOutlet weak var dagouHeight: NSLayoutConstraint!
    @IBOutlet weak var dagouView: UIImageView!
    @IBOutlet weak var personIcon: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var sexLabel: UILabel!
    
    
    @IBOutlet weak var ageLabel: UILabel!
    
    @IBOutlet weak var classLabel: UILabel!
    
    
    @IBOutlet weak var functionLabel: UILabel!
    
    @IBOutlet weak var tellbtn: UIButton!
    
    
    
    @IBOutlet weak var personHeight: NSLayoutConstraint!
    
    @IBOutlet weak var personWidth: NSLayoutConstraint!
    
    @IBOutlet weak var nameWidth: NSLayoutConstraint!
    
    @IBOutlet weak var sexWidth: NSLayoutConstraint!
    
    
    
    @IBOutlet weak var classWidth: NSLayoutConstraint!
    
    @IBOutlet weak var diaheight: NSLayoutConstraint!
    
    @IBOutlet weak var diaWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.dagouView.image = UIImage(named: "LC_dagou")
        self.dagouView.alpha = 0.7
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    override func layoutSubviews() {
        self.contentView.bringSubview(toFront: dagouView)
        self.personWidth.constant = SCREEN_WIDTH*30/320
        self.personHeight.constant = SCREEN_WIDTH*30/320
        self.dagouWidth.constant = SCREEN_WIDTH*30/320
        self.dagouHeight.constant = SCREEN_WIDTH*30/320
    
        self.nameWidth.constant = SCREEN_WIDTH*51/320
        self.sexWidth.constant = SCREEN_WIDTH*15/320
        //self.ageWidth.constant = SCREEN_WIDTH*30/320
        self.classWidth.constant = SCREEN_WIDTH*63/320
        self.diaWidth.constant = SCREEN_WIDTH*25/320
        self.diaheight.constant = SCREEN_WIDTH*25/320
    
        /*if SCREEN_WIDTH == secondScreenW{
          nameLabel.font = UIFont.systemFont(ofSize: 15)
          sexLabel.font = UIFont.systemFont(ofSize: 15)
           ageLabel.font = UIFont.systemFont(ofSize: 15)
            classLabel.font = UIFont.systemFont(ofSize: 13)
            functionLabel.font = UIFont.systemFont(ofSize: 13)
        }
        if SCREEN_WIDTH == thirdScreenW{
            nameLabel.font = UIFont.systemFont(ofSize: 15)
            sexLabel.font = UIFont.systemFont(ofSize: 15)
            ageLabel.font = UIFont.systemFont(ofSize: 15)
            classLabel.font = UIFont.systemFont(ofSize: 15)
            functionLabel.font = UIFont.systemFont(ofSize: 15)
            
        }*/
        
    }
    public func setupWmgModelDataWith(_ model:WNGMemberModel){
       self.nameLabel.text = model.Name ?? "暂无"
        if model.Sex == 0 {
            self.sexLabel.text = "男"
        }else if model.Sex==1{
            self.sexLabel.text = "女"
        }else{
            sexLabel.text = "暂无"
        }
        if (model.Age != nil) {
            ageLabel.text = String(model.Age!)+"岁"
        }else{
            ageLabel.text = "暂无"
        }
        classLabel.text = model.WorkGroupName ?? "暂无"
        functionLabel.text = model.WorkType ?? "暂无"
        let path = model.PhotoPath ?? "scene_touxiang"
        self.personIcon.kf.setImage(with: URL.init(string: (ImageUrlPrefix + path)), placeholder: UIImage(named: "scene_touxiang"), options: nil, progressBlock: nil, completionHandler: nil)
        self.personIcon.layer.cornerRadius = SCREEN_WIDTH*15/320
        self.personIcon.layer.masksToBounds = true
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
