//
//  LCCompanyManagerViewController.swift
//  mggz
//
//  Created by QinWei on 2018/1/17.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCCompanyManagerViewController: UIViewController {
    var currentProject: LCProjectListModel? {
        willSet {
            self.currentProject = newValue
        }
    }
    @IBOutlet weak var tableView: UITableView!
    let titleArray = ["考勤时段管理","记工方式设置","黑名单"]
    let imageArray = ["sdtime","computer","blackperson"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "公司管理"
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .singleLine
        //self.tableView.backgroundColor = UIColor.LightGreyBG
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.LightGreyBG
        self.tableView.register(UINib.init(nibName: "CompanyManagerCell", bundle: nil), forCellReuseIdentifier: "basic")
        
    }
}
extension LCCompanyManagerViewController:UITableViewDelegate,UITableViewDataSource{
    
    /*func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 3))
        headerView.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.titleArray.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }*/
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CompanyManagerCell = tableView.dequeueReusableCell(withIdentifier: "basic") as! CompanyManagerCell
        cell.titleLabel.text = self.titleArray[indexPath.row]
        cell.iconView.image = UIImage.init(named: self.imageArray[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let timeInterval = TimeIntervalManagerViewController(nibName: "TimeIntervalManagerViewController", bundle: nil)
         self.navigationController?.pushViewController(timeInterval, animated: true)
            
        }
        /*if indexPath.section == 1{
        let attenceSet = AttenceStyleSetViewController(nibName: "AttenceStyleSetViewController", bundle: nil)
        self.navigationController?.pushViewController(attenceSet, animated: true)
        }*/
        if indexPath.row == 1{
           let recordWorkType = RecordWorkerStyleViewController(nibName: "RecordWorkerStyleViewController", bundle: nil)
            recordWorkType.currentProject = self.currentProject
            self.navigationController?.pushViewController(recordWorkType, animated: true)
        }
        
        if indexPath.row == 2{
            let blackVc = BlackPersonController(nibName: "BlackPersonController", bundle: nil)
            self.navigationController?.pushViewController(blackVc, animated: true)
            
        }
    }
}
