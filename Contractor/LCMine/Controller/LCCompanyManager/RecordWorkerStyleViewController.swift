//
//  RecordWorkerStyleViewController.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/3/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import YYText

class RecordWorkerStyleViewController: UIViewController {
    var currentProject: LCProjectListModel? {
        willSet {
            self.currentProject = newValue
        }
    }
    
    @IBOutlet weak var firstMarkView: UIImageView!
    
    @IBOutlet weak var secondMarkView: UIImageView!
    
    @IBOutlet weak var thirdMarkView: UIImageView!
    fileprivate var workType:Int?
    @IBOutlet weak var firstView: UIView!
    
    @IBOutlet weak var secondView: UIView!
    
    @IBOutlet weak var thirdView: UIView!
    
    lazy var tellabel:YYLabel = {
        
        var attr = NSMutableAttributedString.init(string: "如需调整记工方式，联系客服4008229555", attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14),NSAttributedStringKey.foregroundColor : UIColor.DeepBlack])
        //attr.yy_setColor(UIColor.LightBlue, range: NSRange.init(location: 13, length: 10))
        attr.yy_setTextHighlight(NSRange.init(location: 13, length: 10), color: UIColor.LightBlue, backgroundColor: UIColor.clear, tapAction: { [weak self] (view: UIView, text: NSAttributedString, range: NSRange, rect: CGRect) in
            print("print")
            UIApplication.shared.openURL(URL(string: "telprompt:4008229555")!)
            }, longPressAction: nil)
        var label = YYLabel()
        label.attributedText = attr
        label.textAlignment = .center
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "记工方式设置"
        /*let tap1 = UITapGestureRecognizer(target: self, action: #selector(firstViewClick(_:)))
        self.firstView.addGestureRecognizer(tap1)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(secondViewClick(_:)))
        self.secondView.addGestureRecognizer(tap2)
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(thirdViewClick(_:)))
        self.thirdView.addGestureRecognizer(tap3)*/
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "保存", fontSize: 15, self, #selector(saveData))
        view.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        self.getWorkTypeData()
        self.view.addSubview(self.tellabel)
        //self.tellabel.addGestureRecognizer(self.tap)
    }
    
    override func viewDidLayoutSubviews() {
        self.tellabel.snp.updateConstraints { (make) in
            make.left.equalTo(self.view.snp.left)
            make.right.equalTo(self.view.snp.right)
            make.top.equalTo(self.thirdView.snp.bottom).offset(20)
        }
    }
    
    fileprivate func getWorkTypeData(){
        
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
        guard let contructionCompanyID = self.currentProject?.CompanyID  else {
            return
        }
        MWApiMainManager.request(.GetWorkDayType(AccountId: accountId, contructionCompanyID: contructionCompanyID)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
                print(jsonString)
                guard let message : String = jsonString["Msg"] as? String else{
                    return
                }
                guard (jsonString["State"]) as! String == "OK" else{
                    QWTextonlyHud(message, target: self.view)
                    return
                }
                
                guard let resultDic = jsonString["Result"] as? Int else{
                    return
                }
                print(jsonString)
               self.workType = resultDic
                if self.workType == 0{
                    self.firstMarkView.isHidden = false
                    self.secondMarkView.isHidden = true
                    self.thirdMarkView.isHidden = true
                }else if self.workType == 1{
                    self.firstMarkView.isHidden = true
                    self.secondMarkView.isHidden = false
                    self.thirdMarkView.isHidden = true
                }else if self.workType == 2{
                    self.firstMarkView.isHidden = true
                    self.secondMarkView.isHidden = true
                    self.thirdMarkView.isHidden = false
                    
                }
                
            }
        }
    }
    
    
    /*@objc fileprivate func saveData(){
        guard let accountId = WWUser.sharedInstance.accountId else{
            return
        }
        guard (self.workType != nil)  else {
            return
        }
        MWApiMainManager.request(.SaveWorkDayType(AccountId: accountId, workDayType: self.workType!)) { (result) in
            if case let .success(response) = result {
                guard  let jsonString :NSDictionary = try?response.mapJSON() as! NSDictionary else{
                    return
                }
                guard let message : String = jsonString["Msg"] as? String else{
                    return
                }
                guard (jsonString["State"]) as! String == "OK" else{
                    
                    QWTextonlyHud(message, target: self.view)
                    
                    return
                    
                }
                self.navigationController?.popViewController(animated: true)
            }
        }
    }*/
    @objc fileprivate func firstViewClick(_ tap:UITapGestureRecognizer){
        self.workType = 0
        self.firstMarkView.isHidden = false
        self.secondMarkView.isHidden = true
        self.thirdMarkView.isHidden = true
        
    }
    @objc fileprivate func secondViewClick(_ tap:UITapGestureRecognizer){
        self.workType = 1
        self.firstMarkView.isHidden = true
        self.secondMarkView.isHidden = false
        self.thirdMarkView.isHidden = true
        
    }
    @objc fileprivate func thirdViewClick(_ tap:UITapGestureRecognizer){
        self.workType = 2
        self.firstMarkView.isHidden = true
        self.secondMarkView.isHidden = true
        self.thirdMarkView.isHidden = false
    }
    
}
