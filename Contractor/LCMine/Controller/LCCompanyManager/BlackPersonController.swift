//
//  BlackPersonController.swift
//  mggz
//
//  Created by Apple on 2018/5/15.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import MJRefresh

class BlackPersonController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    var datavariable:PublishSubject<[BlackItem]> = PublishSubject()
    var datalist:[BlackItem]=[BlackItem]()
    var page:Int = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "黑名单"
        self.tableview.tableFooterView = UIView(frame: CGRect.zero)
        self.tableview.rowHeight = 50
        self.tableview.register(UINib(nibName: "BlackItemCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        let tablehead = MJRefreshNormalHeader(refreshingTarget: self, refreshingAction: #selector(refresh))
        tablehead?.stateLabel.isHidden = true
        self.tableview.mj_header = tablehead
        
        let tablefoot = MJRefreshBackNormalFooter(refreshingTarget: self, refreshingAction: #selector(loadMore))
        tablefoot?.stateLabel.isHidden = true
        self.tableview.mj_footer = tablefoot

        self.datavariable.asObservable().bind(to: self.tableview.rx.items){
            (_,row,element) in
            var cell = self.tableview.dequeueReusableCell(withIdentifier: "cell", for: IndexPath(item: row, section: 0)) as? BlackItemCell
            if cell == nil{
                cell = BlackItemCell()
            }
            cell?.item = element
            return cell!
            
        }
        self.tableview.rx.itemSelected.subscribe({index in
            self.tableview.deselectRow(at: index.element!, animated: true)
            let model = self.datalist[index.element?.row ?? 0]
            let mwDetail = MWDetailInfoViewController(nibName: "MWDetailInfoViewController", bundle: nil)
            mwDetail.DemandBaseMigrantWorkerID = model.MigrantWorkerID ?? ""
            mwDetail.MigrantWorkerID = model.MigrantWorkerID ?? ""
            mwDetail.currentProjectName = ProjectName ?? ""
            mwDetail.currentProjectId = ProjectId ?? ""
            mwDetail.isZG = false
            mwDetail.isBlackPerson = true
            mwDetail.ProjectRange = ProjectMapPoint
            mwDetail.LocateTime = ""
            
            self.navigationController?.pushViewController(mwDetail, animated: true)
        })
        
        
        /*
         
         */
        // Do any additional setup after loading the view.
        self.getData()
    }
    
    func refresh(){
        self.page = 1
        self.getData()
    }
    
    func loadMore(){
        self.page += 1
        self.getData()
    }
    
    func getData(){
        
        var params:[String:Any] = ["pageIndex":"\(page)","pageSize":"\(PageSize)","isBlankList":"true"]
        MNetHttp.sharedInstance.postSingle(API_CALL_METHOD, method: .BLACKPERSON, params: &params, type: BlackList.self, success: { (bean,_) in
            guard let data = bean as? BlackList else{
                return
            }
            if self.page == 1{
                self.tableview.mj_header.endRefreshing()
                self.tableview.mj_footer.endRefreshing()
                self.datalist.removeAll()
            }
            if self.page == 1 && data.Result?.Items?.count == 0{//无数据
                return
            }else if (data.Result?.Items?.count)! < PageSize{ //无更多数据
                self.tableview.mj_footer.endRefreshingWithNoMoreData()
            }else{
                self.tableview.mj_footer.endRefreshing()
            }
            self.datalist.append(contentsOf: data.Result?.Items ?? [BlackItem]())
            self.datavariable.onNext(self.datalist)
            
        }) { (error) in
            switch error{
            case .HTTPERROR:
                break
            case .DATAFAIL:
                break
            case .DATAERROR:
                break
            case .StateFail(_):
                break
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
