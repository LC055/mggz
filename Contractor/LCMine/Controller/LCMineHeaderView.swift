//
//  LCMineHeaderView.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/3/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class LCMineHeaderView: UIView {

    
    @IBOutlet weak var msgview: UIButton!
    @IBOutlet weak var headerView: UIImageView!
    
    @IBOutlet weak var companyName: UILabel!
    
    @IBOutlet weak var managerName: UILabel!
    
    var clickDelegete:NormalClick?
    
    @objc func viewclick(_ sender:UIButton){
        guard var click = self.clickDelegete else {
            return
        }
        click()
    }
    
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    override func layoutSubviews() {
        self.msgview.addTarget(self, action: #selector(viewclick(_:)), for: .touchUpInside)
    }
    
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "LCMineHeaderView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        
        
        
        self.headerView.layer.cornerRadius = 35
        self.headerView.layer.masksToBounds = true
        self.backgroundColor = UIColor.init(red: 89/255.0, green: 115/255.0, blue: 144/255.0, alpha: 1)
        
        return view
    }
}
