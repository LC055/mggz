//
//  LCMineInfoModel.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/3/24.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import ObjectMapper
class LCMineInfoModel: Mappable {
    var ID : String?
    var CompanyName : String?
    var Name : String?
    var Portrait : String?
    required init?(map: Map) {
        
        
    }
    
    func mapping(map: Map) {
        ID       <- map["ID"]
        CompanyName       <- map["CompanyName"]
        Name       <- map["Name"]
        Portrait       <- map["Portrait"]
    }
}
