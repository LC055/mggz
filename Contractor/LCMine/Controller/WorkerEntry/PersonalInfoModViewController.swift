//
//  PersonalInfoModViewController.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/3/8.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class PersonalInfoModViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var idImage:UIImage?
    var informationArray:NSMutableArray?
    var address,age:String?
    fileprivate let titleArray = ["姓名","性别","年龄","身份证号","详细地址"]
    lazy var headerView : HeadImgView = {
        let view = HeadImgView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 130))
        view.setImage(self.idImage!)
        return view
    }()
    lazy var footview:EntryFooterView = {
        let view = EntryFooterView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 100))
        view.makeSureButton.addTarget(self, action: #selector(updata(_:)), for: .touchUpInside)
        return view
    }()
    
    fileprivate func switchDateToAge(_ year:String) -> String{
        let formate = DateFormatter()
        formate.dateFormat = "yyyyMMdd"
        var current = Calendar.current.dateComponents([.year,.month,.day], from: formate.date(from: year)!)
        var cur = Calendar.current.dateComponents([.year], from: Date())
        return "\((cur.year ?? 0) - (current.year ?? 0))"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        // self.tableView.separatorStyle = .none
        self.address = self.informationArray?[4] as? String
        let formate = DateFormatter()
        formate.dateFormat = "yyyyMMdd"
        
        self.age = self.switchDateToAge((self.informationArray![1] as? String)!)
        
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 50;
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 10)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.separatorStyle = .singleLine
        self.tableView.separatorColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        self.tableView.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        view.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        
        self.tableView.register(UINib.init(nibName: "EntryModfiCell", bundle: nil), forCellReuseIdentifier: "modifi")
        self.headerView.backgroundColor = UIColor.white
        self.tableView.tableHeaderView = self.headerView
        self.tableView.tableFooterView = self.footview
    }
    
    @objc func updata(_ sender:UIButton){
        print("\(self.informationArray)")
        for vc in (self.navigationController?.viewControllers.reversed())!{
            if let v = vc as? EntryFirstpageViewController{
                let t = self.informationArray ?? NSMutableArray.init()
                //v.editResult(t)
                v.editResult(self.informationArray!.copy(),_age: self.age ?? "")
                self.navigationController?.popViewController(animated: true)
                break
            }
        }
        //let vc = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)!-2] as? EntryFirstpageViewController
        
    }
    

}
extension PersonalInfoModViewController : UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titleArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : EntryModfiCell = tableView.dequeueReusableCell(withIdentifier: "modifi") as! EntryModfiCell
        cell.titleLabel.text = self.titleArray[indexPath.row]
        if indexPath.row == 0{
            cell.isAddress = false
            cell.detailField.text = self.informationArray?[0] as? String
            cell.isSex(false)
            cell.change = {
                (obj,_) in
                self.informationArray?.replaceObject(at: 0, with: (obj as? String))
            }
            
        }else if indexPath.row == 1{
            cell.isAddress = false
           // cell.detailField.text = self.informationArray?[3] as! String
            if (self.informationArray?[3] as? String) == "男"{
                cell.manButton.setImage(UIImage(named: "select"), for: .normal)
                cell.womenButton.setImage(UIImage(named: "noselect"), for: .normal)
            }else{
                cell.manButton.setImage(UIImage(named: "noselect"), for: .normal)
                cell.womenButton.setImage(UIImage(named: "select"), for: .normal)
            }
            cell.sexChange = {
                (obj,_) in
                self.informationArray?.replaceObject(at: 3, with: (obj as? String))
            }
            
            cell.isSex(true)
            
        }else if indexPath.row == 2{
            cell.isAddress = false
            cell.detailField.text = self.age
            cell.isSex(false)
            cell.change = {
                (obj,_) in
                self.age = (obj as? String)
                //self.informationArray?.replaceObject(at: 1, with: (obj as? String))
            }
        }else if indexPath.row == 3{
            cell.isAddress = false
            cell.isSex(false)
            cell.detailField.text = self.informationArray?[2] as? String
            cell.change = {
                (obj,_) in
                self.informationArray?.replaceObject(at: 2, with: (obj as? String))
            }
            
        }else if indexPath.row == 4{
            cell.reload = {
               (obj,_) in
                var str = obj as? String
                print("\(str)")
                self.address = obj as? String
                self.tableView.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .none)
            }
            cell.change = {
                (obj,_) in
                self.informationArray?.replaceObject(at: 4, with: (obj as? String))
            }
            cell.isAddress = true
            cell.isSex(false)
            cell.addresslabel.text = self.address ?? ""
            cell.detailField.text = self.address ?? ""
            
            
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
    

}
