//
//  EntryModfiCell.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/3/8.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class EntryModfiCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var detailField: UITextField!
    
    @IBOutlet weak var manButton: UIButton!
    
    @IBOutlet weak var womenButton: UIButton!
    
    @IBOutlet weak var addresslabel: UILabel!
    var isEdit:Bool?
    
    var isAddress = false
    
    var change:SuccessResponse?
    
    var reload:SuccessResponse?
    
    var sexChange:SuccessResponse?
    var addressTxt:String?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    func isSex(_ flag:Bool = true){
        isEdit = !flag
        
        self.addresslabel.isUserInteractionEnabled = false
        
        self.manButton.isEnabled = flag
        self.womenButton.isEnabled = flag
        self.manButton.isHidden = !flag
        self.womenButton.isHidden = !flag
        self.detailField.delegate = self
        
        if flag{
            self.manButton.tag = -1
            self.womenButton.tag = 1
            self.manButton.addTarget(self, action: #selector(selectSex(_:)), for: .touchUpInside)
            self.womenButton.addTarget(self, action: #selector(selectSex(_:)), for: .touchUpInside)
            self.detailField.isHidden = true
        }else{
            self.detailField.isHidden = false
        }
        
        if isAddress {
            self.addresslabel.backgroundColor = UIColor.white
        }else{
            self.addresslabel.backgroundColor = UIColor.clear
            self.addresslabel.text = ""
        }
        
    }
    
    @objc func selectSex(_ sender:UIButton){
        if sender.tag == -1 {
            self.sexChange!("男", [String:Any]())
            self.manButton.setImage(UIImage(named: "select"), for: .normal)
            self.womenButton.setImage(UIImage(named: "noselect"), for: .normal)
        }else if sender.tag == 1{
            self.sexChange!("女", [String:Any]())
            self.womenButton.setImage(UIImage(named: "select"), for: .normal)
            self.manButton.setImage(UIImage(named: "noselect"), for: .normal)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
}

extension EntryModfiCell:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return self.isEdit ?? false
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if isAddress{
            self.addresslabel.backgroundColor = UIColor.clear
            self.addresslabel.text = ""
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("\(textField.text)---\(string)")
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.reload != nil{
            self.addresslabel.backgroundColor = UIColor.white
            self.addressTxt = textField.text
            self.reload!(self.addressTxt,[String:Any]())
        }
        
        if let t = self.change {
            t(self.detailField.text!,[String:Any]())
        }
    }
    
    
    
}
