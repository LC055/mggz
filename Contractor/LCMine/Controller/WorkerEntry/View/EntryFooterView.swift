//
//  EntryFooterView.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/3/8.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class EntryFooterView: UIView {

    @IBOutlet weak var makeSureButton: UIButton!
    var myFrame: CGRect?
    override init(frame: CGRect) {
        super.init(frame: frame)
        myFrame = frame
        self.setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    func setupSubviews() {
        let contentView = loadViewFromNib()
        addSubview(contentView)
    }
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: "EntryFooterView", bundle: nil)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        view.frame = myFrame!
        view.backgroundColor = UIColor.init(red: 233/255.0, green: 233/255.0, blue: 233/255.0, alpha: 1)
        return view
    }
    
}
