//
//  BaseLcController.swift
//  mggz
//
//  Created by Apple on 2018/6/8.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit

class BaseLcController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.4, *){//
            self.edgesForExtendedLayout = .all
        }else{
            self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
            //self.navigationController?.navigationBar.backgroundColor = UIColor.white
            //UIApplication.shared.statusBarStyle = .lightContent
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
