//
//  MWApiManager.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/2/23.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation
import Moya
import SwiftyJSON

enum MWApiManager{
    //工程列表
    case GetMigrantWorkerInfo(MigrantWorkerID:String,DemandBaseMigrantWorkerID:String,AccountId:String)
    case GetLCProjectListData(pageIndex:Int,pageSize:Int,IsShowFinish:Bool,DemandBaseMigrantWorkerID:String,AccountId:String)
    case GetLCProjectNoIdListData(pageIndex:Int,pageSize:Int,IsShowFinish:Bool,AccountId:String)
    case joinProjectApplication(SupplyBaseId:String,DataItemList:NSArray,AccountId:String)
    case SetMigrantWorkInfo(MigrantWorkerID:String,DemandBaseMigrantWorkerID:String,workTypeID:String,workGroupID:String,workTimeQuantumID:String,hourSalary:Float,AccountId:String)
    case SetMigrantWorkerDelete(migrantWorkerIDList:NSArray,isSingle:Bool,AccountId:String)
    case SetMigrantWorkerToBlankList(migrantWorkerIDList:NSArray,isSingle:Bool,AccountId:String)
    case GetEmployeeInformation(AccountId:String)
    case GetWorkDayType(AccountId:String,contructionCompanyID:String)
    case SaveWorkDayType(AccountId:String,workDayType:Int)
}
private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}
let MWApiMainManager = MoyaProvider<MWApiManager>()
extension MWApiManager : TargetType{
    public var baseURL: URL {
        return URL.init(string: URL_Json_Header)!
    }
    
    public var path: String {
        switch self {
    case .GetMigrantWorkerInfo,.GetLCProjectListData,.GetLCProjectNoIdListData,.joinProjectApplication,.SetMigrantWorkInfo,.SetMigrantWorkerDelete,.SetMigrantWorkerToBlankList,.GetEmployeeInformation,.GetWorkDayType,.SaveWorkDayType:
            return API_INFIX_CALLMETHOD + "CallMethod"
        default:
            return API_INFIX_CALLMETHOD + "CallMethod"
        }
    }
    
    public var method: Moya.Method {
        return .post
    }
    
    public  var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
    
    public var task: Task {
        switch self {
        case .GetMigrantWorkerInfo(let MigrantWorkerID, let DemandBaseMigrantWorkerID, let AccountId):
            return .requestParameters(parameters: ["MigrantWorkerID":MigrantWorkerID,"DemandBaseMigrantWorkerID":DemandBaseMigrantWorkerID,"AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkInfo.GetMigrantWorkInfo"], encoding: JSONEncoding.default)
        case .GetLCProjectListData(let pageIndex, let pageSize, let IsShowFinish, var DemandBaseMigrantWorkerID, let AccountId):
            return .requestParameters(parameters: ["AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxMarketDemandBase.GetMarketDemandBase","pageIndex":pageIndex,"pageSize":pageSize,"IsShowFinish":IsShowFinish,"DemandBaseMigrantWorkerID":DemandBaseMigrantWorkerID], encoding: JSONEncoding.default)
            
        case .GetLCProjectNoIdListData(let pageIndex, let pageSize, let IsShowFinish, let AccountId):
            return .requestParameters(parameters: ["AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxMarketDemandBase.GetMarketDemandBase","pageIndex":pageIndex,"pageSize":pageSize,"IsShowFinish":IsShowFinish], encoding: JSONEncoding.default)
        case .joinProjectApplication(let SupplyBaseId, let DataItemList, let AccountId):
            return .requestParameters(parameters: ["AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorker.SetDemandBaseMigrantWorker","SupplyBaseId":SupplyBaseId,"DataItemList":DataItemList], encoding: JSONEncoding.default)
        case .SetMigrantWorkInfo(let MigrantWorkerID, let DemandBaseMigrantWorkerID, let workTypeID, let workGroupID, let workTimeQuantumID, let hourSalary, let AccountId):
            return .requestParameters(parameters: ["AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkInfo.SetMigrantWorkInfo","MigrantWorkerID":MigrantWorkerID,"DemandBaseMigrantWorkerID":DemandBaseMigrantWorkerID,"workTypeID":workTypeID,"workGroupID":workGroupID,"workTimeQuantumID":workTimeQuantumID,"daySalary":hourSalary], encoding: JSONEncoding.default)
        case .SetMigrantWorkerDelete(let migrantWorkerIDList, let isSingle, let AccountId):
            return .requestParameters(parameters: ["AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxConstructionTeamDetail.SetMigrantWorkerStop","migrantWorkerIDList":migrantWorkerIDList,"isSingle":isSingle], encoding: JSONEncoding.default)
        case .SetMigrantWorkerToBlankList(let migrantWorkerIDList, let isSingle, let AccountId):
            return .requestParameters(parameters: ["AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxConstructionTeamDetail.SetMigrantWorkerToBlankList","migrantWorkerIDList":migrantWorkerIDList,"isSingle":isSingle], encoding: JSONEncoding.default)
        case .GetEmployeeInformation(let AccountId):
            return .requestParameters(parameters: ["AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxConstructionBasicSet.GetEmployeeInfo"], encoding: JSONEncoding.default)
        case .GetWorkDayType(let AccountId, let contructionCompanyID):
            return .requestParameters(parameters: ["AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxConstructionBasicSet.GetWorkDayType","contructionCompanyID":contructionCompanyID], encoding: JSONEncoding.default)
        case .SaveWorkDayType(let AccountId, let workDayType):
            return .requestParameters(parameters: ["AccountId":AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxConstructionBasicSet.SetWorkDayType","workDayType":workDayType], encoding: JSONEncoding.default)
        default:
            return .requestPlain
        }
    }
    public  var headers: [String : String]? {
        let httpHeader = mobile_cilent_headers()
        return httpHeader
    }
    
}
