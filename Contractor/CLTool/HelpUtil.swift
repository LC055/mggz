//
//  HelpUtil.swift
//  mggz
//
//  Created by Apple on 2018/4/19.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import UIKit
import YYText
import MBProgressHUD

enum ErrorType{
    case HTTPERROR,DATAERROR,DATAFAIL,StateFail(Any)
}



let ScreenWidth = UIScreen.main.bounds.width
let ScreenHeight = UIScreen.main.bounds.height

let GuiJiPointList = "GuiJiPointList" //轨迹点的list数据 (通知)
let DrawLayoutNotification = "DrawLayoutNotification" //侧滑菜单关闭
let XCRefreshNotification = "XCRefreshNotification"//现场页面数据刷新
let MDCQListNotification = "MDCQListNotification" //民工出清列表刷新
let MGQDListNotification = "MGQDListNotification" //民工未签到刷新
let WarnPersonNotification = "WarnPersonNotification"//报警信息

//var Locatetime:String?//包公头 工作轨迹

let PageSize:Int = 10

var ProjectId:String?{
    get{
        return UserDefaults.standard.object(forKey: "projectId") as? String
    }
    set(newValue){
        UserDefaults.standard.setValue(newValue, forKey: "projectId")
        UserDefaults.standard.synchronize()
    }
    
}

var WorkType:Double?{
    get{
        return UserDefaults.standard.object(forKey: "WorkType") as? Double ?? 0
    }
    set(newValue){
        UserDefaults.standard.setValue(newValue, forKey: "WorkType")
        UserDefaults.standard.synchronize()
    }
    
}

var UUID:String?{
    get{
        var uuidobj = CFUUIDCreate(nil)
        var uuid = CFUUIDCreateString(nil, uuidobj) as String
        return uuid
    }
    set(newValue){
        //self = newValue
    }
}

var ProjectName:String?{//项目ID
    get{
        return UserDefaults.standard.object(forKey: "projectName") as! String
    }
    set(newValue){
        UserDefaults.standard.setValue(newValue, forKey: "projectName")
        UserDefaults.standard.synchronize()
    }
}

var ProjectMapPoint:[CLLocationCoordinate2D]?//归档不支持struct

var LoginType:Int?{//-1  没有登录记录  //1 民工  //2包工头  //3监管端
    get{
        return UserDefaults.standard.object(forKey: "LoginType") as? Int ?? -1
    }
    set(newValue){
        UserDefaults.standard.setValue(newValue, forKey: "LoginType")
        UserDefaults.standard.synchronize()
    }
}

var ContractorName:String?{//包工头登录名
    get{
        return UserDefaults.standard.object(forKey: "ContractorName") as? String
    }
    set(newValue){
        UserDefaults.standard.setValue(newValue, forKey: "ContractorName")
        UserDefaults.standard.synchronize()
    }
}
var ContractorPass:String?{//包工头登录密码
    get{
        return UserDefaults.standard.object(forKey: "ContractorPass") as? String
    }
    set(newValue){
        UserDefaults.standard.setValue(newValue, forKey: "ContractorPass")
        UserDefaults.standard.synchronize()
    }
}

func saveProjectId(_ project:String){
    
    //let t = BMKPolygonContainsCoordinate(<#T##point: CLLocationCoordinate2D##CLLocationCoordinate2D#>, <#T##polygon: UnsafeMutablePointer<CLLocationCoordinate2D>!##UnsafeMutablePointer<CLLocationCoordinate2D>!#>, <#T##count: UInt##UInt#>)
    //UserDefaults.standard.set(project, forKey: "projectId")
    UserDefaults.standard.setValue(project, forKey: "projectId")
    UserDefaults.standard.synchronize()
}




typealias SuccessResponse = (_ obj:Any,_ dic:[String:Any])->Void
typealias ErrorResponse = (ErrorType)->Void
typealias NormalClick = ()->Void
typealias BooleanClick = (Bool)->Void
typealias TypeClick = (Int)->Void
typealias DialogSubmit = ()->Void
typealias DialogCancel = ()->Void
typealias DialogDataSubmit = ([String:String])->Void


extension UIView{
    
    public var angle:CGFloat{//圆角
        get{
            return self.layer.cornerRadius
        }
        set(newValue){
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = true;
        }
    }
    
    public var shadowEnable:Bool{//阴影
        get{
            return false
        }
        set(newValue){
            //self.shadowEnable = newValue
            if newValue{
                self.layer.cornerRadius = 3;
                self.layer.masksToBounds = false
                self.layer.shadowColor = UIColor.LightBlack.cgColor
                self.layer.shadowOffset = CGSize(width: 0, height: 3)
                self.layer.shadowOpacity = 0.5
                self.layer.shadowRadius = 4
            }
        }
    }
    
    func drawline(_ lineLength:Int,_lineSpacing lineSpacing:Int,_lineColor lineColor:UIColor){
        
        let shapelayer = CAShapeLayer()
        shapelayer.bounds = self.bounds
        shapelayer.position = CGPoint(x: self.frame.width/2, y: self.frame.height)
        shapelayer.fillColor = UIColor.clear.cgColor
        shapelayer.strokeColor = lineColor.cgColor
        shapelayer.lineWidth = self.frame.height
        shapelayer.lineJoin = kCALineJoinRound
        shapelayer.lineDashPattern = [NSNumber(value: lineLength),NSNumber(value: lineSpacing)]
        var path = CGMutablePath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        shapelayer.path = path
        self.layer.addSublayer(shapelayer)
        
    }
    
}

extension UIColor{
    class var LightGreyBG:UIColor{
        get{
            return UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        }
    }
    
    class var LightGrey:UIColor{
        get{
            return UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 0.5)
        }
    }
    
    class var LightGreen:UIColor{
        get{
            return UIColor(red: 144/255, green: 238/255, blue: 144/255, alpha: 1)
        }
    }
    class var LightRed:UIColor{
        get{
            return UIColor(red: 242/255, green: 111/255, blue: 104/255, alpha: 1)
        }
    }
    
    class var LightBlack:UIColor{
        get{
            return UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
        }
    }
    
    class var DeepBlack:UIColor{
        get{
            return UIColor(red: 90/255, green: 90/255, blue: 90/255, alpha: 1)
        }
    }
    
    class var DeepBlackLight:UIColor{
        get{
            return UIColor(red: 150/255, green: 150/255, blue: 150/255, alpha: 1)
        }
    }
    
    class var LightBeautifutGreen:UIColor{
        get{
            return UIColor(red: 82/255, green: 213/255, blue: 161/255, alpha: 1)
        }
    }
    
    class var LightBeautifutGreen80:UIColor{
        get{
            return UIColor(red: 82/255, green: 213/255, blue: 161/255, alpha: 0.6)
        }
    }
    
    class var LightBlue:UIColor{
        get{
            return UIColor(red: 35/255, green: 162/255, blue: 242/255, alpha: 1)
        }
    }
    
    func createImageWithColor() -> UIImage{
        var rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(self.cgColor)
        context?.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
}

extension Double{
    func roundTo(_ places:Int) -> String {
        
        let result = String(format: "%.\(places)f", self)
        return result
        
    }
}

extension String{
    
    //计算高度
    func getMessageHeight() -> CGFloat{
        var introText = NSMutableAttributedString.init(string: self)
        introText.yy_font = UIFont.systemFont(ofSize: 17)
        introText.yy_lineSpacing = 0
        let layout = YYTextLayout.init(containerSize: CGSize(width: ScreenWidth, height: CGFloat.greatestFiniteMagnitude), text: introText)
        return (layout?.textBoundingSize.height)!
        
    }
    
    func timeSimple() -> String{//YYYY-MM-DD HH:MM
        var p = self as String
        if p.count > 16{
            p = p.replacingOccurrences(of: "T", with: " ", options: String.CompareOptions.literal, range: nil)
            let r = NSMakeRange(16, p.count-16)
            let rag:Range<String.Index> = Range<String.Index>(r, in: p)!
            p.removeSubrange(rag)
        }
        
        return p
    }
    
    func timeYearAndMonth()->String{
        var p = self as String
        p = p.replacingOccurrences(of: "T", with: " ", options: String.CompareOptions.literal, range: nil)
        let r = NSMakeRange(10, p.count-10)
        let rag:Range<String.Index> = Range<String.Index>(r, in: p)!
        p.removeSubrange(rag)
        return p
    }
    //2018-04-23T14:03:43.937
    func timeYearDate()->String{
        var p = self as String
        guard p != nil && p != "" && p.count >= 16 else {
            return ""
        }
        p = p.replacingOccurrences(of: "T", with: " ", options: String.CompareOptions.literal, range: nil)
        let r = NSMakeRange(16, p.count-16)
        let rag:Range<String.Index> = Range<String.Index>(r, in: p)!
        p.removeSubrange(rag)
        return p
    }
    
    //2018-04-24T00:00:00+08:00    to    2018年4月24日
    func timeMonthAndDate() -> String{
        var p = self
        p.removeSubrange(Range<String.Index>(NSMakeRange(10, p.count-10), in: p)!)
        
        let f1 = DateFormatter()
        f1.dateFormat = "yyyy-MM-dd"
        let com = Calendar.current.dateComponents([.year,.month,.day], from: (f1.date(from: p))!)
        return "\((com.month)!)月\((com.day)!)日"
        
        return p
    }
    
    func timeYearAndMonthStyle2() -> String{
        var p = self
        p.removeSubrange(Range<String.Index>(NSMakeRange(10, p.count-10), in: p)!)
        
        let f1 = DateFormatter()
        f1.dateFormat = "yyyy-MM-dd"
        let com = Calendar.current.dateComponents([.year,.month,.day], from: (f1.date(from: p))!)
        return "\((com.year)!)年\((com.month)!)日"
        
        return p
    }
    
    func getMoneyAttr(_ fontsize:Float = 20) -> NSMutableAttributedString{ //金钱的字体格式
        var moneyTxt = self
        var attr = NSMutableAttributedString(string: moneyTxt)
        attr.addAttribute(.font, value: UIFont(name: "Arial-BoldMT", size: CGFloat(fontsize)), range: NSMakeRange(0, moneyTxt.count))
        return attr
    }
    
    func getSubstring(_ start:Int,end end:Int) -> String{
        var indexStr = self
        if indexStr == nil{
            return ""
        }else if end > indexStr.count{
            return self
        }else{
            let startIndex = indexStr.index(indexStr.startIndex, offsetBy: start)
            let endIndex = indexStr.index(indexStr.startIndex, offsetBy: end)
            let rr = indexStr[startIndex..<endIndex]
            return String(rr)
        }
        return ""
    }
    
}
