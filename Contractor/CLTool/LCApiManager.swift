//
//  LCApiManager.swift
//  mggz
//
//  Created by 匀视网Mac on 2018/1/6.
//  Copyright © 2018年 爱丽丝的梦境. All rights reserved.
//

import Foundation
import Moya
import SwiftyJSON
enum LCAPiManager{
    //工程列表
    case GetProjectList(pageIndex:String,pageSize:String,IsShowFinish:String,AccountId:String)
    //选定项目
    case ChooseProject
    //获取工人位置
    case GetMWLotion(start:String,limit:String,sql:String,sqlParam:String,supplybaseid:String,AccountId:String)
    case GetMWTrack(DemandBaseMigrantWorkerID:String,MarketSupplyBaseID:String,locateDate:String,IsShowDetail:Bool,AccountId:String)
    //班组业务
    case CheckClassList(MarketSupplyBaseID:String,AccountId:String)
    case GETClassDetail(WorkGroupID:String,AccountId:String)
    case NewClassBody(orderId:String,jsonData:String,AccountId:String,operate : String)
    case GETLCNewGuid(number:Int,AccountId:String)
    case GETAttendanceTime(start:Int,limit:Int,AccountId:String)
    //成员查询
    case GETClassMembers(MarketSupplyBaseID:String,pageIndex:Int,pageSize:Int,AccountId:String)
    //设置班长
    case SETClassMonitor(orderId:String,workGroupDetailID:String,AccountId:String)
    case DismissClass(ids:String,AccountId:String)
    //移除班组成员
    case RemoveClassMember(orderId:String,workGroupDetailIDList:NSArray,AccountId:String)
    //班组添加成员
    case AddClassMembers(orderId:String,demandBaseWorkIDList:NSArray,AccountId:String)
    //在岗民工查询
    case GetOnlineWorker(MarketSupplyBaseID:String,pageIndex:Int,pageSize:Int,AccountId:String)
    //在岗民工移除
    case RemoveOnlineWorker(DemandBaseMigrantWorkerIDList:[String],AccountId:String)
    //班组所以成员设置
    case SETClassAllMembers(DemandBaseMigrantWorkerIDList:[String],workGroupId:String,AccountId:String)
    
    //调整岗位
    case AdjustThePost(MigrantWorkerID:String,DemandBaseMigrantWorkerID:String,workTypeID:String,workGroupID:String,workTimeQuantumID:String,hourSalary:String,AccountId:String)
    //成员加入项目
    case mwJoinTheProject(SupplyBaseId:String,DataItemList:NSArray,AccountId:String)
    //公司在职民工
    case GetOnlineWorkerOfCompany(pageIndex:Int,pageSize:Int,AccountId:String,marketSupplyBaseID:String)
    //考勤时段查询
    case CheckTimeInterVal(start:Int,limit:Int,AccountId:String)
    //考勤时段添加、修改、删除
    case AddTimeInterval(orderId:String,jsonData:String,operate:String,AccountId:String)
    
}
private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}

//let LCAPiMainManager = MoyaProvider<LCAPiManager>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])
let LCAPiMainManager = MoyaProvider<LCAPiManager>()
extension LCAPiManager : TargetType{
   public var baseURL: URL {
        return URL.init(string: URL_Json_Header)!
    }
    
   public var path: String {
        switch self {
        case .GetMWLotion,.GetMWTrack,.CheckClassList,.GETClassDetail,.NewClassBody,.GETLCNewGuid,.GETAttendanceTime,.GETClassMembers,.SETClassMonitor,.DismissClass,.RemoveClassMember,.AddClassMembers,.GetOnlineWorker,.RemoveOnlineWorker,.SETClassAllMembers,.AdjustThePost,.mwJoinTheProject,.GetOnlineWorkerOfCompany,.CheckTimeInterVal,.AddTimeInterval:
            return API_INFIX_CALLMETHOD + "CallMethod"
        default:
            return API_INFIX_CALLMETHOD + "CallMethod"
        }
    }
    
   public var method: Moya.Method {
        return .post
    }
    
  public  var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
    
   public var task: Task {
    switch self {
    case .GetMWLotion(let start, let limit, let sql,let sqlParam,let supplybaseid,let AccountId):
        return .requestParameters(parameters: ["start":start,"limit":limit,"sql":sql,"sqlParam":sqlParam,"supplybaseid":supplybaseid,"AccountId": AccountId,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxDBHandler.FindPagingOrderData","pageName":"MarTian.List.MinGongGuiJi","orderManagerConfigId":"5e797081-b073-6495-65ae-c2f524dc3345"], encoding: JSONEncoding.default)
    case .GetMWTrack(let DemandBaseMigrantWorkerID,let MarketSupplyBaseID,let locateDate, let IsShowDetail, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkerLocation.GetMigrantWorkerLocation","DemandBaseMigrantWorkerID":DemandBaseMigrantWorkerID,"MarketSupplyBaseID": MarketSupplyBaseID,"locateDate": locateDate,"IsShowDetail":IsShowDetail,"AccountId":AccountId], encoding: JSONEncoding.default)
        
    case .CheckClassList(let MarketSupplyBaseID, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkGroup.GetWorkGroup","MarketSupplyBaseID":MarketSupplyBaseID,"AccountId":AccountId], encoding: JSONEncoding.default)
    case .GETClassDetail(let WorkGroupID, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkGroup.GetWorkGroupInfo","WorkGroupID":WorkGroupID,"AccountId":AccountId], encoding: JSONEncoding.default)
        
    case .NewClassBody(let orderId, let jsonData, let AccountId,let operate):
    return .requestParameters(parameters: ["pageName":"MarTian.Edit.MinGongBanZuWeiHu","orderConfigId":"ecf7a5e5-d63d-0ee0-c09e-4d7b1d9968c5","operate":operate,"Method":"MarTian.MigrantWorkerManage.Handler.AjaxDBHandler.SaveData","AccountId":AccountId,"orderId":orderId,"jsonData":jsonData], encoding: JSONEncoding.default)
    case .GETLCNewGuid(let number, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.BasicHandler.AjaxCommonApp.GetNewGuid","number":number,"AccountId":AccountId], encoding: JSONEncoding.default)
    case .GETAttendanceTime(let start, let limit, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDBHandler.FindPagingOrderData","orderManagerConfigId":"862BCA72-AB8A-B2ED-520B-A14C2BE74850","AccountId":AccountId,"start":start,"limit":limit], encoding: JSONEncoding.default)
    case .GETClassMembers(let MarketSupplyBaseID, let pageIndex, let pageSize, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorker.GetGroupMigrantWorkerSelect","pageIndex":pageIndex,"pageSize":pageSize,"AccountId":AccountId,"MarketSupplyBaseID":MarketSupplyBaseID], encoding: JSONEncoding.default)
    case .SETClassMonitor(let orderId, let workGroupDetailID, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkGroup.SetWorkGroupHeadman","orderId":orderId,"workGroupDetailID":workGroupDetailID,"AccountId":AccountId], encoding: JSONEncoding.default)
    case .DismissClass(let ids, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDBHandler.DeleteOrderData","orderConfigId":"ecf7a5e5-d63d-0ee0-c09e-4d7b1d9968c5","ids":ids,"AccountId":AccountId], encoding: JSONEncoding.default)
    case .RemoveClassMember(let orderId, let workGroupDetailIDList, let AccountId):
       return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkGroup.RemoveWorkGroupMember","orderId":orderId,"workGroupDetailIDList":workGroupDetailIDList,"AccountId":AccountId], encoding: JSONEncoding.default)
    case .AddClassMembers(let orderId, let demandBaseWorkIDList, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkGroup.AddWorkGroupMember","orderId":orderId,"demandBaseWorkIDList":demandBaseWorkIDList,"AccountId":AccountId], encoding: JSONEncoding.default)
    case .GetOnlineWorker(let MarketSupplyBaseID, let pageIndex, let pageSize, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorker.GetDemandBaseMigrantWorkerView","MarketSupplyBaseID":MarketSupplyBaseID,"pageIndex":pageIndex,"pageSize":pageSize,"AccountId":AccountId], encoding: JSONEncoding.default)
    case .RemoveOnlineWorker(let DemandBaseMigrantWorkerIDList, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorker.SetDemandBaseMigrantWorkerStop","DemandBaseMigrantWorkerIDList":DemandBaseMigrantWorkerIDList,"AccountId":AccountId], encoding: JSONEncoding.default)
    case .SETClassAllMembers(let DemandBaseMigrantWorkerIDList, let workGroupId, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorker.SetDemandBaseMigrantWorkerGroup","DemandBaseMigrantWorkerIDList":DemandBaseMigrantWorkerIDList,"workGroupId":workGroupId,"AccountId":AccountId], encoding: JSONEncoding.default)
    case .AdjustThePost(let MigrantWorkerID, let DemandBaseMigrantWorkerID, let workTypeID, let workGroupID, let workTimeQuantumID, let hourSalary, let AccountId):
     return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxMigrantWorkInfo.SetMigrantWorkInfo","MigrantWorkerID":MigrantWorkerID,"DemandBaseMigrantWorkerID":DemandBaseMigrantWorkerID,"workGroupID":workGroupID,"workTypeID":workTypeID,"workTimeQuantumID":workTimeQuantumID,"hourSalary":hourSalary,"AccountId":AccountId], encoding: JSONEncoding.default)
    case .mwJoinTheProject(let SupplyBaseId, let DataItemList, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDemandBaseMigrantWorker.SetDemandBaseMigrantWorker","AccountId":AccountId,"SupplyBaseId":SupplyBaseId,"DataItemList":DataItemList], encoding: JSONEncoding.default)
    case .GetOnlineWorkerOfCompany(let pageIndex, let pageSize, let AccountId, let marketSupplyBaseID):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxConstructionTeamDetail.GetMigrantWorker","AccountId":AccountId,"pageIndex":pageIndex,"pageSize":pageSize,"marketSupplyBaseID":marketSupplyBaseID,"isBlankList":false], encoding: JSONEncoding.default)
    case .CheckTimeInterVal(let start, let limit, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDBHandler.FindPagingOrderData","AccountId":AccountId,"orderManagerConfigId":"862BCA72-AB8A-B2ED-520B-A14C2BE74850","start":start,"limit":limit], encoding: JSONEncoding.default)
    case .AddTimeInterval(let orderId, let jsonData, let operate, let AccountId):
        return .requestParameters(parameters: ["Method":"MarTian.MigrantWorkerManage.Handler.AjaxDBHandler.SaveData","AccountId":AccountId,"pageName":"KouFine.Container.Form","orderConfigId":"a840da05-a150-bada-700e-9de7fe386801","orderId":orderId,"jsonData":jsonData,"operate":operate], encoding: JSONEncoding.default)
    default:
        return .requestPlain
    }
    }
  public  var headers: [String : String]? {
         let httpHeader = mobile_cilent_headers()
         return httpHeader
  }
    
}






















